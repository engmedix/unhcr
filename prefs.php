<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Preferences</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
		
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<?php if(isset($_GET['delegate'])){ include("include/do_insert.php"); } ?>
									
									<div class="col-sm-7">
										<!-- #section:elements.tab.position -->
										<div class="tabbable">
											<ul class="nav nav-tabs" id="myTab">
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-envelope bigger-120"></i>
														Email Alerts
														
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#messages">
														Notifications
														
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#logins">
														Password
														
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#docs">
														Documents
														
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#staffId">
														Staff ID
														
													</a>
												</li>

												
											</ul>

											<div class="tab-content">
												<div id="home" class="tab-pane fade in active">
													<p>Enable email alerts to receive an email whenever a new document is assigned to you.</p>
													
													<label>
														<input name="email_alerts" id="email_alerts" class="ace ace-switch ace-switch-1" type="checkbox" <?php if($pref_email>0){ echo "checked"; } ?> />
														<span class="lbl"> &nbsp;&nbsp;&nbsp;Email Alerts</span>
													</label>
													
													<br />
													<div id="email_prefs">
													
													<label style="margin-left:15px">
														<input name="email_pref" id="email_pref1" value="1" type="radio" class="ace" <?php if($pref_email==1){ echo "checked"; } ?> />
														<span class="lbl"> Email me instantly</span>
													</label>
													<br />
													<label style="margin-left:15px">
														<input name="email_pref" id="email_pref2" value="2" type="radio" class="ace" <?php if($pref_email==2){ echo "checked"; } ?> />
														<span class="lbl"> Send me a daily compilation</span>
													</label>
													
													</div>
												</div>

												<div id="messages" class="tab-pane fade">
													<p></p>
													
													<label>
														<input name="switch-field-1" class="ace ace-switch ace-switch-1" checked type="checkbox" />
														<span class="lbl"> &nbsp;&nbsp;&nbsp;Notifications</span>
													</label>
													<br />
													
												</div>

												<div id="logins" class="tab-pane fade">
													<p>Reset your password</p>
													<div id="pwd_reset"></div>
													<div class="form-group">
														<label class="" for="doc_ref_number">New Password:</label>

														<div class="">
															<div class="clearfix">
																<input name="password1" id="pwd1" class="col-xs-12 col-sm-4" type="password">
															</div>
														</div>
													</div>
															
													<div class="form-group">
														<label class="" for="undp_ref_number">Confirm:</label>

														<div class="">
															<div class="clearfix">
																<input name="password2" id="pwd2" class="col-xs-12 col-sm-4" type="password">
															</div>
														</div>
													</div>
													
													<input type="hidden" value="<?php echo $myid;?>" id="uid" />
													
													<div class="input-group">
														<button class="btn btn-primary" id="pwd_button" >
															<i class="ace-icon fa fa-floppy-o bigger-120"></i>
																Reset
														</button>
													</div>
													
													
												</div>

												<div id="docs" class="tab-pane fade">
													
													<div class="form-group">
														<label class="" for="doc_ref_number">Default View:</label>

														<div class="">
															<div class="clearfix">
																<select>
																	<option value="0">Tabular</option>
																	<option value="1">List</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="" for="doc_ref_number">Archives: Show for</label>

														<div class="">
															<div class="clearfix">
																<select>
																	<option value="0">All</option>
																	<option value="1">Last 6 Months</option>
																	<option value="2">Last 3 Months</option>
																	<option value="3">Last 1 Month</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												
												<div id="staffId" class="tab-pane fade">
													<div class="msg"></div>
												
													<form method="post" id="staffIdForm" action="include/do_update_staffId.php">
														<input type="hidden" name="extraId" value="6768978yihhkhjk" />
														<input type="hidden" value="<?php echo $myid;?>" name="uid" />
														<div class="form-group">
															<label class="" for="form-field-idNo">Staff Id Number:</label>

															<div class="">
																<div class="clearfix">
																	<input type="text" id="form-field-idNo" name="idNo" placeholder="IDNo" value="<?php echo $myID; ?>"  />
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="" for="idExpiry">Expiry date</label>

															<div class="">
																<div class="clearfix">
																	<input name="idExpiry" id="idExpiry" class="form-control date-picker col-xs-12 col-sm-4" type="text" value="<?php echo $myIDExpiry; ?>" placeholder="Expiry Date" data-date-format="yyyy-mm-dd"  >
																</div>
															</div>
														</div>
														
														<div class="form-group">
															<input type="button" value="Save ID" class="saveId" />
														</div>
														
													</form>
												</div>
												
											</div>
										</div>

										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									<div class="col-sm-5">
										
										<!-- #section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Delegation</h4>

												
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													
													
													<div>
													<form method="post" action="?delegate">
															<label for="form-field-select-3">Delegate your documents To:</label>

															<br />
														
															<select name="data[delegate_to]" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Staff from your Unit...">
																<option value="">  </option>
															<?php $form_query = "SELECT * FROM admins WHERE department_id='$myUnit' AND id<>'$id' ORDER BY fname, lname"; 
																  $form_result = $conn->query($form_query);
																  while($ro_data = $form_result->fetch_assoc()) {   ?>	
																<option value="<?php echo $ro_data['id']; ?>"><?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?></option>
																<?php } ?>
																
															</select>
															
															<br />
															<label for="id-date-range-picker-1">Duration:</label>
															<br />
															<div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>

																	<input class="form-control" type="text" name="data[duration]" id="date-range-picker" />
															</div>
															
															
															<div class="form-group">
															<label for="form-field-username">Reason</label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[reason]" rows="3" ></textarea>
															</div>
														</div>
															<div class="input-group">
																<p><em> NOTE: Your documents will be forwarded to the specified person for the specified duration if the Unit Manager approves your Delegation Request</em></p>
															</div>
															
															<label></label>
															<br />
															<div class="input-group">
																	<button class="btn btn-primary">
																		<i class="ace-icon fa fa-floppy-o bigger-120"></i>
																		Submit
																	</button>
															</div>
															<input type="hidden" name="action" value="NEW DELEGATE" >	
															<input type="hidden" name="table" value="delegations">
															<input type="hidden" name="data[requested_by]" value="<?php echo $myid; ?>" >	

														</form>	
													</div>
													
													
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
									</div>
									
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		<script src="assets/js/bootbox.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
		jQuery(function($) {
			if($('input[name="email_alerts"]:checked').length == 0){ $("#email_prefs").hide();  }	
			$('#email_alerts').change(function() { 
				if($('input[name="email_alerts"]:checked').length > 0){ $("#email_prefs").show();  }
				else { $("#email_prefs").hide();  }
			});
			
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}

				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;

					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			 

			if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
				
				}


				$('.saveId').on('click', function(){
					
					//saving info on pressing next
							//alert('oh yes')
							var frm = $(this).parents('form').attr('id');
							var values = $('#'+frm).serialize();
							var frmAction = $('#'+frm).attr('action');
							$.ajax(
								{
									type: "POST",
									url: frmAction,
									data: "myData=" + values,
									cache: false,
									success: function(message)
									{	
										completeHandler(frm,message);
									},
									error: function(message)
									{
										alert('failed'+message);
									}
								});
					
					//end of saving function
				});

				
								
			//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
				$('#date-range-picker').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});	
				
				
			/////////////////////////////////////////////////
			///    PWD RESET    ///////////////////
			$("#pwd_button").click(function() {
				
				var pwd1 = $("#pwd1").val();
				var pwd2 = $("#pwd2").val();
				var uid = $("#uid").val();
				
				var msg = "";
				
				if(pwd1==""){ msg = 'ERROR: New password cannot be blank!'; }
				else if(pwd1!=pwd2){ msg = 'ERROR: Passwords must match!'; }
				else { 
				
					var dataString = 'pwd='+ pwd1 + '&id=' + uid;
					
					$.ajax({
						type: "POST",
						url: "include/q_pwd_change.php",
						data: dataString,
						success: function() {
						 msg='Your new DocTracker Password is being saved';
						
						alert(msg);
						
						 $("#pwd1").val("");
						 $("#pwd2").val("");

						}
					});

				}

				bootbox.dialog({
					message: msg, 
					buttons: {
						"success" : {
						  "label" : "OK",
						  "className" : "btn-sm btn-primary",
						  callback :function(e){

							}
						}
					}
				});
								
			});		
			
		});
		
		function completeHandler(frm,message){
				//alert(message);
				$('.msg').html(message);
				$('.msg').show();
				setTimeout(function(){
					$(".msg").hide("slow");
				},7000);
			}
		</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
