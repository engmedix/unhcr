<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>
<h5 class="widget-title bigger lighter"><i class="ace-icon fa fa-table"></i> Search Results </h5>


<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Date initiated</th>
			<th>Initiated by</th>
			<th>Document type / Ref #</th>
			<th>Current Holder</th>
			<th>Description</th>
		</tr>
	</thead>

	<tbody>

	<?php
	/*$conditions = '1';
	if(@$_POST['doc_type'] != '') $conditions .= ' AND documents.document_type="'.$_POST['doc_type'].'"';
	if(@$_POST['internal_ref_number'] != '') $conditions .= ' AND internal_ref_number LIKE "%'.$_POST['internal_ref_number'].'%"';
	if(@$_POST['staff'] != '') $conditions .= ' AND (doc_track.to_id = '.$_POST['staff'].' OR doc_track.from_id = '.$_POST['staff'].') ';
	if(@$_POST['unit'] != '') $conditions .= ' AND (admins2.department_id = '.$_POST['unit'].' ) ';
	if(@$_POST['branch'] != '') $conditions .= ' AND (admins2.station_id = '.$_POST['branch'].' ) ';
	if(@$_POST['source'] != '') $conditions .= ' AND source LIKE "%'.$_POST['source'].'%" ';
	if(@$_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-j H:i',strtotime($dates[0]));
		$endDate = date('Y-m-j H:i',strtotime($dates[1]));

		$conditions .= ' AND (date >= "'.$startDate.'" AND date <= "'.$endDate.'")';
	}*/

	  //error_log($form_query, 0);
	 
	$conditions = '1';
	$value='';
	$values_ar=[];
	$placeholder='';
	if($_POST['organisation'] != '') {
		$conditions .= ' AND organisation LIKE "%?%"';
		//$value.=$_POST['organisation'];
		$arr=array_push($values_ar, $_POST['organisation']);
		$placeholder.='s';
	    }
	if($_POST['doc_type'] != '') {
		$conditions .= ' AND documents.document_type= ?';
		//$value.=$_POST['doc_type'];
		$arr=array_push($values_ar, $_POST['doc_type']);
		$placeholder.='s';
	     }
	if($_POST['internal_ref_number'] != ''){
	 $conditions .= ' AND internal_ref_number=?';
	// $value.=$_POST['internal_ref_number'];
	 $arr=array_push($values_ar, $_POST['internal_ref_number']);
	 $placeholder.='i';
	 }
	if($_POST['staff'] != ''){
	 $conditions .= ' AND (doc_track.to_id=? OR doc_track.from_id = ?)';
     //$value.=$_POST['staff'];
	 $arr=array_push($values_ar, $_POST['staff']);
	 //$value.=$_POST['staff'];
	 $arr=array_push($values_ar, $_POST['staff']);
	 $placeholder.='ss';
	}
	if($_POST['unit'] != '') {
		$conditions .= ' AND admins2.department_id = ?';
	    //$value.=$_POST['unit'];
	    $arr=array_push($values_ar, $_POST['unit']);
	    $placeholder.='s';
	   }
	if($_POST['source'] != '') {
		$conditions .= ' AND source LIKE "%?%" ';
		$arr=array_push($values_ar, $_POST['source']);
	    $placeholder.='s';
	   }
		 if($_POST['branch'] != '') {
	 		$conditions .= ' AND organisation = ? ';
	 		$arr=array_push($values_ar, $_POST['branch']);
	 	    $placeholder.='s';
	 	   }
	if($_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-d H:i:s',strtotime($dates[0]));
		$endDate = date('Y-m-d H:i:s',strtotime($dates[1]. ' + 1 day'));
		$conditions .= ' AND (date >= ? AND date <= ?)';
		$arr=array_push($values_ar,$startDate);
		$arr=array_push($values_ar, $endDate);
	    $placeholder.='ss';
	}
   $count=count($values_ar);
   $stmt=$conn->prepare("SELECT documents.*, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON documents.created_by = admins.id LEFT JOIN admins admins2 ON doc_track.to_id = admins2.id WHERE $conditions AND documents.id IS NOT NULL GROUP BY doc_id ORDER BY date desc");
   //$form_result =$conn->query($form_query);
  // $stmt =$conn->prepare($form_query);
   $stmt->bind_param($placeholder, ...$values_ar);
   $stmt->execute();
   $form_result = $stmt->get_result();

	  $now = new DateTime();
	  $now->setTime(0,0,0);
	 if($form_result->num_rows>0){
	  while($form_data = $form_result->fetch_assoc()) {

			$readStatus = '';

			if($form_data['file_status'] == 'DOCUMENT SENT' || $form_data['file_status'] == 'REQUEST SENT') $label = ' label-info"';
			elseif($form_data['file_status'] == 'APPROVED') $label = 'label-success';
			elseif($fotrm_daa['file_status'] == 'REJECTED') $label = 'label-danger';
			elseif($form_data['file_status'] == 'FORWARDED') $label = 'label-pink';

			$sentDate = new DateTime($form_data['date']);
			$sentDate->setTime( 0, 0, 0 );

			$diff = $now->diff( $sentDate );

			$periodSpent = $diff->days;
			if($periodSpent == 0) $periodSpent = ' Today';
			elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
			elseif($periodSpent >= 2) $periodSpent .= ' days ago';

	  ?>
		<tr>
			<td><?php echo date("j M Y - h:i a",strtotime($form_data['date'])).'  <span class="label">'.$periodSpent.'</span>'; ?></td>
			<td><?php echo $form_data['fname'].' '.$form_data['lname']; ?></td>
			<td><a href="timeline.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>"><?php echo $form_data['document_type']; ?> / <?php echo $form_data['internal_ref_number']; ?></a></td>
			<td><?php echo getStaffName($form_data['current_holder']); ?></td>
			<td><?php echo $form_data['description']; ?></td>


		</tr>
	  <?php } }else{
	  	echo "No results found";
	  }?>



	</tbody>
</table>
