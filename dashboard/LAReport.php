<?php include('header.php');?>
  <?php
  $form_query = "SELECT COUNT(*) as total FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND date >'2019-04-01'";
  $form_result = $conn->query($form_query);
  $form_data = $form_result->fetch_assoc();
  $form_query1 = "SELECT COUNT(*) as total_approved_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $form_result1 = $conn->query($form_query1);
  $form_data1 = $form_result1->fetch_assoc();
  $form_query2 = "SELECT COUNT(*) as total_rejected_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $form_result2 = $conn->query($form_query2);
  $form_data2 = $form_result2->fetch_assoc();
  $form_query3 = "SELECT COUNT(*) as total_pending_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $form_result3 = $conn->query($form_query3);
  $form_data3 = $form_result3->fetch_assoc();

  $frm_query = "SELECT COUNT(*) as total_closed_business_loans FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result = $conn->query($frm_query);
  $frm_data = $frm_result->fetch_assoc();
  $frm_query1 = "SELECT COUNT(*) as total_rejected_business_loans FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result1 = $conn->query($frm_query1);
  $frm_data1 = $frm_result1->fetch_assoc();
  $frm_query2 = "SELECT COUNT(*) as total_pending_business_loans FROM documents WHERE doc_type_id='87' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result2 = $conn->query($frm_query2);
  $frm_data2 = $frm_result2->fetch_assoc();

  $frm_query_approved = "SELECT COUNT(*) as total_closed_prescored_loans FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result_approved = $conn->query($frm_query_approved);
  $frm_data_approved = $frm_result_approved->fetch_assoc();
  $frm_query_rejected = "SELECT COUNT(*) as total_rejected_prescored_loans FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result_rejected = $conn->query($frm_query_rejected);
  $frm_data_rejected = $frm_result_rejected->fetch_assoc();
  $frm_query_pending = "SELECT COUNT(*) as total_pending_prescored_loans FROM documents WHERE doc_type_id='89' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result_pending = $conn->query($frm_query_pending);
  $frm_data_pending = $frm_result_pending->fetch_assoc();

  $frm_query_approved1 = "SELECT COUNT(*) as total_closed_unsecured_loans FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result_approved1 = $conn->query($frm_query_approved1);
  $frm_data_approved1 = $frm_result_approved1->fetch_assoc();
  $frm_query_rejected1 = "SELECT COUNT(*) as total_rejected_unsecured_loans FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result_rejected1 = $conn->query($frm_query_rejected1);
  $frm_data_rejected1 = $frm_result_rejected1->fetch_assoc();
  $frm_query_pending1 = "SELECT COUNT(*) as total_pending_unsecured_loans FROM documents WHERE doc_type_id='88' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result_pending1 = $conn->query($frm_query_pending1);
  $frm_data_pending1 = $frm_result_pending1->fetch_assoc();

  ?>
  <?php
  $conditions = 'WHERE 1 ';
                            $condition= "AND receiver_status='ARCHIVED'";

                            if(@$_POST['duration'] != '') {
                              $dates = explode(' - ',$_POST['duration']);
                              $startDate = date('Y-m-j H:i',strtotime($dates[0]));
                              $endDate = date('Y-m-j H:i',strtotime($dates[1]));

                              $conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
                            }
 $cont_query = $conn->query("SELECT COUNT(*) num,(IF(action = 'APPROVED','APPROVED/CLOSED',action))act,total FROM doc_track LEFT JOIN (SELECT COUNT(*) total, from_id FROM doc_track $conditions GROUP BY from_id) tt USING(from_id) $conditions  GROUP BY action");
 $cc = mysqli_num_rows($cont_query);
$cont_data = $cont_query->fetch_assoc();

?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
<div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- Sidebar -->
<?php include('sidebar.php');?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
               Dropdown - Alerts 
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

           Nav Item - Messages 
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                 Counter - Messages 
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              Dropdown - Messages 
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>-->

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Valerie Luna</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
               <!--<a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>-->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="http://localhost/fincatest/include/do_signout.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">LOAN APPLICATIONS</h1>
          
          </div>

          <!-- Content Row -->
                    <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">REJECTED LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data2['total_rejected_loans']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">PENDING LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data3['total_pending_loans']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">TOTAL LOAN APPLICATIONS</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?=$form_data['total']?></div>
                        </div>
                        <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>-->

            <!-- Pending Requests Card Example 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">TOTAL APPROVED LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data1['total_approved_loans']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>-->
          </div>
          <!-- Content Row -->

          <div class="row">

    <div class="widget-box transparent">
        										<a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success search-btn pull-right">Export Report to excel</a>

                              <div class="widget-body">
        												<div class="widget-main no-padding">
        													<table class="table table-bordered table-striped" id="dynamic-table">
        														<thead class="thin-border-bottom">
        															<tr>
        																<th>Name</th>
        																<th>Unit</th>
        																<th>Docs In</th>
        																<th>Docs Out</th>
        																<th>Initiated</th>
        																<th>Delegations</th>
        																<th>Forwards</th>
        																<th>Approvals</th>
        																<th>Rejects</th>
        																<th class="hidden-480">
        																	<span class="label label-success">Fast</span>
        																</th>
        																<th class="hidden-480">
        																	<span class="label label-warning">Normal</span>
        																</th>
        																<th class="hidden-480">
        																	<span class="label label-danger">Late</span>
        																</th>
        															</tr>
        														</thead>

        														<tbody>
        														<?php

        														$conditions = 'WHERE to_id <> from_id ';

        														if($_POST['duration'] != '') {
        															$dates = explode(' - ',$_POST['duration']);
        															$startDate = date('Y-m-j H:i',strtotime($dates[0]));
        															$endDate = date('Y-m-j H:i',strtotime($dates[1]));

        															$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
        														}

        														$form_query = "
        														SELECT admins.*, COALESCE(docsout_,0) docsout,COALESCE(docsin_,0) docsin,COALESCE(fast,0) ft, COALESCE(slow,0) sl,COALESCE(late,0) lt, counts.* FROM admins
        														LEFT JOIN
        															(SELECT COUNT(*) docsout_, from_id FROM doc_track $conditions GROUP BY from_id) d_o ON admins.id = d_o.from_id
        														LEFT JOIN
        															(SELECT COUNT(*) docsin_, to_id FROM doc_track $conditions GROUP BY to_id) d_i ON admins.id = d_i.to_id
        														LEFT JOIN
        															(SELECT to_id u_id,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) <= 1,1,0)) fast,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 1 AND DATEDIFF(archive_date,actionDate) <= 3,1,0)) slow,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 3,1,0)) late
        																FROM doc_track $conditions GROUP BY to_id
        															)stats ON admins.id = stats.u_id
        														LEFT JOIN
        															(SELECT from_id,
        																	SUM(IF(action = 'DOCUMENT SENT',1,0)) initiated,
        																	SUM(IF(action = 'DELEGATED',1,0)) delegations,
        																	SUM(IF(action = 'FORWARDED',1,0)) forwards,
        																	SUM(IF(action = 'REJECTED',1,0)) rejects,
        																	SUM(IF(action = 'APPROVED',1,0)) approvals
        																FROM doc_track $conditions GROUP BY from_id
        															) counts ON admins.id = counts.from_id


        														WHERE id<>'1' ORDER BY department_id, fname";

        												  $form_result = $conn->query($form_query);
        												  //echo $form_query;
        												  while($form_data = $form_result->fetch_assoc()) {
        													$w_fast = @floor(($form_data['ft']/$form_data['docsin'])*100);
        													$w_slow = @floor(($form_data['sl']/$form_data['docsin'])*100);
        													$w_late = @floor(($form_data['lt']/$form_data['docsin'])*100);
        												  ?>
        															<tr>
        																<td><?php echo $form_data['fname']." ".$form_data['lname']." ".$form_data['oname']; ?></td>
        																<td><?php echo getDept($form_data['department_id']); ?></td>
        																<td><?php echo ($form_data['docsin']); ?></td>
        																<td><?php echo ($form_data['docsout']); ?></td>
        																<td><?php echo ($form_data['initiated']); ?></td>
        																<td><?php echo ($form_data['delegations']); ?></td>
        																<td><?php echo ($form_data['forwards']); ?></td>
        																<td><?php echo ($form_data['approvals']); ?></td>
        																<td><?php echo ($form_data['rejects']); ?></td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-success" style="width:<?php echo $w_fast; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_fast); ?>%</span>
        																	</div>
        																</td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-warning" style="width:<?php echo $w_slow; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_slow); ?>%</span>
        																	</div>
        																</td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-danger" style="width:<?php echo $w_late; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_late); ?>%</span>
        																	</div>
        																</td>
        															</tr>
        														<?php } ?>

        														</tbody>
        													</table>
        												</div><!-- /.widget-main -->
        											</div><!-- /.widget-body -->
        										</div><!-- /.widget-box -->
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
           <span>Copyright &copy; finca@cinnamonerp.com <?=date('Y');?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="http://localhost/fincatest/include/do_signout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <?php include('footer.php');?>
































<?php include('header.php');?>

<body>

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <?php include('sidebar.php');?>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="search-box pull-left">
                            <form action="#">
                                <input type="text" name="search" placeholder="Search..." required>
                                <i class="ti-search"></i>
                            </form>
                        </div>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-6 col-sm-4 clearfix">
                        <ul class="notification-area pull-right">
                            <li id="full-view"><i class="ti-fullscreen"></i></li>
                            <li id="full-view-exit"><i class="ti-zoom-out"></i></li>
                            <li class="dropdown">
                                <i class="ti-bell dropdown-toggle" data-toggle="dropdown">
                                    <span>2</span>
                                </i>
                                <div class="dropdown-menu bell-notify-box notify-box">
                                    <span class="notify-title">You have 3 new notifications <a href="#">view all</a></span>
                                    <div class="nofity-list">
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                            <div class="notify-text">
                                                <p>You have Changed Your Password</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <i class="fa fa-envelope-o dropdown-toggle" data-toggle="dropdown"><span>3</span></i>
                                <div class="dropdown-menu notify-box nt-enveloper-box">
                                    <span class="notify-title">You have 3 new notifications <a href="#">view all</a></span>
                                    <div class="nofity-list">
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb">
                                                <img src="assets/images/author/author-img1.jpg" alt="image">
                                            </div>
                                            <div class="notify-text">
                                                <p>Aglae Mayer</p>
                                                <span class="msg">Hey I am waiting for you...</span>
                                                <span>3:15 PM</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="settings-btn">
                                <i class="ti-settings"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                          <h4 class="page-title pull-left"><a href="index.php">Dashboard</a></h4>
                          <ul class="breadcrumbs pull-left">
                              <li><a href="LAReport.php">Loan application report</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?=$_SESSION['username'];?> <i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Message</a>
                                <a class="dropdown-item" href="#">Settings</a>
                                <a class="dropdown-item" href="http://localhost/fincatest/include/do_signout.php">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

              <div class="main-content-inner">
                   <div class="row">
                                 <div class="col-12 mt-5">
                                     <div class="card">
                                         <div class="card-body">
                                             <h4 class="header-title">Unit Performance</h4>
                                            <form action="" name="frmFilter" method="post">
                                                <input type="hidden" name="extra" value="dhfkjhsdfjhsdjfhdskj"/>
                                               <div class="form-group row">
                                                 <label for="" class="col-md-2 control-label">Date</label>
                                                 <div class="col-sm-10">
                                                 <input type="text" class="form-control daterange" name="duration" style="width:50%" />
                                                 </div>
                                               </div>
                                               <div class="form-group">
                                                   <div class=" col-sm-6 pull-right">
                                                         <input type="submit" value="Search" class="btn btn-sm btn-success">
                                                   </div>
                                               </div>
                                             </form>
                                         </div>
                                     </div>
                                 </div>
                                 <!-- basic form end -->
                           </div>
                       </div>

        								<div class="widget-box transparent">
        										<a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success search-btn pull-right">Export Report to excel</a>

                              <div class="widget-body">
        												<div class="widget-main no-padding">
        													<table class="table table-bordered table-striped" id="dynamic-table">
        														<thead class="thin-border-bottom">
        															<tr>
        																<th>Name</th>
        																<th>Unit</th>
        																<th>Docs In</th>
        																<th>Docs Out</th>
        																<th>Initiated</th>
        																<th>Delegations</th>
        																<th>Forwards</th>
        																<th>Approvals</th>
        																<th>Rejects</th>
        																<th class="hidden-480">
        																	<span class="label label-success">Fast</span>
        																</th>
        																<th class="hidden-480">
        																	<span class="label label-warning">Normal</span>
        																</th>
        																<th class="hidden-480">
        																	<span class="label label-danger">Late</span>
        																</th>
        															</tr>
        														</thead>

        														<tbody>
        														<?php

        														$conditions = 'WHERE to_id <> from_id ';

        														if($_POST['duration'] != '') {
        															$dates = explode(' - ',$_POST['duration']);
        															$startDate = date('Y-m-j H:i',strtotime($dates[0]));
        															$endDate = date('Y-m-j H:i',strtotime($dates[1]));

        															$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
        														}

        														$form_query = "
        														SELECT admins.*, COALESCE(docsout_,0) docsout,COALESCE(docsin_,0) docsin,COALESCE(fast,0) ft, COALESCE(slow,0) sl,COALESCE(late,0) lt, counts.* FROM admins
        														LEFT JOIN
        															(SELECT COUNT(*) docsout_, from_id FROM doc_track $conditions GROUP BY from_id) d_o ON admins.id = d_o.from_id
        														LEFT JOIN
        															(SELECT COUNT(*) docsin_, to_id FROM doc_track $conditions GROUP BY to_id) d_i ON admins.id = d_i.to_id
        														LEFT JOIN
        															(SELECT to_id u_id,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) <= 1,1,0)) fast,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 1 AND DATEDIFF(archive_date,actionDate) <= 3,1,0)) slow,
        																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 3,1,0)) late
        																FROM doc_track $conditions GROUP BY to_id
        															)stats ON admins.id = stats.u_id
        														LEFT JOIN
        															(SELECT from_id,
        																	SUM(IF(action = 'DOCUMENT SENT',1,0)) initiated,
        																	SUM(IF(action = 'DELEGATED',1,0)) delegations,
        																	SUM(IF(action = 'FORWARDED',1,0)) forwards,
        																	SUM(IF(action = 'REJECTED',1,0)) rejects,
        																	SUM(IF(action = 'APPROVED',1,0)) approvals
        																FROM doc_track $conditions GROUP BY from_id
        															) counts ON admins.id = counts.from_id


        														WHERE id<>'1' ORDER BY department_id, fname";

        												  $form_result = $conn->query($form_query);
        												  //echo $form_query;
        												  while($form_data = $form_result->fetch_assoc()) {
        													$w_fast = @floor(($form_data['ft']/$form_data['docsin'])*100);
        													$w_slow = @floor(($form_data['sl']/$form_data['docsin'])*100);
        													$w_late = @floor(($form_data['lt']/$form_data['docsin'])*100);
        												  ?>
        															<tr>
        																<td><?php echo $form_data['fname']." ".$form_data['lname']." ".$form_data['oname']; ?></td>
        																<td><?php echo getDept($form_data['department_id']); ?></td>
        																<td><?php echo ($form_data['docsin']); ?></td>
        																<td><?php echo ($form_data['docsout']); ?></td>
        																<td><?php echo ($form_data['initiated']); ?></td>
        																<td><?php echo ($form_data['delegations']); ?></td>
        																<td><?php echo ($form_data['forwards']); ?></td>
        																<td><?php echo ($form_data['approvals']); ?></td>
        																<td><?php echo ($form_data['rejects']); ?></td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-success" style="width:<?php echo $w_fast; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_fast); ?>%</span>
        																	</div>
        																</td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-warning" style="width:<?php echo $w_slow; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_slow); ?>%</span>
        																	</div>
        																</td>
        																<td>
        																	<div class="my-progress-container">
        																		<div class="label-danger" style="width:<?php echo $w_late; ?>%;">&nbsp;</div>
        																		<span><?php echo ($w_late); ?>%</span>
        																	</div>
        																</td>
        															</tr>
        														<?php } ?>

        														</tbody>
        													</table>
        												</div><!-- /.widget-main -->
        											</div><!-- /.widget-body -->
        										</div><!-- /.widget-box -->

        				</div>
<?php include('footer.php');?>
        <!-- main content area end -->
