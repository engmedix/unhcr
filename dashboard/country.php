<?php

/*Include the `fusioncharts.php` file that contains functions
  to embed the charts.
*/
  include("fusioncharts/fusioncharts.php");

  /* The following 4 code lines contain the database connection information. Alternatively, you can move these code lines to a separate file and include the file here. You can also modify this code based on your database connection.   */

   $hostdb = "localhost";  // MySQl host
   $userdb = "root";  // MySQL username
   $passdb = "";  // MySQL password
   $namedb = "countries";  // MySQL database name

   // Establish a connection to the database
   $dbhandle = new mysqli($hostdb, $userdb, $passdb, $namedb);

   // Render an error message, to avoid abrupt failure, if the database connection parameters are incorrect
   if ($dbhandle->connect_error) {
    exit("There was an error with your connection: ".$dbhandle->connect_error);
   }

?>

<html>
   <head>
    <title>FusionCharts XT - Column 2D Chart - Data from a database</title>
    <link  rel="stylesheet" type="text/css" href="resources/css/style.css" />
	<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>

  <!--  Include the `fusioncharts.js` file. This file is needed to render the chart. Ensure that the path to this JS file is correct. Otherwise, it may lead to JavaScript errors. -->

      <script src="resources/js/fusioncharts.js"></script>
   </head>
   <body>
    <?php

      // Form the SQL query that returns the top 10 most populous countries
      $strQuery = "SELECT name, population, id FROM tbl_countries ORDER BY population DESC LIMIT 10";

      // Execute the query, or else return the error message.
      $result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");

      // If the query returns a valid response, prepare the JSON string
      if ($result) {
          // The `$arrData` array holds the chart attributes and data
          $arrData = array(
                "chart" => array(
                    "caption" => "Top 10 Most Populous Countries",
                    "showValues"=> "0",
                    "theme"=> "zune"
                )
            );

          $arrData["data"] = array();

  // Push the data into the array

          while($row = mysqli_fetch_array($result)) {
            array_push($arrData["data"], array(
                "label" => $row["name"],
                "value" => $row["population"],
                "link" => "countryDrillDown.php?country_id=".$row["id"]
                )
            );
          }

          /*JSON Encode the data to retrieve the string containing the JSON representation of the data in the array. */

          $jsonEncodedData = json_encode($arrData);

          /*Create an object for the column chart. Initialize this object using the FusionCharts PHP class constructor. The constructor is used to initialize the chart type, chart id, width, height, the div id of the chart container, the data format, and the data source. */

          $columnChart = new FusionCharts("column2D", "myFirstChart" , 600, 300, "chart-1", "json", $jsonEncodedData);

          // Render the chart
          $columnChart->render();

          // Close the database connection
          $dbhandle->close();

      }

    ?>
    <div id="chart-1"><!-- Fusion Charts will render here--></div>
	<br><br>
	<div id="chart-container">FusionCharts will render here</div>
	<br><br>
	<script>
	
	FusionCharts.ready(function() {
  var satisfactionChart = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container',
    width: '400',
    height: '300',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Top 3 Juice Flavors",
        "subcaption": "Last year",
        "xaxisname": "Flavor",
        "yaxisname": "Amount (In USD)",
        "numberprefix": "$",
        "theme": "fusion",
        "rotateValues": "0"
      },
      "data": [{
        "label": "Apple",
        "value": "810000",
        "link": "newchart-xml-apple"
      }, {
        "label": "Cranberry",
        "value": "620000",
        "link": "newchart-xml-cranberry"
      }, {
        "label": "Grapes",
        "value": "350000",
        "link": "newchart-xml-grapes"
      }],
      "linkeddata": [{
        "id": "apple",
        "linkedchart": {
          "chart": {
            "caption": "Apple Juice - Quarterly Sales",
            "subcaption": "Last year",
            "numberprefix": "$",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "Q1",
            "value": "157000"
          }, {
            "label": "Q2",
            "value": "172000"
          }, {
            "label": "Q3",
            "value": "206000"
          }, {
            "label": "Q4",
            "value": "275000"
          }]
        }
      }, {
        "id": "cranberry",
        "linkedchart": {
          "chart": {
            "caption": "Cranberry Juice - Quarterly Sales",
            "subcaption": "Last year",
            "numberprefix": "$",
            "theme": "fusion",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "Q1",
            "value": "102000"
          }, {
            "label": "Q2",
            "value": "142000"
          }, {
            "label": "Q3",
            "value": "187000"
          }, {
            "label": "Q4",
            "value": "189000"
          }]
        }
      }, {
        "id": "grapes",
        "linkedchart": {
          "chart": {
            "caption": "Grapes Juice - Quarterly Sales",
            "subcaption": "Last year",
            "numberprefix": "$",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "Q1",
            "value": "45000"
          }, {
            "label": "Q2",
            "value": "72000"
          }, {
            "label": "Q3",
            "value": "95000"
          }, {
            "label": "Q4",
            "value": "108000"
          }]
        }
      }]
    },
    "events": {
      'beforeRender': function(evt, args) {
        if (!document.getElementById('eventmessages')) {
          // Create container div for radio controls
          var wrapper = document.createElement('div');
          var label = document.createElement('label');
          label.innerText = "Click on the data plot for the event to fire";
          var controllers = document.createElement('div');
          controllers.setAttribute('id', 'eventmessages');
          controllers.innerHTML = "";

          wrapper.appendChild(label);
          wrapper.appendChild(controllers);

          // args.container.parentNode.insertBefore(controllers, args.container.nextSibling);

          args.container.parentNode.insertBefore(wrapper, args.container.nextSibling);

          // set css style for controllers div
          controllers.style.cssText = 'position:inherit; width:400px; height: 100px; border: 1px solid #cccccc; margin-top:10px;padding-left:5px; overflow: scroll;';
        }
        evt.sender.configureLink({
          type: "pie2d",
          overlayButton: {
            message: 'close',
            fontColor: '880000',
            bgColor: 'FFEEEE',
            borderColor: '660000'
          }
        }, 0);

      },
      'beforeLinkedItemOpen': function(evt, args) {
        document.getElementById('eventmessages').innerHTML = " Event - <b>beforeLinkedItemOpen</b> invoked<br>" + document.getElementById('eventmessages').innerHTML;
      },
      'linkedItemOpened': function(evt, args) {
        document.getElementById('eventmessages').innerHTML = " Event - <b>linkedItemOpened</b> invoked<br>" + document.getElementById('eventmessages').innerHTML;
      },
      'beforeLinkedItemClose': function(evt, args) {
        document.getElementById('eventmessages').innerHTML = " Event - <b>beforeLinkedItemClose</b> invoked<br>" + document.getElementById('eventmessages').innerHTML;
      },
      'linkedItemClosed': function(evt, args) {
        document.getElementById('eventmessages').innerHTML = " Event - <b>linkedItemClosed</b> invoked<br>" + document.getElementById('eventmessages').innerHTML;
      }
    }
  });

  satisfactionChart.render();

});

	</script>
   </body>
</html>