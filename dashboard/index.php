<?php include('header.php');?>
  <?php
  $form_query = "SELECT COUNT(*) as total FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND date >'2019-04-01'";
  $form_result = $conn->query($form_query);
  $form_data = $form_result->fetch_assoc();
  $form_query1 = "SELECT COUNT(*) as total_approved_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $form_result1 = $conn->query($form_query1);
  $form_data1 = $form_result1->fetch_assoc();
  $form_query2 = "SELECT COUNT(*) as total_rejected_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $form_result2 = $conn->query($form_query2);
  $form_data2 = $form_result2->fetch_assoc();
  $form_query3 = "SELECT COUNT(*) as total_pending_loans FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $form_result3 = $conn->query($form_query3);
  $form_data3 = $form_result3->fetch_assoc();

  $frm_query = "SELECT COUNT(*) as total_closed_business_loans FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result = $conn->query($frm_query);
  $frm_data = $frm_result->fetch_assoc();
  $frm_query1 = "SELECT COUNT(*) as total_rejected_business_loans FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result1 = $conn->query($frm_query1);
  $frm_data1 = $frm_result1->fetch_assoc();
  $frm_query2 = "SELECT COUNT(*) as total_pending_business_loans FROM documents WHERE doc_type_id='87' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result2 = $conn->query($frm_query2);
  $frm_data2 = $frm_result2->fetch_assoc();

  $frm_query_approved = "SELECT COUNT(*) as total_closed_prescored_loans FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result_approved = $conn->query($frm_query_approved);
  $frm_data_approved = $frm_result_approved->fetch_assoc();
  $frm_query_rejected = "SELECT COUNT(*) as total_rejected_prescored_loans FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result_rejected = $conn->query($frm_query_rejected);
  $frm_data_rejected = $frm_result_rejected->fetch_assoc();
  $frm_query_pending = "SELECT COUNT(*) as total_pending_prescored_loans FROM documents WHERE doc_type_id='89' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result_pending = $conn->query($frm_query_pending);
  $frm_data_pending = $frm_result_pending->fetch_assoc();

  $frm_query_approved1 = "SELECT COUNT(*) as total_closed_unsecured_loans FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
  $frm_result_approved1 = $conn->query($frm_query_approved1);
  $frm_data_approved1 = $frm_result_approved1->fetch_assoc();
  $frm_query_rejected1 = "SELECT COUNT(*) as total_rejected_unsecured_loans FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
  $frm_result_rejected1 = $conn->query($frm_query_rejected1);
  $frm_data_rejected1 = $frm_result_rejected1->fetch_assoc();
  $frm_query_pending1 = "SELECT COUNT(*) as total_pending_unsecured_loans FROM documents WHERE doc_type_id='88' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
  $frm_result_pending1 = $conn->query($frm_query_pending1);
  $frm_data_pending1 = $frm_result_pending1->fetch_assoc();

  ?>
  <?php
  $conditions = 'WHERE 1 ';
                            $condition= "AND receiver_status='ARCHIVED'";

                            if(@$_POST['duration'] != '') {
                              $dates = explode(' - ',$_POST['duration']);
                              $startDate = date('Y-m-j H:i',strtotime($dates[0]));
                              $endDate = date('Y-m-j H:i',strtotime($dates[1]));

                              $conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
                            }
 $cont_query = $conn->query("SELECT COUNT(*) num,(IF(action = 'APPROVED','APPROVED/CLOSED',action))act,total FROM doc_track LEFT JOIN (SELECT COUNT(*) total, from_id FROM doc_track $conditions GROUP BY from_id) tt USING(from_id) $conditions  GROUP BY action");
 $cc = mysqli_num_rows($cont_query);
$cont_data = $cont_query->fetch_assoc();

$total_business_loans=$frm_data1['total_rejected_business_loans']+$frm_data['total_closed_business_loans']+$frm_data2['total_pending_business_loans'];
$total_prescored_loans=$frm_data_rejected['total_rejected_prescored_loans']+$frm_data_approved['total_closed_prescored_loans']+$frm_data_pending['total_pending_prescored_loans'];
$total_unsecured_loans=$frm_data_rejected1['total_rejected_unsecured_loans']+$frm_data_approved1['total_closed_unsecured_loans']+$frm_data_pending1['total_pending_unsecured_loans'];
$total_loans=$total_business_loans+$total_prescored_loans+$total_unsecured_loans;

$percentage_business_loans=number_format(($total_business_loans/$total_loans)*100,2);
$percentage_prescored_loans=number_format(($total_prescored_loans/$total_loans)*100,2);
$percentage_unsecured_loans=number_format(($total_unsecured_loans/$total_loans)*100,2);

$percentage_approved=number_format(($form_data1['total_approved_loans']/$total_loans)*100,2);
$percentage_rejected=number_format(($form_data2['total_rejected_loans']/$total_loans)*100,2);
$percentage_pending=number_format(($form_data3['total_pending_loans']/$total_loans)*100,2);

//get total business loans for each of last five years
$end_year=date('Y');
$start_year=date('Y')-4;
$start_year2=date('Y')-3;
$start_year3=date('Y')-2;
$start_year4=date('Y')-1;
$i=0;
$month=1;
$j=0;
$namesArray=array();
$labelsArray=array();
$arr_month=array();
$bl_array1=array(); $bl_array2=array(); $bl_array3=array(); $bl_array4=array(); $bl_array5=array();
$psl_array1=array(); $psl_array2=array(); $psl_array3=array(); $psl_array4=array(); $psl_array5=array();
$sel_array1=array(); $sel_array2=array(); $sel_array3=array(); $sel_array4=array(); $sel_array5=array();
$arr_month = array(["JAN", "1"],["FEB", "2"],["MARCH", "3"],["APRIL", "4"],["MAY", "5"],["JUNE", "6"],["JULY", "7"],["AUG", "6"],["SEP", "9"],["OCT", "10"],["NOV", "11"],["DEC", "12"]);
for($i=0;$i<5;$i++){
	$query="SELECT COUNT(*) AS total_business_loans FROM documents WHERE (YEAR(date) = $start_year) AND doc_type_id=87";
	$result=$conn->query($query);
	$res=$result->fetch_assoc();
	$data=array("value"=>$res["total_business_loans"],"link"=>"newchart-xml-business_loans_$start_year");
     array_push($namesArray,$data);
     $labels=array("label"=>"$start_year");
	 array_push($labelsArray,$labels);
  $start_year++;
}

//number of loan applications per month for each year

	 for($j=0;$j<12;$j++){
	 $business_loans_year1 = "SELECT COUNT(*) as total_monthly_business_loans FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year)";
     $bl_result1=$conn->query($business_loans_year1);
	 $bl_res1=$bl_result1->fetch_assoc();
	 $bl_data1=array("label"=>$arr_month[$j][0],"value"=>$bl_res1["total_monthly_business_loans"]);
     array_push($bl_array1,$bl_data1);

	 $business_loans_year2 = "SELECT COUNT(*) as total_monthly_business_loans1 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year2)";
     $bl_result2=$conn->query($business_loans_year2);
	 $bl_res2=$bl_result2->fetch_assoc();
	 $bl_data2=array("label"=>$arr_month[$j][0],"value"=>$bl_res2["total_monthly_business_loans1"]);
     array_push($bl_array2,$bl_data2);

	 $business_loans_year3 = "SELECT COUNT(*) as total_monthly_business_loans2 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year3)";
     $bl_result3=$conn->query($business_loans_year3);
	 $bl_res3=$bl_result3->fetch_assoc();
	 $bl_data3=array("label"=>$arr_month[$j][0],"value"=>$bl_res3["total_monthly_business_loans2"]);
     array_push($bl_array3,$bl_data3);

	 $business_loans_year4 = "SELECT COUNT(*) as total_monthly_business_loans3 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year4)";
     $bl_result4=$conn->query($business_loans_year4);
	 $bl_res4=$bl_result4->fetch_assoc();
	 $bl_data4=array("label"=>$arr_month[$j][0],"value"=>$bl_res4["total_monthly_business_loans3"]);
     array_push($bl_array4,$bl_data4);

	 $business_loans_year5 = "SELECT COUNT(*) as total_monthly_business_loans4 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $end_year)";
     $bl_result5=$conn->query($business_loans_year5);
	 $bl_res5=$bl_result5->fetch_assoc();
	 $bl_data5=array("label"=>$arr_month[$j][0],"value"=>$bl_res5["total_monthly_business_loans4"]);
     array_push($bl_array5,$bl_data5);
	$month++;
	 }
  $encoded_data=json_encode($namesArray);
  $encoded_labels=json_encode($labelsArray);
  $bl_encode1=json_encode($bl_array1);
  $bl_encode2=json_encode($bl_array2);
  $bl_encode3=json_encode($bl_array3);
  $bl_encode4=json_encode($bl_array4);
  $bl_encode5=json_encode($bl_array5);
//get total secured loans for each of last five years

$namesArray1=array();
$start_year1=date('Y')-4;
for($i=0;$i<5;$i++){
	$query1="SELECT COUNT(*) AS total_secured_loans FROM documents WHERE (YEAR(date) = $start_year1) AND doc_type_id=88";
	$result1=$conn->query($query1);
	$res1=$result1->fetch_assoc();
	$data1=array("value"=>$res1["total_secured_loans"],"link"=>"newchart-xml-secured_loans_$start_year1");
  array_push($namesArray1,$data1);
  $start_year1++;
}
////////////////////////////////
$sel_month=1;
 for($j=0;$j<12;$j++){
	 $sel_year1 = "SELECT COUNT(*) as total_monthly_sel FROM documents WHERE (MONTH(date) = $sel_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year)";
     $sel_result1=$conn->query($sel_year1);
	 $sel_res1=$sel_result1->fetch_assoc();
	 $sel_data1=array("label"=>$arr_month[$j][0],"value"=>$sel_res1["total_monthly_sel"]);
     array_push($sel_array1,$sel_data1);

	 $sel_year2 = "SELECT COUNT(*) as total_monthly_sel1 FROM documents WHERE (MONTH(date) = $sel_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year2)";
     $sel_result2=$conn->query($sel_year2);
	 $sel_res2=$sel_result2->fetch_assoc();
	 $sel_data2=array("label"=>$arr_month[$j][0],"value"=>$sel_res2["total_monthly_sel1"]);
     array_push($sel_array2,$sel_data2);

	 $sel_year3 = "SELECT COUNT(*) as total_monthly_sel2 FROM documents WHERE (MONTH(date) = $sel_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year3)";
     $sel_result3=$conn->query($sel_year3);
	 $sel_res3=$sel_result3->fetch_assoc();
	 $sel_data3=array("label"=>$arr_month[$j][0],"value"=>$sel_res3["total_monthly_sel2"]);
     array_push($sel_array3,$sel_data3);

	 $sel_year4 = "SELECT COUNT(*) as total_monthly_sel3 FROM documents WHERE (MONTH(date) = $sel_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year4)";
     $sel_result4=$conn->query($sel_year4);
	 $sel_res4=$sel_result4->fetch_assoc();
	 $sel_data4=array("label"=>$arr_month[$j][0],"value"=>$sel_res4["total_monthly_sel3"]);
     array_push($sel_array4,$sel_data4);

	 $sel_year5 = "SELECT COUNT(*) as total_monthly_sel4 FROM documents WHERE (MONTH(date) = $sel_month) AND doc_type_id='88'  AND (YEAR(date) = $end_year)";
     $sel_result5=$conn->query($sel_year5);
	 $sel_res5=$sel_result5->fetch_assoc();
	 $sel_data5=array("label"=>$arr_month[$j][0],"value"=>$sel_res5["total_monthly_sel4"]);
     array_push($sel_array5,$sel_data5);
	$sel_month++;
	 }
  $encoded_labels=json_encode($labelsArray);
  $sel_encode1=json_encode($sel_array1);
  $sel_encode2=json_encode($sel_array2);
  $sel_encode3=json_encode($sel_array3);
  $sel_encode4=json_encode($sel_array4);
  $sel_encode5=json_encode($sel_array5);
  $encoded_data1=json_encode($namesArray1);

//get total pre scored loans for each of last five years

$namesArray2=array();
 $psl_start_year=date('Y')-4;
for($i=0;$i<5;$i++){
	$query2="SELECT COUNT(*) AS total_prescored_loans FROM documents WHERE (YEAR(date) = $psl_start_year) AND doc_type_id=89 ";
	$result2=$conn->query($query2);
	$res2=$result2->fetch_assoc();
	$data2=array("value"=>$res2["total_prescored_loans"],"link"=>"newchart-xml-prescored_loans_$psl_start_year");
  array_push($namesArray2,$data2);
  $psl_start_year++;
}


/////Drill down months////////////////
//number of loan applications per month for each year
	 $psl_month=1;
	 for($j=0;$j<12;$j++){
	 $psl_year1 = "SELECT COUNT(*) as total_monthly_psl FROM documents WHERE (MONTH(date) = $psl_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year)";
     $psl_result1=$conn->query($psl_year1);
	 $psl_res1=$psl_result1->fetch_assoc();
	 $psl_data1=array("label"=>$arr_month[$j][0],"value"=>$psl_res1["total_monthly_psl"]);
     array_push($psl_array1,$psl_data1);

	 $psl_year2 = "SELECT COUNT(*) as total_monthly_psl1 FROM documents WHERE (MONTH(date) = $psl_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year2)";
     $psl_result2=$conn->query($psl_year2);
	 $psl_res2=$psl_result2->fetch_assoc();
	 $psl_data2=array("label"=>$arr_month[$j][0],"value"=>$psl_res2["total_monthly_psl1"]);
     array_push($psl_array2,$psl_data2);

	 $psl_year3 = "SELECT COUNT(*) as total_monthly_psl2 FROM documents WHERE (MONTH(date) = $psl_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year3)";
     $psl_result3=$conn->query($psl_year3);
	 $psl_res3=$psl_result3->fetch_assoc();
	 $psl_data3=array("label"=>$arr_month[$j][0],"value"=>$psl_res3["total_monthly_psl2"]);
     array_push($psl_array3,$psl_data3);

	 $psl_year4 = "SELECT COUNT(*) as total_monthly_psl3 FROM documents WHERE (MONTH(date) = $psl_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year4)";
     $psl_result4=$conn->query($psl_year4);
	 $psl_res4=$psl_result4->fetch_assoc();
	 $psl_data4=array("label"=>$arr_month[$j][0],"value"=>$psl_res4["total_monthly_psl3"]);
     array_push($psl_array4,$psl_data4);

	 $psl_year5 = "SELECT COUNT(*) as total_monthly_psl4 FROM documents WHERE (MONTH(date) = $psl_month) AND doc_type_id='89'  AND (YEAR(date) = $end_year)";
     $psl_result5=$conn->query($psl_year5);
	 $psl_res5=$psl_result5->fetch_assoc();
	 $psl_data5=array("label"=>$arr_month[$j][0],"value"=>$psl_res5["total_monthly_psl4"]);
     array_push($psl_array5,$psl_data5);
	$psl_month++;
	 }
  $encoded_data2=json_encode($namesArray2);
  $encoded_labels=json_encode($labelsArray);
  $psl_encode1=json_encode($psl_array1);
  $psl_encode2=json_encode($psl_array2);
  $psl_encode3=json_encode($psl_array3);
  $psl_encode4=json_encode($psl_array4);
  $psl_encode5=json_encode($psl_array5);

/////////////////////////////////////
 $arrChartConfig = array(
        "chart" => array(
            "caption" => "TOTAL LOAN APPLICATION STATISTICS",
            "subCaption" => "",
            "xAxisName" => "LOAN TYPES",
            "yAxisName" => "# OF LOAN APPLICATIONS",
            "numberSuffix" => "",
            "theme" => "umber"
        )
    );

    // An array of hash objects which stores data
    $arrChartData = array(
["BUSINESS LOANS", "$total_business_loans"],
["PRE SCORED LOANS", "$total_prescored_loans"],
["SECURED LOANS", "$total_unsecured_loans"]
);
$arrLabelValueData = array();

    // Pushing labels and values
    for($i = 0; $i < count($arrChartData); $i++) {
        array_push($arrLabelValueData, array(
            "label" => $arrChartData[$i][0], "value" => $arrChartData[$i][1]
        ));
    }
    $arrChartConfig["data"] = $arrLabelValueData;

    // JSON Encode the data to retrieve the string containing the JSON representation of the data in the array.
    $jsonEncodedData = json_encode($arrChartConfig);

    // chart object
    $Chart = new FusionCharts("pie3d", "MyFirstChart" , "300", "430", "chart-container", "json", $jsonEncodedData);

    // Render the chart
    $Chart->render();


include("fusioncharts/fusioncharts.php");
$columnChart = new FusionCharts("mscolumn3d", "ex1", "100%", 400, "chart-2", "json", '{
  "chart": {
    "caption": "LOAN APPLICATIONS FOR LAST FIVE YEARS",
    "yaxisname": "# OF LOAN APPLICATIONS",
    "subcaption": "'.$year1.'-'.$year2.'",
    "showhovereffect": "1",
    "numbersuffix": "",
    "drawcrossline": "1",
    "plottooltext": "<b>$dataValue</b> of $seriesName",
    "theme": "fusion"
  },
  "categories": [
    {
      "category":'.$encoded_labels.'
    }
  ],
  "dataset": [
    {
      "seriesname": "BUSINESS LOANS",
      "data":'.$encoded_data.'
    },
    {
      "seriesname": "PRE SCORED LOANS",
      "data":'.$encoded_data2.'
    },
    {
      "seriesname": "SECURED LOANS",
      "data":'.$encoded_data1.'
    }
  ],
  linkeddata: [
      {
        id: "business_loans_2019",
        linkedchart: {
          chart: {
            caption: "Apple Juice - Quarterly Sales",
            subcaption: "Last year",
            xaxisname: "Quarter",
            yaxisname: "Amount (In USD)",
            numberprefix: "$",
            theme: "fusion",
            rotateValues: "0"
          },
          data: [
            {
              label: "Q1",
              value: "157000"
            },
            {
              label: "Q2",
              value: "172000"
            },
            {
              label: "Q3",
              value: "206000"
            },
            {
              label: "Q4",
              value: "275000",
              rotateValues: "0"
            }
          ]
        }
      },
      {
        id: "cranberry",
        linkedchart: {
          chart: {
            caption: "Cranberry Juice - Quarterly Sales",
            subcaption: "Last year",
            xaxisname: "Quarter",
            yaxisname: "Amount (In USD)",
            numberprefix: "$",
            theme: "fusion",
            rotateValues: "0"
          },
          data: [
            {
              label: "Q1",
              value: "102000"
            },
            {
              label: "Q2",
              value: "142000"
            },
            {
              label: "Q3",
              value: "187000"
            },
            {
              label: "Q4",
              value: "189000"
            }
          ]
        }
      },
      {
        id: "grapes",
        linkedchart: {
          chart: {
            caption: "Grape Juice - Quarterly Sales",
            subcaption: "Last year",
            xaxisname: "Quarter",
            yaxisname: "Amount (In USD)",
            numberprefix: "$",
            theme: "fusion",
            rotateValues: "0"
          },
          data: [
            {
              label: "Q1",
              value: "45000"
            },
            {
              label: "Q2",
              value: "72000"
            },
            {
              label: "Q3",
              value: "95000"
            },
            {
              label: "Q4",
              value: "108000"
            }
          ]
	  }
	  }
}');

$columnChart->render();
?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
<div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- Sidebar -->
<?php include('sidebar.php');?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


            <!-- Nav Item - Alerts
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
               Dropdown - Alerts
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

           Nav Item - Messages
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                 Counter - Messages
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              Dropdown - Messages
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>-->
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome, <?php echo $fname; ?></span>

              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <!--<a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>-->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

          </div>

          <!-- Content Row -->
          <div class="row">
           <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">TOTAL LOAN APPLICATIONS</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?=$form_data['total']?></div>
                        </div>
                        <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 100%" aria-valuenow="<?=$form_data['total']?>" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">REJECTED LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data2['total_rejected_loans']?></div>
                     <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-danger" role="progressbar" style="width:<?=$percentage_rejected?>%" aria-valuenow="<?=$percentage_rejected?>" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
						</div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">PENDING LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data3['total_pending_loans']?></div>

                         <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: <?=$percentage_pending?>%" aria-valuenow="<?=$percentage_pending?>" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
						 </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">APPROVED LOAN APPLICATIONS</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$form_data1['total_approved_loans']?></div>

                       <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: <?=$percentage_approved?>%" aria-valuenow="<?=$form_data1['total_approved_loans']?>" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
						</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">LOAN APPLICATION SINCE 2019</h6>

                </div>
                <!-- Card Body -->
                <div class="card-body">


                <center>
        <div id="chart-container1"></div>
    </center>

                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">LOAN APPLICATIONS PIE CHART</h6>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                 <div id="chart-container"></div>
                </div>
              </div>
            </div>
          </div>
        <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"></h6>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                <center>
                <div id="chart-1"></div>
                 </center>

                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">DIFFERENT LOAN APPLICATIONS APPLIED BY CLIENTS.</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold">BUSINESS LOANS <span class="float-right"><?=$percentage_business_loans?>%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-success" role="progressbar" style="width: <?=$percentage_business_loans?>%" aria-valuenow="<?=$percentage_business_loans?>" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">PRE-SCORED LOANS<span class="float-right"><?=$percentage_prescored_loans?>%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: <?=$percentage_prescored_loans?>%" aria-valuenow="<?=$percentage_prescored_loans?>" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">SECURED LOANS <span class="float-right"><?=$percentage_unsecured_loans?>%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-info" role="progressbar" style="width: <?=$percentage_unsecured_loans?>%" aria-valuenow="<?=$percentage_unsecured_loans?>" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; finca@cinnamonerp.com</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="http://localhost/fincatest/include/dashboard_do_signout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <?php include('footer.php');?>
