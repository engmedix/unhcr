<!-- Bootstrap core JavaScript-->
  <script src="public/vendor/jquery/jquery.min.js"></script>
  <script src="public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="public/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="public/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
 <!-- <script src="public/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
 <!-- <script src="public/js/demo/chart-area-demo.js"></script>
 <!-- <script src="public/js/demo/chart-pie-demo.js"></script>-->
  
<script type="text/javascript">
$('.daterange').daterangepicker();
$('#date-range-picker').daterangepicker({
'applyClass' : 'btn-sm btn-success',
'cancelClass' : 'btn-sm btn-default',
locale: {
applyLabel: 'Apply',
cancelLabel: 'Cancel',
}
});
$(document).on('click','.search-btn', function(){

//search functionality

$('#search-results').html('');

var frm = $(this).parents('form').attr('id');
var values = $('#'+frm).serialize();
var frmAction = $('#'+frm).attr('action');
$.ajax(
{
  type: "POST",
  url: frmAction,
  data: "myData=" + values,
  cache: false,
  success: function(message)
  {
    completeHandler(frm,message);
  },
  error: function(message)
  {
    alert('failed'+message);
  }
});

//end of saving function

});

function completeHandler(frm,message){
$('#search-results').html(message);
$('html').animate({scrollTop:$('#search-results').offset().top},600,'swing')

}
function export_to(tableId){
var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
var textRange; var j=0;
tab = document.getElementById(tableId); // id of table

for(j = 0 ; j < tab.rows.length ; j++)
{
tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
//tab_text=tab_text+"</tr>";
}

tab_text=tab_text+"</table>";
tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

var ua = window.navigator.userAgent;
var msie = ua.indexOf("MSIE ");

if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
{
txtArea1.document.open("txt/html","replace");
txtArea1.document.write(tab_text);
txtArea1.document.close();
txtArea1.focus();
sa=txtArea1.document.execCommand("SaveAs",true,"save report.xls");
}
else                 //other browser not tested on IE 11
sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

return (sa);
}
</script>

</script>
<script>

</script>
<script>
	//++;
//++;
//++;
 FusionCharts.ready(function() {
  var satisfactionChart = new FusionCharts({
    type: 'column3d',
    renderAt: 'chart-container1',
    width: '600',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "LOAN APPLICATION TYPES",
        "subcaption": "SINCE 2019",
        "xaxisname": "LOAN APPLICATION TYPES",
        "yaxisname": "",
        "numberprefix": "",
        "theme": "fusion",
        "rotateValues": "0"
      },
      "data": [{
        "label": "BUSINESS LOANS",
        "value": "<?= $total_business_loans?>",
        "link": "newchart-xml-business_loans"
      }, {
        "label": "PRE SCORED LOANS",
        "value": "<?=$total_prescored_loans?>",
        "link": "newchart-xml-prescored_loans"
      }, {
        "label": "SECURED LOANS",
        "value": "<?=$total_unsecured_loans?>",
        "link": "newchart-xml-unsecured_loans"
      }],
      "linkeddata": [{
        "id": "business_loans",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "SINCE 2019",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "APPROVED",
            "value": "<?=$frm_data['total_closed_business_loans']?>"
          }, {
            "label": "PENDING",
            "value": "<?=$frm_data2['total_pending_business_loans']?>"
          }, {
            "label": "REJECTED",
            "value": "<?=$frm_data1['total_rejected_business_loans']?>"
          }]
        }
      }, {
        "id": "prescored_loans",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "SINCE 2019",
            "numberprefix": "",
            "theme": "fusion",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "APPROVED",
            "value": "<?=$frm_data_approved['total_closed_prescored_loans']?>"
          }, {
            "label": "PENDING",
            "value": "<?=$frm_data_pending['total_pending_prescored_loans']?>"
          }, {
            "label": "REJECTED",
            "value": "<?=$frm_data_rejected['total_rejected_prescored_loans']?>"
          }]
        }
      }, {
        "id": "unsecured_loans",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "SINCE 2019",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$label, $dataValue,  $percentValue"
          },
          "data": [{
            "label": "APPROVED",
            "value": "<?=$frm_data_approved1['total_closed_unsecured_loans']?>"
          }, {
            "label": "PENDING",
            "value": "<?=$frm_data_pending1['total_pending_unsecured_loans']?>"
          }, {
            "label": "REJECTED",
            "value": "<?=$frm_data_rejected1['total_rejected_unsecured_loans']?>"
          }]
        }
      }]
    },
    "events": {
      'beforeRender': function(evt, args) {
        if (!document.getElementById('eventmessages')) {
          // Create container div for radio controls
          var wrapper = document.createElement('div');
          var label = document.createElement('label');
          label.innerText = "Click on the data plot for more details";
          var controllers = document.createElement('div');
          controllers.setAttribute('id', 'eventmessages');
          controllers.innerHTML = "";

          wrapper.appendChild(label);
          wrapper.appendChild(controllers);

          // args.container.parentNode.insertBefore(controllers, args.container.nextSibling);

          args.container.parentNode.insertBefore(wrapper, args.container.nextSibling);

          // set css style for controllers div
         // controllers.style.cssText = 'position:inherit; width:400px; height: 100px; border: 1px solid #cccccc; margin-top:10px;padding-left:5px; overflow: scroll;';
        }
        evt.sender.configureLink({
          type: "pie3d",
          overlayButton: {
            message: 'close',
            fontColor: '880000',
            bgColor: 'FFEEEE',
            borderColor: '660000'
          }
        }, 0);

      },
    }
  });

  satisfactionChart.render();
  
  
   var myChart = new FusionCharts({
    type: 'mscolumn3d',
    renderAt: 'chart-1',
    width: '600',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      "chart": {
		"caption": "LOAN APPLICATIONS FOR LAST FIVE YEARS",
		"yaxisname": "# OF LOAN APPLICATIONS",
		"subcaption": "<?=($end_year - 4)."-".$end_year?>",
		"showhovereffect": "1",
		"numbersuffix": "",
		"drawcrossline": "1",
		"plottooltext": "<b>$dataValue </b> of $seriesName",
		"theme": "fusion"
	  },
      "categories": [
      {
        "category":<?=$encoded_labels?>
      }
     ],
     "dataset": [
    {
      "seriesname": "BUSINESS LOANS",
      "data":<?=$encoded_data?> 
    },
    {
      "seriesname": "PRE SCORED LOANS",
      "data":<?=$encoded_data2?>
    },
    {
      "seriesname": "SECURED LOANS",
      "data":<?=$encoded_data1?>
    }
  ],
      "linkeddata": [{
        "id": "business_loans_<?=$end_year - 4?>",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "<?=$end_year - 4?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$bl_encode1?>
        }
      }, {
        "id": "business_loans_<?=$end_year - 3?>",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "<?=$end_year - 3?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$bl_encode2?>
        }
      }, {
        "id": "business_loans_<?=$end_year - 2?>",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "<?=$end_year - 2?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$bl_encode3?>
        }
      },
	  {
        "id": "business_loans_<?=$end_year - 1?>",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "<?=$end_year - 1?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$bl_encode4?>
        }
      },
	  {
        "id": "business_loans_<?=$end_year?>",
        "linkedchart": {
          "chart": {
            "caption": "BUSINESS LOANS",
            "subcaption": "2019",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$bl_encode5?>
        }
      },
	  {
        "id": "prescored_loans_<?=$end_year - 4?>",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "<?=$end_year - 4?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$psl_encode1?>
        }
      }, {
        "id": "prescored_loans_<?=$end_year - 3?>",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "<?=$end_year - 3?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$psl_encode2?>
        }
      }, {
        "id": "prescored_loans_<?=$end_year - 2?>",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "<?=$end_year - 2?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$psl_encode3?>
        }
      },
	  {
        "id": "prescored_loans_<?=$end_year - 1?>",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "<?=$end_year - 1?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$psl_encode4?>
        }
      },
	  {
        "id": "prescored_loans_<?=$end_year?>",
        "linkedchart": {
          "chart": {
            "caption": "PRE SCORED LOANS",
            "subcaption": "<?=$end_year?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$psl_encode5?>
        }
      },
	    {
        "id": "secured_loans_<?=$end_year - 4?>",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "<?=$end_year - 4?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$sel_encode1?>
        }
      }, {
        "id": "secured_loans_<?=$end_year - 3?>",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "<?=$end_year - 3?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$sel_encode2?>
        }
      }, {
        "id": "secured_loans_<?=$end_year - 2?>",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "<?=$end_year - 2?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$sel_encode3?>
        }
      },
	  {
        "id": "secured_loans_<?=$end_year - 1?>",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "<?=$end_year - 1?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$sel_encode4?>
        }
      },
	  {
        "id": "secured_loans_<?=$end_year?>",
        "linkedchart": {
          "chart": {
            "caption": "SECURED LOANS",
            "subcaption": "<?=$end_year?>",
            "numberprefix": "",
            "theme": "fusion",
            "rotateValues": "0",
            "plottooltext": "$dataValue"
          },
          "data":<?=$sel_encode5?>
        }
      }
	  
]
    },
    "events": {
      'beforeRender': function(evt, args) {
        if (!document.getElementById('eventmessages')) {
          // Create container div for radio controls
          var wrapper = document.createElement('div');
          var label = document.createElement('label');
          label.innerText = "Click on the data plot for more details";
          var controllers = document.createElement('div');
          controllers.setAttribute('id', 'eventmessages');
          controllers.innerHTML = "";

          wrapper.appendChild(label);
          wrapper.appendChild(controllers);

          // args.container.parentNode.insertBefore(controllers, args.container.nextSibling);

          args.container.parentNode.insertBefore(wrapper, args.container.nextSibling);

          // set css style for controllers div
         // controllers.style.cssText = 'position:inherit; width:400px; height: 100px; border: 1px solid #cccccc; margin-top:10px;padding-left:5px; overflow: scroll;';
        }
        evt.sender.configureLink({
          type: "column3d",
          overlayButton: {
            message: 'close',
            fontColor: '880000',
            bgColor: 'FFEEEE',
            borderColor: '660000'
          }
        }, 0);

      },
    }
  });

  myChart.render(); 
  
  

});

</script>
</body>

</html>
