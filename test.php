<?php
// test json

$json1='[{"label":"Loan application form","help":"dated, fully filled, stating loan period, loan amount, ID numbers, Computer number, authorized signatory","name":"attachment1"},
{"label":"Certificate of Registration","help":"Current for previous 3 months, Stamped and Signed by the bank","name":"attachment2"},
{"label":"Guarantor Questionnaire","help":"Employment details, confirmation, one latest transfer letter in case employee was transferred - Not required for replacement loans.","name":"attachment3"},
{"label":"Call Memo","help":"In the event of alterations or Crossing","name":"attachment4"},
{"label":"Licenses","help":"Client signature in Part A, B, C and Branch Manager, Agent Signatures","name":"attachment5"},
{"label":"Lease agreement/Utility Bill","help":"Employer ID and National ID","name":"attachment6"},
{"label":"Collateral Evaluation form","help":"Properly calculated","name":"attachment7"},
{"label":"Tax Clearance Certificate","help":"","name":"attachment8"},
{"label":"Collateral Pictures and signed collateral surrender form","help":"","name":"attachment9"},
{"label":"Shareholders IDs","help":"Should be within the last three months","name":"attachment10"},
{"label":"Loan and security agreements with other financial institutions","help":"Optional, however for refinancing hand written letter of cancellation from client is required","name":"attachment11"},
{"label":"Board resolution","help":"Provide Buy Off letter and recent quotation from the institution being consolidated","name":"attachment12"},
{"label":"Credit Reference Bureau Report","help":"","name":"attachment13"},
{"label":"Comprehensive insurance","help":"1 for Application form certified at the back, 2 for Standing Order Stanbic Bank standing order 1 photograph sales staff to sign at the back of the photo to confirm identity","name":"attachment14"},
{"label":"Patents and companies registration agency search print out","help":"Signed by the client, witness information, sales agent declaration sign","name":"attachment15"},
{"label":"Ministry of Lands/Council search","help":"2 Copies","name":"attachment16"},
{"label":"Letter of consent authorizing use of collateral for third party  security","help":"","name":"attachment17"},
{"label":"Proof of residence/residential status for foreign business owners","help":"","name":"attachment18"},
{"label":"PACRA Collateral Verification","help":"","name":"attachment19"},
{"label":"Loan motivation","help":"","name":"attachment20"}]';
var_dump(json_decode($json1));
?>