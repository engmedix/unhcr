jQuery(function($) {
		
	$("body").on("click","a.delete-item", function(e) {
		e.preventDefault();
		
		var $this = $(this);
		$.confirm({
			title: 'Delete Item',
			content: 'Are you sure you want to delete this item?', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var $id = $this.attr('data-id');
						var $tbl = $this.attr('data-tbl');
						var tab_div = 'li'+$id;
						var content_div = 'home'+$id;
						var dataString = 'table='+$tbl+'&item_id='+$id;
						
						$.ajax({
							type: "POST",
							url: "include/ajax_delete.php",
							data: dataString,
							success: function(data) {
							  if(data=="DELETED SUCCESSFULLY"){
								$('#'+tab_div).remove();
							    $('#'+content_div).remove();
								$('#myTab3 li').first().addClass("active");
								$('.tab-content div.tab-pane').first().addClass("active");
							  } else {
								$.alert("<b>ERROR!</b>Delete failed!!!");  
							  }
							},
							error: function(err){
								$.alert("Errrrrrrrrrr");
							}
						  }); 
					}
				},
				cancel: function () {
					$.alert('Delete <strong>canceled</strong>');
				}
			}

		});
		
	});
	
	$("body").on("click","a.save-item", function(e) {
		e.preventDefault();
		var $this = $(this);
		$.confirm({
			title: 'Save Item',
			content: 'Confirm SAVE? NOTE: No UNDO function for this action.', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var doc_id = $this.attr("doc-id");
						var formId = "form"+doc_id;
						$('#'+formId).submit();
						//save doc info
						/*
						var dataString = $('#'+formId).serialize();
						
						$.ajax({
							type: "POST",
							url: "include/do_edit.php",
							data: dataString,
							success: function(data) {
							  alert(data);
							},
							error: function(err){
								alert(JSON.stringify(err));  
							}
						  }); //*/
						//save template info
						
						//save workflow settings
												
						
					}
				},
				cancel: function () {
					$.alert('SAVE <strong>canceled</strong>');
				}
			}

		});
		
	});
	
	$('body').on('click','.remove-row',function(){
		$(this).parents('.table-row').remove();
	});

	 
	$('.timepicker').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	 $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		
	
		

			
});