var _folder_ = '/finca';
var _base_path_ = 'http://'+document.location.host+_folder_;

var kebela = {

	init:function(div,url,callback){ //function to fetch external page containing the panel DIVs
		ajaxFetcher.init({
			contentsource: [div, url, callback]
		});
	},
}

var ajaxFetcher={

///////Stop configuring beyond here///////////////////////////
	
detectwebkit: navigator.userAgent.toLowerCase().indexOf("applewebkit")!=-1, //detect WebKit browsers (Safari, Chrome etc)
detectie6: document.all && !window.XMLHttpRequest,

getandswapcontent:function($, setting){ //function to fetch external page containing the panel DIVs

	if(currentPageId != '' && setting.contentsource[0] == 'magicLoad')
		var $menucontainer=$('#'+currentPageId+' #'+setting.contentsource[0]) //reference empty div on page that will hold menu
	else
		var $menucontainer=$('#'+setting.contentsource[0]) //reference empty div on page that will hold menu
		
//alert(currentPageId)		
	if(setting.contentsource[0] == 'magicLoad'){
		loader = "<div class='loader-container'><div id='circular3dG'>	<div id='circular3d_1G' class='circular3dG'></div>	<div id='circular3d_2G' class='circular3dG'></div>	<div id='circular3d_3G' class='circular3dG'></div>	<div id='circular3d_4G' class='circular3dG'></div>	<div id='circular3d_5G' class='circular3dG'></div>	<div id='circular3d_6G' class='circular3dG'></div>	<div id='circular3d_7G' class='circular3dG'></div>	<div id='circular3d_8G' class='circular3dG'></div></div></div>";
	}
	else{
		loader = "<div class='loading2'></div>";
	}
	var callback = setting.contentsource[2]
//alert($menucontainer.html())
	if(!$menucontainer.hasClass('messages')) $menucontainer.html(loader)
	
	$.ajax({
		url: setting.contentsource[1], //path to external menu file
		async: true,
		error:function(ajaxrequest){
			$menucontainer.html('Error fetching content. Server Response: '+ajaxrequest.responseText)
		},
		success:function(content){
			$menucontainer.html(content);
			if(typeof callback === 'function') callback()
				
		}
		
	})
},
instantAjaxLoader:function($, setting){ //function to fetch external page containing the panel DIVs
	var $menucontainer=$('#'+setting.contentsource[0]) //reference empty div on page that will hold menu
	$.ajax({
		url: setting.contentsource[1], //path to external menu file
		async: true,
		error:function(ajaxrequest){
			$menucontainer.html('Error fetching content. Server Response: '+ajaxrequest.responseText)
		},
		success:function(content){
			$menucontainer.html(content);
		}
		
	})
},
init:function(setting){
	jQuery(document).ready(function($){ //ajax menu?
		if (typeof setting.contentsource=="object"){ //if external ajax menu
			ajaxFetcher.getandswapcontent($, setting)
		}
	})
},
initInstant:function(setting){
	jQuery(document).ready(function($){ //ajax menu?
		if (typeof setting.contentsource=="object"){ //if external ajax menu
			ajaxFetcher.instantAjaxLoader($, setting)
		}
	})
}

}

