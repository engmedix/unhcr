<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>DocTracker Workflow</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		#searchFrm input, #searchFrm select {width:100%;}
		.profile-info-value,.profile-user-info-striped .profile-info-value{padding:0;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->

		<script type="text/javascript">

		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function addCommas(nStr) {

		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }

		  return x1 + x2;

		}

		function newDepositBal(){
			var old = document.getElementById('deposit_stat_bal').value*1;
			var nw = document.getElementById('deposit_amount').value.replace(/,/g, '')*1;
			return old+nw;
		}

		function newWithdrawBal(){
			var old = document.getElementById('withdraw_stat_bal').value*1;
			var nw = document.getElementById('withdraw_amount').value.replace(/,/g, '')*1;
			var newbal = old-nw;
			if(newbal<0){
				alert("Insurficient Funds");
				document.getElementById('withdraw_amount').value = "";
				document.getElementById('show_new_bal').value = old;
				return old;
			} else{
			return old-nw;
			}


		}

		function updateBalances1(nStr){
			//first update the balance
			var newBalance = newDepositBal();
		  document.getElementById('deposit_new_bal').value = newBalance;
		  document.getElementById('show_new_bal').value = addCommas(newBalance);
		  document.getElementById('deposit_amount').value = addCommas(nStr);

		}

		function updateBalances2(nStr){
			//first update the balance
			document.getElementById('withdraw_amount').value = addCommas(nStr);
			var newBalance = newWithdrawBal();
		  document.getElementById('withdraw_new_bal').value = newBalance;
		  document.getElementById('show_new_bal').value = addCommas(newBalance);


		}



		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12 col-sm-6 widget-container-col">
										<!-- #section:custom/widget-box -->
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="widget-title">Search documents</h5>

												<!-- #section:custom/widget-box.toolbar -->
												<div class="widget-toolbar">
													<div class="widget-menu">
														<a href="#" data-action="settings" data-toggle="dropdown">
															<i class="ace-icon fa fa-bars"></i>
														</a>

														<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
															<li>
																<a data-toggle="tab" href="#dropdown1">Option#1</a>
															</li>

															<li>
																<a data-toggle="tab" href="#dropdown2">Option#2</a>
															</li>
														</ul>
													</div>

													<a href="#" data-action="fullscreen" class="orange2">
														<i class="ace-icon fa fa-expand"></i>
													</a>

													<a href="#" data-action="reload">
														<i class="ace-icon fa fa-refresh"></i>
													</a>

													<a href="#" data-action="collapse">
														<i class="ace-icon fa fa-chevron-up"></i>
													</a>

													<a href="#" data-action="close">
														<i class="ace-icon fa fa-times"></i>
													</a>
												</div>

												<!-- /section:custom/widget-box.toolbar -->
											</div>

											<div class="widget-body">
												<div class="widget-main">

													<form name="searchFrm" action="q.php" method="post" id="searchFrm">
													<input type="hidden" name="extra" value="dhfkjhsdfjhsdjfhdskj"/>
                                                    <div class="profile-user-info profile-user-info-striped">
                                                        <div class="profile-info-row">
                                                            <div class="profile-info-name"> Date submitted </div>

                                                            <div class="profile-info-value">
                                                                <div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>

																	<input class="form-control" type="text" name="duration" id="date-range-picker" />
																</div>
                                                            </div>
                                                        </div>

                                                        <div class="profile-info-row">
                                                            <div class="profile-info-name"> Document type </div>

                                                            <div class="profile-info-value">
                                                                <span class="editable" id="username">
																	<select name="doc_type">
																	<option value="">All</option>
																	<?php	$sql14 = "SELECT * FROM document_types ORDER BY doc_classification, doc_type";
																			$result14 = $conn->query($sql14);
																			while($row14 = $result14->fetch_assoc()) { ?>
																	<option value="<?php echo $row14['doc_type']; ?>"><?php echo $row14['doc_type']; ?></option>
																	<?php } ?>
																	</select>
																</span>
                                                            </div>
                                                        </div>
                                                        <div class="profile-info-row">
                                                            <div class="profile-info-name"> Reference number </div>

                                                            <div class="profile-info-value">
                                                                <span class="editable" id="username"><input name="internal_ref_number" type="text"/></span>
                                                            </div>
                                                        </div>
														<div class="profile-info-row">
                                                            <div class="profile-info-name"> Staff (Passed Through) </div>

                                                            <div class="profile-info-value">
                                                                <select  name="staff"  class="chosen-select" data-placeholder="select staff...">
																	<option value="">All</option>
																<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
																	  $form_result = $conn->query($form_query);
																	  while($ro_data = $form_result->fetch_assoc()) {   ?>
																	<option value="<?php echo $ro_data['id']; ?>">
																		<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																	</option>
																	<?php } ?>
																</select>
                                                            </div>
                                                        </div>

														<div class="profile-info-row">
															<div class="profile-info-name"> Originating Branch </div>

															<div class="profile-info-value">
																<select  name="branch"  class="chosen-select" data-placeholder="select staff...">
																	<option value="">All</option>
																<?php $form_query = "SELECT * FROM dutystations ORDER BY duty_station";
																	  $form_result = $conn->query($form_query);
																	  while($ro_data = $form_result->fetch_assoc()) {   ?>
																	<option value="<?php echo $ro_data['duty_station']; ?>">
																		<?php echo $ro_data['duty_station']; ?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>


                                                        <div class="profile-info-row">
                                                        	<div class="profile-info-name">&nbsp;</div>
                                                            <div class="profile-info-value">
                                                                <a class="btn btn-sm btn-success search-btn"> Search
                                                                    <i class="ace-icon fa fa-search bigger-110"></i>
                                                                </a>
                                                            </div>
                                                        </div>




                                                    </div>

                                                    </form>

												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box -->
									</div>

									<div class="col-xs-12 col-sm-6 widget-container-col">
										<?php if(isset($_GET['track'])){ include("include/do_insert.php"); } ?>
										<div class="widget-box ">
											<!-- #section:custom/widget-box.options -->
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">
													<i class="ace-icon fa fa-table"></i>
													Document Trackers
												</h5>
												<div class="widget-toolbar">
													<div class="widget-menu">
														<a href="#" data-action="settings" data-toggle="dropdown">
															<i class="ace-icon fa fa-search"></i>
														</a>


													</div>

													<a href="#" data-action="fullscreen" class="orange2">
														<i class="ace-icon fa fa-expand"></i>
													</a>

													<a href="#" data-action="reload">
														<i class="ace-icon fa fa-refresh"></i>
													</a>

													<a href="#" data-action="collapse">
														<i class="ace-icon fa fa-chevron-up"></i>
													</a>


												</div>

											</div>

											<!-- /section:custom/widget-box.options -->
											<div class="widget-body">
												<div class="widget-main no-padding">
													<table class="table table-striped table-bordered table-hover">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-file"></i>
																	Document Tracker
																</th>

																<th>
																	<i class="ace-icon fa fa-user"></i>
																	Sent To
																</th>
																<th class="hidden-480">Status</th>
															</tr>
														</thead>

														<tbody>
														<?php $form_query = "SELECT * FROM trackers WHERE staff_id='$myid'";
														  $form_result = $conn->query($form_query);
														  while($form_data = $form_result->fetch_assoc()) { $doc = $form_data['doc_id'];  ?>
															<tr>
																<td class=""><?php echo $form_data['title']; ?></td>

																<?php
																	$form_t = $conn->query("SELECT * FROM documents WHERE id='$doc'");
																	while($data = $form_t->fetch_assoc()) {  ?>
																<td>
																	<a href="timeline.php?ac=<?php echo $doc;   ?>">
																	<?php echo getStaffName($data['current_holder']);   ?>
																	</a>
																</td>

																<td class="hidden-480">
																	<span class="label label-warning"><?php echo $data['file_status'];   ?></span>
																	<a href="timeline.php?ac=<?php echo $doc;   ?>">
																	<i class="fa fa-exchange"></i>
																	</a>
																</td>
																<?php }  ?>
															</tr>
														  <?php } ?>
														  <!--
															<tr>
																<td class="">My Consultant</td>

																<td>
																	<a href="#">Finance (Annet)</a>
																</td>

																<td class="hidden-480">
																	<span class="label label-success arrowed-in arrowed-in-right">Approved</span>
																</td>
															</tr>

															<tr>
																<td class="">Laptop requisition</td>

																<td>
																	<a href="#">Procurement (Diana)</a>
																</td>

																<td class="hidden-480">
																	<span class="label label-info">Received</span>
																</td>
															</tr>
															-->

														</tbody>
													</table>
												</div>
											</div>
											<div>
												<form action="?search" method="post">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-check"></i>
													</span>

													<input type="text" class="form-control search-query" placeholder="Find documents to track" />
													<span class="input-group-btn">
														<button type="button" class="btn btn-purple btn-sm"  onclick="this.form.submit()">
															<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
															Search
														</button>
													</span>
												</div>
												</form>
											</div>

											<?php if(isset($_GET['search'])){ ?>
											<div>
											<h4 class="blue">
												<i class="green ace-icon fa fa-search bigger-100"></i>
												Search Results
											</h4>

											<div class="space-8"></div>

											<div id="faq-list-2" class="panel-group accordion-style1 accordion-style2">
												<?php $form_query = "SELECT * FROM documents WHERE file_status<>'REJECTED' AND file_status<>'APPROVED'";
												  $form_result = $conn->query($form_query);
												  while($form_data = $form_result->fetch_assoc()) {   ?>
												<div class="panel panel-default">
													<div class="panel-heading">
														<a href="#faq-2-<?php echo $form_data['id']; ?>" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">
															<i class="ace-icon fa fa-chevron-right smaller-80" data-icon-hide="ace-icon fa fa-chevron-down align-top" data-icon-show="ace-icon fa fa-chevron-right"></i>&nbsp;
															<?php echo $form_data['document_type']; ?>
															<?php if($form_data['doc_ref_number']){ echo " - REF:". $form_data['doc_ref_number']; } ?>
															<?php if($form_data['document_type']=='PURCHASE REQUISITION'){ ?>
															<?php echo " - <i>Submited by: ". getStaffName($form_data['from_id']); ?>
															<?php echo " (".getStaffUnit($form_data['from_id']).")</i>"; ?>
															<?php } else { ?>
															<?php echo " - <i>Submited by: ". $form_data['submitted_by']; ?>
															<?php echo " (".$form_data['organisation'].")</i>"; ?>
															<?php } ?>
														</a>
													</div>

													<div class="panel-collapse collapse" id="faq-2-<?php echo $form_data['id']; ?>">
														<div class="panel-body">
															<b>Track this document</b><br />
															<table class="table table-striped table-bordered">

																<tbody>
																	<tr>
																		<form action="?track" method="post">
																		<td class="">
																		<input type="text" class="form-control" name="data[title]" placeholder="Tracker Name" />
																		<input type="hidden"  name="data[staff_id]" value="<?php echo $myid; ?>" />
																		<input type="hidden"  name="data[doc_id]" value="<?php echo $form_data['id']; ?>" />
																		<input type="hidden"  name="table" value="trackers" />
																		<input type="hidden"  name="action" value="NEW TRACKER" />

																		</td>

																		<td class="">
																				<button type="button" class="btn btn-purple btn-sm"  onclick="this.form.submit()">
																					<span class="ace-icon fa fa-eye icon-on-right bigger-110"></span>
																					Start Tracking
																				</button>

																		</td>
																		</form>
																	</tr>

																</tbody>
															</table>
														</div>
													</div>
												</div>
												  <?php } ?>


											</div>


										</div>
											<?php } ?>
										</div>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>




							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->

                        <div id="search-results"></div>


					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>



		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });


			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			// picture upload
			$('#pic_upclose').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Scanned Copy',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,

					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});

			});

			$(document).on('click','.search-btn', function(){

				//search functionality

				$('#search-results').html('');

				var frm = $(this).parents('form').attr('id');
				var values = $('#'+frm).serialize();
				var frmAction = $('#'+frm).attr('action');
				$.ajax(
					{
						type: "POST",
						url: frmAction,
						data: "myData=" + values,
						cache: false,
						success: function(message)
						{
							completeHandler(frm,message);
						},
						error: function(message)
						{
							alert('failed'+message);
						}
					});

				//end of saving function

			});

			function completeHandler(frm,message){
				$('#search-results').html(message);
				$('html').animate({scrollTop:$('#search-results').offset().top},600,'swing')

			}

			$('#date-range-picker').daterangepicker({
				'applyClass' : 'btn-sm btn-success',
				'cancelClass' : 'btn-sm btn-default',
				locale: {
					applyLabel: 'Apply',
					cancelLabel: 'Cancel',
				}
			})
		</script>

				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
