<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>DocTracker Report</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		#searchFrm input, #searchFrm select {width:100%;}
		.profile-info-value,.profile-user-info-striped .profile-info-value{padding:0;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->

		<script type="text/javascript">

		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function addCommas(nStr) {

		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }

		  return x1 + x2;

		}

		function newDepositBal(){
			var old = document.getElementById('deposit_stat_bal').value*1;
			var nw = document.getElementById('deposit_amount').value.replace(/,/g, '')*1;
			return old+nw;
		}

		function newWithdrawBal(){
			var old = document.getElementById('withdraw_stat_bal').value*1;
			var nw = document.getElementById('withdraw_amount').value.replace(/,/g, '')*1;
			var newbal = old-nw;
			if(newbal<0){
				alert("Insurficient Funds");
				document.getElementById('withdraw_amount').value = "";
				document.getElementById('show_new_bal').value = old;
				return old;
			} else{
			return old-nw;
			}


		}




		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12 col-sm-6 widget-container-col">
										<!-- #section:custom/widget-box -->
										<div class="widget-box">
											<div class="widget-header">
												<h5 class="widget-title">DocTracker Report</h5>

												<!-- #section:custom/widget-box.toolbar -->
												<div class="widget-toolbar">
													<div class="widget-menu">
														<a href="#" data-action="settings" data-toggle="dropdown">
															<i class="ace-icon fa fa-bars"></i>
														</a>

														<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
															<li>
																<a data-toggle="tab" href="#dropdown1">Option#1</a>
															</li>

															<li>
																<a data-toggle="tab" href="#dropdown2">Option#2</a>
															</li>
														</ul>
													</div>

													<a href="#" data-action="fullscreen" class="orange2">
														<i class="ace-icon fa fa-expand"></i>
													</a>

													<a href="#" data-action="reload">
														<i class="ace-icon fa fa-refresh"></i>
													</a>

													<a href="#" data-action="collapse">
														<i class="ace-icon fa fa-chevron-up"></i>
													</a>

													<a href="#" data-action="close">
														<i class="ace-icon fa fa-times"></i>
													</a>
												</div>

												<!-- /section:custom/widget-box.toolbar -->
											</div>

											<div class="widget-body">
												<div class="widget-main">

													<form name="searchFrm" action="q_rpt.php" method="post" id="searchFrm">
													<input type="hidden" name="extra" value="dhfkjhsdfjhsdjfhdskj"/>
                                                    <div class="profile-user-info profile-user-info-striped">

														<div class="profile-info-row">
                                                            <div class="profile-info-name"> Staff </div>

                                                            <div class="profile-info-value">
                                                                <select  name="staff"  class="chosen-select" data-placeholder="select staff...">
																	<option value="">  </option>
																<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
																	  $form_result = $conn->query($form_query);
																	  while($ro_data = $form_result->fetch_assoc()) {   ?>
																	<option value="<?php echo $ro_data['id']; ?>">
																		<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																	</option>
																	<?php } ?>
																</select>
                                                            </div>
                                                        </div>

														<div class="profile-info-row">
                                                            <div class="profile-info-name"> UNHCR Reference number </div>

                                                            <div class="profile-info-value">
                                                                <span class="editable" id="username"><input name="undp_ref_number" type="text"/></span>
                                                            </div>
                                                        </div>

														<div class="profile-info-row">
                                                            <div class="profile-info-name"> Document type </div>

                                                            <div class="profile-info-value">
                                                                <span class="editable" id="username">
																	<select name="doc_type">
																	<option></option>
																	<?php	$sql14 = "SELECT * FROM document_types ORDER BY doc_classification, doc_type";
																			$result14 = $conn->query($sql14);
																			while($row14 = $result14->fetch_assoc()) { ?>
																	<option value="<?php echo $row14['doc_type']; ?>"><?php echo $row14['doc_type']; ?></option>
																	<?php } ?>
																	</select>
																</span>
                                                            </div>
                                                        </div>

														<div class="profile-info-row">
                                                            <div class="profile-info-name"> Days </div>

                                                            <div class="profile-info-value">
                                                                <span class="editable" id="username"><input name="days" type="number"/></span>
                                                            </div>
                                                        </div>


														<div class="profile-info-row">
                                                            <div class="profile-info-name"> Date </div>

                                                            <div class="profile-info-value">
                                                                <div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>

																	<input class="form-control" type="text" name="duration" id="date-range-picker" />
																</div>
                                                            </div>
                                                        </div>




                                                        <div class="profile-info-row">
                                                        	<div class="profile-info-name">&nbsp;</div>
                                                            <div class="profile-info-value">
                                                                <a class="btn btn-sm btn-success search-btn"> Apply Filter
                                                                    <i class="ace-icon fa fa-search bigger-110"></i>
                                                                </a>
                                                            </div>
                                                        </div>




                                                    </div>

                                                    </form>

												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box -->
									</div>


								</div><!-- /.row -->

								<div class="space-24"></div>




							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->

                        <div id="search-results"></div>


					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>



		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });


			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			// picture upload
			$('#pic_upclose').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Scanned Copy',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,

					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});

			});

			$(document).on('click','.search-btn', function(){

				//search functionality

				$('#search-results').html('');

				var frm = $(this).parents('form').attr('id');
				var values = $('#'+frm).serialize();
				var frmAction = $('#'+frm).attr('action');
				$.ajax(
					{
						type: "POST",
						url: frmAction,
						data: "myData=" + values,
						cache: false,
						success: function(message)
						{
							completeHandler(frm,message);
						},
						error: function(message)
						{
							alert('failed'+message);
						}
					});

				//end of saving function

			});

			function completeHandler(frm,message){
				$('#search-results').html(message);
				$('html').animate({scrollTop:$('#search-results').offset().top},600,'swing')

			}

			$('#date-range-picker').daterangepicker({
				'applyClass' : 'btn-sm btn-success',
				'cancelClass' : 'btn-sm btn-default',
				locale: {
					applyLabel: 'Apply',
					cancelLabel: 'Cancel',
				}
			})


			function export_to(tableId){
	var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById(tableId); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"save report.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}
		</script>

				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
