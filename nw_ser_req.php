<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>New Service Request</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="assets/css/dropzone.css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		.subprocess_lists ul{list-style:none;}
		.subprocess_lists ul li{float:left; width:30%;}
		.subprocess_lists ul.header_list{font-weight:bold;}
		
		.input-sm{height:20px; line-height:0;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		
		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		
		function addCommas(nStr) {
		  		  
		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }
		  
		  return x1 + x2;
		  
		}
		
		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
<!-- Let me first do my stuff here -->
<?php 
	
	$service_options = '';
	$service_data = array();
	
	$rst_services = $conn->query("SELECT services.*,_sub.subs,units.department dept FROM services LEFT JOIN (SELECT GROUP_CONCAT(processName,':::',requirements,':::',delivery_time) subs, serviceId FROM subprocesses GROUP BY serviceId ) _sub USING(serviceId) LEFT JOIN units ON services.department = units.id");
	
	while($row_services = $rst_services->fetch_assoc()){
		$service_options .="<option value='{$row_services['serviceId']}'>{$row_services['serviceName']}</option>";
		
		$service_data[$row_services['serviceId']] =  array(
			'serviceName'=>$row_services['serviceName'],
			'requirements'=>$row_services['requirements'],
			'description'=>$row_services['service_desc'],
			'temp_type'=>$row_services['temp_type'],
			'template'=>$row_services['template'],
			'subprocesses'=>$row_services['subs'],
		);
	}
	
	echo '
	<script>
		var service_data = '.json_encode($service_data).';
	</script>
	';
?>
<!-- //kale am done -->
						
						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Send New service request</h3>

										<div class="">
											<!-- #section:plugins/fuelux.wizard -->
											<div id="">
												

												<hr style="margin:10px 0;" />
												
												<form class="form-horizontal" id="validation-form" method="get">

												<!-- #section:plugins/fuelux.wizard.container -->
												<div class="">
													<div class="step-pane active" data-step="1">

														<div class="form-group" style="margin-top:50px;">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">Select service</label>

															<div class="col-xs-12 col-sm-9">
																<div class="clearfix">
																	<select class="service-select col-sm-7" id="service" name="service">
																		<option value="">------------------</option>
																		<?php echo $service_options ?>
																	</select>
																</div>
															</div>
														</div>
														<div class="space-2"></div>
														
													</div>

													<div class="step-pane" data-step="2">
														<h3 class="blue lighter" style="margin-top:0;">Request for: <em class="__serviceName"></em></h3>
														<small class="__description"></small>
														<div class="col-xs-12">
															<div class="col-xs-6">
																<h4 class="blue lighter">Requirements</h4>
																<div class="__requirements"></div>
															</div>
															<div class="col-xs-6 subprocess_lists">
																<h4 class="blue lighter">Subprocesses</h4>
																<ul class="header_list"><li>Process</li><li>Requirements</li><li>Time</li></ul>
																<div class="__subprocesses"></div>
															</div>
															<div class="clearfix"></div>
															<hr style="margin:0;" />
															<div class="col-xs-12">
																<h4 class="blue lighter">Service Request details</h4>
																<div class="__template"></div>
															</div>
														</div>
														
														
													</div>

													<div class="step-pane" data-step="3">
														<div class="center">
															<h3 class="blue lighter">This is step 3</h3>
														</div>
													</div>

													<div class="step-pane" data-step="4">
														<div class="center">
															<h3 class="green">Congrats!</h3>
															Your product is ready to ship! Click finish to continue!
														</div>
													</div>
												</div>

												<!-- /section:plugins/fuelux.wizard.container -->
												
												</form>
												
											</div>

											<hr />
											<div class="wizard-actions">
												<!-- #section:plugins/fuelux.wizard.buttons -->
												<button class="btn btn-prev">
													<i class="ace-icon fa fa-arrow-left"></i>
													Prev
												</button>

												<button class="btn btn-success btn-next" data-last="Finish">
													Next
													<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
												</button>

												<!-- /section:plugins/fuelux.wizard.buttons -->
											</div>

											<!-- /section:plugins/fuelux.wizard -->
										</div><!-- /.widget-main -->
								

									</div>
								</div><!-- /.row -->
					
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/fuelux/fuelux.wizard.js"></script>
		<script src="assets/js/additional-methods.js"></script>
		<script src="assets/js/bootbox.js"></script>
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		<script src="assets/js/jquery.validate.js"></script>
		
		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			 
			 var $validation = true;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				.on('finished.fu.wizard', function(e) {
					bootbox.dialog({
						message: "Thank you! Your information was successfully saved!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
					//e.preventDefault();//this will prevent clicking and selecting steps
				});
			
			
				//jump to a step
				/**
				var wizard = $('#fuelux-wizard-container').data('fu.wizard')
				wizard.currentStep = 3;
				wizard.setState();
				*/
			
				//determine selected step
				//wizard.selectedItem().step
			
			 
			 $('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						email: {
							required: true,
							email:true
						},
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						name: {
							required: true
						},
						phone: {
							required: true,
							phone: 'required'
						},
						url: {
							required: true,
							url: true
						},
						comment: {
							required: true
						},
						state: {
							required: true
						},
						service: {
							required: true
						},
						description: {
							required: true
						},
						age: {
							required: true,
						},
						agree: {
							required: true,
						}
					},
			
					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						state: "Please choose state",
						subscription: "Please choose at least one option",
						gender: "Please choose gender",
						agree: "Please accept our policy"
					},
			
			
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
					},
					invalidHandler: function (form) {
					}
				});
			
				
				
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				
				/**
				$('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				
				$('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				*/
				
				
				$(document).one('ajaxloadstart.page', function(e) {
					//in ajax mode, remove remaining elements before leaving page
					$('[class*=select2]').remove();
				});
			 
			 
			 
			 if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						 else $('#form-field-select-4').removeClass('tag-input-style');
					});
				}
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 
			 $(document).on('change','.service-select',function(){
				 if($(this).val() != ''){
					 
					 //alert($(this).val())
					 
					 //alert(service_data[$(this).val()])
					 
					 var selected_service = service_data[$(this).val()]
					 
					 var reqs = selected_service['requirements'].split(',')
					 var reqs_str = '<ul><li>'+reqs.join('</li><li>')+'</li></ul>'
					 var subs_str = ''
					 
					 if(typeof selected_service.subprocesses !== 'undefined' && selected_service.subprocesses !== null){
						 var subs = selected_service['subprocesses'].split(',')
						 $.each(subs,function(index,item){
							 //alert(item)
							 itm = item.split(':::')
							 itm_str = '<ul><li>'+itm.join('</li><li>')+'</li></ul>'
							 
							 subs_str += itm_str
						 })
					 }
					 
					 var template_str = ''
					 var template_type = selected_service['temp_type']
					 var template = selected_service['template']
					 
					if(template != ''){
						if(template_type == 'Attachment'){
							template_str = 'Download template file ' + 
								'<a target="_blank" class="btn btn-success btn-white btn-sm" href="assets/img/templates/'+template+'"><span><i class="fa fa-download"></i></span> '+template+'</a>' + 
								'<br><br><div class="form-group">' +
									'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="doc_ref_number">Reference No:</label>' +

									'<div class="col-xs-12 col-sm-9">' +
										'<div class="clearfix">' +
											'<input name="data[doc_ref_number]" id="doc_ref_number" class="col-xs-12 col-sm-4" type="text">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="form-group">' +
									'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Description</label>' +

									'<div class="col-xs-12 col-sm-9">' +
										'<div class="clearfix">' +
											'<textarea class="input-xlarge" name="data[description]" id="description"></textarea>' +
										'</div>' +
									'</div>' +
								'</div>'
						}
						else if(template_type == 'HTML'){
							template_str = template.replace(/({{.+}})/g, function changeInput(str){
								x = str.replace(/{{|}}/g,"")
								return '<input class="input-sm" name="'+x+'" type="text" />'
							}); 
							
						}
						else if(template_type == 'Form elements'){
							var strArray = template.split(",")
							$.each(strArray,function(index,val){
								var inputs = val.split(";")
								template_str += '<div class="form-group">' +
									'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'+inputs[0]+'">'+inputs[0].charAt(0).toUpperCase()+inputs[0].slice(1)+'</label>' +
									'<div class="col-xs-12 col-sm-9">' +
										'<div class="clearfix">' +
											'<input name="_'+inputs[0].replace(/\s/g,"_")+'" id="'+inputs[0]+'" class="col-xs-12 col-sm-4" type="'+inputs[1]+'" />' +
										'</div>' +
									'</div>' +
								'</div>'
								
							})
							
							
							
						}
						
					}
					
					template_str += '<hr>' + 
								'<div class="form-group">' +
									'<label class="control-label col-xs-12 col-sm-3 no-padding-right" >Attachment</label>' +

									'<div class="col-xs-12 col-sm-3">' +
										
										'<input type="file"  name="photo"  id="pic_upclose" />' +
									
									'</div>' +
								'</div>'
					 
					 //subs_str = '<ul><li>'+subs_str+'</li></ul>'
					 
					 //construct service details form
					 $(".__serviceName").html(selected_service['serviceName'])
					 $('.__description').html(selected_service['description'])
					 $('.__requirements').html(reqs_str)
					 $('.__subprocesses').html(subs_str)
					 $('.__template').html(template_str)
				 }
				 else{
					 //clear
					 $('.__serviceName').html('')
					 $('.__description').html('')
					 $('.__requirements').html('')
					 $('.__subprocesses').html('')
					 $('.__template').html(template_str)
				 }
				 
				 
				 
				 if($(this).val() == 'Other') $('#input_doc_type').show(); else $('#input_doc_type').hide();
				 
				 $('#hidden_doc_type').val($(this).val());
			});
			
			$(document).on('change','#submitted_by_select',function(){
				$('#phone').val($('option:selected',this).attr('data-phone'));
				$('#email').val($('option:selected',this).attr('data-email'));
				$('#organisation').val($('option:selected',this).attr('data-agency'));
			});
			
			$(document).on('change','#sendto',function(){
				var delegate = $('option:selected',this).attr('data-delegate');
				var $input =  '';
				if(delegate != '' && delegate != '0'){
					//alert('we are delegating');
					$input =  '<input type="hidden" name="delegate_to" value="'+delegate+'" >';
				}
				$('#delegateto').html($input)
			});
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			
			
			
			
			
			
			});
			
			function changeInput(str){
				alert(str)
			}
			
			
		</script>
		
		
	</body>
</html>
