<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php"); if(isset($_GET['ac'])){ $acid=$_GET['ac']; } else { echo '<script>window.location.href="accounts.php";</script>'; } 
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Details</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		
		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		
		function addCommas(nStr) {
		  		  
		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }
		  
		  return x1 + x2;
		  
		}
		
		function newDepositBal(){
			var old = document.getElementById('deposit_stat_bal').value*1;
			var nw = document.getElementById('deposit_amount').value.replace(/,/g, '')*1;
			return old+nw;	
		}
		
		function newWithdrawBal(){
			var old = document.getElementById('withdraw_stat_bal').value*1;
			var nw = document.getElementById('withdraw_amount').value.replace(/,/g, '')*1;
			var newbal = old-nw;
			if(newbal<0){
				alert("Insurficient Funds");
				document.getElementById('withdraw_amount').value = "";
				document.getElementById('show_new_bal').value = old;
				return old;
			} else{
			return old-nw;
			}
			

		}
		
		function updateBalances1(nStr){
			//first update the balance
			var newBalance = newDepositBal();
		  document.getElementById('deposit_new_bal').value = newBalance;
		  document.getElementById('show_new_bal').value = addCommas(newBalance);
		  document.getElementById('deposit_amount').value = addCommas(nStr);
		 
		}
		
		function updateBalances2(nStr){
			//first update the balance
			document.getElementById('withdraw_amount').value = addCommas(nStr);
			var newBalance = newWithdrawBal();
		  document.getElementById('withdraw_new_bal').value = newBalance;
		  document.getElementById('show_new_bal').value = addCommas(newBalance);
		  
		 
		}
		
		
		
		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						
					<?php if(isset($_GET['ac'])){
							$form_query = "SELECT * FROM documents WHERE id='$acid'"; 
							$form_result = $conn->query($form_query);
							while($form_data = $form_result->fetch_assoc()) { 
					?>
						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue"><?php echo $form_data['document_type']; ?></h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										
										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>
											<div >
														
														<form novalidate class="form-horizontal" id="validation-form" method="post" action="accounts.php?edit" enctype="multipart/form-data">
															
															
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="organisation">Organisation/Company</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input name="data[organisation]" id="organisation" value="<?php echo $form_data['organisation']; ?>" class="col-xs-12 col-sm-6" type="text">
																	</div>
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Submited By</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input name="data[submitted_by]" id="submitted_by" value="<?php echo $form_data['submitted_by']; ?>" class="col-xs-12 col-sm-6" type="text">
																	</div>
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="designation">Title</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input name="data[designation]" id="designation" value="<?php echo $form_data['designation']; ?>" class="col-xs-12 col-sm-6" type="text">
																	</div>
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">Remarks</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<textarea class="input-xlarge" name="data[remarks]" id="comment"><?php echo $form_data['remarks']; ?></textarea>
																	</div>
																</div>
															</div>


															<div class="hr hr-dotted"></div>
																													
															<div class="space-2"></div>
																														
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="tribe"></label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<a class="btn btn-sm btn-success" href="<?php echo $form_data['file_location']; ?>" target="_blank">
																			Open/Download Document  
																			<i class="ace-icon fa fa-download bigger-110"></i>
																		</a>
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="section">Actions</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<a class="btn btn-sm btn-info"  id="bootbox-regular">Approve</a>
																		<a class="btn btn-sm btn-danger" href="reject.php?ac=<?php echo $form_data['id']; ?>">Reject</a>
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>

															

															<div class="space-8"></div>
															
															

															<input type="hidden" name="action" value="EDIT ACCOUNT" >	
															<input type="hidden" name="table" value="accounts">
															<input type="hidden" name="acid" value="<?php echo $form_data['id']; ?>" >
															
															
															
														</form>
													</div>
										</div>
									</div>
								</div><!-- /.row -->
							<?php } } ?>
					
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		
		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
			// picture upload
			$('#pic_upclose').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Passport Photo',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,
					
					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});
			$('#pic_upclose').ace_file_input('show_file_list', [{type: 'image', name: $('#avatar').attr('src')}]);
			
			
			});
		</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
