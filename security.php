<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Security Settings</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
		
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<?php if(isset($_GET['new'])){ include("include/do_insert.php"); } ?>
						<?php if(isset($_GET['update'])){ include("include/do_edit.php"); } ?>

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<?php // if(isset($_GET['delegate'])){ include("include/do_insert.php"); } ?>
									
									<div class="col-sm-7">
										<!-- #section:elements.tab.position -->
										
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Staff Passowrds</h4>
											</div>
											<div class="widget-body">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
										
										
										
										
										<div>
											
											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>Staff</th>
														<th>Login Name</th>
														<th>Last Login</th>
														<th>Pwd Last changed</th>
														<th>Status</th>
														<th>Force Pwd Change</th>												
														<th></th>												
													</tr>
												</thead>

												<tbody>
												
												<?php $form_query = "SELECT * FROM admins WHERE id<>'1'"; 
												  $form_result = $conn->query($form_query);
												  while($form_data = $form_result->fetch_assoc()) {   ?>
													<tr>
														<td><?php echo $form_data['fname']." ".$form_data['lname']." ".$form_data['oname']; ?></td>
														<td><?php echo $form_data['uname']; ?></td>
														<td><?php echo $form_data['last_login']=="0000-00-00 00:00:00"?"Never":$form_data['last_login']; ?></td>
														<td><?php echo $form_data['pwd_changed']=="0000-00-00 00:00:00"?"Never":$form_data['pwd_changed']; ?></td>
														<td><?php echo $form_data['status']; ?></td>
														<td>
															<label>
																<input name="switch-field-1" class="ace ace-switch ace-switch-3 btn-flat force-pwd" item-id="<?php echo $form_data['id']; ?>" type="checkbox" <?php echo $form_data['force_pwd_change']=="1"?"checked":""; ?>  />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a href="#modal-form" class="btn btn-minier btn-light change-pwd blue" role="button" data-toggle="modal"  item-id="<?php echo $form_data['id']; ?>" >
																	<i class="ace-icon fa fa-lock"></i>Reset Pwd
																</a>
															</div>
														</td>
													</tr>
												  <?php } ?>
												

													
												</tbody>
											</table>
										</div>
										
										</div>
										</div>
										</div>

										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									<div class="col-sm-5">
										
										<!-- #section:custom/widget-box.options.transparent -->
										<form action="security.php?update" method="post">
										
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Password Complexity Rules</h4>
												<button type="submit" class="btn btn-white btn-success btn-bold btn-round no-border">
													<i class="ace-icon fa fa-floppy-o bigger-120 green"></i>
													Save Rules
												</button>
											</div>
											<?php $pwd_set = $conn->query("SELECT * FROM cinnamon_clients WHERE  id='".$_SESSION["client_id"]."' ");
													$pwd_=$pwd_set->fetch_assoc(); ?>
											
											<div class="widget-body">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													
													<input type="hidden" name="acid" value="1" >	
													<input type="hidden" name="action" value="UPDATE SET" >	
													<input type="hidden" name="table" value="cinnamon_clients">
													
													<div class="profile-feed row">
														<div class="col-sm-12">
															
															<div class="profile-activity clearfix">
																<div>
																	<p>Minimum Charachters</p>
																	<label>
																		<input type="text" name="data[set_pwd_min_char]" value="<?= $pwd_['set_pwd_min_char']; ?>" />
																	</label>
																</div>

															</div>
															
															<div class="profile-activity clearfix">
																<div>
																	<p>Maximum Login fails</p>
																	<label>
																		<input type="text" name="data[set_pwd_max_fails]" value="<?= $pwd_['set_pwd_max_fails']; ?>" />
																	</label>
																</div>

															</div>
															
															<div class="profile-activity clearfix">
																<div>
																	<p>Password Expiry</p>
																	<label>
																		<input name="set_pwd_expiry_days" class="ace ace-switch ace-switch-1" type="checkbox" <?= $pwd_['set_pwd_expiry_days']>0?"checked":""; ?> >
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;</span>
																		<input class="<?= $pwd_['set_pwd_expiry_days']>0?"":"hide"; ?>" name="data[set_pwd_expiry_days]" type="text" size="6" value="<?= $pwd_['set_pwd_expiry_days']; ?>" />
																		<span class="lbl <?= $pwd_['set_pwd_expiry_days']>0?"":"hide"; ?>"> days</span>
																		
																	</label>
																</div>

															</div>
															
															<div class="profile-activity clearfix">
																<div>
																	<p>Must have at leaset one numeric charachter (0-9)</p>
													
																	<label>
																		<input name="data[set_pwd_require_numeric]" type="hidden" value="0" >
																		<input name="data[set_pwd_require_numeric]" value="1" class="ace ace-switch ace-switch-1" type="checkbox" <?= $pwd_['set_pwd_require_numeric']>0?"checked":""; ?> >
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Numeric charachter</span>
																	</label>
																</div>

										
															</div>

															<div class="profile-activity clearfix">
																<div>	
																	<p>Must have at least one capitalised letter (A-Z)</p>
													
																	<label>
																		<input name="data[set_pwd_require_caps]" type="hidden" value="0" >
																		<input name="data[set_pwd_require_caps]" value="1" class="ace ace-switch ace-switch-1" type="checkbox" <?= $pwd_['set_pwd_require_caps']>0?"checked":""; ?> >
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Capital letter</span>
																	</label>
																</div>
															</div>

															<div class="profile-activity clearfix">
																<div>	
																	<p>Must have at least one special charachter (!@#$%^&*{}[]<>?|\/,.-+)</p>
													
																	<label>
																		<input name="data[set_pwd_require_special_char]" type="hidden" value="0" >
																		<input name="data[set_pwd_require_special_char]" value="1" class="ace ace-switch ace-switch-1" type="checkbox" <?= $pwd_['set_pwd_require_special_char']>0?"checked":""; ?> >
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Special charachters</span>
																	</label>
																</div>
															</div>

														</div><!-- /.col -->

														
													</div>
													
													
												</div>
											</div>
											
										</div>
										
										</form>
										
										<div class="widget-box ">
											<div class="widget-header">
												<h4 class="widget-title lighter">Offline Backup</h4>

												
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													
													
													<div class="profile-feed row">
														<div class="col-sm-12">
															
															
															<div class="clearfix">
																<div class="clearfix center">
																	<button class="btn btn-white btn-success btn-bold btn-round no-border">
																		<i class="ace-icon fa fa-floppy-o bigger-120 green"></i>
																		New Incremental Backup
																	</button>
																	&nbsp;&nbsp;&nbsp;&nbsp;
																	<button class="btn btn-white btn-info btn-bold btn-round no-border">
																		<i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
																		New Full Backup
																	</button>
													
																	
																</div>

																<hr>
															</div>
															
															

															<div class="profile-activity clearfix">
																<div>	
																	<p><b>Full-2017-09-25-13-01.bak</b> | Created: 25th 09 2017 | Size: 23 GB</p>
													
																	<a href=""><i class="fa fa-cloud-download"></i> download</a> <span class="pull-right">Auto delete in: 5 days <a href=""><i class="fa fa-trash-o"></i> Delete now</a></span>
																</div>
															</div>

															<div class="profile-activity clearfix">
																<div>	
																	<p><b>Inc-2017-09-25-13-01.bak</b> | Created: 25th 09 2017 | Size: 1.2 GB</p>
													
																	<a href=""><i class="fa fa-cloud-download"></i> download</a> <span class="pull-right">Auto delete in: 5 days <a href=""><i class="fa fa-trash-o"></i> Delete now</a></span>
																</div>
															</div>

																														
														</div><!-- /.col -->

														
													</div>
													
													
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">System Log</h4>
											</div>
																					
											<div class="widget-body">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													<div class="profile-feed row">
														<div class="col-sm-12">
															
															<a href="log.php">
															<div class="profile-activity clearfix">
																<div>
																	<p>Access Logs</p>
																</div>
															</div>
															</a>
															
															<a href="log.php?db">
															<div class="profile-activity clearfix">
																<div>
																	<p>Database Logs</p>
																</div>
															</div>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>			
										
										
										<!-- /section:custom/widget-box.options.transparent -->
									</div>
										
										
									</div>
									
									
									
									
									
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>
			
			<div id="modal-form" class="modal" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
					<form action="security.php?update" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="blue bigger">Change Password</h4>
						</div>

						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12">

									<div class="space-4"></div>

									<div class="form-group">
										<label for="form-field-username">New Password</label>
										<div>
											<input class="form-control" type="password" id="form-field-pwd1"  name="data[pwd]" placeholder="Password" required />
										</div>
									</div>
									<div class="form-group">
										<label for="form-field-username">Confirm Password</label>
										<div>
											<input class="form-control" type="password" id="form-field-pwd2"  name="pwd" placeholder="Password" required />
										</div>
									</div>
									
									<input type="hidden" id="acid-pwd" name="acid" value="" >	
									<input type="hidden" name="action" value="PWD RESET" >	
									<input type="hidden" name="table" value="admins">
																						
								</div>
							</div>
						</div>

						<div class="modal-footer">
							
							<button class="btn btn-sm btn-primary">
								<i class="ace-icon fa fa-check"></i>
								Save
							</button>
							
							<button class="btn btn-sm" data-dismiss="modal">
								<i class="ace-icon fa fa-times"></i>
								Cancel
							</button>

							
						</div>
						</form>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		<script src="assets/js/bootbox.js"></script>
		
		<script src="assets/js/dataTables/jquery.dataTables.js"></script>
		<script src="assets/js/dataTables/jquery.dataTables.bootstrap.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
		jQuery(function($) {
			if($('input[name="email_alerts"]:checked').length == 0){ $("#email_prefs").hide();  }	
			$('#email_alerts').change(function() { 
				if($('input[name="email_alerts"]:checked').length > 0){ $("#email_prefs").show();  }
				else { $("#email_prefs").hide();  }
			});
			
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}

				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;

					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			 

			if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
				
				}


				$('.saveId').on('click', function(){
					
					//saving info on pressing next
							//alert('oh yes')
							var frm = $(this).parents('form').attr('id');
							var values = $('#'+frm).serialize();
							var frmAction = $('#'+frm).attr('action');
							$.ajax(
								{
									type: "POST",
									url: frmAction,
									data: "myData=" + values,
									cache: false,
									success: function(message)
									{	
										completeHandler(frm,message);
									},
									error: function(message)
									{
										alert('failed'+message);
									}
								});
					
					//end of saving function
				});

				
								
			//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
				$('#date-range-picker').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});	
				
				
			/////////////////////////////////////////////////
			///    PWD RESET    ///////////////////
			$("#pwd_button").click(function() {
				
				var pwd1 = $("#pwd1").val();
				var pwd2 = $("#pwd2").val();
				var uid = $("#uid").val();
				
				var msg = "";
				
				if(pwd1==""){ msg = 'ERROR: New password cannot be blank!'; }
				else if(pwd1!=pwd2){ msg = 'ERROR: Passwords must match!'; }
				else { 
				
					var dataString = 'pwd='+ pwd1 + '&id=' + uid;
					
					$.ajax({
						type: "POST",
						url: "include/q_pwd_change.php",
						data: dataString,
						success: function() {
						 msg='Your new DocTracker Password is being saved';
						
						alert(msg);
						
						 $("#pwd1").val("");
						 $("#pwd2").val("");

						}
					});

				}

				bootbox.dialog({
					message: msg, 
					buttons: {
						"success" : {
						  "label" : "OK",
						  "className" : "btn-sm btn-primary",
						  callback :function(e){

							}
						}
					}
				});
								
			});	

			$('#dynamic-table').dataTable({
				"bSort":false
			});

			$('body').on('click','a.change-pwd',function(){
				var id = $(this).attr('item-id');
				$('#acid-pwd').val(id);
			});	
			
			$('body').on('change','.force-pwd',function(){
				var id = $(this).attr('item-id');
				var ch;
				if($(this).is(':checked')){ ch=1; } else { ch=0; }
				var pst = 'table=admins&acid='+id+'&data[force_pwd_change]='+ch
				
				$.ajax({
					type: "POST",
					url: "include/do_edit.php",
					data: pst,
					success: function(resp) { 
						if(ch==1){
							alert("Password change enforced");
						} else {	
							alert("Forced Password removed");
						}
					}
				}); 
			});	
			
		});
		
		function completeHandler(frm,message){
				//alert(message);
				$('.msg').html(message);
				$('.msg').show();
				setTimeout(function(){
					$(".msg").hide("slow");
				},7000);
			}
		</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
</html>
