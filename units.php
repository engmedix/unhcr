<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Departments</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" type="text/css" href="assets/plugins/jconfirm/css/jquery-confirm.css"/>

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="assets/css/chosen.css" />
		
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<?php if(isset($_GET['new'])){ include("include/do_insert.php"); } ?>
									<?php if(isset($_GET['update'])){ include("include/do_edit.php"); } ?>
									
									<div class="col-sm-7">
										<!-- #section:elements.tab.position -->
										<div class="tabbable tabs-left widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Departments</h4>

												
											</div>
											<ul class="nav nav-tabs nav-tabs2" id="myTab3">
											
											<?php $form_query = "SELECT * FROM units WHERE parent_unit='0' ORDER BY department"; 
												  $form_result = $conn->query($form_query);
												  while($ro_data = $form_result->fetch_assoc()) {   ?>	
												<li id="li<?php echo $ro_data['id']; ?>"  <?php if(isset($psn_last_id)){ if($ro_data['id']==$psn_last_id) echo 'class="active"'; } else { if($ro_data['id']==1) echo 'class="active"'; }  ?> >
													<a data-toggle="tab" href="#home<?php echo $ro_data['id']; ?>" >
														<i class="pink ace-icon fa fa-users bigger-110"></i>
														<?php echo $ro_data['department']; ?>
													</a>
												</li>
											<?php } ?>
												
											</ul>

											<div class="tab-content">
											
											<?php $form_query2 = "SELECT units.*,children FROM units LEFT JOIN (SELECT GROUP_CONCAT(id) children,parent_unit FROM units GROUP BY parent_unit) ch ON units.id = ch.parent_unit WHERE units.parent_unit = 0 ORDER BY department"; 
												  $form_result2 = $conn->query($form_query2);
												  while($ro_data2 = $form_result2->fetch_assoc()) { 
													
														$unit_id = $ro_data2['id']; 
														$children = $ro_data2['children'];  
														
														if($children == ''){
															$in = $unit_id;
														}
														else{
															$in = $unit_id.','.$children;
														}
														?>	
												  
												
												<div id="home<?php echo $unit_id; ?>" class="tab-pane <?php if(isset($psn_last_id)){ if($ro_data2['id']==$psn_last_id) echo "in active"; } else { if($ro_data2['id']==1) echo "in active"; } ?>">
												
												<div class="widget-box transparent" style="margin-top:-10px">
													<div class="widget-header" style="margin-bottom:10px">
														<h4 class="widget-title lighter"><?php echo $ro_data2['department']; ?> department</h4>

														<div class="widget-toolbar no-border">
															
															
															<a href="#" title="Fullscreen" data-action="fullscreen">
																<i class="ace-icon fa fa-arrows-alt bigger-120"></i>
															</a>
															
															<a href="#" data-action="reload">
																<i class="ace-icon fa fa-refresh"></i>
															</a>
															
															<a href="#" class="save-item" data-id="<?php echo $unit_id; ?>" title="Save Changes">
																<i class="ace-icon fa fa-save green bigger-120"></i>
															</a>
															
															<a href="#" class="delete-item" data-tbl="units" data-id="<?php echo $unit_id; ?>" title="Delete department">
																<i class="ace-icon fa fa-trash bigger-120 red"></i>
															</a>
														</div>
													</div>

													<div class="widget-body">
												
												
												
												
												<form method="post" id="form<?php echo $unit_id; ?>" action="?update">
												<input type="hidden" name="extraId" value="6768978yihhkhjk" />
												<input type="hidden" name="table" value="units" />
												<input type="hidden" name="acid" value="<?php echo $unit_id; ?>" />
												<input type="hidden" name="action" value="EDIT DEPARTMENT" />
													
													<p> </p>
													<p>Department: </p>
													<input type="text" class="form-control" name="data[department]" value="<?php echo $ro_data2["department"]; ?>" />
													<p>Department Head: </p>
													<select name="data[unit_head]" class="select-unit-head form-control" data-placeholder="Staff from your Department...">
																<option value="0">  </option>
															<?php $form_query = "SELECT * FROM admins WHERE department_id IN ($in) AND id<>'$id' ORDER BY fname, lname"; 
																  $form_result = $conn->query($form_query);
																  while($un_data = $form_result->fetch_assoc()) {   ?>	
																<option value="<?php echo $un_data['id']; ?>" <?php if($un_data['id']==$ro_data2['unit_head']) echo "selected"; ?> ><?php echo $un_data['fname']." ".$un_data['lname']." ".$un_data['oname']; ?></option>
																<?php } ?>
																
													</select>
													<p> </p>
													<p>Staff: </p>
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>Name</th>
																<th>Documents Held</th>
																<th>Delegation </th>
															</tr>
														</thead>

														<tbody>
														<?php $form_query = "SELECT *,COALESCE(docsin_,0) docsin FROM admins LEFT JOIN 
															(SELECT COUNT(*) docsin_, to_id FROM doc_track WHERE receiver_status <> 'ARCHIVED' GROUP BY to_id) d_i ON admins.id = d_i.to_id WHERE department_id IN ($in) ORDER BY department_id, fname"; 
														  $form_result = $conn->query($form_query);
														  $depart = 0;
														  while($form_data = $form_result->fetch_assoc()) {   
															if($depart != $form_data['department_id']) {
																echo '<tr><th colspan="3">'.getDept($form_data['department_id']).'</th></tr>';
																$depart = $form_data['department_id'];
															}
																
														  ?>
															<tr>
																<td><?php echo $form_data['fname']." ".$form_data['lname']." ".$form_data['oname']; ?></td>
																<td><?php echo $form_data['docsin'] ?></td>
																<td><?php echo (($form_data['delegate_to'] == 0)?'None':getStaffName($form_data['delegate_to'])); ?></td>
															</tr>
														  <?php } ?>
															
														</tbody>
													</table>
													
												

												</form>	
												</div>
												
												</div>
											
											</div>
											<?php } ?> 
											
										</div> <!-- /.tab-content -->
										</div> <!-- /.tabbable -->
										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									<div class="col-sm-5">
									
										
										<!-- #section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Add New Department</h4>

												
											</div>

											<div class="widget-body" style="background-color: #f9f9f9; padding-left:10px; padding-right:10px;">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													<form method="post" action="?new">
														<div class="form-group">
															<label for="form-field-username">Department Name</label>
															<div>
																<input type="text" class="form-control" id="form-field-username"  name="data[department]" placeholder="department"  />
															</div>
														</div>
														
														<div class="form-group">
															<label for="form-field-username">Parent department</label>
															<div>
																
																<select name="data[parent_unit]" class="chosen-select form-control" data-placeholder="Select parent department if applicable">
																<option value="0"></option>
															<?php $form_query = "SELECT * FROM units WHERE parent_unit='0' ORDER BY department"; 
																  $form_result = $conn->query($form_query);
																  while($ro_data = $form_result->fetch_assoc()) {   ?>	
																<option value="<?php echo $ro_data['id']; ?>" ><?php echo $ro_data['department']; ?></option>
																<?php } ?>
																
																	</select>
																
																
															</div>
														</div>
														
														<div class="form-group">
															<label for="form-field-username">Description</label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[description]" rows="5" ></textarea>
															</div>
														</div>
														
														<button class="btn btn-primary pull-right">
															<i class="ace-icon fa fa-floppy-o bigger-120"></i>
															Save new department
														</button>
														<input type="hidden" name="action" value="NEW UNIT" >	
														<input type="hidden" name="table" value="units">
														
													</form>
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
									</div>
									
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>

		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/plugins/jconfirm/js/jquery-confirm.js" ></script>
		
		<script src="assets/js/master.js"></script>

		
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			$('.select-unit-head').on('change', function(){
					
					//saving info on pressing next
							//alert('oh yes')
							var frm = $(this).parents('form').attr('id');
							var values = $('#'+frm).serialize();
							var frmAction = $('#'+frm).attr('action');
							$.ajax(
								{
									type: "POST",
									url: frmAction,
									data: "myData=" + values,
									cache: false,
									success: function(message)
									{	
										completeHandler(frm,message);
									},
									error: function(message)
									{
										alert('failed'+message);
									}
								});
					
					//end of saving function
					
					
				});
				
				if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						 else $('#form-field-select-4').removeClass('tag-input-style');
					});
				}
			
			
			});
			
			function completeHandler(frm,message){
				//alert(message);
			}
		</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
