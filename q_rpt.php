<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>
<h5 class="widget-title bigger lighter"><i class="ace-icon fa fa-table"></i> Workflow Report <a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success">Export Report to excel</a></h5>


<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Ref No</th>
			<th>Document type</th>
			<th>Originating Branch</th>
			<th>Created by</th>
			<th>Date of Created</th>
			<th>Date of Closed</th>
			<th>Workflow Time</th>
			<th>#Steps</th>
			<th>Status</th>
		</tr>
	</thead>

	<tbody>
	
	<?php
	$conditions = ' doc_track.to_id <> "" ';
	if(@$_POST['doc_type'] != '') $conditions .= ' AND document_type="'.$_POST['doc_type'].'"';
	if(@$_POST['internal_ref_number'] != '') $conditions .= ' AND internal_ref_number LIKE "%'.$_POST['internal_ref_number'].'%"';
	if(@$_POST['staff'] != '') $conditions .= ' AND (doc_track.to_id = '.$_POST['staff'].' OR doc_track.from_id = '.$_POST['staff'].') ';
	if(@$_POST['unit'] != '') $conditions .= ' AND (admins2.department_id = '.$_POST['unit'].' ) ';
	if(@$_POST['branch'] != '') $conditions .= ' AND (admins2.station_id = '.$_POST['branch'].' ) ';
	if(@$_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-j H:i',strtotime($dates[0]));
		$endDate = date('Y-m-j H:i',strtotime($dates[1]));
		
		$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
	}
	
	if($_POST['days'] != ''){
		$conditions .= ' AND DATEDIFF(IF(archive_date = "0000-00-00 00:00:00",NOW(),archive_date),actionDate) = '.$_POST['days'];
	}


	
	

	//$form_query = "SELECT documents.*, admins.fname,admins.lname,admins.oname, DATEDIFF(IF(archive_date = '0000-00-00 00:00:00 	',NOW(),archive_date),actionDate) dur, actionDate, archive_date, doc_track.action FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.to_id = admins.id WHERE $conditions AND documents.id IS NOT NULL ORDER BY actionDate desc"; 
	
	$form_query = "SELECT a.*,DATEDIFF(IF(a.date_closed = '0000-00-00 00:00:00',NOW(),a.date_closed),a.created) dur, b.fname,b.lname, (SELECT COUNT(*) FROM doc_track WHERE doc_id=a.id) AS steps FROM documents a LEFT JOIN admins b ON a.created_by = b.id";

	  $form_result = $conn->query($form_query);
	  //echo $form_result->mysqli_error();
	  
	  $now = new DateTime();
	  $now->setTime(0,0,0);
	  while($form_data = $form_result->fetch_assoc()) {   
			
			$readStatus = '';
			
			if($form_data['status_label'] == 'CLOSED') $label = ' label-success"';
			else {  $label = 'label-info'; }
			
			$sentDate = new DateTime($form_data['date']);
			$sentDate->setTime( 0, 0, 0 );
			
			$diff = $now->diff( $sentDate );
			
			$periodSpent = $diff->days;
			if($periodSpent == 0) $periodSpent = ' Today';
			elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
			elseif($periodSpent >= 2) $periodSpent .= ' days ago';
	  
	  ?>
		<tr>
			<td><a href="timeline.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>"><?php echo $form_data['internal_ref_number']; ?></a></td>
			<td><?php echo $form_data['document_type']; ?></td>
			<td><?php echo getStaffStation($form_data["created_by"]); ?></td>
			<td><?php echo $form_data["fname"]." ".$form_data["lname"]; ?></td>
			<td><?php echo date("j M Y",strtotime($form_data['date'])); ?></td>
			<td><?php echo $form_data['status_label']!='CLOSED'?"OPEN":date("j M Y",strtotime($form_data['date_closed'])); ?></td>
			<td><?php echo $form_data['dur']; ?> days</td>
			<td><?php echo $form_data['steps']; ?></td>
			<td><span class="label <?php echo $label; ?>"><?php echo $form_data['status_label']!='CLOSED'?"OPEN":"CLOSED"; ?></span></td>
			
			
			
		</tr>
	  <?php } ?>
	

		
	</tbody>
</table>