<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>
<h5 class="widget-title bigger lighter"><i class="ace-icon fa fa-table"></i> Filter Results </h5>


<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Stage</th>
			<th>No. Active</th>
			<th>No. Unread</th>
			<th>No. Staff</th>
			<th>Av Time</th>
			<th>Lowest Time</th>
			<th>Highest Time</th>
		</tr>
	</thead>

	<tbody>
	
	<?php
	$conditions = '1';
	if(@$_POST['doc_type'] != '') $conditions .= ' AND documents.document_type="'.$_POST['doc_type'].'"';
	if(@$_POST['branch'] != '') $conditions .= ' AND (admins2.station_id = '.$_POST['branch'].' ) ';
	if(@$_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-j H:i',strtotime($dates[0]));
		$endDate = date('Y-m-j H:i',strtotime($dates[1]));
		
		$conditions .= ' AND (date >= "'.$startDate.'" AND date <= "'.$endDate.'")';
	}
	
	$form_query = "SELECT documents.stage_label, COUNT(documents.id) AS deals, count(doc_track.receiver_status) AS admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON documents.sender_id = admins.id LEFT JOIN admins admins2 ON doc_track.to_id = admins2.id WHERE $conditions AND documents.id IS NOT NULL GROUP BY doc_id ORDER BY date desc"; 
	  
	  //error_log($form_query, 0);
	  
	  $form_result = $conn->query($form_query);
	  //echo $form_result->mysqli_error();
	  
	  $now = new DateTime();
	  $now->setTime(0,0,0);
	  while($form_data = $form_result->fetch_assoc()) {   
		
			$readStatus = '';
			
			$sentDate = new DateTime($form_data['date']);
			$sentDate->setTime( 0, 0, 0 );

			$diff = $now->diff( $sentDate );
			
			$periodSpent = $diff->days;
			if($periodSpent == 0) $periodSpent = ' Today';
			elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
			elseif($periodSpent >= 2) $periodSpent .= ' days ago';
	  
	  ?>
		<tr>
			<td><?php echo date("j M Y - h:i a",strtotime($form_data['date'])).'  <span class="label">'.$periodSpent.'</span>'; ?></td>
			<td><?php echo $form_data['fname'].' '.$form_data['lname']; ?></td>
			<td><a href="timeline.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>"><?php echo $form_data['document_type']; ?> / <?php echo $form_data['internal_ref_number']; ?></a></td>
			<td><?php echo getStaffName($form_data['current_holder']); ?></td>
			<td><?php echo $form_data['description']; ?></td>
			
			
		</tr>
	  <?php } ?>
	

		
	</tbody>
</table>