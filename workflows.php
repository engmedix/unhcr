<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Workflows</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/bootstrap-duallistbox.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="assets/css/excel.css" />
		<link rel="stylesheet" type="text/css" href="assets/plugins/jconfirm/css/jquery-confirm.css"/>
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		<style>
		table.sheet{
			margin-bottom:2px;
		}
		table.sheet input[type=text]{
			margin:0px;
			padding:0px;
			border:0px;
			width:100%;
		}
		table.sheet select{
			margin:0px;
			padding:0px;
			border:0px;
			width:100%;
		}
		</style>
		

		<?php $usr_list = $conn->query("SELECT id, CONCAT(fname,' ',lname) nme FROM admins ORDER BY fname");
			  $usr_json = json_encode($usr_list->fetch_all(MYSQLI_ASSOC));
			  $cc_list = "";
			  while($cc = $usr_list->fetch_assoc()) { 
					$cc_list .= '<option value="'.$cc['id'].'">'.$cc['nme'].'</option>';
			  } 
			  unset($cc); unset($usr_list);

			  $job_list = $conn->query("SELECT id, job_title FROM job_titles ORDER BY job_title");
			  $job_json = json_encode($job_list->fetch_all(MYSQLI_ASSOC));
			  
			  $brc_list = $conn->query("SELECT id, duty_station FROM dutystations ORDER BY duty_station");
			  $brc_json = json_encode($brc_list->fetch_all(MYSQLI_ASSOC));
			  
			  $dep_list = $conn->query("SELECT id, department FROM units ORDER BY department");
			  $dep_json = json_encode($dep_list->fetch_all(MYSQLI_ASSOC));

		?>
		
		<script type="text/javascript">
			function usrList(){
				var str = '<?php echo $usr_json; ?>';
				return str;
			}
			function jobList(){
				var str = '<?php echo $job_json; ?>';
				return str;
			}
			function brcList(){
				var str = '<?php echo $brc_json; ?>';
				return str;
			}
			function depList(){
				var str = '<?php echo $dep_json; ?>';
				return str;
			}
			
		</script>
		
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<?php if(isset($_GET['new'])){ include("include/do_insert.php"); } ?>
									<?php if(isset($_GET['update'])){ include("include/do_edit.php"); } ?>
									

									
									<div class="col-sm-12">
										<!-- #section:elements.tab.position -->
										<div class="tabbable tabs-left widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Workflows</h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

												<h4><?php if (!empty($_SESSION['workflow_msg'])) {
                                                 echo $_SESSION['workflow_msg'];
                                                 unset($_SESSION['workflow_msg']);
                                                  }
                                                 ?></h4>
                                             </div>
											<ul class="nav nav-tabs nav-tabs2" id="myTab3">
											
											<?php $first_query = $conn->query("SELECT id FROM document_types ORDER BY doc_type LIMIT 1");
												  $frst = $first_query->fetch_assoc();
												  
												  $form_query = "SELECT * FROM document_types ORDER BY doc_type"; 
												  $form_result = $conn->query($form_query);
												  while($ro_data = $form_result->fetch_assoc()) {   ?>	
												<li id="li<?php echo $ro_data['id']; ?>" <?php if(isset($_POST['data']['doc_type_id'])){ if($ro_data['id']==$_POST['data']['doc_type_id']) echo 'class="active"'; } else { if($ro_data['id']==$frst['id']) echo 'class="active"'; } ?> >
													<a data-toggle="tab" href="#home<?php echo $ro_data['id']; ?>" >
														<i class="pink ace-icon fa fa-file bigger-110"></i>
														<?php if($ro_data['temp_type']!="none"){
															echo strtoupper($ro_data['doc_type']).'<i class="ace-icon fa fa-bookmark"></i>';
														} else { echo $ro_data['doc_type']; } ?>
													</a>
													
												</li>
											<?php } ?>
												
											</ul>

											<div class="tab-content">
											
											
											<?php $form_query2 = "SELECT * FROM document_types ORDER BY doc_classification, doc_type"; 
												  $form_result2 = $conn->query($form_query2);
												  while($ro_data2 = $form_result2->fetch_assoc()) { $unit_id = $ro_data2['id'];  ?>	
												
												<div id="home<?php echo $unit_id; ?>" class="tab-pane widget-container-col <?php if(isset($_POST['data']['doc_type_id'])){ if($ro_data2['id']==$_POST['data']['doc_type_id']) echo "in active"; } else { if($ro_data2['id']==$frst['id']) echo "in active"; } ?>">
													
													<div class="widget-box transparent" style="margin-top:-10px">
														<div class="widget-header" style="margin-bottom:10px">
															<h4 class="widget-title lighter"><?php echo strtoupper($ro_data2['doc_type']); ?></h4>

															<div class="widget-toolbar no-border">
																
																
																<a href="#" title="Fullscreen" data-action="fullscreen">
																	<i class="ace-icon fa fa-arrows-alt bigger-120"></i>
																</a>
																
																<a href="#" class="hide" data-action="reload">
																	<i class="ace-icon fa fa-refresh"></i>
																</a>
																
																<a href="#" class="save-doc" doc-id="<?php echo $unit_id; ?>" title="Save Changes">
																	<i class="ace-icon fa fa-save green bigger-120"></i>
																</a>
																
																<a href="#" class="delete-doc hide" data-tbl="document_types" data-id="<?php echo $unit_id; ?>" class="delete-doc" title="Delete document type">
																	<i class="ace-icon fa fa-trash bigger-120 red"></i>
																</a>
															</div>
														</div>

														<div class="widget-body">		
													<div id="form<?php echo $unit_id; ?>" action="?update" method="post"><p>Workflow Sequence: </p>
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>Stage</th>
																<th>Description</th>
																<th>Action</th>
															</tr>
														</thead>

														<tbody>
															<tr>
																<td>Initiation</td>
																<td>When [<?= $ro_data2['doc_type']; ?>] is first submitted.</td>
																<td>
																	<?php $actions = json_decode($ro_data2['actions'],true); $w=1; $c=count($actions); foreach($actions as $action){ ?>
																		<?php if($action['type']!="submit"){?><a href="#" title="Remove action"><i class="fa fa-minus-circle"></i></a><?php } ?> 
																		<a href="#"><?php echo $action['label']; ?></a><?php echo $w<$c?",":""; ?> &nbsp;		
																	<?php $w++;	} ?>
																</td>
															</tr>
															<?php $wf3_q = $conn->query("SELECT * FROM document_type_stages WHERE doc_type_id='".$ro_data2['id']."'"); 
																  if($wf3_q->num_rows>0){
																  while($wf3 = $wf3_q->fetch_assoc()) { ?>
															<tr>
																<td><?php echo $wf3['stage']; ?></td>
																<td><?php echo $wf3['description']; ?></td>
																<td>
																	<?php $actions3 = json_decode($wf3['actions'],true); $w3=1; $c3=count($actions3); if($c3>0){ foreach($actions3 as $action3){ ?>
																	<a href="#"><?php echo $action3['label']; ?></a><?php echo $w3<$c3?",":""; ?> &nbsp;		
																	<?php $w3++;	} } else { echo "No Actions"; }
																	
																	
																	if($c3<4){?>
																	<span class="btn btn-minier addActionBtn" data-target="#myModal222" data-toggle="modal" ><i class="fa fa-plus"></i> add action</span>
																	<?php } ?>
																	<span class="btn btn-minier addActionBtn"><a href="new.php?doc_id=<?php echo $ro_data2['id'];?>&edit=<?php echo $wf3['id']; ?>">
																	<i class="fa fa-edit"></i>Edit
																    </a></span>
																	<span class="btn btn-minier addActionBtn">
                                                                     <a href="#" class="delete-item" data-tbl="document_type_stages" data-id="<?php echo $wf3['id']; ?>" title="Delete Position">
																    <i class="ace-icon fa fa-trash bigger-120 red"></i>
															         </a>Delete</span>
															     </td>
															</tr>
															
															<?php }   } else {   ?>
														
															<tr>
																<td>Processing</td>
																<td>None</td>
																<td>
																<?php $actions2 = json_decode($ro_data2['workflow'],true); $w2=1; $c2=count($actions2); foreach($actions2 as $action2){ ?>
																	<a href="#"><?php echo $action2['label']; ?></a><?php echo $w2<$c2?",":""; ?> &nbsp;		
																	<?php $w2++;	} ?>
																</td>			
															</tr>
															
															<?php } ?>
														
															
														</tbody>
													</table>
													<div class="col-sm-12 releaseBtnClass">
														<a class="btn btn-minier addStageBtn" wf-id="<?php echo $unit_id; ?>"  data-target="#myModal333" ><i class="fa fa-plus"></i> add workflow stage</a>
													</div>
													</div>
												</div> <!-- /.widget-body -->
												</div> <!-- /.widget-box -->
												</div> <!-- /.tab-pane -->
												
											<?php } ?> 
												
											</div>
										</div>

										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									
									
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		
		<div class="modal fade" id="myModal333" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			<form id="formWF" action="?new" method="post">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add New Workflow Stage</h4>
			  </div>
			  										
			  <div class="modal-body modal-widget-body">
				
					<input type="hidden" name="action" value="ADD STAGE" />
					<input type="hidden" name="table" value="document_type_stages" />
					<input type="hidden" class="doc_type_id" name="data[doc_type_id]" value="" />
				
				<p>Stage Name</p>
				<input name="data[stage]" type="text" value="" class="form-control" required  />
													
				<p>Stage Description</p>
				<textarea name="data[description]" class="form-control" ></textarea>
				
				<p>Step Time (Hrs:Mins:Secs)<a data-rel="tooltip" data-original-title="Maximum expected time at this stage"><i class="fa fa-question-circle"></i></a></p>
				<p>
					<div class="input-group bootstrap-timepicker">
						<input name="data[duration]" class="form-control timepicker" type="text" value="<?php echo $ro_data2['duration_escalate']; ?>" />
						<span class="input-group-addon">
							<i class="fa fa-clock-o bigger-110"></i>
						</span>
					</div>
				</p>
													
				<!-- end template -->
				
			  </div>
			  
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success form-submit1" >Submit</button>
			  </div>
			  
			  </form>
			  
			</div>
			
		  </div>
		</div> 
		
		
		<div class="modal fade" id="myModal222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			<form id="formWF" action="?new" method="post">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">New Action Button</h4>
			  </div>
			  										
			  <div class="modal-body modal-widget-body">
			  
				
					<input type="hidden" name="action" value="ADD STAGE" />
					<input type="hidden" name="table" value="document_type_stages" />
					<input type="hidden" class="doc_type_id" name="data[doc_type_id]" value="" />
				
				<p>Action Settings: </p>
				<textarea name="data[stage]" class="form-control" required ></textarea>
													
				
				<!-- start define template -->
				<div class="form-group" style="overflow:hidden;">
					<label for="form-field-template">Action Form Template Type</label><br/>
					
					<select class="form-control set-temp-type" name="data[temp_type]" >
						<option value="none" >Default</option>
						<option value="form" >Form elements</option>
						<option value="spreadsheet" >Spreadsheet</option>
						<option value="attachments" >Attachments</option>
					</select>
				</div>
				
				<div class="form-group form-div" style="display:none" >
					<label>Form Elements</label>
					<table class="table table-bordered sheet form-elements">
						<thead class="thin-border-bottom">
							<tr>
								<th>Label</th>
								<th>Field</th>
								<th>Default<br>Value(s)</th>
								<th>Required</th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td><input type="text" placeholder="Field Label" name="template[form][label][]" value="" /></td>
								<td>
									<select name="template[form][field][]">
										<option value="text" >Short Text</option>
										<option value="number" >Number</option>
										<option value="date" >Date</option>
										<option value="textarea" >Large text</option>
										<option value="select" >Single Select</option>
										<option value="multiselect" >Multiple Select</option>
									</select>
								</td>
								<td><input type="text" placeholder="" name="template[form][default][]" value="" /></td>
								<td>
									<select name="template[form][required][]" >
										<option value="required" >Yes</option>
										<option value="" >No</option>
									</select>
								</td>
								<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>
							</tr>
									
						</tbody>
					</table>	
					<div class="col-sm-12 releaseBtnClass">
						<span class="btn btn-minier addRowBtn"><i class="fa fa-plus"></i> add form field</span>
					</div>														
				</div>
				
				<div class="form-group spreadsheet-div" style="display:none;" >		
					<div style="margin-top:10px;" >															
						<div class="col-sm-4">
							<input type="text" class="form-control cols" placeholder="No. of columns" />
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control rows" placeholder="No. of rows" />
						</div>
						<div class="col-sm-4">
							<a class="btn btn-sm make-spreadsheet"> generate</a>
						</div>
						
					</div>
					<div class="s-sheet" style="overflow: scroll; min-width: 250px;">
					
					</div>
				</div>	
				
				<!-- end template -->
				
			  </div>
			  
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success form-submit1" >Submit</button>
			  </div>
			  
			  </form>
			  
			</div>
			
		  </div>
		</div> 
		
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/jquery.bootstrap-duallistbox.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		<script src="assets/plugins/jconfirm/js/jquery-confirm.js" ></script>
	
		

<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
	var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
	var container1 = demo1.bootstrapDualListbox('getContainer');
	container1.find('.btn').addClass('btn-white btn-info btn-bold');

	 var $sidebar = $('.sidebar').eq(0);
	 if( !$sidebar.hasClass('h-sidebar') ) return;
	
	 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
		if( event_name !== 'sidebar_fixed' ) return;
	
		var sidebar = $sidebar.get(0);
		var $window = $(window);
	
		//return if sidebar is not fixed or in mobile view mode
		var sidebar_vars = $sidebar.ace_sidebar('vars');
		if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
			$sidebar.removeClass('lower-highlight');
			//restore original, default marginTop
			sidebar.style.marginTop = '';
	
			$window.off('scroll.ace.top_menu')
			return;
		}
	
	
		 var done = false;
		 $window.on('scroll.ace.top_menu', function(e) {
	
			var scroll = $window.scrollTop();
			scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
			if (scroll > 17) scroll = 17;
	
	
			if (scroll > 16) {			
				if(!done) {
					$sidebar.addClass('lower-highlight');
					done = true;
				}
			}
			else {
				if(done) {
					$sidebar.removeClass('lower-highlight');
					done = false;
				}
			}
	
			sidebar.style['marginTop'] = (17-scroll)+'px';
		 }).triggerHandler('scroll.ace.top_menu');
	
	 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
	
	 $(window).on('resize.ace.top_menu', function() {
		$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
	 });
	 
	 $('[data-rel=tooltip]').tooltip();

	 
	 $('body').on('change','.set-temp-type',function(){ 
		var $ths = $(this);
		var widgetContainer = $ths.parents('.modal-widget-body');
		var temp_type =  $('option:selected', $ths).val();
		widgetContainer.find('div.form-div, div.spreadsheet-div').hide();

		if(temp_type=="form"){
			widgetContainer.find('div.form-div').show();
		}
		if(temp_type=="spreadsheet"){
			widgetContainer.find('div.spreadsheet-div').show();
		}
			
	 });
	 
	 $("body").on("click","a.make-spreadsheet", function(e) {
		e.preventDefault();
		var widgetContainer = $(this).parents('.modal-widget-body');
		var sheetContainer = $(this).parents('.spreadsheet-div');
		var cols = sheetContainer.find('input.cols').val();
		var rows = sheetContainer.find('input.rows').val();
		var table = '<form class="doc-template"><table class="table-spreadsheet" >';
		for(i=0;i<=rows;i++){
			table += "<tr>";
			if(i==0){
				var cell_col = 'A';
				for(j=0;j<=cols;j++){
					if(j==0){ table+='<th class="heading">&nbsp;</th>'; }
					else { table+='<th>'+cell_col+'</th>'; cell_col= String.fromCharCode(cell_col.charCodeAt(0) + 1) ; } 
				}
			} else{
				for(j=0;j<=cols;j++){
					if(j==0){ table+='<td class="heading" >'+i+'</td>'; }
					else { table+='<td><input type="text" name="r['+i+']['+j+']" /></td>'; }
				}
			}	
			table += "</tr>";
		}
		table+='</table></form>';
		widgetContainer.find('div.s-sheet').html(table).show();	
	});
	 
	 $('body').on('click','.addRowBtn',function(){
		var widgetContainer = $(this).parents('.modal-widget-body');
		var newRow = '<tr>'+
		'	<td><input type="text" placeholder="Field Label" name="field[label][]"/></td>'+
		'	<td>'+
		'		<select name="">'+
		'			<option value="text">Short Text</option>'+
		'			<option value="number">Number</option>'+
		'			<option value="date">Date</option>'+
		'			<option value="textarea">Large text</option>'+
		'			<option value="select">Options</option>'+
		'		</select>'+
		'	</td>'+
		'	<td><input type="text" placeholder="" name="field[label][]"/></td>'+
		'	<td><input type="checkbox" class="col-sm-12" name="field[required][]"/></td>'+
		'</tr>';
		widgetContainer.find('.form-elements tbody').append(newRow);

	});
	
	$('body').on('click','.addFilterBtn',function(){
		var widgetContainer = $(this).parents('.widget-body');
		var newRow = '<tr>'+
					'<td>'+
					'	<select class="sendFilterType" name="senders[field][]">'+
					'		<option value="" >No Filter</option>'+
					'		<option value="id" >User</option>'+
					'		<option value="job_title_id" >Position</option>'+
					'		<option value="station_id" >Branch</option>'+
					'		<option value="department_id" >Department</option>'+
					'	</select>'+
					'</td>'+
					'<td>'+
					'	<select class="sendFilterVals" name="senders[value][]">'+
					'		<option value="" ></option>'+
					'	</select>'+
					'</td>'+
					'<td>'+
					'	<select name="senders[operator][]" >'+
					'		<option value="" ></option>'+
					'		<option value="AND" >AND</option>'+
					'		<option value="OR" >OR</option>'+
					'	</select>'+
					'</td>'+
					'<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'+
					'</tr>';
		widgetContainer.find('.init-filters tbody').append(newRow);

	});
	
	
	$("body").on("change",".sendFilterType", function(e) {
		var $ths = $(this);
		var widgetContainer = $ths.parents('tr');
		var filter_type =  $('option:selected', $ths).val();
		
		var selct = "";
					
		if(filter_type=="id"){
			selct = '<option value="" > - select user - </option>';
			var usrData = $.parseJSON(usrList());
			$.each(usrData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.nme+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="job_title_id"){
			selct = '<option value="" > - select title - </option>';
			var jobData = $.parseJSON(jobList());
			$.each(jobData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.job_title+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="station_id"){
			selct = '<option value="" > - select branch - </option>';
			var brcData = $.parseJSON(brcList());
			$.each(brcData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.duty_station+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="department_id"){
			selct = '<option value="" > - select dept - </option>';
			var depData = $.parseJSON(depList());
			$.each(depData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.department+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		
	});
	
	
	$("body").on("click","a.delete-doc", function(e) {
		e.preventDefault();
		var $this = $(this);
		$.confirm({
			title: 'Delete Document Type',
			content: 'Are you sure you want to delete this document type?', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var $id = $this.attr('data-id');
						var $tbl = $this.attr('data-tbl');
						var tab_div = 'li'+$id;
						var content_div = 'home'+$id;
						var dataString = 'table='+$tbl+'&item_id='+$id;
						
						$.ajax({
							type: "POST",
							url: "include/ajax_delete.php",
							data: dataString,
							success: function(data) {
							  if(data=="DELETED SUCCESSFULLY"){
								$('#'+tab_div).remove();
							    $('#'+content_div).remove();
								$('#myTab3 li').first().addClass("active");
								$('.tab-content div.tab-pane').first().addClass("active");
							  } else {
								$.alert("<b>ERROR!</b>Delete failed!!!");  
							  }
							},
							error: function(err){
								$.alert("Errrrrrrrrrr");
							}
						  }); 
					}
				},
				cancel: function () {
					$.alert('Delete <strong>canceled</strong>');
				}
			}

		});
		
	});
	
	$("body").on("click","a.save-doc", function(e) {
		e.preventDefault();
		var $this = $(this);
		$.confirm({
			title: 'Save Document Type',
			content: 'Confirm SAVE? NOTE: No UNDO function for this action.', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var doc_id = $this.attr("doc-id");
						var formId = "form"+doc_id;
						$('#'+formId).submit();
						//save doc info
						
						/*var dataString = $('#'+formId).serialize();
						
						$.ajax({
							type: "POST",
							url: "include/do_edit.php",
							data: dataString,
							success: function(data) {
							  alert(data);
							},
							error: function(err){
								alert(JSON.stringify(err));  
							}
						  }); */
						//save template info
						
						//save workflow settings
												
						
					}
				},
				cancel: function () {
					$.alert('SAVE <strong>canceled</strong>');
				}
			}

		});
		
	});
		

	$('body').on('click','.addStageBtn',function(){
		var wfId = $(this).attr("wf-id");	
		var mdal = $(this).attr("data-target");
		$(mdal).find('input.doc_type_id').val(wfId);
		$(mdal).modal('show');
	});	
		
	/*$('body').on('click','.form-submit1',function(){
		$('#formWF').submit();
	}); */
	
	$('body').on('click','.remove-row',function(){
		$(this).parents('.table-row').remove();
	});

	 
	 $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		
	// picture upload
	$('#pic_upclose').ace_file_input({
			style:'well',
			btn_choose:'Click to upload Scanned Copy',
			btn_change:null,
			no_icon:'ace-icon fa fa-picture-o',
			thumbnail:'large',
			droppable:true,
			
			allowExt: ['jpg', 'jpeg', 'png', 'gif'],
			allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
	});	
			
});
</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>

	</body>
</html>
