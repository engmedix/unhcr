<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Services</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		.table-row{ margin-bottom:10px; overflow:hidden;}
		.table-row span.remove-row{display:none; position:absolute; cursor:pointer; left:19px;}
		.table-row:hover span.remove-row{display:inline-block;}
			span.remove-row:hover{color:red;}
		.row-container .rows > .table-row:last-child{display:none;}
		.row-container>.releaseBtnClass{displaysd:none;}
		.edit_form_inactive input,.edit_form_inactive select,.edit_form_inactive textarea,.edit_form_inactive button,.edit_form_inactive .releaseBtnClass,edit_form_inactive .addRowBtn{display:none}
		
		.edit_form_action .cancel_{display:inline-block;}
		.edit_form_action .edit_{display:none;}
		.edit_form_inactive .edit_form_action .cancel_{display:none;}
		.edit_form_inactive .edit_form_action .edit_{display:inline-block;}
		
		form small{display:none; border-bottom:thin dotted #ccc;}
		.edit_form_inactive small{display:block;}
		
		a{cursor:pointer;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									
									
									<?php
										$action = $_POST['action'];

										if($action == 'NEW SERVICE'){ include("include/do_insert.php"); }
										elseif($action == 'UPDATE SERVICE') include("include/do_edit.php");
										
										
										$department_options = '';
										$qry = "SELECT * FROM units";
										$result = $conn->query($qry);
										while($row = $result->fetch_assoc()){
											$department_options .= "<option value='{$row['id']}'>{$row['department']}</option>";
										}
									
									?>

									
									<div class="col-sm-7">
										<!-- #section:elements.tab.position -->
										<div class="tabbable tabs-left widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Services</h4>

												
											</div>
											<ul class="nav nav-tabs nav-tabs2" id="myTab3">
											
											<?php $form_query = "SELECT * FROM services ORDER BY department, serviceName"; 
												  $form_result = $conn->query($form_query);
												  while($ro_data = $form_result->fetch_assoc()) {   ?>	
												<li <?php if($ro_data['serviceId']==1) echo 'class="active"'; ?>>
													<a data-toggle="tab" href="#home<?php echo $ro_data['serviceId']; ?>" >
														<i class="pink ace-icon fa fa-file bigger-110"></i>
														<?php echo $ro_data['serviceName']; ?>
													</a>
												</li>
											<?php } ?>
												
											</ul>

											<div class="tab-content">
											
											<?php $form_query2 = "SELECT services.*,_sub.subs,units.department dept FROM services LEFT JOIN (SELECT GROUP_CONCAT(id,':::',processName,':::',requirements,':::',delivery_time) subs, serviceId FROM subprocesses GROUP BY serviceId ) _sub USING(serviceId) LEFT JOIN units ON services.department = units.id ORDER BY department, serviceName"; 
												  $form_result2 = $conn->query($form_query2);
												  while($ro_data2 = $form_result2->fetch_assoc()) { $serviceId = $ro_data2['serviceId'];  ?>	
												<div id="home<?php echo $serviceId; ?>" class="tab-pane edit_form_inactive <?php if($ro_data2['serviceId']==1) echo "in active"; ?>">
													<div class="widget-main padding-6 no-padding-left no-padding-right">
													<a class="edit_form_action"><i class="fa fa-edit"></i> <span class="edit_">Edit Service</span><span class="cancel_">Cancel Edit</span></a>
													<form method="post" action="" >

														<div class="form-group">
															<label for="form-field-username">Service</label>
															<div>
																<input type="text" class="form-control" id="form-field-username"  name="data[serviceName]" placeholder="Service" value="<?php echo $ro_data2['serviceName'] ?>"  />
																<small class=""><?php echo $ro_data2['serviceName'] ?></small>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-department">Department</label>
															<div>
																<select class="form-control" id="form-field-department"  name="data[department]">
																	<option value="<?php echo $ro_data2['department'] ?>"><?php echo $ro_data2['dept'] ?></option>
																	<?php echo $department_options ?>
																</select>
																<small><?php echo $ro_data2['dept'] ?></small>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Service description</label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[service_desc]" rows="3" placeholder="Service description" ><?php echo $ro_data2['service_desc'] ?></textarea>
																<small><?php echo $ro_data2['service_desc'] ?></small>
															</div>
														</div>
														<div class="form-group" style="overflow:hidden;">
															
															<div class="col-sm-6">
																<label for="form-field-turnaround">Turnaround time <em>(days)</em></label>
																<input class="form-control" id="form-field-turnaround"  name="data[turnaround_time]" rows="3" placeholder="Turnaround time" value="<?php echo $ro_data2['turnaround_time'] ?>" />
																<small><?php echo $ro_data2['turnaround_time'] ?></small>
															</div>
															<div class="col-sm-6">
																<label for="form-field-cut">Cut off time <em>(days)</em></label>
																<input class="form-control" id="form-field-cut"  name="data[cutoff_time]" rows="3" placeholder="Cut off time" value="<?php echo $ro_data2['cutoff_time'] ?>"/>
																<small><?php echo $ro_data2['cutoff_time'] ?></small>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Requirements <em>(Separate with commas)</em></label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[requirements]" rows="3" placeholder="Requirements" ><?php echo $ro_data2['requirements'] ?></textarea>
																<small><?php echo $ro_data2['requirements'] ?></small>
															</div>
													
														</div>
														<div class="form-group">
															<label for="form-field-username">Customer</label>
															<div>
																<input type="text" class="form-control" id="form-field-username"  name="data[customer]" placeholder="Customer" value="<?php echo $ro_data2['customer'] ?>"  />
																<small><?php echo $ro_data2['customer'] ?></small>
															</div>
														</div>
														<div class="form-group" style="overflow:hidden;">
															<label for="form-field-template">Template</label><br/>
															<span>Create template as </span>
															<select name="data[temp_type]" id="temp-type">
																<option selected="selected" disabled><?php echo $ro_data2['temp_type'] ?></option>
																<option>HTML</option>
																<option>Form elements</option>
																<option>Attachment</option>
															</select>
															<small><?php echo $ro_data2['temp_type'] ?></small>
															<div style="margin-top:10px;">
																<span><em class="temp-desc grey"></em></span>
																<div class="template-container">
																	<textarea class="form-control"  name="data[template]" rows="3" placeholder="Template box" ><?php echo $ro_data2['template'] ?></textarea>
																	<small><?php echo $ro_data2['template'] ?></small>
																</div>
																
															</div>
														</div>
														<label>Sub processes</label>
														<div class="form-group row-container" style="overflow:hidden;">
															<div class="rows">
																<div class="table-row" style="overflow:hidden;" >
																	<div class="col-sm-4">Process</div>
																	<div class="col-sm-3">Delivery time</div>
																	<div class="col-sm-5">Requirements</div>
																</div>
																
																<?php 
																$services_ = explode(',',$ro_data2['subs']);
																foreach($services_ as $row_service){
																	$rservice = explode(':::',$row_service);
																	
																	echo "
																<div class='table-row' >
																	<span class='remove-row'><i class='fa fa-close'></i></span>
																	<input type='hidden' name='subprocesses_count[]' value='1' />
																	<input type='hidden' name='subprocesses[serviceId][]' value='{$serviceId}' />
																	<!--<input type='hidden' name='subprocesses[id][]' value='{$rservice[0]}' />-->
																	<div class='col-sm-4'><input type='text' class='col-sm-12' placeholder='Process' name='subprocesses[processName][]' value='{$rservice[1]}'/><small>{$rservice[1]}</small></div>
																	<div class='col-sm-3'><input type='text' class='col-sm-12' placeholder='Time' name='subprocesses[delivery_time][]' value='{$rservice[3]}'/><small>{$rservice[3]}</small></div>
																	<div class='col-sm-5'><input type='text' class='col-sm-12' placeholder='Requirements' name='subprocesses[requirements][]' value='{$rservice[2]}'/><small>{$rservice[2]}</small></div>
																</div>	
																	";
																}
																?>
																
																<div class="table-row" >
																	<span class="remove-row"><i class="fa fa-close"></i></span>
																	<input type="hidden" name="subprocesses_count[]" value="1" />
																	<input type="hidden" name="subprocesses[serviceId][]" value="<?php echo $serviceId ?>" />
																	<!--<input type="hidden" name="subprocesses[id][]" value="" />-->
																	<div class="col-sm-4"><input type="text" class="col-sm-12" placeholder="Process" name="subprocesses[processName][]"/></div>
																	<div class="col-sm-3"><input type="text" class="col-sm-12" placeholder="Time" name="subprocesses[delivery_time][]"/></div>
																	<div class="col-sm-5"><input type="text" class="col-sm-12" placeholder="Requirements" name="subprocesses[requirements][]"/></div>
																</div>
																
																
															</div>
															
															<div class="col-sm-12 releaseBtnClass">
																<span class="btn btn-minier addRowBtn"><i class="fa fa-plus"></i> add</span>
															</div>
															
														</div>
														
															
														
															<button class="btn btn-primary" style="margin-top:20px;" >
																<i class="ace-icon fa fa-floppy-o bigger-120"></i>
																Save Changes
															</button>
														
														<input type="hidden" name="acid" value="<?php echo $ro_data2['serviceId']; ?>" />
														<input type="hidden" name="f_id" value="serviceId" />
														
														<input type="hidden" name="action" value="UPDATE SERVICE" >	
														<input type="hidden" name="table" value="services">
														
													</form>
												</div>
													<div class="hr hr-dotted"></div>
													
													
												</div>
											<?php } ?> 
												
											</div>
										</div>

										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									<div class="col-sm-5">
										
										
										<!-- #section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Add New Service</h4>
												
											</div>

											<div class="widget-body" style="background-color: #f9f9f9; padding-left:10px; padding-right:10px;">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													<form method="post" action="" >

														<div class="form-group">
															<label for="form-field-username">Service</label>
															<div>
																<input type="text" class="form-control" id="form-field-username"  name="data[serviceName]" placeholder="Service"  />
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Department</label>
															<div>
																<select class="form-control" id="form-field-username"  name="data[department]">
																	<?php echo $department_options; ?>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Service description</label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[service_desc]" rows="3" placeholder="Service description" ></textarea>
															</div>
														</div>
														<div class="form-group" style="overflow:hidden;">
															
															<div class="col-sm-6">
																<label for="form-field-turnaround">Turnaround time <em>(days)</em></label>
																<input class="form-control" id="form-field-turnaround"  name="data[turnaround_time]" rows="3" placeholder="Turnaround time" />
															</div>
															<div class="col-sm-6">
																<label for="form-field-cut">Cut off time <em>(days)</em></label>
																<input class="form-control" id="form-field-cut"  name="data[cutoff_time]" rows="3" placeholder="Cut off time"/>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Requirements <em>(Separate with commas)</em></label>
															<div>
																<textarea class="form-control" id="form-field-username"  name="data[requirements]" rows="3" placeholder="Requirements" ></textarea>
															</div>
															
															
															
															
															
															
															
														</div>
														<div class="form-group">
															<label for="form-field-username">Customer</label>
															<div>
																<input type="text" class="form-control" id="form-field-username"  name="data[customer]" placeholder="Customer"  />
															</div>
														</div>
														<div class="form-group" style="overflow:hidden;">
															<label for="form-field-template">Template</label><br/>
															<span>Create template as </span>
															<select name="data[temp_type]" id="temp-type">
																<option value="">None</option>
																<option>HTML</option>
																<option>Form elements</option>
																<option>Attachment</option>
															</select>
															<div style="margin-top:10px;">
																<span><em class="temp-desc grey"></em></span>
																<div class="template-container">
																	
																</div>
																
															</div>
														</div>
														<label>Sub processes</label>
														<div class="form-group row-container" style="overflow:hidden;">
															<div class="rows">
																<div class="table-row" style="overflow:hidden;" >
																	<div class="col-sm-4">Process</div>
																	<div class="col-sm-3">Delivery time</div>
																	<div class="col-sm-5">Requirements</div>
																</div>
																<div class="table-row" >
																	<span class="remove-row"><i class="fa fa-close"></i></span>
																	<input type="hidden" name="subprocesses_count[]" value="1" />
																	<input type="hidden" name="subprocesses[serviceId][]" value="" />
																	<!--<input type="hidden" name="subprocesses[id][]" value="" />-->
																	<div class="col-sm-4"><input type="text" class="col-sm-12" placeholder="Process" name="subprocesses[processName][]"/></div>
																	<div class="col-sm-3"><input type="text" class="col-sm-12" placeholder="Time" name="subprocesses[delivery_time][]"/></div>
																	<div class="col-sm-5"><input type="text" class="col-sm-12" placeholder="Requirements" name="subprocesses[requirements][]"/></div>
																</div>
															</div>
															
															<div class="col-sm-12 releaseBtnClass">
																<span class="btn btn-minier addRowBtn"><i class="fa fa-plus"></i> add</span>
															</div>
															
														</div>
														
															
														
															<button class="btn btn-primary" style="margin-top:20px;" >
																<i class="ace-icon fa fa-floppy-o bigger-120"></i>
																Save Service
															</button>
														
														<input type="hidden" name="action" value="NEW SERVICE" >	
														<input type="hidden" name="table" value="services">
														
													</form>
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
									</div>
									
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>
								
								<div style="display:none;">
									<form name="frmPhoto" enctype="multipart/form-data" method="post" id="target"></form>
								</div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			
			

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/inc/ajaxupload.js"></script>
		
		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			 
			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
			// picture upload
			$('#pic_upclose').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Scanned Copy',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,
					
					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});	
			
			});
			
			$(document).ready(function(){
				//control for adding new row item
				$('body').on('click','.addRowBtn',function(){
					var newRowHtml = '<div class="table-row">'+$(this).parents('.row-container').find($('.table-row')).last().html()+'</div>';
					$(this).parents('.row-container').find($('.table-row')).last().after(newRowHtml);
				});
				$('body').on('click','.remove-row',function(){
					$(this).parents('.table-row').remove();
				});
				$('body').on('change','#temp-type',function(){
					
					var $type = $(this).val()
					var $html = ''
					var $tempInfo = ''
					
					if($type == 'HTML' || $type == 'Form elements'){
						$html = '<textarea class="form-control"  name="data[template]" rows="3" placeholder="Template box" ></textarea>';
						$tempInfo = (($type=='HTML')?'Paste HTML content in textbox': 'Format: input_name;input_type;input_size. <b>*Separate elements with commas*</b>');
					}
					else if($type == 'Attachment'){
						$html = '<span id="imageFrame" class="user-image-circular-big formImageInput"  style="">' +
									'<input type="hidden" value="" name="data[template]" />' +
								'</span>' +
								'<input type="file" name="file" id="inputElement" class="uploadimg services imageFrame inputElement" style="float:left;" accept="*" capture />'
						$tempInfo = 'Browse attachement';
					}
					
					$(this).parents('.form-group').find('.temp-desc').html($tempInfo);
					$(this).parents('.form-group').find('.template-container').html($html);
				});
				
				// control for image upload
				$('body').on('change','.uploadimg',function(){
					var selObj = $(this).attr('class').split(' ');
					var frm = selObj[1]; var ele = selObj[3]; var plan = selObj[2]; 
					uploadImage('include/att_uploader.php?form='+frm,ele,plan);
				});
				
				$('body').on('click','.edit_form_action',function(){
					if($(this).parents('.tab-pane').hasClass('edit_form_inactive')){
						$('.tab-pane').addClass('edit_form_inactive');
					}
					$(this).parents('.tab-pane').toggleClass('edit_form_inactive');
				})
				
			})
			
			
		</script>
		
				
	</body>
</html>



















