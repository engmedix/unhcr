<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php"); if(isset($_GET['ac'])){ $acid=$_GET['ac']; } else { echo '<script>window.location.href="accounts.php";</script>'; }
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Details</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="assets/css/excel.css" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		.widget-header{
			background: rgba(107,107,107,.1) !important;
		}
			.widget-header,.widget-header h5{font-size:10px;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->

		<script type="text/javascript">

		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function addCommas(nStr) {

		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }

		  return x1 + x2;

		}

		document.ready



		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">

					<?php if(isset($_GET['update'])){ include('include/do_edit.php');  } ?>

					<?php if(isset($_GET['ac'])){
							$form_query = "SELECT documents.*,admins.fname,admins.lname,admins.oname,units.department, document_types.workflow FROM documents LEFT JOIN admins ON documents.sender_id = admins.id LEFT JOIN units ON department_id = units.id LEFT JOIN document_types ON documents.doc_type_id=document_types.id WHERE documents.id='$acid'";
							$form_result = $conn->query($form_query);
							$form_data = $form_result->fetch_assoc();
					?>

						<div class="row">
									<div class="col-sm-7">
										<h3 class="header smaller lighter blue"><?php echo $form_data['document_type']; ?></h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>
											<?php if($form_data['capture_method'] == 'hc' && $form_data['doc_classification'] != 'OUTGOING'){?>
											<div >

												<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Date submitted </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo toDate($form_data['date']); ?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Document Type </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo $form_data['document_type']; ?></span>
														<span class="editable"><?php echo $form_data['security_screening']; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Submitted By </div>

													<div class="profile-info-value">
														<span class="editable" id="country"><?php echo $form_data['submitted_by']; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> From </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo $form_data['organisation']; ?></span>
													</div>
												</div>

												<?php if($form_data['source'] != ''){ ?>
												<div class="profile-info-row">
													<div class="profile-info-name"> Source of Correspondence </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo $form_data['source']; ?></span>
													</div>
												</div>
												<?php } ?>

												<?php if($form_data['phone'] != ''){ ?>
												<div class="profile-info-row">
													<div class="profile-info-name"> Phone </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo $form_data['phone']; ?></span>
													</div>
												</div>
												<?php } ?>

												<div class="profile-info-row">
													<div class="profile-info-name"> Email </div>

													<div class="profile-info-value">
														<span class="editable"><?php echo $form_data['email']; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Document Ref number </div>

													<div class="profile-info-value">
														<span class="editable" id="age"><?php echo $form_data['doc_ref_number']; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> System Ref number </div>

													<div class="profile-info-value">
														<span class="editable" id="age"><?php echo $form_data['internal_ref_number']; ?></span>
													</div>
												</div>

												<?php if($form_data['atlas_number'] != ''){ ?>

												<div class="profile-info-row">
													<div class="profile-info-name"> PO/LOC Number </div>

													<div class="profile-info-value">
														<span class="editable" id="signup"><?php echo $form_data['atlas_number']; ?></span>
													</div>
												</div>
												<?php } ?>

												<?php if($form_data['template'] != ''){ ?>
												<div class="profile-info-row">
													<div class="profile-info-name"> Details  </div>

													<div class="profile-info-value">
														<span class="editable">
															<a class="btn btn-sm btn-default" data-target="#myModal333" data-toggle="modal" >
																Click to view
															</a>

															<?php if(in_array($my['job_title_id'], array('7'))){?>
															<a class="btn btn-sm btn-info" data-target="#myModal444" data-toggle="modal" >
																<i class="fa fa-edit"></i> EDIT
															</a>
															<?php } ?>

														</span>
													</div>
												</div>
<?php //var_dump($form_data);?>
												<div class="modal fade" id="myModal333" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												  <div class="modal-dialog">
													<div class="modal-content">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title" id="myModalLabel"><?php echo $form_data['document_type']; ?></h4>
													  </div>
													  <div class="modal-body">

														<?php
														if($form_data['temp_type']=="html"){ echo $form_data['template']; }
														if($form_data['temp_type']=="form"){
															$a = json_decode($form_data['template'],true);
															foreach($a as $key=>$val){
																if($key!="template"){
																	echo str_replace("_"," ",$key).": <b>";
																	if(is_numeric($val)){ echo number_format($val); } else { echo $val; }
																	echo "</b><br>";
																}
															}
														}

														if($form_data['temp_type']=="spreadsheet"){
															if($form_data['template']!="") {
																$temp_rows = explode("\n",$form_data['template']);
																$count_cols = count(explode(",",$temp_rows[0]));

																echo '<table class="table-spreadsheet" id="spread-sheet" ><tr>';
																$cell_col = 'A';
																for($j=0;$j<=$count_cols;$j++){
																	if($j==0){ echo '<th class="heading">&nbsp;</th>'; }
																	else { echo '<th>'.$cell_col.'</th>'; $cell_col++; }
																}
																echo '</tr>';

																$i=1;
																foreach($temp_rows as $temp_row){
																	$temp_cols = explode(',',$temp_row);
																	$k=0;
																	echo '<tr>';
																	foreach($temp_cols as $temp_col){
																		if($k==0){ echo '<td class="heading" >'.$i.'</td>'; }
																		echo '<td>'.$temp_col.'</td>';
																		$k++;
																	}
																	echo '</tr>';
																	$i++;
																}

																echo '</table>';

																echo '<a href="javascript:none" table="spread-sheet" target="_blank" class="btn btn-sm btn-success export-excel">Export to Excel</a>';
																echo '<a href="javascript:none" table="spread-sheet" target="_blank" class="btn btn-sm btn-info export-csv pull-right">Export to CSV</a>';
															}
														}

														if($form_data['temp_type']=="custom"){

															if($form_data['doc_type_id']=='87'||$form_data['doc_type_id']=='88'||$form_data['doc_type_id']=='89'){

																$docT  = $conn->query("SELECT * FROM document_types WHERE id='".$form_data['doc_type_id']."'");
																$docType = $docT->fetch_assoc();
																$atts = json_decode($docType['template'],true);
																$cols = array();
																foreach($atts as $att){
																	array_push($cols, $att["name"]);
																}
																$exclude = array("appId","document_id","doc_status","loan_rate");
																$cols = array_merge($cols, $exclude);
																if($form_data['doc_type_id']=='89'){
																 $custom_result = $conn->query("SELECT * FROM service_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();
																$db_cols = array_keys($custom_data);
																}
																if($form_data['doc_type_id']=='88'){
																 $custom_result = $conn->query("SELECT * FROM construction_works_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();
																$db_cols = array_keys($custom_data);
																}
																if($form_data['doc_type_id']=='87'){
																 $custom_result = $conn->query("SELECT * FROM goods_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();
																$db_cols = array_keys($custom_data);
																}

																echo '<table class="table table-striped" >';
																//var_dump($db_cols);
																foreach($db_cols as $db_col){
																	if(!in_array($db_col,$cols) AND $custom_data[$db_col]<>""){
																	if($db_col=='branch'){
																		$ctm_result = $conn->query("SELECT * FROM dutystations WHERE id='".$custom_data[$db_col]."'");
																        $cstm_data = $ctm_result->fetch_assoc();
																     $ctm_data =$cstm_data['duty_station'];
																	}elseif($db_col=='client_rel_officer'){
                                                                     $ctm_result = $conn->query("SELECT * FROM admins WHERE id='".$custom_data[$db_col]."'");
																     $cstm_data = $ctm_result->fetch_assoc();
																     $ctm_data =$cstm_data['fname']." ".$cstm_data['lname']." ".$cstm_data['oname'];
																	}else{
                                                                       $ctm_data=$custom_data[$db_col];
																	}
																	echo '<tr>';
																	echo '<td class="heading" >'.ucfirst(str_replace("_"," ",$db_col)).'</td>';
																	echo '<td>'.$ctm_data.'</td>';
																	echo '</tr>';
																	}

																}

																echo '</table>';

															}
														}
														?>

													  </div>
													  <div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													</div>
												  </div>
												</div>


												<div class="modal fade" id="myModal444" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												  <div class="modal-dialog">
													<div class="modal-content">

													  <form action="<?= basename($_SERVER['REQUEST_URI'])."&update"; ?>" method="post">

													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title" id="myModalLabel"><?php echo $form_data['document_type']; ?></h4>
													  </div>
													  <div class="modal-body">

														<?php

														if($form_data['temp_type']=="custom"){

															if($form_data['doc_type_id']=='87'||$form_data['doc_type_id']=='88'||$form_data['doc_type_id']=='89'){
														       $docT  = $conn->query("SELECT * FROM document_types WHERE id='".$form_data['doc_type_id']."'");
																$docType = $docT->fetch_assoc();
																$atts = json_decode($docType['template'],true);
																$cols = array();
																foreach($atts as $att){
																	array_push($cols, $att["name"]);
																}
																$exclude = array("appId","document_id","doc_status");
																$cols = array_merge($cols, $exclude);
																if($form_data['doc_type_id']=='87'){
																$custom_result = $conn->query("SELECT * FROM goods_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();

																}
																if($form_data['doc_type_id']=='88'){
																$custom_result = $conn->query("SELECT * FROM construction_works_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();
																}
																if($form_data['doc_type_id']=='89'){
																$custom_result = $conn->query("SELECT * FROM service_payment_invoice WHERE document_id='".$form_data['id']."'");
																$custom_data = $custom_result->fetch_assoc();
																}
																echo '<input type="hidden" name="table" value="goods_payment_invoice" />';
																echo '<input type="hidden" name="action" value="EDIT INVOICE DETAILS" />';
																echo '<input type="hidden" name="acid" value="'.$form_data['id'].'" />';
																echo '<input type="hidden" name="f_id" value="document_id" />';

																$db_cols = array_keys($custom_data);

																echo '<table class="table table-striped" >';

																foreach($db_cols as $db_col){
																	if(!in_array($db_col,$cols) AND $custom_data[$db_col]<>""){
																	echo '<tr>';
																	echo '<td class="heading" >'.ucfirst(str_replace("_"," ",$db_col)).'</td>';
																	echo '<td><input type="text" class="form-control" name="data['.$db_col.']" value="'.$custom_data[$db_col].'" /></td>';
																	echo '</tr>';
																	}

																}

																echo '</table>';

															}
														}
														?>

													  </div>
													  <div class="modal-footer">
														<button type="submit" class="btn btn-success">SAVE</button>
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													  </div>
													  </form>

													</div>
												  </div>
												</div>



												<?php } ?>

												<div class="profile-info-row hide">
													<div class="profile-info-name"> Remarks </div>

													<div class="profile-info-value">
														<span class="editable" id="signup"><?php echo $form_data['description']; ?></span>
													</div>
												</div>

												<?php if(!in_array($form_data['doc_type_id'], array(87,88,89))) {?>
												<div class="profile-info-row">
													<div class="profile-info-name"> Attachements </div>

													<div class="profile-info-value">
														<span class="editable" id="login">

														<?php if($form_data['file_location'] !='') {
                                                              $attachments=json_decode($form_data['file_location']);
                                                              $file_name=json_decode($form_data['doc_name']);
                                                              $count=count($attachments);
                                                              for($i=0;$i<$count;$i++){
															?>
															<p><?php echo $file_name[$i]?>   <a class="btn btn-sm btn-success" href="<?php echo $attachments[$i]; ?>" target="_blank">
															<i class="ace-icon fa fa-download bigger-110"></i>
															</a></p>
														<?php }}else{ ?>
															<div class="label btn-sm btn-default">
																No attachments
																<i class="ace-icon fa bigger-110"></i>
															</div>
														<?php } ?>

														</span>
													</div>
												</div>
												<?php } ?>

												<div class="profile-info-row ">
													<div class="profile-info-name"> Receipt Status </div>

													<div class="profile-info-value">
														<span class="editable" id="about">
															<?php

																/* if($form_data['current_holder']!=$_SESSION['user']){

																echo '<div class="well well-sm" style="margin-bottom:0px;"> This document is currently with: <br><b>'.getStaffName($form_data['current_holder']).'</b></div>';

																} else { */

																$frst_query = "SELECT * FROM doc_track WHERE id=".$_GET['tId'];
																$frst = $conn->query($frst_query);
																$docStatus = $frst->fetch_assoc();

																$doc = $docStatus['receiver_status'];

																if($doc == 'READ')
																	echo 'Documents Received <i class="ace-icon fa fa-check bigger-110"></i>';
																else
																	echo '
																		<a class="btn btn-sm btn-success receiveDoc">
																			Receive Documents
																		</a>';
																//}
															?>
														</span>
													</div>
												</div>


											<?php
												$atlas = '';
												if($form_data['document_type'] == 'INVOICE' && $_SESSION['grp'] == 0){
													$atlas = '&at=1';
												}
											?>



												<div class="profile-info-row showActions" <?php if($doc != 'READ') echo 'style="display:none"'; ?> >
													<div class="profile-info-name "> Actions </div>
													<?php
                          $docType_array=[87,88,89];
													if(!in_array($form_data['doc_type_id'],$docType_array)){
													?>
													<div class="profile-info-value">
													 <div class="clearfix">
														 <a href="new.php?type=action&a=update&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId'].$atlas; ?>" class="btn btn-sm btn-success">Update Status</a>
														 <a href="new.php?type=action&a=forward&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId'].$atlas; ?>" class="btn btn-sm btn-info">Forward</a>
														 <a href="new.php?type=action&a=approve&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&fd=<?php echo $form_data['department'] ?>&tId=<?php echo $_GET['tId']; ?>" class="btn btn-sm btn-primary">Approve</a>
														 <a href="new.php?type=action&a=close&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&fd=<?php echo $form_data['department'] ?>&tId=<?php echo $_GET['tId']; ?>" class="btn btn-sm btn-inverse">Approve&Close</a>
														 <a href="new.php?type=action&a=reject&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId']; ?>" class="btn btn-sm btn-danger">Reject</a>
													 </div>
												 </div>
											 <?php }?>
													<div class="profile-info-value">
														<div class="clearfix">

														<?php 	$stmt = $conn->prepare("SELECT * FROM document_type_stages WHERE doc_type_id=? AND stage=?");
                                                                $stmt->bind_param('is',$form_data['doc_type_id'],$form_data['stage_label']);
                                                                $stmt->execute();
	                                                            $wf_q = $stmt->get_result();
	                                                            if($wf_q->num_rows>0){
																while($wf = $wf_q->fetch_assoc()) { $actions1 = json_decode($wf['actions'],true);
										var_dump($actions1);
																foreach($actions1 as $action1){ ?>
																<a href="new.php?type=action&a=<?php echo $action1['type']; ?>&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId'].$atlas; ?>" class="btn btn-sm btn-<?php echo $action1['color']; ?> "><?php echo $action1['label']; ?></a>
																&nbsp;
																<?php } ?>
														<?php }   } else {

														$actions2 = json_decode($form_data['workflow'],true);
														$w=1; $c=count($actions2);
														foreach($actions2 as $action2){ ?>
															<a href="new.php?type=action&a=<?php echo $action2['type']; ?>&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId'].$atlas; ?>" class="btn btn-sm btn-<?php echo $action2['color']; ?> "><?php echo $action2['label']; ?></a>
															&nbsp;
														<?php } ?>

														<?php } ?>

														</div>
													</div>
												</div>
											<?php  ?>

											</div>

               	</div>
							<?php } else {?>
									<div >



														<div class="profile-user-info profile-user-info-striped">
															<div class="profile-info-row">
																<div class="profile-info-name"> Date </div>

																<div class="profile-info-value">
																	<span class="editable"><?php echo toDate($form_data['date']) ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Reference No </div>

																<div class="profile-info-value">
																	<span class="editable" id="country"><?php echo $form_data['internal_ref_number'] ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> From </div>

																<div class="profile-info-value">
																	<span class="editable" id="age"><?php echo $form_data['fname'].' '.$form_data['lname'].' ('.$form_data['department'] ?>)</span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Name of Consignee </div>

																<div class="profile-info-value">
																	<span class="editable" id="signup"><?php echo $form_data['consignee'] ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Recipient Tel Contact </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['recipient_tel'] ?></span>
																</div>
															</div>
															<div class="profile-info-row">
																<div class="profile-info-name"> No. of documents </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['no_of_docs'] ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Attached documents </div>

																<div class="profile-info-value">
																	<span class="editable" id="login">

																	<?php if($form_data['file_location'] !='') {?>

																		<a class="btn btn-sm btn-success" href="<?php echo $form_data['file_location']; ?>" target="_blank">
																			Open/Download Document
																			<i class="ace-icon fa fa-download bigger-110"></i>
																		</a>
																	<?php }else{ ?>
																		<div class="label btn-sm btn-default">
																			No attachments
																			<i class="ace-icon fa bigger-110"></i>
																		</div>
																	<?php } ?>

																	</span>
																</div>
															</div>


															<?php if($form_data['delivered_by'] !='') {?>

															<div class="profile-info-row">
																<div class="profile-info-name"> Delivered by </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['delivered_by'] ?></span>
																</div>
															</div>
															<?php } ?>
															<?php if($form_data['delivery_contact'] !='') {?>

															<div class="profile-info-row">
																<div class="profile-info-name"> Contact </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['delivery_contact'] ?></span>
																</div>
															</div>
															<?php } ?>
															<?php if($form_data['tracking_no'] !='') {?>

															<div class="profile-info-row">
																<div class="profile-info-name"> Courier Tracking no </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['tracking_no'] ?></span>
																</div>
															</div>
															<?php } ?>


															<?php if($form_data['received_by'] !='') {?>

															<div class="profile-info-row">
																<div class="profile-info-name"> Received By </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['received_by'] ?></span>
																</div>
															</div>
															<?php } ?>

															<?php if($form_data['tel'] !='') {?>

															<div class="profile-info-row">
																<div class="profile-info-name"> Receiver tel </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo $form_data['tel'] ?></span>
																</div>
															</div>
															<?php } ?>





															<div class="profile-info-row">
																<div class="profile-info-name"> Actions </div>

																<div class="profile-info-value">
																	<div class="clearfix">

																	<?php if($form_data['delivered_by'] =='') {?>
																		<a href="new.php?type=action&a=dispatch&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId'].$atlas; ?>" class="btn btn-sm btn-info">Dispatch</a>
																	<?php } else echo ''; ?>

																		<a href="new.php?type=action&a=delivery&md=<?php echo $form_data['capture_method'] ?>&ac=<?php echo $form_data['id']; ?>&tId=<?php echo $_GET['tId']; ?>" class="btn btn-sm btn-inverse">Delivery</a>

																	</div>
																</div>
															</div>
														</div>


													</div>

							<?php } ?>
										</div>
									</div>
									<div class="col-sm-5">
										<h3 class="header smaller lighter blue">Document Timeline.
											<div class="pull-right tableTools-container"><button class="print-link no-print" id="doPrint">Print</button></div>
										</h3>
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
                   <div id="printDiv">
										<div id="timeline-1" style="max-height:390px; overflow-y:auto; overflow-x:hidden; ">

									<div class="row">
										<div class="col-xs-12 col-sm-10 col-sm-offset-1">
											<div class="timeline-container">
												<?php

												  $line_query = "SELECT doc_track.*,documents.document_type,documents.doc_ref_number,documents.capture_method, admins.fname,admins.lname,admins.oname, original.fname or_fname,original.lname or_lname, original.department_id or_dept, _to.fname to_fname,_to.lname to_lname, _to.department_id to_dept FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.from_id = admins.id LEFT JOIN admins original ON documents.sender_id = original.id LEFT JOIN admins _to ON doc_track.to_id = _to.id WHERE doc_id = ".$_GET['ac'];
												  $line_result = $conn->query($line_query);

												  $now = new DateTime();
												  $now->setTime( 0, 0, 0 );

												  $index = 0;
												  while($line_data = $line_result->fetch_assoc()) {


														if($line_data['action'] == 'DOCUMENT SENT' || $line_data['action'] == 'REQUEST SENT') {
															$label = ' label-info"';
															$headerMsg = 'Document initiated by '.$line_data['or_fname'].' '. $line_data['or_lname'].' ('.getStaffDept($line_data['to_id']).')';
															$actionMsg = '<span class="grey">Initiated by <a href="#" class="blue">'.$line_data['fname'].'</a> and sent to </span>
																	<a href="#" class="blue">'.$line_data['to_fname'].'</a> ('.getStaffDept($line_data['to_dept']).')';
															$btnClass = ' fa fa-print btn btn-info green ';
															$boxClass = ' alert ';
														}
														elseif($line_data['action'] == 'APPROVED') {
															$label = 'label-inverse';
															$actionMsg = 'Forwarded by <a href="#" class="blue">'.$line_data['fname'].'</a> ('.getStaffDept($line_data['to_id']).')';
															$btnClass = ' fa fa-check btn btn-inverse green ';
															$boxClass = ' alert alert-inverse ';
														}
														elseif($line_data['action'] == 'REJECTED') {
															$label = 'label-danger';
															$actionMsg = 'Forwarded by <a href="#" class="blue">'.$line_data['fname'].'</a>';
															$btnClass = ' fa fa-ban btn btn-danger green ';
															$boxClass = ' alert alert-danger ';
														}
														elseif($line_data['action'] == 'FORWARDED') {
															$label = 'label-info';
															$actionMsg = 'Forwarded by <a href="#" class="blue">'.$line_data['fname'].'</a> to <a href="#" class="blue">'.$line_data['to_fname'].'</a> ('.getStaffDept($line_data['to_id']).')';
															$btnClass = ' fa fa-exchange btn btn-info green ';
															$boxClass = ' alert alert-info ';
														}
														elseif($line_data['action'] == 'DELEGATED') {
															$label = 'label-warning';
															$actionMsg = 'Delegated by <a href="#" class="blue">'.$line_data['fname'].'</a> to <a href="#" class="blue">'.$line_data['to_fname'].'</a> ('.getStaffDept($line_data['to_id']).')';
															$btnClass = ' fa fa-share btn btn-warning green ';
															$boxClass = ' alert alert-warning ';
														}
														else {
															$label = 'label-success';
															$actionMsg = '<a href="#" class="blue">'.$line_data['fname'].'</a> updated status: '.$line_data['action'];
															$btnClass = ' fa fa-pencil-square-o btn btn-success green ';
															$boxClass = ' alert alert-success ';
														}

													?>

											<!-- #section:pages/timeline -->
											 <?php
												if($index == 0){
											 ?>


												<div class="timeline-items">

												<?php
												} ?>


													<!-- #section:pages/timeline.item -->
													<div class="timeline-item clearfix">

														<?php
															if($index == 0){
														 ?>
														<!-- #section:pages/timeline.info -->
														<div class="timeline-info">
															<img alt="User" src="assets/avatars/user.jpg" />
															<span class="label label-info label-sm"><?php //echo toDate($line_data['actionDate']); ?></span>
														</div>
														<?php }

														else{
														?>
														<div class="timeline-info">
															<i class="timeline-indicator ace-icon <?php echo $btnClass; ?>"></i>
														</div>
														<?php } ?>

														<!-- /section:pages/timeline.info -->
														<div class="widget-box <?php echo $boxClass; ?>">
															<div class="widget-header widget-header-small">
																<h5 class="widget-title smaller">
																	<?php echo $actionMsg; ?>
																</h5>
																<span class="widget-toolbar no-border">
																	<i class="ace-icon fa fa-clock-o bigger-110"></i>
																	<?php echo toDate($line_data['actionDate']); ?>
																</span>

															</div>

															<div class="widget-body">
																<div class="widget-main">
																	<?php echo $line_data['remark']; ?>
																</div>
															</div>
															<?php if($line_data['attachment'] !='') {
                                                               $attachments=json_decode($line_data['attachment']);
                                                              $file_name=json_decode($line_data['doc_name']);
                                                              $count=count($attachments);
                                                              for($i=0;$i<$count;$i++){
																?>
																<div class="widget-footer widget-footer-small">
																	<div class="widget-main">
																		<p><?php echo basename($attachments[$i])//$file_name[$i]?>   <a class="btn-sm btn-success" href="<?php echo $attachments[$i]; ?>" target="_blank">
																			<i class="ace-icon fa fa-download bigger-110"></i>
																		</a>
																	</div>
																</div>
															<?php }} ?>


														</div>
													</div>


												  <?php $index ++; } ?>

												</div><!-- /.timeline-items -->
											</div><!-- /.timeline-container -->


											<!-- /section:pages/timeline -->
										</div>
									</div>
								</div>
								</div>
								</div>


								<?php if($form_data['doc_type_id']=='88'||$form_data['doc_type_id']=='87'||$form_data['doc_type_id']=='89'){

									if($form_data['current_holder']!=$_SESSION['user']){

										$readers = array();
										$readers_q = $conn->query("SELECT DISTINCT to_id FROM doc_track WHERE receive_date<>'documents' AND doc_id='".$_GET['ac']."'");
										while($read_data = $readers_q->fetch_assoc()) {
											array_push($readers, $read_data['to_id']);
										}
										if(!in_array($_SESSION['user'],$readers)){ $hide_att=true; }
									} else {

										if($doc!='READ'){ $hide_att=true; }
									}

								?>
									<div class="col-md-12 <?php if(isset($hide_att)){ echo "hide"; } ?>" id="loanAttachemts">

										<h3 class="header smaller lighter blue">ATTACHMENTS
											<!--<a href="download.php?doc=<?php echo $form_data['id']; ?>" target="_blank"><small><i class="fa fa-download-o"></i>Download All</small></a>--></h3>
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>

										<div class="profile-feed row">
										<?php
										$cols = array_diff($cols,$exclude);
										foreach($cols as $col){  $indx = array_search($col, array_column($atts, "name"));
										$fTyp = strtolower(substr(strrchr(basename($custom_data[$col]),'.'),1));
										if(basename($custom_data[$col])<>""){	?>
										<div class="col-sm-4">
													<a href="<?php echo $custom_data[$col]; ?>" target="_blank">
													<div class="profile-activity clearfix">
														<div>
															<img class="pull-left" alt="" src="assets/images/<?= $fTyp; ?>.png">
															<strong> <?php echo $atts[$indx]['label']; ?> </strong><br>
															<?php echo basename($custom_data[$col]); ?>
														</div>
													</div>
													</a>
										</div>
										<?php } } ?>

										</div>
										<?php if($form_data['file_location'] !='' && ($form_data['document_type']=='Goods Payment Invoice'||$form_data['document_type']=='Construction Works Payment Invoice'||$form_data['document_type']=='Service Payment Invoice ')) {
                                         $attachments=json_decode($form_data['file_location']);
                                         $file_name=json_decode($form_data['doc_name']);
                                         $count=count($attachments);
                                         ?>

										<div class="profile-feed row">
										<h4>Additional Attachments</h4>
										<?php for($i=0;$i<$count;$i++){
											$fTyp = strtolower(substr(strrchr(basename($attachments[$i]),'.'),1));
											?>
										<div class="col-sm-4">
													<a href="<?php echo $attachments[$i]; ?>" target="_blank">
													<div class="profile-activity clearfix">
														<div>
															<img class="pull-left" alt="" src="assets/images/<?= $fTyp; ?>.png">
															<strong> <?php echo basename($attachments[$i]); ?> </strong><br>

														</div>
													</div>
													</a>
										</div>
										<?php } ?>
										</div>
										<?php } ?>

										<?php $att2_result = $conn->query("SELECT a.* FROM uploads a LEFT JOIN doc_track b ON a.append_id=b.id WHERE a.append_type='doc_track' AND b.doc_id='".$_GET['ac']."'");

										if($att2_result->num_rows>0){?>
										<div class="profile-feed row">
										<h4>Requested Attachments (Work in Progress)</h4>
										<?php while($att2_data = $att2_result->fetch_assoc()) { $fTyp = strtolower(substr(strrchr(basename($att2_data['file_location']),'.'),1));  ?>
										<div class="col-sm-4">
													<a href="<?php echo $att2_data['file_location']; ?>" target="_blank">
													<div class="profile-activity clearfix">
														<div>
															<img class="pull-left" alt="" src="assets/images/<?= $fTyp; ?>.png">
															<strong> <?php echo $att2_data['description']; ?> </strong><br>
															<?php echo basename($att2_data['file_location']); ?>
														</div>
													</div>
													</a>
										</div>
										<?php } ?>
										</div>
										<?php } ?>

									</div>
								<?php } ?>




								</div><!-- /.row -->
							<?php } ?>



					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">

			jQuery(function($) {
				//$(document).ready(function() {
				//	$("div.timeline-1").scrollTop($("div.timeline-1")[0].scrollHeight);
				//});
			});

			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			 $(document).on('click','.receiveDoc',function(){

				 var thisBtn = $(this);

				 $.post("server_ajax.php?edit", {
					 'data[receiver_status]': "READ",
					 'action': "DOCUMENT RECEIVED",
					 table: "doc_track",
					 acid: <?php echo $_GET['tId']; ?>,
					 f_id: "id"
				 }, function (response) {
					 if(response.indexOf('UPDATED SUCCESSFULLY') > 0){
						 thisBtn.replaceWith('Documents Received <i class="ace-icon fa fa-check bigger-110"></i>');

						 //$('.profile-user-info .profile-info-row').last().after(approvalHTML);
						 $('.showActions').show();
						 $('div#loanAttachemts').removeClass("hide");
					 }

					 console.log(response);
				 });


			 });


			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });


			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			// picture upload
			$('#pic_upclose').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Passport Photo',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,

					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});

			$('#pic_upclose').ace_file_input('show_file_list', [{type: 'image', name: $('#avatar').attr('src')}]);

			$('body').on('click','a.export-excel',function(){
				var tbl = $(this).attr("table");
				export_to(tbl);
			});

			function export_to(tableId){
				var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
				var textRange; var j=0;
				tab = document.getElementById(tableId); // id of table

				for(j = 0 ; j < tab.rows.length ; j++)
				{
					tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
					//tab_text=tab_text+"</tr>";
				}

				tab_text=tab_text+"</table>";
				tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
				tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
				tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE ");

				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
				{
					txtArea1.document.open("txt/html","replace");
					txtArea1.document.write(tab_text);
					txtArea1.document.close();
					txtArea1.focus();
					sa=txtArea1.document.execCommand("SaveAs",true,"save report.xls");
				}
				else                 //other browser not tested on IE 11
					sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

				return (sa);
			}

			});
				document.getElementById("doPrint").addEventListener("click", function() {
				var printContents = document.getElementById('printDiv').innerHTML;
				var originalContents = document.body.innerHTML;
				document.body.innerHTML = printContents;
				window.print();
				document.body.innerHTML = originalContents;
			});
		</script>

				<!-- inline scripts related to this page -->


	</body>
</html>
