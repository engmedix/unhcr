

			<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<a href="https://doctracker.co/" target="_blank">
						<span class="bigger-120 blue">
							DocTracker <span style="font-size:70%;">WorkFlow</span>
							 &copy; <?php echo date('Y'); ?>
						</span>
						</a>
						&nbsp; &nbsp;

					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

<!--Start of Tawk.to Script-->
<!--
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58d5619af97dd14875f59fc6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
-->
<!--End of Tawk.to Script-->
