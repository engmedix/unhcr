<?php
if(!isset($_SESSION)){ session_start(); }
$ths_pg_uri = basename($_SERVER['REQUEST_URI']);
$ths_pg = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);  ?>
		<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini"  style="max-width:400px" >
						<img src="assets/img/doctracker_logo.jpeg" width="200px" height="60px"/>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="<?php if($ths_pg == "psfu" || $ths_pg == "index.php") echo 'active open'; ?> hover">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="<?php if($ths_pg == "documents.php") echo 'active open'; ?> hover">
						<a href="documents.php">
							<i class="menu-icon fa fa-file-o"></i>

							<span class="menu-text">
								My Documents

								<!-- #section:basics/sidebar.layout.badge -->
								<?php
									$qry = $conn->query("SELECT * FROM documents WHERE current_holder =".$_SESSION['user']."");
									$num = $qry->num_rows;

									if($num == 0) echo '<span class="badge">'.$num.'</span>';
									else  echo '<span class="badge badge-warning">'.$num.'</span>';	?>

								<!-- /section:basics/sidebar.layout.badge -->
							</span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="<?php if($ths_pg == "archive.php") echo 'active open'; ?> hover">
						<a href="archive.php">
							<i class="menu-icon fa fa-briefcase"></i>

							<span class="menu-text">
								My Archives

								<!-- #section:basics/sidebar.layout.badge -->


								<!-- /section:basics/sidebar.layout.badge -->
							</span>
						</a>

						<b class="arrow"></b>
					</li>

					<?php if($_SESSION['grp']==1 OR $_SESSION['job_title_id']==26){?>
					<li class="<?php if($ths_pg == "archives.php") echo 'active open'; ?> hover">
						<a href="archives.php">
							<i class="menu-icon fa fa-briefcase"></i>

							<span class="menu-text">
								General Archives

								<!-- #section:basics/sidebar.layout.badge -->


								<!-- /section:basics/sidebar.layout.badge -->
							</span>
						</a>

						<b class="arrow"></b>
					</li><?php } ?>
					<li class="<?php if($ths_pg == "tracking.php") echo 'active open'; ?> hover">
						<a href="tracking.php">
							<i class="menu-icon fa fa-eye"></i>
							<span class="menu-text"> Tracking </span>
						</a>
						<b class="arrow"></b>
					</li>

					<li class="<?php if($ths_pg == "staffperformance.php") echo 'active open'; ?> hover">
						<a href="staffperformance.php">
							<i class="menu-icon fa fa-bar-chart-o"></i>
							<span class="menu-text"> My Performance </span>
						</a>
						<b class="arrow"></b>
					</li>

					<button type="button" class="sidebar-collapse btn btn-white btn-primary pull-right" data-target="#sidebar">
										<i class="ace-icon fa fa-angle-double-up" data-icon1="ace-icon fa fa-angle-double-up" data-icon2="ace-icon fa fa-angle-double-down"></i>

									</button>

				</ul><!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>
