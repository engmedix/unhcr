	<link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>

<div id="navbar" class="navbar navbar-default navbar-collapse h-navbar">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>

	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<!-- #section:basics/navbar.layout.brand -->
			<a href="./" class="navbar-brand">
				<small>
					Doctracker Workflow
				</small>
			</a>

			<!-- /section:basics/navbar.layout.brand -->

			<!-- #section:basics/navbar.toggle -->
			<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
				<span class="sr-only">Toggle user menu</span>

				<img src="assets/avatars/user.jpg" alt="Jason's Photo" />
			</button>

			<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>
			</button>

			<!-- /section:basics/navbar.toggle -->
		</div>

		<!-- #section:basics/navbar.dropdown -->
		<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
			<ul class="nav ace-nav">

				<li class="light-blue  user-min"><h3><?php echo getBranch($myBranch); ?></h3></li>
				<!-- #section:basics/navbar.user_menu -->
				<li class="light-blue user-min">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" src="assets/avatars/user.jpg" alt="<?php echo $fname; ?>'s Photo" />
						<span class="user-info">
							<small>Welcome,</small>
							<?php echo $fname; ?>
						</span>

						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
							<a href="profile.php">
								<i class="ace-icon fa fa-user"></i>
								Profile
							</a>
						</li>

						<li>
							<a href="help/" target="_blank">
								<i class="ace-icon fa fa-question-circle"></i>
								Help
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href="include/do_signout.php">
								<i class="ace-icon fa fa-power-off"></i>
								Logout
							</a>
						</li>
					</ul>
				</li>

				<!-- /section:basics/navbar.user_menu -->
			</ul>
		</div>

		<div class="navbar-buttons navbar-header pull-right collapse navbar-collapse" role="navigation">

			<ul style="display:none"  class="nav ace-nav">
				<li class="transparent">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<i class="ace-icon fa fa-bell icon-animated-bell"></i>
							<span class="badge badge-important">3</span>
						</a>

						<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
							<li class="dropdown-header">
								<i class="ace-icon fa fa-exclamation-triangle"></i>
								3 Notifications
							</li>

							<li class="dropdown-content">
								<ul class="dropdown-menu dropdown-navbar navbar-pink">
									<li>
										<a href="#">
											<div class="clearfix">
												<span class="pull-left">
													<i class="btn btn-xs no-hover btn-pink fa fa-times"></i>
													Document(s) rejected
												</span>
												<span class="pull-right badge badge-info">+12</span>
											</div>
										</a>
									</li>

									<li>
										<a href="#">
											<i class="btn btn-xs btn-primary fa fa-exchange"></i>
											Document(s) forwarded.
										</a>
									</li>

									<li>
										<a href="#">
											<div class="clearfix">
												<span class="pull-left">
													<i class="btn btn-xs no-hover btn-success fa fa-check"></i>
													Document(s) approved
												</span>
												<span class="pull-right badge badge-success">+8</span>
											</div>
										</a>
									</li>


								</ul>
							</li>

							<li class="dropdown-footer">
								<a href="tracking.php">
									See all notifications
									<i class="ace-icon fa fa-arrow-right"></i>
								</a>
							</li>
						</ul>
					</li>

				</ul>
			</div>

		<!-- /section:basics/navbar.dropdown -->
		<nav role="navigation" class="navbar-menu pull-left collapse navbar-collapse">
			<!-- #section:basics/navbar.nav -->
			<ul class="nav navbar-nav" id="menu">

				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="ace-icon fa fa-file bigger-110"></i>
						New Document&nbsp;
						<i class="ace-icon fa fa-angle-down bigger-110"></i>
					</a>
					<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
						<li>
									<a href="new.php?type=doc">
										<i class="ace-icon fa fa-file bigger-110 blue"></i>
										External
									</a>
								</li>

								<li>
									<a href="new.php?type=doc_i">
										<i class="ace-icon fa fa-file bigger-110 blue"></i>
										Internal
									</a>
								</li>
						<li>
							<a href="new.php?type=doc_gp">
								<i class="ace-icon fa fa-dollar bigger-110 red"></i>
								Goods Payment
							</a>
						</li>
						<li>
							<a href="new.php?type=doc_cwp">
								<i class="ace-icon fa fa-dollar bigger-110 blue"></i>
								Construction Work payment
							</a>
						</li>
						<li>
							<a href="new.php?type=doc_sp">
								<i class="ace-icon fa fa-dollar bigger-110 green"></i>
								Service Payment
							</a>
						</li>
					</ul>
				</li>


				<?php if($myGroup==1){ ?>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Administration&nbsp;
						<i class="ace-icon fa fa-angle-down bigger-110"></i>
					</a>
					<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
						<li>
							<a href="units.php">
								<i class="ace-icon fa fa-users bigger-110 blue"></i>
								Departments
							</a>
						</li>
						<li>
							<a href="stations.php">
								<i class="ace-icon fa fa-home bigger-110 blue"></i>
								Duty Stations
							</a>
						</li>
						<li>
							<a href="jobtitles.php">
								<i class="ace-icon fa fa-chain bigger-110 blue"></i>
								Positions
							</a>
						</li>
						<li>
							<a href="staff.php">
								<i class="ace-icon fa fa-user bigger-110 blue"></i>
								Staff
							</a>
						</li>
						<li>
							<a href="groups.php">
								<i class="ace-icon fa fa-desktop bigger-110 blue"></i>
								Groups
							</a>
						</li>
						<li>
							<a href="doctypes.php">
								<i class="ace-icon fa fa-file bigger-110 blue"></i>
								Document Types
							</a>
						</li>
						<li>
							<a href="workflows.php">
								<i class="ace-icon fa fa-recycle bigger-110 blue"></i>
								Workflows
							</a>
						</li>
						<li>
							<a href="security.php">
								<i class="ace-icon fa fa-lock bigger-110 blue"></i>
								Security
							</a>
						</li>
						<li>
							<a href="troubleshoot.php">
								<i class="ace-icon fa fa-gear bigger-110 blue"></i>
								Troubleshooting
							</a>
						</li>
						<li style="display:none">
							<a  href="responsemail.php">
								<i class="ace-icon fa fa-envelope bigger-110 blue"></i>
								Acknowledgement Email
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($myGroup==1 OR $myid==getUnitHead($myUnit)){ ?>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Management&nbsp;
						<i class="ace-icon fa fa-angle-down bigger-110"></i>
					</a>
					<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
						<li>
							<a href="delegation.php">
								<i class="ace-icon fa fa-share bigger-110 blue"></i>
								Delegation&nbsp;
								<?php $dlg_count = "SELECT * FROM delegations WHERE approved='0' AND requested_by IN (SELECT id FROM admins WHERE department_id='$myUnit')";
								$dlg_result = $conn->query($dlg_count);
								if(mysqli_num_rows($dlg_result)>0){   ?>
								<span class="badge badge-important"><?php echo mysqli_num_rows($dlg_result); ?></span>
								<?php } ?>
							</a>
						</li>
						<li>
							<a href="outofoffice.php">
								<i class="ace-icon fa fa-external-link bigger-110 blue"></i>
								Out of Office
							</a>
						</li>
						<li>
							<a href="escalation.php">
								<i class="ace-icon fa fa-clock-o bigger-110 blue"></i>
								Escalation
							</a>
						</li>
					</ul>


				<?php } ?>

				<?php if($myGroup==1 OR inGroup($myid,2)){ ?>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Reports&nbsp;
						<i class="ace-icon fa fa-angle-down bigger-110"></i>
					</a>
					<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
						<li>
							<a href="staffperformance.php">
								<i class="ace-icon fa fa-bar-chart-o bigger-110 blue"></i>
								Staff Performance&nbsp;
							</a>
						</li>
						<li>
							<a href="unitperformance.php">
								<i class="ace-icon fa fa-group bigger-110 blue"></i>
								Unit Performance
							</a>
						</li>
						<li>
							<a href="report.php">
								<i class="ace-icon fa fa-file-text bigger-110 blue"></i>
								Workflow Report
							</a>
						</li>

					</ul>

				</li>
				<?php } ?>


			</ul>

			<!-- /section:basics/navbar.nav -->

			<!-- #section:basics/navbar.form -->


			<!-- /section:basics/navbar.form -->
		</nav>
	</div><!-- /.navbar-container -->
</div>
