<?php include("include/cnx.php");?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>EDMS</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<link rel="stylesheet" href="assets/css/daterangepicker.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		<script type="text/javascript" src="dashboard/resources/js/fusioncharts.js"></script>
		<script type="text/javascript" src="dashboard/resources/js/themes/fusioncharts.theme.fusion.js"></script>
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		.my-progress-container{
			border:thin solid #ccc;
			position:relative;
			margin-top:10px;
		}
		.my-progress-container div{
			 position:absolute;
		}
		.my-progress-container span{
			z-index:1;
			position:relative;
		}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<?php
			$form_query = "SELECT COUNT(*) as total FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND date >'2019-04-01'";
		  $form_result = $conn->query($form_query);
		  $form_data = $form_result->fetch_assoc();
		  $form_query1 = "SELECT COUNT(*) as total_approved_invoices FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
		  $form_result1 = $conn->query($form_query1);
		  $form_data1 = $form_result1->fetch_assoc();
		  $form_query2 = "SELECT COUNT(*) as total_rejected_invoices FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
		  $form_result2 = $conn->query($form_query2);
		  $form_data2 = $form_result2->fetch_assoc();
		  $form_query3 = "SELECT COUNT(*) as total_pending_invoices FROM documents WHERE (doc_type_id='87' OR doc_type_id='88' OR doc_type_id='89') AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
		  $form_result3 = $conn->query($form_query3);
		  $form_data3 = $form_result3->fetch_assoc();

		  $frm_query = "SELECT COUNT(*) as total_closed_goods_payment_invoices FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
		  $frm_result = $conn->query($frm_query);
		  $frm_data = $frm_result->fetch_assoc();
		  $frm_query1 = "SELECT COUNT(*) as total_rejected_goods_payment_invoices FROM documents WHERE doc_type_id='87' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
		  $frm_result1 = $conn->query($frm_query1);
		  $frm_data1 = $frm_result1->fetch_assoc();
		  $frm_query2 = "SELECT COUNT(*) as total_pending_goods_payment_invoices FROM documents WHERE doc_type_id='87' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
		  $frm_result2 = $conn->query($frm_query2);
		  $frm_data2 = $frm_result2->fetch_assoc();

		  $frm_query_approved = "SELECT COUNT(*) as total_closed_service_payment_invoices FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
		  $frm_result_approved = $conn->query($frm_query_approved);
		  $frm_data_approved = $frm_result_approved->fetch_assoc();
		  $frm_query_rejected = "SELECT COUNT(*) as total_rejected_service_payment_invoices FROM documents WHERE doc_type_id='89' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
		  $frm_result_rejected = $conn->query($frm_query_rejected);
		  $frm_data_rejected = $frm_result_rejected->fetch_assoc();
		  $frm_query_pending = "SELECT COUNT(*) as total_pending_service_payment_invoices FROM documents WHERE doc_type_id='89' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
		  $frm_result_pending = $conn->query($frm_query_pending);
		  $frm_data_pending = $frm_result_pending->fetch_assoc();

		  $frm_query_approved1 = "SELECT COUNT(*) as total_closed_const_works_payment_invoices FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Approved' AND date >'2019-04-01'";
		  $frm_result_approved1 = $conn->query($frm_query_approved1);
		  $frm_data_approved1 = $frm_result_approved1->fetch_assoc();
		  $frm_query_rejected1 = "SELECT COUNT(*) as total_rejected_const_works_payment_invoices FROM documents WHERE doc_type_id='88' AND stage_label='CLOSED' AND status_label='Rejected' AND date >'2019-04-01'";
		  $frm_result_rejected1 = $conn->query($frm_query_rejected1);
		  $frm_data_rejected1 = $frm_result_rejected1->fetch_assoc();
		  $frm_query_pending1 = "SELECT COUNT(*) as total_pending_const_works_payment_invoices FROM documents WHERE doc_type_id='88' AND (status_label='Submitted' OR status_label='Sent Back' OR status_label='Forwarded') AND date >'2019-04-01'";
		  $frm_result_pending1 = $conn->query($frm_query_pending1);
		  $frm_data_pending1 = $frm_result_pending1->fetch_assoc();

		  ?>
		  <?php
		  $conditions = 'WHERE 1 ';
		                            $condition= "AND receiver_status='ARCHIVED'";

		                            if(@$_POST['duration'] != '') {
		                              $dates = explode(' - ',$_POST['duration']);
		                              $startDate = date('Y-m-j H:i',strtotime($dates[0]));
		                              $endDate = date('Y-m-j H:i',strtotime($dates[1]));

		                              $conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
		                            }
		 $cont_query = $conn->query("SELECT COUNT(*) num,(IF(action = 'APPROVED','APPROVED/CLOSED',action))act,total FROM doc_track LEFT JOIN (SELECT COUNT(*) total, from_id FROM doc_track $conditions GROUP BY from_id) tt USING(from_id) $conditions  GROUP BY action");
		 $cc = mysqli_num_rows($cont_query);
		$cont_data = $cont_query->fetch_assoc();

		$total_gp=$frm_data1['total_rejected_goods_payment_invoices']+$frm_data['total_closed_goods_payment_invoices']+$frm_data2['total_pending_goods_payment_invoices'];
		$total_sp=$frm_data_rejected['total_rejected_service_payment_invoices']+$frm_data_approved['total_closed_service_payment_invoices']+$frm_data_pending['total_pending_service_payment_invoices'];
		$total_cwp=$frm_data_rejected1['total_rejected_const_works_payment_invoices']+$frm_data_approved1['total_closed_const_works_payment_invoices']+$frm_data_pending1['total_pending_const_works_payment_invoices'];
		$total_invoices=$total_gp+$total_sp+$total_cwp;

		$percentage_gp=number_format(($total_gp/$total_invoices)*100,2);
		$percentage_sp=number_format(($total_sp/$total_invoices)*100,2);
		$percentage_cwp=number_format(($total_cwp/$total_invoices)*100,2);

		$percentage_approved=number_format(($form_data1['total_approved_invoices']/$total_invoices)*100,2);
		$percentage_rejected=number_format(($form_data2['total_rejected_invoices']/$total_invoices)*100,2);
		$percentage_pending=number_format(($form_data3['total_pending_invoices']/$total_invoices)*100,2);
			//get total business loans for each of last five years
			$end_year=date('Y');
			$start_year=date('Y')-4;
			$start_year2=date('Y')-3;
			$start_year3=date('Y')-2;
			$start_year4=date('Y')-1;
			$i=0;
			$month=1;
			$j=0;
			$namesArray=array();
			$labelsArray=array();
			$arr_month=array();
			$gp_array1=array(); $gp_array2=array(); $gp_array3=array(); $gp_array4=array(); $gp_array5=array();
			$sp_array1=array(); $sp_array2=array(); $sp_array3=array(); $sp_array4=array(); $sp_array5=array();
			$cwp_array1=array(); $cwp_array2=array(); $cwp_array3=array(); $cwp_array4=array(); $cwp_array5=array();
			$arr_month = array(["JAN", "1"],["FEB", "2"],["MARCH", "3"],["APRIL", "4"],["MAY", "5"],["JUNE", "6"],["JULY", "7"],["AUG", "6"],["SEP", "9"],["OCT", "10"],["NOV", "11"],["DEC", "12"]);
			for($i=0;$i<5;$i++){
			 $query="SELECT COUNT(*) AS total_goods_payment_invoices FROM documents WHERE (YEAR(date) = $start_year) AND doc_type_id=87";
			 $result=$conn->query($query);
			 $res=$result->fetch_assoc();
			 $data=array("value"=>$res["total_goods_payment_invoices"],"link"=>"newchart-xml-goods_payment_invoices_$start_year");
					 array_push($namesArray,$data);
					 $labels=array("label"=>"$start_year");
				array_push($labelsArray,$labels);
				$start_year++;
			}

			//number of loan applications per month for each year

				for($j=0;$j<12;$j++){
				$goods_payment_invoices_year1 = "SELECT COUNT(*) as total_monthly_goods_payment_invoices FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year)";
					 $gp_result1=$conn->query($goods_payment_invoices_year1);
				$gp_res1=$gp_result1->fetch_assoc();
				$gp_data1=array("label"=>$arr_month[$j][0],"value"=>$gp_res1["total_monthly_goods_payment_invoices"]);
					 array_push($gp_array1,$gp_data1);

				$goods_payment_invoices_year2 = "SELECT COUNT(*) as total_monthly_goods_payment_invoices1 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year2)";
				$gp_result2=$conn->query($goods_payment_invoices_year2);
				$gp_res2=$gp_result2->fetch_assoc();
				$gp_data2=array("label"=>$arr_month[$j][0],"value"=>$gp_res2["total_monthly_goods_payment_invoices1"]);
					 array_push($gp_array2,$gp_data2);

				$goods_payment_invoices_year3 = "SELECT COUNT(*) as total_monthly_goods_payment_invoices2 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year3)";
				$gp_result3=$conn->query($goods_payment_invoices_year3);
				$gp_res3=$gp_result3->fetch_assoc();
				$gp_data3=array("label"=>$arr_month[$j][0],"value"=>$gp_res3["total_monthly_goods_payment_invoices2"]);
					 array_push($gp_array3,$gp_data3);

				$goods_payment_invoices_year4 = "SELECT COUNT(*) as total_monthly_goods_payment_invoices3 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $start_year4)";
					 $gp_result4=$conn->query($goods_payment_invoices_year4);
				$gp_res4=$gp_result4->fetch_assoc();
				$gp_data4=array("label"=>$arr_month[$j][0],"value"=>$gp_res4["total_monthly_goods_payment_invoices3"]);
					 array_push($gp_array4,$gp_data4);

				$goods_payment_invoices_year5 = "SELECT COUNT(*) as total_monthly_goods_payment_invoices4 FROM documents WHERE (MONTH(date) = $month) AND doc_type_id='87'  AND (YEAR(date) = $end_year)";
					 $gp_result5=$conn->query($goods_payment_invoices_year5);
				$gp_res5=$gp_result5->fetch_assoc();
				$gp_data5=array("label"=>$arr_month[$j][0],"value"=>$gp_res5["total_monthly_goods_payment_invoices4"]);
					 array_push($gp_array5,$gp_data5);
			 $month++;
				}
				$encoded_data=json_encode($namesArray);
				$encoded_labels=json_encode($labelsArray);
				$gp_encode1=json_encode($gp_array1);
				$gp_encode2=json_encode($gp_array2);
				$gp_encode3=json_encode($gp_array3);
				$gp_encode4=json_encode($gp_array4);
				$gp_encode5=json_encode($gp_array5);
			//get total secured loans for each of last five years

			$namesArray1=array();
			$start_year1=date('Y')-4;
			for($i=0;$i<5;$i++){
			 $query1="SELECT COUNT(*) AS total_const_works_payment_invoices FROM documents WHERE (YEAR(date) = $start_year1) AND doc_type_id=88";
			 $result1=$conn->query($query1);
			 $res1=$result1->fetch_assoc();
			 $data1=array("value"=>$res1["total_const_works_payment_invoices"],"link"=>"newchart-xml-const_works_payment_invoices_$start_year1");
				array_push($namesArray1,$data1);
				$start_year1++;
			}
			////////////////////////////////
			$cwp_month=1;
			 for($j=0;$j<12;$j++){
				$cwp_year1 = "SELECT COUNT(*) as total_monthly_cwp FROM documents WHERE (MONTH(date) = $cwp_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year)";
					 $cwp_result1=$conn->query($cwp_year1);
				$cwp_res1=$cwp_result1->fetch_assoc();
				$cwp_data1=array("label"=>$arr_month[$j][0],"value"=>$cwp_res1["total_monthly_cwp"]);
					 array_push($cwp_array1,$cwp_data1);

				$cwp_year2 = "SELECT COUNT(*) as total_monthly_cwp1 FROM documents WHERE (MONTH(date) = $cwp_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year2)";
					 $cwp_result2=$conn->query($cwp_year2);
				$cwp_res2=$cwp_result2->fetch_assoc();
				$cwp_data2=array("label"=>$arr_month[$j][0],"value"=>$cwp_res2["total_monthly_cwp1"]);
					 array_push($cwp_array2,$cwp_data2);

				$cwp_year3 = "SELECT COUNT(*) as total_monthly_cwp2 FROM documents WHERE (MONTH(date) = $cwp_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year3)";
					 $cwp_result3=$conn->query($cwp_year3);
				$cwp_res3=$cwp_result3->fetch_assoc();
				$cwp_data3=array("label"=>$arr_month[$j][0],"value"=>$cwp_res3["total_monthly_cwp2"]);
					 array_push($cwp_array3,$cwp_data3);

				$cwp_year4 = "SELECT COUNT(*) as total_monthly_cwp3 FROM documents WHERE (MONTH(date) = $cwp_month) AND doc_type_id='88'  AND (YEAR(date) = $start_year4)";
					 $cwp_result4=$conn->query($cwp_year4);
				$cwp_res4=$cwp_result4->fetch_assoc();
				$cwp_data4=array("label"=>$arr_month[$j][0],"value"=>$cwp_res4["total_monthly_cwp3"]);
					 array_push($cwp_array4,$cwp_data4);

				$cwp_year5 = "SELECT COUNT(*) as total_monthly_cwp4 FROM documents WHERE (MONTH(date) = $cwp_month) AND doc_type_id='88'  AND (YEAR(date) = $end_year)";
					 $cwp_result5=$conn->query($cwp_year5);
				$cwp_res5=$cwp_result5->fetch_assoc();
				$cwp_data5=array("label"=>$arr_month[$j][0],"value"=>$cwp_res5["total_monthly_cwp4"]);
					 array_push($cwp_array5,$cwp_data5);
			 $cwp_month++;
				}
				$encoded_labels=json_encode($labelsArray);
				$cwp_encode1=json_encode($cwp_array1);
				$cwp_encode2=json_encode($cwp_array2);
				$cwp_encode3=json_encode($cwp_array3);
				$cwp_encode4=json_encode($cwp_array4);
				$cwp_encode5=json_encode($cwp_array5);
				$encoded_data1=json_encode($namesArray1);

			//get total pre scored loans for each of last five years

			$namesArray2=array();
			 $sp_start_year=date('Y')-4;
			for($i=0;$i<5;$i++){
			 $query2="SELECT COUNT(*) AS total_const_works_payment_invoices FROM documents WHERE (YEAR(date) = $sp_start_year) AND doc_type_id=89 ";
			 $result2=$conn->query($query2);
			 $res2=$result2->fetch_assoc();
			 $data2=array("value"=>$res2["total_const_works_payment_invoices"],"link"=>"newchart-xml-const_works_payment_invoices_$sp_start_year");
				array_push($namesArray2,$data2);
				$sp_start_year++;
			}


			/////Drill down months////////////////
			//number of loan applications per month for each year
				$sp_month=1;
				for($j=0;$j<12;$j++){
				$sp_year1 = "SELECT COUNT(*) as total_monthly_sp FROM documents WHERE (MONTH(date) = $sp_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year)";
					 $sp_result1=$conn->query($sp_year1);
				$sp_res1=$sp_result1->fetch_assoc();
				$sp_data1=array("label"=>$arr_month[$j][0],"value"=>$sp_res1["total_monthly_sp"]);
					 array_push($sp_array1,$sp_data1);

				$sp_year2 = "SELECT COUNT(*) as total_monthly_sp1 FROM documents WHERE (MONTH(date) = $sp_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year2)";
					 $sp_result2=$conn->query($sp_year2);
				$sp_res2=$sp_result2->fetch_assoc();
				$sp_data2=array("label"=>$arr_month[$j][0],"value"=>$sp_res2["total_monthly_sp1"]);
					 array_push($sp_array2,$sp_data2);

				$sp_year3 = "SELECT COUNT(*) as total_monthly_sp2 FROM documents WHERE (MONTH(date) = $sp_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year3)";
					 $sp_result3=$conn->query($sp_year3);
				$sp_res3=$sp_result3->fetch_assoc();
				$sp_data3=array("label"=>$arr_month[$j][0],"value"=>$sp_res3["total_monthly_sp2"]);
					 array_push($sp_array3,$sp_data3);

				$sp_year4 = "SELECT COUNT(*) as total_monthly_sp3 FROM documents WHERE (MONTH(date) = $sp_month) AND doc_type_id='89'  AND (YEAR(date) = $start_year4)";
					 $sp_result4=$conn->query($sp_year4);
				$sp_res4=$sp_result4->fetch_assoc();
				$sp_data4=array("label"=>$arr_month[$j][0],"value"=>$sp_res4["total_monthly_sp3"]);
					 array_push($sp_array4,$sp_data4);

				$sp_year5 = "SELECT COUNT(*) as total_monthly_sp4 FROM documents WHERE (MONTH(date) = $sp_month) AND doc_type_id='89'  AND (YEAR(date) = $end_year)";
					 $sp_result5=$conn->query($sp_year5);
				$sp_res5=$sp_result5->fetch_assoc();
				$sp_data5=array("label"=>$arr_month[$j][0],"value"=>$sp_res5["total_monthly_sp4"]);
					 array_push($sp_array5,$sp_data5);
			 $sp_month++;
				}
				$encoded_data2=json_encode($namesArray2);
				$encoded_labels=json_encode($labelsArray);
				$sp_encode1=json_encode($sp_array1);
				$sp_encode2=json_encode($sp_array2);
				$sp_encode3=json_encode($sp_array3);
				$sp_encode4=json_encode($sp_array4);
				$sp_encode5=json_encode($sp_array5);

 			 /////////////////////////////////////
			 include('dashboard/fusioncharts/fusioncharts.php');
 			  $arrChartConfig = array(
 			         "chart" => array(
 			             "caption" => "TOTAL GOODS PAYMENT INVOICES",
 			             "subCaption" => "",
 			             "xAxisName" => "INVOICES TYPES",
 			             "yAxisName" => "# OF GOODS PAYMENT INVOICES",
 			             "numberSuffix" => "",
 			             "theme" => "umber"
 			         )
 			     );

 			     // An array of hash objects which stores data
 			     $arrChartData = array(
 			 ["GOODS PAYMENT INVOICES", "$total_gp"],
 			 ["CONSTRUCTION WORKS INVOICES", "$total_cwp"],
 			 ["SERVICE PAYMENT INVOICES", "$total_sp"]
 			 );
 			 $arrLabelValueData = array();

 			     // Pushing labels and values
 			     for($i = 0; $i < count($arrChartData); $i++) {
 			         array_push($arrLabelValueData, array(
 			             "label" => $arrChartData[$i][0], "value" => $arrChartData[$i][1]
 			         ));
 			     }
 			     $arrChartConfig["data"] = $arrLabelValueData;

 			     // JSON Encode the data to retrieve the string containing the JSON representation of the data in the array.
 			     $jsonEncodedData = json_encode($arrChartConfig);

 			     // chart object
 			     $Chart = new FusionCharts("pie3d", "MyFirstChart" , "600", "400", "chart-container", "json", $jsonEncodedData);

 			     // Render the chart
 			     $Chart->render();

			 ?>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<!-- /section:settings.box -->
						<div class="page-header">
							<h1>
								Document Management
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Dashboard
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
									<div class="space-6"></div>

									<!-- #section:pages/dashboard.infobox -->

									</div>

									<div class="col-sm-6">

										<div id="task-tab" class="tab-pane">
											<h4 class="smaller lighter green">
												<i class="ace-icon fa fa-list"></i>
												Incoming Documents
											</h4>

											<!-- #section:pages/dashboard.tasks -->
											<ul id="tasks" class="item-list">

										<?php $form_query = "SELECT doc_track.*,documents.document_type,documents.capture_method,documents.doc_ref_number, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.from_id = admins.id WHERE to_id = '".$_SESSION['user']."' AND receiver_status <> 'ARCHIVED'"; //

												  $form_result = $conn->query($form_query);
												  echo $conn->error;

												  $now = new DateTime();
												  $now->setTime( 0, 0, 0 );

												   $ii=0;
												  while($form_data = $form_result->fetch_assoc()) {
														if($form_data['receiver_status'] == 'SENT') $readStatus = 'message-unread';
														else $readStatus = '';

														if($form_data['action'] == 'DOCUMENT SENT' || $form_data['action'] == 'REQUEST SENT') $label = ' label-info"';
														elseif($form_data['action'] == 'APPROVED') $label = 'label-success';
														elseif($form_data['action'] == 'REJECTED') $label = 'label-danger';
														elseif($form_data['action'] == 'FORWARDED') $label = 'label-pink';

														$sentDate = new DateTime($form_data['actionDate']);
														$sentDate->setTime( 0, 0, 0 );

														$diff = $now->diff( $sentDate );

														$periodSpent = $diff->days;
														if($periodSpent == 0) $periodSpent = ' Today';
														elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
														elseif($periodSpent >= 2) $periodSpent .= ' days ago';

												  if($readStatus=="message-unread" AND $ii<5){ ?>
													<li class="item-green clearfix">
															<a href="document.php?ac=<?php echo $form_data['doc_id']; ?>&md=<?php echo $form_data['capture_method']; ?>&tId=<?php echo $form_data['id']; ?>">

																	<span class="lbl" title="<?php echo $form_data['fname'].' '.$form_data['lname']; ?>"><?php echo $form_data['doc_ref_number']; ?> | <?php echo $form_data['document_type']; ?> &nbsp;&nbsp;&nbsp;<b>FROM:</b> <?php echo $form_data['fname'].' '.$form_data['lname']; ?></span>
																	<span class="time"><?php echo '<span class="label">Sent '.$periodSpent.'</span>'; ?></span>

																</a>

													</li>
												  <?php $ii++; } }
												if($ii==0){
												  ?>
												<li class="item-green clearfix">
													<span class="lbl"><i>You don't have any new documents</i> </span>
												</li>
												<?php } ?>
											</ul>

													<!-- /section:pages/dashboard.tasks -->
										</div>




										<div class="row">
											<div class="col-sm-12 col-md-12">
											<br>
												Search Contacts: <input id="search" type="text" class="form-control" placeholder="Type name" />
												<div class="space-4"></div>
											</div>
                  <?php if($myGroup==1){ ?>
											<div class="col-sm-12">

												<div class="vspace-12-sm"></div>

												<div class="widget-box">
													<div class="widget-header widget-header-flat widget-header-small">
														<h5 class="widget-title">
															<i class="ace-icon fa fa-signal"></i>
														INVOICES FOR LAST FIVE YEARS
														</h5>

													</div>

													<div class="widget-body">
														<div class="widget-main">
															<div id="chart-1"></div>

															<div class="hr hr8 hr-double"></div>

														</div><!-- /.widget-main -->
													</div><!-- /.widget-body -->
												</div><!-- /.widget-box -->

												<div class="widget-box">
													<div class="widget-header widget-header-flat widget-header-small">
														<h5 class="widget-title">
															<i class="ace-icon fa fa-signal"></i>
															Document stats
														</h5>

													</div>

													<div class="widget-body">
														<div class="widget-main">
															<div id="chart-container"></div>

															<div class="hr hr8 hr-double"></div>

														</div><!-- /.widget-main -->
													</div><!-- /.widget-body -->
												</div><!-- /.widget-box -->
											</div><!-- /.col -->
										<?php } ?>
										</div>





									</div><!-- /.col -->

									<div class="vspace-12-sm"></div>

									<?php
									if(isset($_POST['filter'])){
										$periodHeaders = array('thisWeek'=>'This Week', 'lastWeek'=>'Last Week','thisMonth'=>'This Month','lastMonth'=>'Last Month');
										$selected = $periodHeaders[$_POST['filter']];
									}
									else{
										$selected = 'This Week';
									}
									?>


									<div class="col-sm-5">
										<form method="post" action="" id="formId" >
											<input type="hidden" name="extraId" value="iudioufsdfdsfidsuf" />
											<input type="hidden" name="filter" id="filter" value="" />
												<div class="">
													<div class="">


														<input class="form-control filter-links" type="text" name="duration" id="date-range-picker" placeholder="Select date range" value="<?php echo @$_POST['duration']; ?>" />



													</div>
												</div>

										</form>

									</div>


									<div class="col-sm-6">

										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													My Performance
												</h5>

											</div>

											<div class="widget-body">

												<div class="widget-main">

													<div class="clearfix">

												<?php

														$conditions = 'WHERE 1 ';
														$condition= "AND receiver_status='ARCHIVED'";

														if(@$_POST['duration'] != '') {
															$dates = explode(' - ',$_POST['duration']);
															$startDate = date('Y-m-j H:i',strtotime($dates[0]));
															$endDate = date('Y-m-j H:i',strtotime($dates[1]));

															$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
														}

														$form_query = "
														SELECT admins.*, COALESCE(docsout_,0) docsout,COALESCE(docsin_,0) docsin,COALESCE(fast,0) ft, COALESCE(slow,0) sl,COALESCE(late,0) lt FROM admins
														LEFT JOIN
															(SELECT COUNT(*) docsout_, from_id FROM doc_track $conditions AND action='FORWARDED' AND from_stage<>'INITIATION' GROUP BY from_id) d_o ON admins.id = d_o.from_id
														LEFT JOIN
															(SELECT COUNT(*) docsin_, to_id FROM doc_track $conditions GROUP BY to_id) d_i ON admins.id = d_i.to_id
														LEFT JOIN
															(SELECT to_id u_id,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00'|| archive_date <> '',archive_date,NOW() ),actionDate) <= 1,1,0)) fast,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00'|| archive_date <> '',archive_date,NOW() ),actionDate) > 1 AND DATEDIFF(archive_date,actionDate) <= 3,1,0)) slow,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00'|| archive_date <> '',archive_date,NOW() ),actionDate) > 3,1,0)) late
																FROM doc_track $conditions GROUP BY to_id
															)stats ON admins.id = stats.u_id
														WHERE id = $id ORDER BY department_id, fname";

												  $form_result = $conn->query($form_query);
												  while($form_data = $form_result->fetch_assoc()) {
													$w_fast = @floor(($form_data['ft']/$form_data['docsin'])*100);
													$w_slow = @floor(($form_data['sl']/$form_data['docsin'])*100);
													$w_late = @floor(($form_data['lt']/$form_data['docsin'])*100);
												  ?>


														<div class="grid5">
															<span class="grey">

																&nbsp; Docs In
															</span><br>
															<h4 class="bigger pull-right"><?php echo ($form_data['docsin']); ?></h4>
														</div>
														<div class="grid5">
															<span class="grey">

																&nbsp; Docs Out
															</span><br>
															<h4 class="bigger pull-right"><?php echo ($form_data['docsout']); ?></h4>
														</div>
														<div class="grid5">
															<span class="label label-success">Fast</span>
															<div class="my-progress-container">
																<div class="label-success" style="width:<?php echo $w_fast; ?>%;">&nbsp;</div>
																<span><?php echo ($w_fast); ?>%</span>
															</div>
														</div>
														<div class="grid5">
															<span class="label label-warning">Normal</span>
															<div class="my-progress-container">
																<div class="label-warning" style="width:<?php echo $w_slow; ?>%;">&nbsp;</div>
																<span><?php echo ($w_slow); ?>%</span>
															</div>
														</div>
														<div class="grid5">
															<span class="label label-danger">Late</span>
															<div class="my-progress-container">
																<div class="label-danger" style="width:<?php echo $w_late; ?>%;">&nbsp;</div>
																<span><?php echo ($w_late); ?>%</span>
															</div>
														</div>

														<?php } ?>

													</div>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->

										<div class="vspace-12-sm"></div>

										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													Document stats
												</h5>

											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div id="piechart-placeholder"></div>

													<div class="hr hr8 hr-double"></div>

												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
                  <?php if($myGroup==1){ ?>
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													INVOICE TYPES
												</h5>

											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div id="chart-container1"></div>

													<div class="hr hr8 hr-double"></div>

												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->

										</div><!-- /.widget-box -->

                  <?php } ?>

									</div><!-- /.col -->
								</div><!-- /.row -->


					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery-ui.custom.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.js"></script>
		<script src="assets/js/jquery.easypiechart.js"></script>
		<script src="assets/js/jquery.sparkline.js"></script>
		<script src="assets/js/flot/jquery.flot.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.js"></script>

		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/bootstrap-wysiwyg.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>


		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
						size: size
					});
				})

				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html',
									 {
										tagValuesAttribute:'data-values',
										type: 'bar',
										barColor: barColor ,
										chartRangeMin:$(this).data('min') || 0
									 });
				});





			  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
			  //but sometimes it brings up errors with normal resize event handlers
			  $.resize.throttleWindow = false;

			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [

			  <?php $c = 0;
				$cont_query = $conn->query("SELECT COUNT(*) num,(IF(action = 'APPROVED','APPROVED/CLOSED',action))act,total FROM doc_track LEFT JOIN (SELECT COUNT(*) total, from_id FROM doc_track $conditions GROUP BY from_id) tt USING(from_id) $conditions AND from_id = $id GROUP BY action");
				$cc = mysqli_num_rows($cont_query);
				$colors = array("APPROVED/CLOSED"=>"#68BC31","DOCUMENT SENT"=>"#2091CF","FORWARDED"=>"#FEE074","REQUEST SENT"=>"#2091CF","REJECTED"=>"#DA5430","Sourcing"=>"grey","Evaluation"=>"DarkViolet","Contracting"=>"purple","LOC/LPO issued"=>"cyan","Contract Management"=>"maroon","Delivered"=>"SeaGreen","Payment Processed"=>"MintCream","On Hold"=>"PaleVioletRed","Awaiting Specifications"=>"DarkOrange");
				while($cont_data = $cont_query->fetch_assoc()) {
				echo '{ label: "'.$cont_data['act'].'", data: "'.number_format($cont_data['num']/$cont_data['total']*100,1).'", color: "'.((isset($colors[$cont_data['act']]))? $colors[$cont_data['act']] : 'grey').'", url: "http://kjljlk.com" }'; if($c<$cc) echo ',';
				$c++; }
				?>

			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne",
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);

			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);


			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;

			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}

			 });

			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });

			//////////contacts search box
			//custom autocomplete (category selection)
			$.widget("custom.catcomplete", $.ui.autocomplete,{
				_create: function() {
					this._super();
					this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
				},
				_renderMenu: function( ul, items ) {
					var that = this,
					currentCategory = "";
					$.each( items, function( index, item ) {
						var li;
						if ( item.category != currentCategory ) {
							ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
							currentCategory = item.category;
						}
						li = that._renderItemData( ul, item );
							if ( item.category ) {
							li.attr( "aria-label", item.category + " : " + item.label );
						}
					});
				}
			});
			var data = [
				<?php $c = 0;
				$cont_query = $conn->query("SELECT * FROM admins ORDER BY department_id");
				$cc = mysqli_num_rows($cont_query);
				while($cont_data = $cont_query->fetch_assoc()) {
				echo '{ label: "'.$cont_data['fname'].' '.$cont_data['lname'].' '.$cont_data['oname'].' | EXT:'.$cont_data['ext'].' | MOB: '.$cont_data['phone'].' ", category: "'.strtoupper(getDept($cont_data['department_id'])).'" }'; if($c<$cc) echo ',';
				$c++; }
				?>
			];
			$("#search").catcomplete({
				delay: 0,
				source: data
			});

			$('#date-range-picker').daterangepicker({
				'applyClass' : 'btn-sm btn-success',
				'cancelClass' : 'btn-sm btn-default',
				locale: {
					applyLabel: 'Apply',
					cancelLabel: 'Cancel',
				},
				 ranges: {
				   'Today': [moment(), moment()],
				   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				   'This Month': [moment().startOf('month'), moment().endOf('month')],
				   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			});

			$('#date-range-picker').on('apply.daterangepicker', function(ev, picker) {
			  //do something, like clearing an input
				//alert('am in');
				$('#formId')[0].submit();
			});



		});
		</script>

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
		<script>
		FusionCharts.ready(function() {
		 var satisfactionChart = new FusionCharts({
			 type: 'column3d',
			 renderAt: 'chart-container1',
			 width: '600',
			 height: '400',
			 dataFormat: 'json',
			 dataSource: {
				 "chart": {
					 "caption": "INVOICE TYPES",
					 "subcaption": "SINCE 2019",
					 "xaxisname": "INVOICE TYPES",
					 "yaxisname": "",
					 "numberprefix": "",
					 "theme": "fusion",
					 "rotateValues": "0"
				 },
				 "data": [{
					 "label": "GOODS PAYMENT INVOICES",
					 "value": "<?= $total_gp?>",
					 "link": "newchart-xml-goods_payment_invoices"
				 }, {
					 "label": "CONSTRUCTION WORKS PAYMENT INVOICES",
					 "value": "<?=$total_cwp?>",
					 "link": "newchart-xml-const_works_payment_invoices"
				 }, {
					 "label": "SERVICE PAYMENT INVOICES",
					 "value": "<?=$total_sp?>",
					 "link": "newchart-xml-service_payment_invoices"
				 }],
				 "linkeddata": [{
					 "id": "goods_payment_invoices",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "SINCE 2019",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$label, $dataValue,  $percentValue"
						 },
						 "data": [{
							 "label": "APPROVED",
							 "value": "<?=$frm_data['total_closed_goods_payment_invoices']?>"
						 }, {
							 "label": "PENDING",
							 "value": "<?=$frm_data2['total_pending_goods_payment_invoices']?>"
						 }, {
							 "label": "REJECTED",
							 "value": "<?=$frm_data1['total_rejected_goods_payment_invoices']?>"
						 }]
					 }
				 }, {
					 "id": "const_works_payment_invoices",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "SINCE 2019",
							 "numberprefix": "",
							 "theme": "fusion",
							 "plottooltext": "$label, $dataValue,  $percentValue"
						 },
						 "data": [{
							 "label": "APPROVED",
							 "value": "<?=$frm_data_approved['total_closed_const_works_payment_invoices']?>"
						 }, {
							 "label": "PENDING",
							 "value": "<?=$frm_data_pending['total_pending_const_works_payment_invoices']?>"
						 }, {
							 "label": "REJECTED",
							 "value": "<?=$frm_data_rejected['total_rejected_const_works_payment_invoices']?>"
						 }]
					 }
				 }, {
					 "id": "service_payment_invoices",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "SINCE 2019",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$label, $dataValue,  $percentValue"
						 },
						 "data": [{
							 "label": "APPROVED",
							 "value": "<?=$frm_data_approved1['total_closed_service_payment_invoices']?>"
						 }, {
							 "label": "PENDING",
							 "value": "<?=$frm_data_pending1['total_pending_service_payment_invoices']?>"
						 }, {
							 "label": "REJECTED",
							 "value": "<?=$frm_data_rejected1['total_rejected_service_payment_invoices']?>"
						 }]
					 }
				 }]
			 },
			 "events": {
				 'beforeRender': function(evt, args) {
					 if (!document.getElementById('eventmessages')) {
						 // Create container div for radio controls
						 var wrapper = document.createElement('div');
						 var label = document.createElement('label');
						 label.innerText = "Click on the data plot for more details";
						 var controllers = document.createElement('div');
						 controllers.setAttribute('id', 'eventmessages');
						 controllers.innerHTML = "";

						 wrapper.appendChild(label);
						 wrapper.appendChild(controllers);

						 // args.container.parentNode.insertBefore(controllers, args.container.nextSibling);

						 args.container.parentNode.insertBefore(wrapper, args.container.nextSibling);

						 // set css style for controllers div
						// controllers.style.cssText = 'position:inherit; width:400px; height: 100px; border: 1px solid #cccccc; margin-top:10px;padding-left:5px; overflow: scroll;';
					 }
					 evt.sender.configureLink({
						 type: "pie3d",
						 overlayButton: {
							 message: 'close',
							 fontColor: '880000',
							 bgColor: 'FFEEEE',
							 borderColor: '660000'
						 }
					 }, 0);

				 },
			 }
		 });

		 satisfactionChart.render();


			var myChart = new FusionCharts({
			 type: 'mscolumn3d',
			 renderAt: 'chart-1',
			 width: '600',
			 height: '400',
			 dataFormat: 'json',
			 dataSource: {
				 "chart": {
			 "caption": "INVOICES FOR LAST FIVE YEARS",
			 "yaxisname": "# OF INVOICES",
			 "subcaption": "<?=($end_year - 4)."-".$end_year?>",
			 "showhovereffect": "1",
			 "numbersuffix": "",
			 "drawcrossline": "1",
			 "plottooltext": "<b>$dataValue </b> of $seriesName",
			 "theme": "fusion"
			 },
				 "categories": [
				 {
					 "category":<?=$encoded_labels?>
				 }
				],
				"dataset": [
			 {
				 "seriesname": "GOODS PAYMENT INVOICES",
				 "data":<?=$encoded_data?>
			 },
			 {
				 "seriesname": "CONSTRUCTION WORKS PAYMENT INVOICES",
				 "data":<?=$encoded_data2?>
			 },
			 {
				 "seriesname": "SERVICE PAYMENT INVOICES",
				 "data":<?=$encoded_data1?>
			 }
		 ],
				 "linkeddata": [{
					 "id": "goods_payment_invoices_<?=$end_year - 4?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 4?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$gp_encode1?>
					 }
				 }, {
					 "id": "goods_payment_invoices_<?=$end_year - 3?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 3?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$gp_encode2?>
					 }
				 }, {
					 "id": "goods_payment_invoices_<?=$end_year - 2?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 2?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$gp_encode3?>
					 }
				 },
			 {
					 "id": "goods_payment_invoices_<?=$end_year - 1?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 1?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$gp_encode4?>
					 }
				 },
			 {
					 "id": "goods_payment_invoices_<?=$end_year?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "GOODS PAYMENT INVOICES",
							 "subcaption": "2019",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$gp_encode5?>
					 }
				 },
			 {
					 "id": "const_works_payment_invoices_<?=$end_year - 4?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 4?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode1?>
					 }
				 }, {
					 "id": "const_works_payment_invoices_<?=$end_year - 3?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 3?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode2?>
					 }
				 }, {
					 "id": "const_works_payment_invoices_<?=$end_year - 2?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 2?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode3?>
					 }
				 },
			 {
					 "id": "const_works_payment_invoices_<?=$end_year - 1?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 1?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode4?>
					 }
				 },
			 {
					 "id": "const_works_payment_invoices_<?=$end_year?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "CONSTRUCTION WORKS PAYMENT INVOICES",
							 "subcaption": "<?=$end_year?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode5?>
					 }
				 },
				 {
					 "id": "secured_loans_<?=$end_year - 4?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 4?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode1?>
					 }
				 }, {
					 "id": "secured_loans_<?=$end_year - 3?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 3?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$sp_encode2?>
					 }
				 }, {
					 "id": "secured_loans_<?=$end_year - 2?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 2?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$cwp_encode3?>
					 }
				 },
			 {
					 "id": "secured_loans_<?=$end_year - 1?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "<?=$end_year - 1?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$cwp_encode4?>
					 }
				 },
			 {
					 "id": "secured_loans_<?=$end_year?>",
					 "linkedchart": {
						 "chart": {
							 "caption": "SERVICE PAYMENT INVOICES",
							 "subcaption": "<?=$end_year?>",
							 "numberprefix": "",
							 "theme": "fusion",
							 "rotateValues": "0",
							 "plottooltext": "$dataValue"
						 },
						 "data":<?=$cwp_encode5?>
					 }
				 }

		]
			 },
			 "events": {
				 'beforeRender': function(evt, args) {
					 if (!document.getElementById('eventmessages')) {
						 // Create container div for radio controls
						 var wrapper1 = document.createElement('div');
						 var label1 = document.createElement('label');
						 label1.innerText = "Click on the data plot for more details";
						 var controllers1 = document.createElement('div');
						 controllers.setAttribute('id', 'eventmessages');
						 controllers.innerHTML = "";

						 wrapper.appendChild(label1);
						 wrapper.appendChild(controllers1);

						 // args.container.parentNode.insertBefore(controllers, args.container.nextSibling);

						 args.container.parentNode.insertBefore(wrapper1, args.container.nextSibling);

						 // set css style for controllers div
						// controllers.style.cssText = 'position:inherit; width:400px; height: 100px; border: 1px solid #cccccc; margin-top:10px;padding-left:5px; overflow: scroll;';
					 }
					 evt.sender.configureLink({
						 type: "column3d",
						 overlayButton: {
							 message: 'close',
							 fontColor: '880000',
							 bgColor: 'FFEEEE',
							 borderColor: '660000'
						 }
					 }, 0);

				 },
			 }
		 });

		 myChart.render();



		});


		</script>
	</body>
</html>
