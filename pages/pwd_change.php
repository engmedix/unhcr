<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>ACCESS DENIED</title>

		<meta name="description" content="404 Error Page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- /section:basics/sidebar.mobile.toggle -->
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="#" class="navbar-brand">
						<small>
							EDMS
						</small>
					</a>

				</div>

			</div><!-- /.navbar-container -->
		</div>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			
			<div class="main-content">
				<div class="main-content-inner">
					
					<div class="page-content">
						
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<!-- #section:pages/error -->
								<div class="error-container">
									<div class="well">
										<h1 class="grey lighter smaller">
											<span class="blue bigger-125">
												<i class="ace-icon fa fa-lock"></i>
												FORCED PASSWORD CHANGE
											</span>
											
										</h1>

										<hr />
										<h3 class="lighter smaller">You must set a new password for your account before you proceed!</h3>

										<div>
											<form class="form-search" action="login.php?pwdreset" method="post">

												<input type="hidden" name="staff_id" value="<?php echo $my["id"]; ?>" />
												<input type="hidden" name="staff_fn" value="<?php echo $my["fname"]; ?>" />
												<input type="hidden" name="staff_em" value="<?php echo $my["email"]; ?>" />
												<input type="hidden" name="staff_un" value="<?php echo $my["uname"]; ?>" />
												<input type="hidden" name="action" value="FORCED PWD CHANGE" />
												
												<div class="col-xs-12 col-sm-4">

													<div class="space-4"></div>

													<div class="form-group">
														<label for="form-field-username">New Password</label>
														<div>
															<input type="password" name="newpwd1" class="form-control" placeholder="Password" required />
														</div>
													</div>
													<div class="form-group">
														<label for="form-field-username">Confirm Password</label>
														<div>
															<input type="password" name="newpwd2" class="form-control" placeholder="Repeat password" required />
														</div>
													</div>
													
													
													<div class="form-group">
														<button class="btn btn-success" type="submit" >Set Password</button>
													</div>
																										
												</div>
												
											</form>

											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>
											<div class="space"></div>

										
										</div>

										<hr />
										<div class="space"></div>

										<div class="center">
											<a href="include/do_signout.php" class="btn btn-grey">
												<i class="ace-icon fa fa-arrow-left"></i>
												Sign Out
											</a>

											<a href="#" class="btn btn-primary">
												<i class="ace-icon fa fa-question"></i>
												Help
											</a>
										</div>
									</div>
								</div>

								<!-- /section:pages/error -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='../assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		
		<!-- inline scripts related to this page -->

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		
	</body>
</html>
