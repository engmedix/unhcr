<?php

if(!isset($_SESSION)){ session_start(); }
if(!isset($conn)){ require_once("../../include/cnx.php");  }
if(!function_exists(databaseInsert)){ require_once("../../include/database.php"); }
if(!isset($my)){ require_once("../../include/session_tracker.php"); }
 if(isset($_POST['myData'])){
?>

<h5 class="widget-title bigger lighter"><i class="ace-icon fa fa-table"></i> <b>LOAN APPLICATION REPORT</b> <a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success">Export Report to excel</a></h5>
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<thead>
			<tr>
				<th>No.</th>
				<th>Client Name</th>
				<th>Client ID</th>
				<th>Loan amount</th>
				<th>Loan Type</th>
				<th>Originating Branch</th>
				<th>Initiation Date</th>
				<th>Loan Appl. Status</th>
				<th>Turn Around Time</th>
				<th>SLA</th>
			</tr>
		</thead>
	</thead>

	<tbody>

	<?php
	$conditions = '1';
	$value='';
	$values_ar=[];
	$placeholder='';
	if($_POST['organisation'] != '') {
		$conditions .= ' AND organisation LIKE "%?%"';
		//$value.=$_POST['organisation'];
		$arr=array_push($values_ar, $_POST['organisation']);
		$placeholder.='s';
	    }
	if($_POST['doc_type'] != '') {
		$conditions .= ' AND documents.document_type= ?';
		//$value.=$_POST['doc_type'];
		$arr=array_push($values_ar, $_POST['doc_type']);
		$placeholder.='s';
	     }
			 if($_POST['doc_type'] == '') {
				 $conditions .= ' AND (documents.document_type= ? OR documents.document_type= ? OR documents.document_type= ? OR documents.document_type= ?)';
				 //$value.=$_POST['doc_type'];
				 $arr=array_push($values_ar, 'Business Loan');
				 $arr=array_push($values_ar, 'SE Loan');
				 $arr=array_push($values_ar, 'PRE-SCORED Loan');
				 $arr=array_push($values_ar, 'BRANCH IDEAL LOANS');
				 $placeholder.='ssss';
						}
	if($_POST['internal_ref_number'] != ''){
	 $conditions .= ' AND internal_ref_number=?';
	// $value.=$_POST['internal_ref_number'];
	 $arr=array_push($values_ar, $_POST['internal_ref_number']);
	 $placeholder.='i';
	 }
	if($_POST['staff'] != ''){
	 $conditions .= ' AND (doc_track.to_id=? OR doc_track.from_id = ?)';
     //$value.=$_POST['staff'];
	 $arr=array_push($values_ar, $_POST['staff']);
	 //$value.=$_POST['staff'];
	 $arr=array_push($values_ar, $_POST['staff']);
	 $placeholder.='ss';
	}
	if($_POST['unit'] != '') {
		$conditions .= ' AND admins2.department_id = ?';
	    //$value.=$_POST['unit'];
	    $arr=array_push($values_ar, $_POST['unit']);
	    $placeholder.='s';
	   }
	if($_POST['source'] != '') {
		$conditions .= ' AND source LIKE "%?%" ';
		$arr=array_push($values_ar, $_POST['source']);
	    $placeholder.='s';
	   }
		 if($_POST['branch'] != '') {
	 		$conditions .= ' AND organisation = ? ';
	 		$arr=array_push($values_ar, $_POST['branch']);
	 	    $placeholder.='s';
	 	   }
	if($_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-d H:i:s',strtotime($dates[0]));
		$endDate = date('Y-m-d H:i:s',strtotime($dates[1]. ' + 1 day'));
		$conditions .= ' AND (date >= ? AND date <= ?)';
		$arr=array_push($values_ar,$startDate);
		$arr=array_push($values_ar, $endDate);
	    $placeholder.='ss';
	}

   $count=count($values_ar);
   $stmt=$conn->prepare("SELECT documents.* FROM documents LEFT JOIN doc_track ON documents.id = doc_track.doc_id  WHERE $conditions AND documents.id IS NOT NULL GROUP BY doc_id ORDER BY date desc");
   //$form_result =$conn->query($form_query);
  // $stmt =$conn->prepare($form_query);
   $stmt->bind_param($placeholder, ...$values_ar);
   $stmt->execute();
   $form_result = $stmt->get_result();

    $i=1;
		while($form_data = $form_result->fetch_assoc()) {
	 ?>
		 <?php if($form_data['doc_type_id']=="87"){
			 $id=$form_data['id'];
			  $doc_type_id=87;
			 $doc_type="BUSINESS LOAN";
		$query="SELECT * from cstm_business_loan where document_id='$id' ";
	 $result= $conn->query($query);
	 $frm_data = $result->fetch_assoc();
	 ?>

		 <?php  } ?>
		 <?php if($form_data['doc_type_id']=="88"){
			 $id=$form_data['id'];
			 $doc_type_id=88;
			 $doc_type="SE LOAN";
		$query="SELECT * from cstm_se_loan where document_id='$id' ";
	 $result= $conn->query($query);
	 $frm_data = $result->fetch_assoc();
	 ?>

		 <?php  } ?>
		 <?php if($form_data['doc_type_id']=="89"){
			 $id=$form_data['id'];
			 $doc_type_id=89;
		$query="SELECT * from pre_scored_loan where document_id='$id' ";
	 $result= $conn->query($query);
	 $frm_data = $result->fetch_assoc();
?>

		 <?php  } ?>

	 	<?php $timeline_query = "SELECT
	 							dt.id,dt.actionDate,dt.document_type,dt.doc_id,dt.from_id,dt.to_id,dt.to_stage,dt.from_id,dt.receive_date,dt.archive_date,
	 							dt.to_status,doc.created, doc.document_type,doc.created_by,
	 							admins.id, _to.id,
	 							TIME_TO_SEC(wf.duration) dur,TIME_TO_SEC(wf.duration1) t1,TIME_TO_SEC(wf.duration2) t2
	 							 FROM
	 							doc_track dt
	 							LEFT JOIN documents doc ON dt.doc_id = doc.id
	 							LEFT JOIN admins ON dt.from_id = admins.id
	 							LEFT JOIN admins _to ON dt.to_id = _to.id
	 							LEFT JOIN document_type_stages wf ON wf.stage=dt.to_stage AND wf.doc_type_id='".$doc_type_id."'
	 							 WHERE
	 							doc_id = ".$id."
								 GROUP BY
								 dt.doc_id
	 							 ORDER BY
	 							dt.actionDate ASC";

	 				$timeline_result = $conn->query($timeline_query);
	 				$now = new DateTime();
	 				$now->setTime( 0, 0, 0 );

	 				$index = 0;
	 				$total_dur=0;
	 				$total_sla=0;
	 				 while($timeline = $timeline_result->fetch_assoc()) {
	 				 //	var_dump($timeline_result->fetch_assoc());
	 				if($timeline['receive_date']=='0000-00-00 00:00:00'&& $timeline['archive_date']=='0000-00-00 00:00:00'){
	 					$time = 0;
	 					$unread = biss_hours($timeline['actionDate'],date("Y-m-d H:i:s"));
	 					//$unread = strtotime(date("Y-m-d H:i:s"))-strtotime($timeline['actionDate']);
	 				} elseif($timeline['archive_date']=='0000-00-00 00:00:00'&& $timeline['receive_date']!='0000-00-00 00:00:00'){
	 					//$time = strtotime(date("Y-m-d H:i:s"))-strtotime($timeline['receive_date']);
	 					//$unread = strtotime($timeline['receive_date'])-strtotime($timeline['actionDate']);
	 					$time = biss_hours($timeline['receive_date'],date("Y-m-d H:i:s"));
	 					$unread = biss_hours($timeline['actionDate'],$timeline['receive_date']);
	 				} else {
	 					//$time = strtotime($timeline['archive_date'])-strtotime($timeline['receive_date']);
	 					//$unread = strtotime($timeline['receive_date'])-strtotime($timeline['actionDate']);
	 					$time = biss_hours($timeline['receive_date'],$timeline['archive_date']);
	 					$unread = biss_hours($timeline['actionDate'],$timeline['receive_date']);
	 				}

	 				?>

	 			<?php  if($index == 0){  ?>

	 			<?php	} if($timeline['receive_date']!='0000-00-00 00:00:00'&& $timeline['to_stage']!='CLOSED'){ ?>


	 					<?php
	 						$total_dur=$total_dur+$unread;
	 						$total_sla=$total_sla+$timeline['dur'];
	 				?>



	 					<?php $total_dur=$total_dur+$time;

	 				?>

	 			<?php  } elseif($timeline['receive_date']=='0000-00-00 00:00:00' && $timeline['archive_date']=='0000-00-00 00:00:00') { ?>
	 			 <?php if($timeline['to_stage']!='CLOSED'){?>

	 					<?php
						$total_dur=$total_dur+$unread;
	 					$total_sla=$total_sla+$timeline['dur'];
	 				?>



	 			<?php }else{?>

	 			<?php }} else {?>


	 			<?php } ?>
	 			<?php $index ++; } ?>

	 	 <tr>
		 <td><?php echo $i; ?></td>
		 <td><?php echo $frm_data['applicant_name'] ?></td>
		 <td><a href="timeline.php?ac=<?php echo $form_data['id']; ?>"><?php echo $frm_data['appId']; ?></a></td>
		 <td><?php echo $frm_data['loan_amount']; ?></td>
		 <td><?php echo $form_data['document_type']; ?></td>
		 <td><?php echo getStaffStation($form_data["created_by"]); ?></td>
		 <td><?php echo date("j M Y",strtotime($form_data['date'])); ?></td>
		 <td><?php echo $form_data["status_label"]; ?></td>
		 <td><b><?php echo duration($total_dur);?></td>
			<td><b><?php echo duration($total_sla);?></td>
	 </tr>
	 <?php $i++;}?>

	</tbody>
</table>

<?php } else { ?>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->


		<div class="row">
			<div class="col-xs-12 col-sm-6 widget-container-col">
				<!-- #section:custom/widget-box -->
				<div class="widget-box">
					<div class="widget-header">
						<h5 class="widget-title">Loan Application Report</h5>
						<!-- #section:custom/widget-box.toolbar -->
						<div class="widget-toolbar">
							<a href="#" data-action="collapse">
								<i class="ace-icon fa fa-chevron-up"></i>
							</a>
						</div>
						<!-- /section:custom/widget-box.toolbar -->
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<form name="searchFrm" action="pages/reports/custom.php?type=21" method="post" id="searchFrm">
								<input type="hidden" name="extra" value="dhfkjhsdfjhsdjfhdskj"/>
																					<div class="profile-user-info profile-user-info-striped">
																							<div class="profile-info-row">
																									<div class="profile-info-name"> Date submitted </div>

																									<div class="profile-info-value">
																											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>

												<input class="form-control" type="text" name="duration" id="date-range-picker" />
											</div>
																									</div>
																							</div>

																							<div class="profile-info-row">
																									<div class="profile-info-name"> Loan type </div>

																									<div class="profile-info-value">
																											<span class="editable" id="username">
												<select name="doc_type">
												<option value="">All</option>
												<?php	$sql14 = "SELECT * FROM document_types WHERE id=87 OR id=88 OR id=89 OR id=90 ORDER BY doc_classification, doc_type";
														$result14 = $conn->query($sql14);
														while($row14 = $result14->fetch_assoc()) { ?>
												<option value="<?php echo $row14['doc_type']; ?>"><?php echo $row14['doc_type']; ?></option>
												<?php } ?>
												</select>
											</span>
																									</div>
																							</div>
																							<div class="profile-info-row">
																									<div class="profile-info-name"> Reference number </div>

																									<div class="profile-info-value">
																											<span class="editable" id="username"><input name="internal_ref_number" type="text"/></span>
																									</div>
																							</div>
									<div class="profile-info-row">
																									<div class="profile-info-name"> Staff (Passed Through) </div>

																									<div class="profile-info-value">
																											<select  name="staff"  class="chosen-select" data-placeholder="select staff...">
												<option value="">All</option>
											<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
													$form_result = $conn->query($form_query);
													while($ro_data = $form_result->fetch_assoc()) {   ?>
												<option value="<?php echo $ro_data['id']; ?>">
													<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
												</option>
												<?php } ?>
											</select>
																									</div>
																							</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Originating Branch </div>

										<div class="profile-info-value">
											<select  name="branch"  class="chosen-select" data-placeholder="select staff...">
												<option value="">All</option>
											<?php $form_query = "SELECT * FROM dutystations ORDER BY duty_station";
													$form_result = $conn->query($form_query);
													while($ro_data = $form_result->fetch_assoc()) {   ?>
												<option value="<?php echo $ro_data['duty_station']; ?>">
													<?php echo $ro_data['duty_station']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</div>


																							<div class="profile-info-row">
																								<div class="profile-info-name">&nbsp;</div>
																									<div class="profile-info-value">
																											<a class="btn btn-sm btn-success search-btn"> Search
																													<i class="ace-icon fa fa-search bigger-110"></i>
																											</a>
																									</div>
																							</div>




																					</div>

							</form>
						</div>
					</div>
				</div>
				<!-- /section:custom/widget-box -->
			</div>
		</div><!-- /.row -->

		<div class="space-24"></div>
	</div><!-- /.col -->
</div><!-- /.row --><!-- /.row -->

<?php  } ?>

<?php function get_duration($doc_id, $doc_type_id){?>

<?php }?>
