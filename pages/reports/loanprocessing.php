<?php if(isset($_POST['getReport'])){
session_start();
require_once("../../include/cnx.php");
require_once("../../include/database.php");
include("../../include/session_tracker.php"); ?>
<h5 class="widget-title bigger lighter"><i class="ace-icon fa fa-table"></i> Loan Processing Report <a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success">Export Report to excel</a></h5>
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Client Name</th>
			<th>Client ID</th>
			<th>Product</th>
			<th>Approved Amount</th>
			<th>Application Date</th>
			<th>Credit Committee Date</th>
			<th>Branch</th>
			<th>Disbursement Date</th>
			<th>Date of Request</th>
			<th>Date of Closure</th>
			<th>Service Time</th>
			<th>Steps</th>
		</tr>
	</thead>

	<tbody>

	<?php
	$conditions = ' doc_track.to_id <> "" ';
	if(@$_POST['organisation'] != '') $conditions .= ' AND organisation LIKE "%'.$_POST['organisation'].'%"';
	if(@$_POST['doc_type'] != '') $conditions .= ' AND document_type="'.$_POST['doc_type'].'"';
	if(@$_POST['undp_ref_number'] != '') $conditions .= ' AND undp_ref_number LIKE "%'.$_POST['undp_ref_number'].'%"';
	if(@$_POST['staff'] != '') $conditions .= ' AND (doc_track.to_id = '.$_POST['staff'].' OR doc_track.from_id = '.$_POST['staff'].') ';
	if(@$_POST['unit'] != '') $conditions .= ' AND (admins2.department_id = '.$_POST['unit'].' ) ';
	if(@$_POST['source'] != '') $conditions .= ' AND source LIKE "%'.$_POST['source'].'%" ';
	if(@$_POST['duration'] != '') {
		$dates = explode(' - ',$_POST['duration']);
		$startDate = date('Y-m-j H:i',strtotime($dates[0]));
		$endDate = date('Y-m-j H:i',strtotime($dates[1]));

		$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
	}

	if($_POST['days'] != ''){
		$conditions .= ' AND DATEDIFF(IF(archive_date = "0000-00-00 00:00:00",NOW(),archive_date),actionDate) = '.$_POST['days'];
	}

	//$form_query = "SELECT a.*,DATEDIFF(IF(a.date_closed = '0000-00-00 00:00:00',NOW(),a.date_closed),a.created) dur FROM documents a WHERE a.doc_type_id='21'";
	$form_query = "SELECT a.*,DATEDIFF(IF(a.date_closed = '0000-00-00 00:00:00',NOW(),a.date_closed),a.created) dur, b.fname,b.lname, (SELECT COUNT(*) FROM doc_track WHERE doc_id=a.id) AS steps FROM documents a LEFT JOIN admins b ON a.created_by = b.id WHERE a.doc_type_id='21'";

	  $form_result = $conn->query($form_query);

	  while($form_data = $form_result->fetch_assoc()) {   	  $tempData=json_decode($form_data["template"],true);
	  ?>
		<tr>
			<td><?php echo $tempData['Client_Name']; ?></td>
			<td><?php echo $tempData['Client_ID']; ?></td>
			<td><?php echo $tempData['Product']; ?></td>
			<td><?php echo number_format($tempData['Value_of_Loan']); ?></td>
			<td><?php echo $tempData['Date_of_loan_application']; ?></td>
			<td><?php echo $tempData['Date_of_Credit_Committee']; ?></td>
			<td><?php echo getStaffStation($form_data["created_by"]); ?></td>
			<td><?php echo ""; ?></td>
			<td><?php echo date("j M Y",strtotime($form_data['date'])); ?></td>
			<td><?php echo $form_data['file_status']!='APPROVED'?"OPEN":date("j M Y",strtotime($form_data['date_closed'])); ?></td>
			<td><?php echo $form_data['dur']; ?> days</td>
			<td><?php echo $form_data['steps']; ?></td>
		</tr>
	  <?php } ?>



	</tbody>
</table>

<?php } else { ?>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12 col-sm-6 widget-container-col">
				<!-- #section:custom/widget-box -->
				<div class="widget-box">
					<div class="widget-header">
						<h5 class="widget-title">WorkFlow Report</h5>
						<!-- #section:custom/widget-box.toolbar -->
						<div class="widget-toolbar">
							<a href="#" data-action="collapse">
								<i class="ace-icon fa fa-chevron-up"></i>
							</a>
						</div>
						<!-- /section:custom/widget-box.toolbar -->
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<form name="searchFrm" action="pages/reports/loanprocessing.php" method="post" id="searchFrm">
							<input type="hidden" name="extra" value="dhfkjhsdfjhsdjfhdskj"/>
							<input type="hidden" name="getReport" value="gt09cds7348" />
							<div class="profile-user-info profile-user-info-striped">

								<div class="profile-info-row">
									<div class="profile-info-name"> Branch </div>

									<div class="profile-info-value">
										<select  name="staff"  class="chosen-select" data-placeholder="select staff...">
											<option value="">  </option>
										<?php $form_query = "SELECT * FROM dutystations ORDER BY duty_station";
											  $form_result = $conn->query($form_query);
											  while($ro_data = $form_result->fetch_assoc()) {   ?>
											<option value="<?php echo $ro_data['id']; ?>">
												<?php echo $ro_data['duty_station']; ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="profile-info-row">
									<div class="profile-info-name"> Product </div>

									<div class="profile-info-value">
										<span class="editable" id="username"><input name="product" type="text"/></span>
									</div>
								</div>

								<div class="profile-info-row">
									<div class="profile-info-name"> Period </div>

									<div class="profile-info-value">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-calendar bigger-110"></i>
											</span>
											<input class="form-control" type="text" name="duration" id="date-range-picker" />
										</div>
									</div>
								</div>
								<div class="profile-info-row">
									<div class="profile-info-name">&nbsp;</div>
									<div class="profile-info-value">
										<a class="btn btn-sm btn-success search-btn"> Apply Filter
											<i class="ace-icon fa fa-search bigger-110"></i>
										</a>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /section:custom/widget-box -->
			</div>
		</div><!-- /.row -->

		<div class="space-24"></div>
	</div><!-- /.col -->
</div><!-- /.row --><!-- /.row -->
<?php  } ?>
