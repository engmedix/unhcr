<?php require_once("include/cnx.php"); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Doctracker workflow</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="assets/css/ace-rtl.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									&nbsp;
								</h1>
								<h4 class="blue" id="id-company-text"></h4>
							</div>

							<?php if(isset($_GET['loginfail'])){ $msg = explode("$$$",base64_decode($_GET['loginfail'])); ?>
							<div class="alert alert-danger">
								<strong>
									<i class="ace-icon fa fa-times"></i>
										Login Error!
								</strong>
								<?php echo $msg[0]; ?>
								<br>
							</div>
							<?php } ?>
							<?php if(isset($_GET['pwdreset'])){
							$msg[1] = $_POST['staff_un'];

							$usrr_id = $_POST['staff_id'];
							$pwd1 = $_POST['newpwd1'];
							$reset_query = "UPDATE admins SET
							pwd = '$pwd1',
							activation_key = '',
							force_pwd_change = '0',
							pwd_changed = NOW()
							WHERE ID='$usrr_id'";

							if($conn->query($reset_query)===TRUE){ 	?>

							<div class="alert alert-success">
								<strong>
									<i class="ace-icon fa fa-check"></i>
										Password Changed
								</strong>
								Login using your new password.
								<br>
							</div>
							<?php } else{ ?>
							<div class="alert alert-danger">
								<strong>
									<i class="ace-icon fa fa-times"></i>
										Password Change Error!
								</strong>
								System was unable to change your password at this time. Please go to your email and click the the link to try again.
								<br>
							</div>
							<?php } } ?>

							<div class="space-6"></div>

							<?php if(isset($_GET['do'])){ ?>
							<div class="position-relative">
								<div class="login-box visible widget-box no-border">

									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>

											<?php if($_GET['do']=="forgot"){ include('include/do_forgot_password.php'); } ?>

											<?php if($_GET['do']=="reset"){

											if(isset($_GET['code'])){ $act_code = $_GET["code"];
												$sql = "SELECT * FROM admins WHERE activation_key='$act_code'";
											}
											if(isset($_POST['short_code'])){ $act_code = $_POST["short_code"];
												$sql = "SELECT * FROM admins WHERE activation_key like '$act_code%'";
											}

												$result = $conn->query($sql);

											if(mysqli_num_rows($result)>0){ //if the code exists
											$pwdrow = $result->fetch_assoc(); 	?>
											<p> Enter your new password: </p>
											<form method="post" action="?pwdreset">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="newpwd1" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="newpwd2" class="form-control" placeholder="Repeat password" />
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>

													<div class="space-24"></div>

													<input type="hidden" name="staff_id" value="<?php echo $pwdrow["id"]; ?>" />
													<input type="hidden" name="staff_fn" value="<?php echo $pwdrow["fname"]; ?>" />
													<input type="hidden" name="staff_em" value="<?php echo $pwdrow["email"]; ?>" />
													<input type="hidden" name="staff_un" value="<?php echo $pwdrow["uname"]; ?>" />

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="ace-icon fa fa-refresh"></i>
															<span class="bigger-110">Reset</span>
														</button>

														<button type="button" class="width-65 pull-right btn btn-sm btn-danger" onclick="this.form.submit()">
															<i class="ace-icon fa fa-unlock"></i>
															<span class="bigger-110">Save New Password</span>
														</button>
													</div>
												</fieldset>
											</form>
											<?php } else { ?>
											<div class="alert alert-danger">
												<strong>
													<i class="ace-icon fa fa-times"></i>
													Error!
												</strong>
												You entered an Invalid activation code!<br>
											</div>
											<form action="?do=reset" method="post">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="short_code" class="form-control" placeholder="Reset Code" style="text-transform:uppercase;" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="clearfix">

														<button type="button" class="width-35 pull-right btn btn-sm btn-danger" onclick="this.form.submit()">
															<i class="ace-icon fa fa-unlock"></i>
															<span class="bigger-110">Submit</span>
														</button>
													</div>
												</fieldset>

												<div class="space-12"></div>

												<div style="display:none"><input type="submit"/></div>

											</form>
											<?php }  ?>
											<?php } ?>
											<?php if(isset($short_code)){ ?>
											<form action="?do=reset" method="post">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="short_code" class="form-control" placeholder="Reset Code" style="text-transform:uppercase;" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="clearfix">

														<button type="button" class="width-35 pull-right btn btn-sm btn-danger" onclick="this.form.submit()">
															<i class="ace-icon fa fa-unlock"></i>
															<span class="bigger-110">Submit</span>
														</button>
													</div>
												</fieldset>

												<div class="space-12"></div>

												<div style="display:none"><input type="submit"/></div>

											</form>
											<?php } ?>
											<?php if(isset($invalid_email)){ ?>
											<form action="?do=forgot" method="post">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="reset_email" type="email" class="form-control" placeholder="Email" value="<?php echo $invalid_email; ?>" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-danger" onclick="this.form.submit()">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Send Me!</span>
														</button>
													</div>
													<div class="space-4"></div>
													<div style="display:none"><input type="submit" /></div>
												</fieldset>
											</form>
											<?php } ?>

										</div><!-- /.widget-main -->


									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->


							</div>

							<?php } else { ?>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">

									<div class="pull-right" style="padding-left: 22px;">
										<h1>
											<img src="assets/img/unhcr_logo.png" width="80px;" height="80px;">
											&nbsp;
										</h1>
										<h4 class="blue" id="id-company-text"></h4>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger" style="text-align:center">

												Please enter your information
											</h4>

											<div class="space-6"></div>

											<form method="post" action="include/do_login.php">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="userid" class="form-control" value="<?php if(isset($msg)) echo $msg[1]; ?>" placeholder="Username" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label>

														<button type="button" class="width-35 pull-right btn btn-sm btn-primary" onclick="this.form.submit()" >
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>
													<div style="display:none"><input type="submit"/></div>
													<div class="space-4"></div>
												</fieldset>
											</form>



											<div class="space-6"></div>


										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#forgot-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													I forgot my password
												</a>
											</div>

											<div>
												<a href="help/" target="_blank" class="forgot-password-link pull-right" title="Help">
													<i class="ace-icon fa fa-question-circle"></i> Help
												</a>
											</div>

										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<div id="forgot-box" class="login-box widget-box no-border">

									<div class="pull-right" style="padding-left: 22px;">
										<h1>
											<img src="assets/img/unhcr_logo.png">
											&nbsp;
										</h1>
										<h4 class="blue" id="id-company-text"></h4>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>
												Enter your email and to receive instructions
											</p>

											<form action="?do=forgot" method="post">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="reset_email" type="email" class="form-control" placeholder="Email" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-danger" onclick="this.form.submit()">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Send Me!</span>
														</button>
													</div>
													<div class="space-4"></div>
													<div style="display:none"><input type="submit" /></div>
												</fieldset>
											</form>

											<div class="space-6"></div>

										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#login-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													Back to login
												</a>
											</div>

											<div>
												<a href="help/" target="_blank" class="forgot-password-link pull-right" title="Help">
													<i class="ace-icon fa fa-question-circle"></i> Help
												</a>
											</div>
										</div>

									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

							</div><!-- /.position-relative -->
							<?php } ?>

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				//e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});



			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('#btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'blue');

				//e.preventDefault();
			 });
			/* $('#btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');

				//e.preventDefault();
			 });
			 $('#btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'light-blue');

				//e.preventDefault();
			 }); //*/

			});
		</script>
	</body>
</html>
