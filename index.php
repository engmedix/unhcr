<?php session_start();

	include("include/session_tracker.php");

	if($my['force_pwd_change']==1){
		if(!isset($_SESSION['pwd_change'])){
			$_SESSION['pwd_change']="force";
			include("pages/pwd_change.php");
		} else {
			include("pages/pwd_change.php");
		}
	} elseif($my['out_of_office']==1){
		if (!isset($_SESSION['out_of_office'])){
			$_SESSION['out_of_office']="out";
			include("pages/out.php");
		} else {
			include("pages/out.php");
		}
	} else {
		include("pages/dashboard.php");
	}

?>
