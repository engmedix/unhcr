<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>DocTracker Workflow</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>
		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="assets/css/dropzone.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		.my-progress-container{
			border:thin solid #ccc; position:relative;
		}
		.my-progress-container div{
			 position:absolute;
		}
		.my-progress-container span{
			z-index:1;
			position:relative;
		}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->

		<script type="text/javascript">

		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function addCommas(nStr) {

		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }

		  return x1 + x2;

		}

		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">


								<h3 class="header smaller lighter blue">Staff Performance</h3>

								<form action="" name="frmFilter" method="post">
								<div class="profile-user-info profile-user-info-striped">
									<div class="profile-info-row">
										<div class="profile-info-name"> Date </div>

										<div class="profile-info-value">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>

												<input class="form-control" type="text" name="duration" id="date-range-picker" />
											</div>
										</div>
									</div>

									<div class="profile-info-row">
											<div class="profile-info-name">&nbsp;</div>
											<div class="profile-info-value">
												<input type="submit" value="Search" class="btn btn-sm btn-success">
											</div>
									</div>
									</div>
								</form>
								<div class="widget-box transparent">
										<a onclick="export_to('dynamic-table');" target="_blank" class="btn btn-sm btn-success search-btn">Export Report to excel</a>

											<div class="widget-body">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped" id="dynamic-table">
														<thead class="thin-border-bottom">
															<tr>
																<th>Name</th>
																<th>Unit</th>
																<th>Docs In</th>
																<th>Docs Out</th>
																<th>Initiated</th>
																<th>Delegations</th>
																<th>Forwards</th>
																<th>Approvals</th>
																<th>Rejects</th>
																<th class="hidden-480">
																	<span class="label label-success">Fast</span>
																</th>
																<th class="hidden-480">
																	<span class="label label-warning">Normal</span>
																</th>
																<th class="hidden-480">
																	<span class="label label-danger">Late</span>
																</th>
															</tr>
														</thead>

														<tbody>
														<?php

														$conditions = 'WHERE to_id <> from_id ';

														if($_POST['duration'] != '') {
															$dates = explode(' - ',$_POST['duration']);
															$startDate = date('Y-m-j H:i',strtotime($dates[0]));
															$endDate = date('Y-m-j H:i',strtotime($dates[1]));

															$conditions .= ' AND (actionDate >= "'.$startDate.'" AND actionDate <= "'.$endDate.'")';
														}

														$form_query = "
														SELECT admins.*, COALESCE(docsout_,0) docsout,COALESCE(docsin_,0) docsin,COALESCE(fast,0) ft, COALESCE(slow,0) sl,COALESCE(late,0) lt, counts.* FROM admins
														LEFT JOIN
															(SELECT COUNT(*) docsout_, from_id FROM doc_track $conditions GROUP BY from_id) d_o ON admins.id = d_o.from_id
														LEFT JOIN
															(SELECT COUNT(*) docsin_, to_id FROM doc_track $conditions GROUP BY to_id) d_i ON admins.id = d_i.to_id
														LEFT JOIN
															(SELECT to_id u_id,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) <= 1,1,0)) fast,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 1 AND DATEDIFF(archive_date,actionDate) <= 3,1,0)) slow,
																	SUM(IF(DATEDIFF(IF(archive_date <> '0000-00-00 00:00:00',archive_date,NOW() ),actionDate) > 3,1,0)) late
																FROM doc_track $conditions GROUP BY to_id
															)stats ON admins.id = stats.u_id
														LEFT JOIN
															(SELECT from_id,
																	SUM(IF(action = 'DOCUMENT SENT',1,0)) initiated,
																	SUM(IF(action = 'DELEGATED',1,0)) delegations,
																	SUM(IF(action = 'FORWARDED',1,0)) forwards,
																	SUM(IF(action = 'REJECTED',1,0)) rejects,
																	SUM(IF(action = 'APPROVED',1,0)) approvals
																FROM doc_track $conditions GROUP BY from_id
															) counts ON admins.id = counts.from_id


														WHERE id<>'1' ORDER BY department_id, fname";

												  $form_result = $conn->query($form_query);
												  //echo $form_query;
												  while($form_data = $form_result->fetch_assoc()) {
													$w_fast = @floor(($form_data['ft']/$form_data['docsin'])*100);
													$w_slow = @floor(($form_data['sl']/$form_data['docsin'])*100);
													$w_late = @floor(($form_data['lt']/$form_data['docsin'])*100);
												  ?>
															<tr>
																<td><?php echo $form_data['fname']." ".$form_data['lname']." ".$form_data['oname']; ?></td>
																<td><?php echo getDept($form_data['department_id']); ?></td>
																<td><?php echo ($form_data['docsin']); ?></td>
																<td><?php echo ($form_data['docsout']); ?></td>
																<td><?php echo ($form_data['initiated']); ?></td>
																<td><?php echo ($form_data['delegations']); ?></td>
																<td><?php echo ($form_data['forwards']); ?></td>
																<td><?php echo ($form_data['approvals']); ?></td>
																<td><?php echo ($form_data['rejects']); ?></td>
																<td>
																	<div class="my-progress-container">
																		<div class="label-success" style="width:<?php echo $w_fast; ?>%;">&nbsp;</div>
																		<span><?php echo ($w_fast); ?>%</span>
																	</div>
																</td>
																<td>
																	<div class="my-progress-container">
																		<div class="label-warning" style="width:<?php echo $w_slow; ?>%;">&nbsp;</div>
																		<span><?php echo ($w_slow); ?>%</span>
																	</div>
																</td>
																<td>
																	<div class="my-progress-container">
																		<div class="label-danger" style="width:<?php echo $w_late; ?>%;">&nbsp;</div>
																		<span><?php echo ($w_late); ?>%</span>
																	</div>
																</td>
															</tr>
														<?php } ?>

														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->



					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/bootstrap-wysiwyg.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>



		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });



				var form_initialized = false;
				function initialize_form() {
					if(form_initialized) return;
					form_initialized = true;

					//intialize wysiwyg editor
					$('.message-form .wysiwyg-editor').ace_wysiwyg({
						toolbar:
						[
							'bold',
							'italic',
							'strikethrough',
							'underline',
							null,
							'justifyleft',
							'justifycenter',
							'justifyright',
							null,
							'createLink',
							'unlink',
							null,
							'undo',
							'redo'
						]
					}).prev().addClass('wysiwyg-style1');



					//file input
					$('.message-form input[type=file]').ace_file_input()
					.closest('.ace-file-input')
					.addClass('width-90 inline')
					.wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>');

					//Add Attachment
					//the button to add a new file input
					$('#id-add-attachment')
					.on('click', function(){
						var file = $('<input type="file" name="attachment[]" />').appendTo('#form-attachments');
						file.ace_file_input();

						file.closest('.ace-file-input')
						.addClass('width-90 inline')
						.wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>')
						.parent().append('<div class="action-buttons pull-right col-xs-1">\
							<a href="#" data-action="delete" class="middle">\
								<i class="ace-icon fa fa-trash-o red bigger-130 middle"></i>\
							</a>\
						</div>')
						.find('a[data-action=delete]').on('click', function(e){
							//the button that removes the newly inserted file input
							e.preventDefault();
							$(this).closest('.file-input-container').hide(300, function(){ $(this).remove() });
						});
					});
				}//initialize_form


			$('#date-range-picker').daterangepicker({
				'applyClass' : 'btn-sm btn-success',
				'cancelClass' : 'btn-sm btn-default',
				locale: {
					applyLabel: 'Apply',
					cancelLabel: 'Cancel',
				}
			})

			});

			function export_to(tableId){
				var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
				var textRange; var j=0;
				tab = document.getElementById(tableId); // id of table

				for(j = 0 ; j < tab.rows.length ; j++)
				{
					tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
					//tab_text=tab_text+"</tr>";
				}

				tab_text=tab_text+"</table>";
				tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
				tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
				tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE ");

				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
				{
					txtArea1.document.open("txt/html","replace");
					txtArea1.document.write(tab_text);
					txtArea1.document.close();
					txtArea1.focus();
					sa=txtArea1.document.execCommand("SaveAs",true,"save report.xls");
				}
				else                 //other browser not tested on IE 11
					sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

				return (sa);
			}
		</script>

				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
