<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php"); if(isset($_GET['ac'])){ $acid=$_GET['ac']; } else { echo '<script>window.location.href="accounts.php";</script>'; }
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Delegation Request</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon_io/favicon-32x32.png"/>
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->


	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">

					<?php if(isset($_GET['ac'])){
							$acid = $_GET['ac'];
							$form_query = "SELECT * FROM delegations WHERE id='$acid'";
							$form_result = $conn->query($form_query);
							while($form_data = $form_result->fetch_assoc()) { 	?>
						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Delegation Request</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>

												<div >

														<div class="profile-user-info profile-user-info-striped">
															<div class="profile-info-row">
																<div class="profile-info-name"> Request Date </div>

																<div class="profile-info-value">
																	<span class="editable" id="username"><?php echo $form_data['date'] ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Requested By </div>

																<div class="profile-info-value">
																	<span class="editable" id="country"><b><?php echo getStaffName($form_data['requested_by']); ?></b></span><br>
																	<span class="editable" id="country">DEPARTMENT: <?php echo getStaffDept($form_data['requested_by']); ?></span><br>
																	<span class="editable" id="country">DUTY STATION: <?php echo getStaffBranch($form_data['requested_by']); ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> To be delegated to </div>

																<div class="profile-info-value">
																	<span class="editable" id="age"><?php echo getStaffName($form_data['delegate_to']); ?></span><br>
																	<span class="editable" id="age">DEPARTMENT: <?php echo getStaffDept($form_data['delegate_to']); ?></span><br>
																	<span class="editable" id="age">DUTY STATION: <?php echo getStaffBranch($form_data['delegate_to']); ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Duration </div>

																<div class="profile-info-value">
																	<span class="editable" id="signup"><?php echo 'From: <b>'.date("j M Y", strtotime(substr($form_data['duration'], 0, 10)))."</b> To: <b>".date("j M Y", strtotime(substr($form_data['duration'], 13, 10))).'</b>'; ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Reason </div>

																<div class="profile-info-value">
																	<span class="editable" id="login"><?php echo nl2br($form_data['reason']); ?></span>
																</div>
															</div>

															<div class="profile-info-row">
																<div class="profile-info-name"> Actions </div>

																<div class="profile-info-value">

																	<div class="clearfix">
																		<?php if(date("Y-m-d")>$end){ ?>

																		<span class="label label-danger ">EXPIRED</span>

																		<?php } else { ?>
																		<?php if($form_data['approved']==0){ ?>
																			<a href="delegation.php?do=approve&ac=<?php echo $form_data['id']; ?>" class="btn btn-sm btn-info">Approve</a>
																			&nbsp;&nbsp;&nbsp;&nbsp;
																			<a href="delegation.php?do=reject&ac=<?php echo $form_data['id']; ?>" class="btn btn-sm btn-danger">Reject</a>
																			<?php } elseif($form_data['approved']==1 OR $form_data['approved']==2) { ?>
																			<a href="delegation.php?do=cancel&ac=<?php echo $form_data['id']; ?>" class="btn btn-sm btn-danger">Cancel Delegation</a>
																			<?php } else { ?>
																			<span class="label label-danger ">Delegation canceled</span>
																			<?php } ?>
																		<?php } ?>
																	</div>


																</div>
															</div>
														</div>


													</div>


										</div>
									</div>
								</div><!-- /.row -->
							<?php } } ?>



					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}


				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);



			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });


			});
		</script>

				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="docs/assets/js/rainbow.js"></script>
		<script src="docs/assets/js/language/generic.js"></script>
		<script src="docs/assets/js/language/html.js"></script>
		<script src="docs/assets/js/language/css.js"></script>
		<script src="docs/assets/js/language/javascript.js"></script>
	</body>
</html>
