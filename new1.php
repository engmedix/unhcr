<?php
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php");
$whr="";
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>New Work Order</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
		  <link rel="stylesheet" href="editor/css/froala_editor.css">
		  <link rel="stylesheet" href="editor/css/froala_style.css">
		  <link rel="stylesheet" href="editor/css/plugins/code_view.css">
		  <link rel="stylesheet" href="editor/css/plugins/image_manager.css">
		  <link rel="stylesheet" href="editor/css/plugins/image.css">
		  <link rel="stylesheet" href="editor/css/plugins/table.css">
		  <link rel="stylesheet" href="editor/css/plugins/video.css">
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
		    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/datepicker.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="assets/css/dropzone.css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css"/>



		<!--[if lte IE 9]>
		<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		<link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		<script type="text/javascript">

		    $(document).ready(function() {
		        $('#sendto').multiselect();
		    });

		</script>
		<script type="text/javascript">
		// Enable navigation prompt
		/*window.onbeforeunload = function() {
			return true;
		}; */

		function removeCommas(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function addCommas(nStr) {

		  nStr += '';
		  var comma = /,/g;
		  nStr = nStr.replace(comma,'');
		  x = nStr.split('.');
		  x1 = x[0];
		  x2 = x.length > 1 ? '.' + x[1] : '';
		  var rgx = /(\d+)(\d{3})/;
		  while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  }

		  return x1 + x2;

		}

		</script>
	</head>

	<body class="no-skin">

		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					<?php if(isset($_GET['doc_id'])){
					 $id=$_GET['edit'];
                    $wf_query= "SELECT * FROM document_type_stages where id='$id'";
					$wf_result = $conn->query($wf_query);
					$wf_data=$wf_result->fetch_assoc();
					//var_dump($wf_data);
					?>
					<div class="row">

						<div class="col-xs-12">
							<h4 class="" id="">Edit Workflow Stage</h4>
<?php
$status = "";
if(isset($_POST['action']) && $_POST['action']=='EDIT WORK FLOW')
{
          // var_dump($_POST['dataWF']);
           $duration=$_POST['dataWF']['duration'];
		   $stage=$_POST['dataWF']['stage'];
		   $description=$_POST['dataWF']['description'];
		   $id=$_POST['data']['id'];
		   $tbl=$_POST['data']['table'];
			//$data_update = $conn->real_escape_string($_POST['dataWF']);
			$update=$conn->query("update document_type_stages set stage='".$stage."',
description='".$description."', duration='".$duration."'where id='".$id."'");

		  if ($update === TRUE){
		  	$response = "Record Updated Successfully. </br></br>";
         // echo '<p style="color:#FF0000;">'.$response.'</p>';
	      //unset($_POST);
		  $_SESSION['workflow_msg'] = '<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Record was successfully updated.
</div>';
	      echo '<script>window.location="http://localhost/unhcr2/workflows.php"</script>';
	} else {
	$_SESSION['workflow_msg'] = '<<div class="alert alert-warning alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> Record failed to update.
</div>';
	 }
	}
	?>
													<form class="form-vertical" method="post" action="">

														<input type="hidden" class="form-control" name="data[id]" value="<?php echo $wf_data['id'];?>" />
														<input type="hidden" class="form-control" name="data[doc_type_id]" value="<?php echo $wf_data['doc_type_id'];?>" />
														<input type="hidden" class="form-control" name="action" value="EDIT WORK FLOW" />
														<div class="form-group">
															<label for="form-field-username">Stage Name</label>
															<div>
																<input type="text" class="form-control" name="dataWF[stage]" id="" value="<?php echo $wf_data['stage'];?>"  />
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Description</label>
															<div>
																<input type="text" class="form-control" name="dataWF[description]" value="<?php echo $wf_data['description'];?>" />
															</div>
														</div>
														<div class="form-group">
															<label for="">Step Time (Hrs:Mins:Secs)</label>
														<div class="input-group bootstrap-timepicker">
						                                 <input name="dataWF[duration]" class="form-control timepicker" type="text" value="<?php echo $wf_data['duration'];?>" />
						                                  <span class="input-group-addon">
							                               <i class="fa fa-clock-o bigger-110"></i>
						                                 </span>
					                                    </div>
					                                   </div>
														<p></p>
														<p></p>
														<p></p>
														<button class="btn btn-primary pull-right">
															<i class="ace-icon fa fa-floppy-o bigger-120"></i>
															Save changes
														</button>
														<input type="hidden" name="table" value="document_type_stages">

													</form>
											</div>
										</div>
								<?php }?>

								<?php if(isset($_GET['type']) AND $_GET['type']=="doc"){ ?>
								<div class="row">
								<div class="col-xs-12">
								<h3 class="header smaller lighter blue">Receive New External Document</h3>


									<form class="form-horizontal" id="validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

												<input type="hidden" name="data[sender_id]" value="<?php echo $_SESSION['user'] ?>"/>
												<input type="hidden" name="data[doc_classification]" value="EXTERNAL"/>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="account_type">Document Type<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">

															<div class="clearfix">
																<select class="form-control" id="doc_type" name="dt" required >
																	<option></option>
																	<?php	$sql14 = "SELECT * FROM document_types WHERE doc_classification = 'EXTERNAL' AND id NOT IN (87,88,89) ORDER BY doc_classification, doc_type";
																	$result14 = $conn->query($sql14);
																	while($row14 = $result14->fetch_assoc()) { ?>
															<option data-classification="<?php echo $row14['doc_classification']; ?>" value="<?php echo $row14['doc_type']; ?>"><?php echo $row14['doc_type']; ?></option>
															<?php } ?>

														</select>
														<input type="hidden" id="hidden_doc_type" name="data[document_type]" value=""/>

														<div id="screening" class="pull-left"></div>
														<input type="text" name="other" value="" placeholder="Specify..." id="input_doc_type" style="display:none;" onchange="$('#hidden_doc_type').val(this.value);"/>

															</div>

														</div>
													</div>

													<div class="hr hr-dotted"></div>
								                    <div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="subject">Subject</label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<textarea name="data[subject]" id="subject" class="form-control"></textarea>
															</div>
														</div>
													</div>
													<div class="hr hr-dotted"></div>
													<h4>Submited By</h4>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Name<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix" id="submitted">
																<input name="data[submitted_by]" id="submitted_by" class="form-control" type="text" required />
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="organisation">Organisation<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input name="data[organisation]" id="organisation" class="form-control" type="text" required />
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="organisation">Source of Correspondence<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<select class="form-control" name="data[source]" required>
																	<option value=""></option>
																	<option value="Government">Government</option>
																	<option value="Donors">Donors</option>
																	<option value="UNHCR Projects">UNHCR Projects</option>
																	<option value="Operational">Operational</option>
																	<option value="Others">Others</option>
																</select>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Phone</label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input name="data[phone]" id="phone" class="form-control" type="text">
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Email</label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input name="data[email]" id="email" class="form-control" type="email"/>
															</div>
														</div>
													</div>

													<div class="hr hr-dotted"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="doc_ref_number">Document Reference No:<span class="red">*<span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input name="data[doc_ref_number]" id="doc_ref_number" class="form-control" type="text" required>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Document Description<span class="red">*<span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<textarea class="form-control" name="data[description]" id="description" required></textarea>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Send To<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<select  name="data[current_holder][]"  class="form-control" id="externalsendto" multiple required >
																	<option value="">  </option>
																<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
																	  $form_result = $conn->query($form_query);
																	  while($ro_data = $form_result->fetch_assoc()) {?>
																	<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php if($ro_data['delegate_to']!=0){echo $ro_data['delegate_to'];}else{echo $ro_data['id'];} ?>">
																		<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																		<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
																	</option>
																	<?php } ?>

																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" >Attach files</label>
                           <div class="col-xs-12 col-sm-6">
														<table class="table ">
															<thead class="thin-border-bottom">
																<tr>
																	<th>
																		<i class="ace-icon fa fa-caret-right blue"></i>Remove
																	</th>
																	<th>
																		<i class="ace-icon fa fa-caret-right blue"></i>File Attachments
																	</th>

																</tr>
															</thead>

															<tbody>
																<tr>
																	<td colspan="2">
																		<div class="center"><a class="btn btn-xs addAttachmnt1" >Add attachment</a></div>

																	</td>
																</tr>
															</tbody>
														</table>
                            </div>
													</div>

													<div class="hr hr-dotted"></div>

												<!--	<input type="hidden" name="data[unhcr_ref_number]" value=""/>-->

													<div id="delegateto"></div>

													<input type="hidden" name="action" value="NEW DOC" >
													<input type="hidden" name="table" value="documents">
													<input type="hidden" name="data[file_status]" value="DOCUMENT SENT" >

								                   <div class="form-group center">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">&nbsp;</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input  type="submit" value="Submit" >
															</div>
														</div>
													</div>
												</form>
								</div>
								</div>
								<?php } ?>

								<?php if(isset($_GET['type']) AND $_GET['type']=="doc_i"){ ?>
								<div class="row">
								<div class="col-xs-12">
								<h3 class="header smaller lighter blue">New Internal Document</h3>


								<!-- div.table-responsive -->

								<!-- div.dataTables_borderWrap -->

										<form class="form-horizontal" id="validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

												<input type="hidden" name="data[sender_id]" value="<?php echo $_SESSION['user'] ?>"/>
												<input type="hidden" name="data[doc_classification]" value="INTERNAL"/>
												<input type="hidden" name="data[submitted_by]" value="<?php echo $_SESSION['user']?>"/>

								  		              <div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="account_type">Document Type<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">

															<div class="clearfix">
																<select class="form-control" id="doc_type" name="dt" required>
																	<option></option>
																	<?php	$sql14 = "SELECT * FROM document_types WHERE doc_classification = 'INTERNAL' ORDER BY doc_classification, doc_type";
																			$result14 = $conn->query($sql14);
																			while($row14 = $result14->fetch_assoc()) { ?>
																	<option data-classification="<?php echo $row14['doc_classification']; ?>" value="<?php echo $row14['doc_type']; ?>"><?php echo $row14['doc_type']; ?></option>
																	<?php } ?>

																</select>
																<input type="hidden" id="hidden_doc_type" name="data[document_type]" value=""/>

																<div id="screening" class="pull-left"></div>
																<input type="text" name="other" value="" placeholder="Other" id="input_doc_type" style="display:none;" onchange="$('#hidden_doc_type').val(this.value);"/>


															</div>

														</div>
													</div>

													<div class="hr hr-dotted"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="organisation">Organisation<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input class="form-control" name="data[organisation]" id="organisation" class="col-xs-12 col-sm-6" type="text" value="<?php echo $organisation; ?>" required />
															</div>
														</div>

													</div>

													<div class="space-2"></div>
													<div class="hr hr-dotted"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="doc_ref_number">Document Reference No:<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<input name="data[doc_ref_number]" id="doc_ref_number" class="form-control" type="text" required>
															</div>
														</div>
													</div>
													  <div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="subject">Subject</label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<textarea name="data[subject]" id="subject" class="form-control"></textarea>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Document Description<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="" id="">
																<textarea class="form-control" name="data[description]" id="description" required></textarea>
															</div>
														</div>
													</div>
								                    <div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Remarks</label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<textarea class="form-control" name="data[remark]" id=""></textarea>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Send To<span class="red">*</span></label>

														<div class="col-xs-12 col-sm-6">
															<div class="clearfix">
																<select  name="data[current_holder][]"  class="form-control" id="internalsendto"  multiple required >

																<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
																	  $form_result = $conn->query($form_query);
																	  while($ro_data = $form_result->fetch_assoc()) {?>
																	<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php if($ro_data['delegate_to']!=0){echo $ro_data['delegate_to'];}else{echo $ro_data['id'];} ?>">
																		<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																		<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
																	</option>
																	<?php } ?>

																</select>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" >Attach files</label>
                           <div class="col-xs-12 col-sm-6">
														<table class="table ">
															<thead class="thin-border-bottom">
																<tr>
																	<th>
																		<i class="ace-icon fa fa-caret-right blue"></i>Remove
																	</th>
																	<th>
																		<i class="ace-icon fa fa-caret-right blue"></i>File Attachments
																	</th>

																</tr>
															</thead>

															<tbody>
																<tr>
																	<td colspan="2">
																		<div class="center"><a class="btn btn-xs addAttachmnt1" >Add attachment</a></div>

																	</td>
																</tr>
															</tbody>
														</table>
                            </div>
													</div>

													<div class="form-group center">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">&nbsp;</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input  type="submit" value="Submit" >
															</div>
														</div>
													</div>
													<input type="hidden" name="data[undp_ref_number]" value=""/>

													<div id="delegateto"></div>

													<input type="hidden" name="action" value="NEW DOC" >
													<input type="hidden" name="table" value="documents">
													<input type="hidden" name="data[file_status]" value="DOCUMENT SENT" >
								          </form>

								</div>
								</div><!-- /.row -->
								<?php } ?>

								<?php if(isset($_GET['type']) AND $_GET['type']=="doc_gp"){
									//$wf_q = $conn->query("SELECT * FROM document_type_stages WHERE doc_type_id=87 AND stage='Initiation'");
								  //$wf = $wf_q->fetch_assoc();
									//	$actions2 = json_decode($wf['actions'],true);
									//	$action_taken = array_filter_by_value($actions2,"type",'forward');
									//	$action_taken=$action_taken[0];
										//	var_dump($action_taken);

									?>
					<div class="row">
						<div class="col-xs-12">
							<h3 class="header smaller lighter blue">New Goods Payment form <span class="label label-success pull-right">Created By: <?php echo $full_name." (".getDept($myUnit).")"; ?></span></h3>
							<!-- div.table-responsive -->
							<!-- div.dataTables_borderWrap -->
							<div>
								<div >
									<form class="form-horizontal validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

										<input type="hidden" name="data[sender_id]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[created_by]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[client_id]" value="<?php echo $_SESSION['client_id']; ?>"/>
										<input type="hidden" name="data[organisation]" value="<?php echo $my['duty_station']; ?>"/>
										<input type="hidden" name="data[email]" value="<?php echo $my['email']; ?>"/>
										<input type="hidden" name="data[phone]" value="<?php echo $my['phone']; ?>"/>
										<input type="hidden" name="data[submitted_by]" value="<?php echo $my['fullname']."-".$my['duty_station']; ?>"/>
										<input type="hidden" name="data[doc_classification]" value="EXTERNAL" />
										<input type="hidden" name="data[internal_ref_number]" value="" />
										<input type="hidden" name="action" value="NEW DOC" >
										<input type="hidden" name="table" value="documents">
										<input type="hidden" name="data[file_status]" id="file_status" value="SUBMITTED" >
										<input type="hidden" name="data[temp_type]" value="custom" >
										<input type="hidden" name="data[template]" value="*" >

										<input type="hidden" name="data[doc_type_id]" value="87"/>
										<input type="hidden" name="data[document_type]" value="Goods Payment Invoice"/>
										<!--<input type="hidden" name="data[current_holder]" value="<?php echo $_SESSION['user']; ?>"/>-->

										<?php $docT  = $conn->query("SELECT * FROM document_types WHERE id='87'");
												$docType = $docT->fetch_assoc();
												$actions = json_decode($docType['actions'],true);
												$submit = array_filter_by_value($actions,"type","submit");
												$submit = $submit[0];  ?>

										<input type="hidden" name="data[stage_label]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data[status_label]" value="<?php echo $submit['status_label']; ?>"/>
										<input type="hidden" name="data2[to_stage]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data2[to_status]" value="<?php echo $submit['status_label']; ?>"/>
										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">&nbsp;</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">

													<!-- START INVOICE -->

													<div class="tabbable">
														<ul class="nav nav-tabs" id="myTab">

															<li class="active">
																<a data-toggle="tab" href="#tab_loan" aria-expanded="false">
																	<i class="ace-icon fa fa-money bigger-120"></i>
																	Invoice Details
																</a>
															</li>

															<li class="">
																<a data-toggle="tab" href="#tab_others" aria-expanded="false">
																	<i class="ace-icon fa fa-paperclip bigger-120"></i>
																	Additional Files
																</a>
															</li>
														</ul>

														<div class="tab-content">


															<div id="tab_loan" class="tab-pane fade active in">
																<p>Fill in the following information about the invoice (<i>Facilitates Online Search and Statistical Analysis</i>).</p>
                                                                  <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Subject: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[subject]" id="subject" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Date: </label>
																	<div class="col-sm-9">
																		<div class="input-group">
																			<input class="form-control date-picker" value="<?php echo date('Y-m-d'); ?>" name="data3[date_created]" type="text" data-date-format="yyyy-mm-dd" required >
																			<span class="input-group-addon">
																				<i class="fa fa-calendar bigger-110"></i>
																			</span>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">UNHCR Branch: </label>
																	<div class="col-sm-9">
																		<select id="form-field-1-1" name="dataL[branch]" class="form-control" required>
																			 <option value="">Please select UNHCR branch </option>
																			<?php $form_query = "SELECT id, duty_station FROM dutystations WHERE 1 ORDER BY duty_station ASC";
																					$form_result = $conn->query($form_query);
																					while($ro_data = $form_result->fetch_assoc()) {?>
																				<option value="<?php echo $ro_data['id']; ?>"> <?php echo $ro_data['duty_station']; ?>
																				</option>
																				<?php } ?>
																		</select>
																	</div>
																</div>

															<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[external_ref_number]" id="external_ref_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. Ref. Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[doc_ref_number]" id="file_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. description: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[description]" id="description" type="text" >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's full names: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[submitted_by]" class="form-control"  >
																	</div>
																</div>
                                                                <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Organisation/Company: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[organisation]" class="form-control"  >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Email </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[email]" class="form-control"  >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Mobile No. </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[phone]" class="form-control"  >
																	</div>
																</div>

                                                              <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Scanned Invoice:<span class="red">*</span>
																	<span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" title="Attach Invoice"></span>
																	</label>
																	<div class="col-sm-9">
																		<label class="ace-file-input">
																			<input type="file" class="nice-input-file" name="upload[]" required/>
																			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
																		</label>
																	</div>
																</div>
															</div>

															<div id="tab_others" class="tab-pane fade">
																<p>Additional Files</p>



																<table class="table ">
																	<thead class="thin-border-bottom">
																		<tr>
																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>Description
																			</th>

																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>File
																			</th>

																		</tr>
																	</thead>

																	<tbody>
																		<tr>
																			<td colspan="2">
																				<div class="center"><a class="btn btn-xs addAttachmnt" >Add attachment</a></div>
																				<input type="hidden" name="att[append_type]" value="documents" />
																			</td>
																		</tr>
																	</tbody>
																</table>


															</div>


														</div>
													</div>




												</div>
											</div>
										</div>

										<div class="hr hr-dotted"></div>

										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Remarks</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">
													<textarea class="form-control autosize-transition" name="data[description]" id="description" required ></textarea>
												</div>
											</div>
										</div>
										<div class="hr hr-dotted"></div>

										<div id="doc_sendto">
										<?php $_POST['doc_id']=87;	include("include/ajx_doc_actions.php"); unset($_POST['doc_id']);	?>
										</div>
										<div id="delegateto"></div>

									</form>

								</div>
							</div>
						</div>
					</div><!-- /.row -->
					<?php } ?>

					<?php if(isset($_GET['type']) AND $_GET['type']=="doc_cwp"){  ?>
					<div class="row">
						<div class="col-xs-12">
							<h3 class="header smaller lighter blue">New Construction Works Payment form <span class="label label-success pull-right">Created By: <?php echo $full_name." (".getDept($myUnit).")"; ?></span></h3>
							<!-- div.table-responsive -->
							<!-- div.dataTables_borderWrap -->
							<div>
								<div >
									<form class="form-horizontal validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

										<input type="hidden" name="data[sender_id]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[created_by]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[client_id]" value="<?php echo $_SESSION['client_id']; ?>"/>
										<input type="hidden" name="data[organisation]" value="<?php echo $my['duty_station']; ?>"/>
										<input type="hidden" name="data[email]" value="<?php echo $my['email']; ?>"/>
										<input type="hidden" name="data[phone]" value="<?php echo $my['phone']; ?>"/>
										<input type="hidden" name="data[submitted_by]" value="<?php echo $my['fullname']."-".$my['duty_station']; ?>"/>
										<input type="hidden" name="data[doc_classification]" value="EXTERNAL" />
										<input type="hidden" name="data[internal_ref_number]" value="" />
										<input type="hidden" name="action" value="NEW DOC" >
										<input type="hidden" name="table" value="documents">
										<input type="hidden" name="data[file_status]" id="file_status" value="SUBMITTED" >
										<input type="hidden" name="data[temp_type]" value="custom" >
										<input type="hidden" name="data[template]" value="*" >
										<input type="hidden" name="data[doc_type_id]" value="88"/>
										<input type="hidden" name="data[document_type]" value="Construction Works Payment Invoice"/>

										<?php $docT  = $conn->query("SELECT * FROM document_types WHERE id='88'");
											  $docType = $docT->fetch_assoc();
											  $actions = json_decode($docType['actions'],true);
											  $submit = array_filter_by_value($actions,"type","submit");
											  $submit = $submit[0];  ?>

										<input type="hidden" name="data[stage_label]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data[status_label]" value="<?php echo $submit['status_label']; ?>"/>
										<input type="hidden" name="data2[to_stage]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data2[to_status]" value="<?php echo $submit['status_label']; ?>"/>

										<div class="row">

										</div>


										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">&nbsp;</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">

													<!-- START INVOICE -->

													<div class="tabbable">
														<ul class="nav nav-tabs" id="myTab">

															<li class="active">
																<a data-toggle="tab" href="#tab_loan" aria-expanded="false">
																	<i class="ace-icon fa fa-money bigger-120"></i>
																	Invoice Details
																</a>
															</li>

															<li class="">
																<a data-toggle="tab" href="#tab_others" aria-expanded="false">
																	<i class="ace-icon fa fa-paperclip bigger-120"></i>
																	Additional Files
																</a>
															</li>
														</ul>

														<div class="tab-content">


															<div id="tab_loan" class="tab-pane fade active in">
																<p>Fill in the following information about the invoice (<i>Facilitates Online Search and Statistical Analysis</i>).</p>
                                                                  <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Subject: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[subject]" id="subject" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Date: </label>
																	<div class="col-sm-9">
																		<div class="input-group">
																			<input class="form-control date-picker" value="<?php echo date('Y-m-d'); ?>" name="data3[date_created]" type="text" data-date-format="yyyy-mm-dd" required >
																			<span class="input-group-addon">
																				<i class="fa fa-calendar bigger-110"></i>
																			</span>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">UNHCR Branch: </label>
																	<div class="col-sm-9">
																		<select id="form-field-1-1" name="dataL[branch]" class="form-control" required>
																			 <option value="">Please select UNHCR branch </option>
																			<?php $form_query = "SELECT id, duty_station FROM dutystations WHERE 1 ORDER BY duty_station ASC";
																					$form_result = $conn->query($form_query);
																					while($ro_data = $form_result->fetch_assoc()) {?>
																				<option value="<?php echo $ro_data['id']; ?>"> <?php echo $ro_data['duty_station']; ?>
																				</option>
																				<?php } ?>
																		</select>
																	</div>
																</div>

													            <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[external_ref_number]" id="external_ref_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. Ref. Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[doc_ref_number]" id="file_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. description: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[description]" id="description" type="text" >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's full names: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[submitted_by]" class="form-control"  >
																	</div>
																</div>
                                                                <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Organisation/Company: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[organisation]" class="form-control"  >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Email </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[email]" class="form-control"  >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Mobile No. </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[phone]" class="form-control"  >
																	</div>
																</div>

                                                              <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Scanned Invoice:<span class="red">*</span>
																	<span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" title="Attach Invoice"></span>
																	</label>
																	<div class="col-sm-9">
																		<label class="ace-file-input">
																			<input type="file" class="nice-input-file" name="upload[]" required/>
																			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
																		</label>
																	</div>
																</div>
															</div>

															<div id="tab_others" class="tab-pane fade">
																<p>Additional Files</p>



																<table class="table ">
																	<thead class="thin-border-bottom">
																		<tr>
																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>Description
																			</th>

																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>File
																			</th>

																		</tr>
																	</thead>

																	<tbody>
																		<tr>
																			<td colspan="2">
																				<div class="center"><a class="btn btn-xs addAttachmnt" >Add attachment</a></div>
																				<input type="hidden" name="att[append_type]" value="documents" />
																			</td>
																		</tr>
																	</tbody>
																</table>


															</div>


														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="hr hr-dotted"></div>

										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Remarks</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">
													<textarea class="form-control autosize-transition" name="data[description]" id="description" required ></textarea>
												</div>
											</div>
										</div>
										<div class="hr hr-dotted"></div>

										<div id="doc_sendto">
										<?php $_POST['doc_id']=88;	include("include/ajx_doc_actions.php"); unset($_POST['doc_id']);	?>
										</div>
										<div id="delegateto"></div>

									</form>

								</div>
							</div>
						</div>
					</div><!-- /.row -->
					<?php } ?>


					<?php if(isset($_GET['type']) AND $_GET['type']=="doc_sp"){  ?>
					<div class="row">
						<div class="col-xs-12">
							<h3 class="header smaller lighter blue">New Service Payment form <span class="label label-success pull-right">Created By: <?php echo $full_name." (".getDept($myUnit).")"; ?></span></h3>
							<!-- div.table-responsive -->
							<!-- div.dataTables_borderWrap -->
							<div>
								<div >
									<form class="form-horizontal validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

										<input type="hidden" name="data[sender_id]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[created_by]" value="<?php echo $_SESSION['user']; ?>"/>
										<input type="hidden" name="data[client_id]" value="<?php echo $_SESSION['client_id']; ?>"/>
										<input type="hidden" name="data[organisation]" value="<?php echo $my['duty_station']; ?>"/>
										<input type="hidden" name="data[email]" value="<?php echo $my['email']; ?>"/>
										<input type="hidden" name="data[phone]" value="<?php echo $my['phone']; ?>"/>
										<input type="hidden" name="data[submitted_by]" value="<?php echo $my['fullname']."-".$my['duty_station']; ?>"/>
										<input type="hidden" name="data[doc_classification]" value="EXTERNAL" />
										<input type="hidden" name="data[internal_ref_number]" value="" />
										<input type="hidden" name="action" value="NEW DOC" >
										<input type="hidden" name="table" value="documents">
										<input type="hidden" name="data[file_status]" id="file_status" value="SUBMITTED" >
										<input type="hidden" name="data[temp_type]" value="custom" >
										<input type="hidden" name="data[template]" value="*" >

										<input type="hidden" name="data[doc_type_id]" value="89"/>
										<input type="hidden" name="data[document_type]" value="Service Payment Invoice"/>

										<?php $docT  = $conn->query("SELECT * FROM document_types WHERE id='89'");
											  $docType = $docT->fetch_assoc();
											  $actions = json_decode($docType['actions'],true);
											  $submit = array_filter_by_value($actions,"type","submit");
											  $submit = $submit[0];  ?>

										<input type="hidden" name="data[stage_label]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data[status_label]" value="<?php echo $submit['status_label']; ?>"/>
										<input type="hidden" name="data2[to_stage]" value="<?php echo $submit['stage_label']; ?>" />
										<input type="hidden" name="data2[to_status]" value="<?php echo $submit['status_label']; ?>"/>

										<div class="row">

										</div>


										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">&nbsp;</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">

															<!-- START INVOICE -->

													<div class="tabbable">
														<ul class="nav nav-tabs" id="myTab">

															<li class="active">
																<a data-toggle="tab" href="#tab_loan" aria-expanded="false">
																	<i class="ace-icon fa fa-money bigger-120"></i>
																	Invoice Details
																</a>
															</li>

															<li class="">
																<a data-toggle="tab" href="#tab_others" aria-expanded="false">
																	<i class="ace-icon fa fa-paperclip bigger-120"></i>
																	Additional Files
																</a>
															</li>
														</ul>

														<div class="tab-content">


															<div id="tab_loan" class="tab-pane fade active in">
																<p>Fill in the following information about the invoice (<i>Facilitates Online Search and Statistical Analysis</i>).</p>
                                                                  <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Subject: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[subject]" id="subject" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Date: </label>
																	<div class="col-sm-9">
																		<div class="input-group">
																			<input class="form-control date-picker" value="<?php echo date('Y-m-d'); ?>" name="data3[date_created]" type="text" data-date-format="yyyy-mm-dd" required >
																			<span class="input-group-addon">
																				<i class="fa fa-calendar bigger-110"></i>
																			</span>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">UNHCR Branch: </label>
																	<div class="col-sm-9">
																		<select id="form-field-1-1" name="dataL[branch]" class="form-control" required>
																			 <option value="">Please select UNHCR branch </option>
																			<?php $form_query = "SELECT id, duty_station FROM dutystations WHERE 1 ORDER BY duty_station ASC";
																					$form_result = $conn->query($form_query);
																					while($ro_data = $form_result->fetch_assoc()) {?>
																				<option value="<?php echo $ro_data['id']; ?>"> <?php echo $ro_data['duty_station']; ?>
																				</option>
																				<?php } ?>
																		</select>
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Invoice Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[external_ref_number]" id="external_ref_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. Ref. Number: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[doc_ref_number]" id="file_number" type="text" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Doc. description: </label>
																	<div class="col-sm-9">
																		<input class="form-control" value="" name="data[description]" id="description" type="text" >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's full names: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[submitted_by]" class="form-control"  >
																	</div>
																</div>
                                                                <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Organisation/Company: </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[organisation]" class="form-control"  >
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Email </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[email]" class="form-control"  >
																	</div>
																</div>

																<div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Sender's Mobile No. </label>
																	<div class="col-sm-9">
																		<input type="text" id="form-field-1-1" placeholder="" name="dataL[phone]" class="form-control"  >
																	</div>
																</div>

                                                              <div class="form-group">
																	<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Scanned Invoice:<span class="red">*</span>
																	<span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" title="Attach Invoice"></span>
																	</label>
																	<div class="col-sm-9">
																		<label class="ace-file-input">
																			<input type="file" class="nice-input-file" name="upload[]" required/>
																			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
																		</label>
																	</div>
																</div>
															</div>

															<div id="tab_others" class="tab-pane fade">
																<p>Additional Files</p>



																<table class="table ">
																	<thead class="thin-border-bottom">
																		<tr>
																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>Description
																			</th>

																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>File
																			</th>

																		</tr>
																	</thead>

																	<tbody>
																		<tr>
																			<td colspan="2">
																				<div class="center"><a class="btn btn-xs addAttachmnt" >Add attachment</a></div>
																				<input type="hidden" name="att[append_type]" value="documents" />
																			</td>
																		</tr>
																	</tbody>
																</table>


															</div>


														</div>
													</div>

													<!-- END INVOICE -->

												</div>
											</div>
										</div>

										<div class="hr hr-dotted"></div>

										<div class="form-group">
											<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Remarks</label>

											<div class="col-xs-12 col-sm-9">
												<div class="clearfix">
													<textarea class="form-control autosize-transition" name="data[description]" id="description" required ></textarea>
												</div>
											</div>
										</div>

										<div class="hr hr-dotted"></div>

										<div id="doc_sendto">
										<?php $_POST['doc_id']=89;	include("include/ajx_doc_actions.php"); unset($_POST['doc_id']);	?>
										</div>
										<div id="delegateto"></div>

									</form>

								</div>
							</div>
						</div>
					</div><!-- /.row -->
					<?php } ?>

					<?php if(isset($_GET['type']) AND $_GET['type']=="out"){ ?>
						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Outgoing Document</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>
											<div >


														<form class="form-horizontal validation-form" method="post" action="documents.php?new" enctype="multipart/form-data">

														<input type="hidden" name="data[doc_classification]" value="OUTGOING"/>


															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="account_type">Document Type<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-9">

																	<div class="clearfix">
																		<input type="text" name="data[document_type]" class="col-xs-12 col-sm-6" value=""/>
																	</div>

																</div>
															</div>


															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">From<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-5">
																	<div class="clearfix">
																		<select  name="data[sender_id]"  class="chosen-select" id="sendto" data-placeholder="select staff..." required >
																			<option value="">  </option>
																		<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' AND status='ACTIVE' ORDER BY fname, lname";
																			  $form_result = $conn->query($form_query);
																			  while($ro_data = $form_result->fetch_assoc()) {   ?>
																			<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php echo $ro_data['id']; ?>"  <?php echo (($ro_data['id']==$id)?' selected':''); ?>  >
																				<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																				<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
																			</option>
																			<?php } ?>

																		</select>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="account_type">Name of Consignee<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-9">

																	<div class="clearfix">
																		<input name="data[consignee]" id="consignee" class="col-xs-12 col-sm-6" type="text" required />

																	</div>

																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Recipients Tel Contact</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input name="data[recipient_tel]" id="phone" class="col-xs-12 col-sm-6" type="text">
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">No. of documents<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix" id="submitted">
																		<input name="data[no_of_docs]"  class="col-xs-12 col-sm-6" type="text" required />
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="organisation">Description<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<textarea class="input-xlarge" name="data[description]" id="description"></textarea>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" ></label>

																<div class="col-xs-12 col-sm-3">

																	<input type="file"  name="photo"  id="pic_upclose" />

																</div>
															</div>

															<div class="hr hr-dotted"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Send To<span class="red">*</span></label>

																<div class="col-xs-12 col-sm-5">
																	<div class="clearfix">
																		<select  name="data[current_holder]"  class="chosen-select" id="sendto" data-placeholder="select staff..." required >
																			<option value="">  </option>
																		<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' AND department_id=16 AND status='ACTIVE' ORDER BY fname, lname";
																			  $form_result = $conn->query($form_query);
																			  while($ro_data = $form_result->fetch_assoc()) {   ?>
																			<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php echo $ro_data['id']; ?>">
																				<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
																				<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
																			</option>
																			<?php } ?>

																		</select>
																	</div>
																</div>
															</div>


															<div class="hr hr-dotted"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">&nbsp;</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="submit" value="Submit" >
																	</div>
																</div>
															</div>

															<input type="hidden" name="data[undp_ref_number]" value=""/>

															<div id="delegateto"></div>

															<input type="hidden" name="action" value="NEW DOC" >
															<input type="hidden" name="table" value="documents">
															<input type="hidden" name="data[file_status]" value="DOCUMENT SENT" >


														</form>

														<!--
														<form action="documents.php" class="dropzone" id="dropzone" >
																	<div class="fallback">
																		<input type="file"  name="photo"  multiple=""  />
																	</div>
														</form> -->

													</div>
										</div>
									</div>
								</div><!-- /.row -->
					<?php } ?>



					<?php
					$acid=$_GET['ac'];
					if(isset($_GET['ac'])){
							$form_query = "SELECT documents.*,admins.fname,admins.lname,admins.oname,units.department, document_types.workflow FROM documents LEFT JOIN admins ON documents.sender_id = admins.id LEFT JOIN units ON department_id = units.id LEFT JOIN document_types ON documents.doc_type_id=document_types.id WHERE documents.id='$acid]'";
							$form_result = $conn->query($form_query);
							$form_data = $form_result->fetch_assoc();
						}
					    $docType_array=[87,88,89];
							if(in_array($form_data['doc_type_id'],$docType_array)){
				    	if(isset($_GET['type']) AND $_GET['type']=="action"){

							$doc_id = $_GET['ac'];

							$doc_q = $conn->query("SELECT a.*,b.workflow,b.signatures, b.signatures_sequential FROM documents a LEFT JOIN document_types b ON a.doc_type_id=b.id WHERE a.id='".$doc_id."'");
							$doc = $doc_q->fetch_assoc();

							$dTrk_q = $conn->query("SELECT * FROM doc_track WHERE doc_id='".$doc_id."' ORDER BY id DESC LIMIT 1");
							$dTrk = $dTrk_q->fetch_assoc();

							$wf_q = $conn->query("SELECT * FROM document_type_stages WHERE doc_type_id='".$doc['doc_type_id']."' AND stage='".$doc['stage_label']."'");
							//var_dump($wf_q->fetch_assoc());
							if($wf_q->num_rows>0){
								$is_prog_workflow = true;
								$wf = $wf_q->fetch_assoc();
								$actions2 = json_decode($wf['actions'],true);
							} else {
								$actions2 = json_decode($doc['workflow'],true);
								$is_prog_workflow = false;
							}
							$action_taken = array_filter_by_value($actions2,"type",$_GET['a']);
							$action_taken=$action_taken[0];
							$action_page = 'documents.php?new';
//var_dump($action_taken);
							$action_array = array(
								'approve'=>'APPROVED',
								'decline'=>'REJECTED',
								'reject'=>'REJECTED',
								'forward'=>'FORWARDED',
								'forward_in_current_dept'=>'FORWARDED',
								'update'=>'UPDATED STATUS',
								'dispatch'=>'DISPATCHED',
								'delivery'=>'DELIVERED',
							);

							$action_btns = array(
								'approve'=>'SUBMIT',
								'reject'=>'SUBMIT',
								'forward'=>'SUBMIT',
								'forward_in_current_dept'=>'SUBMIT',
								'decline'=>'SUBMIT',
								'update'=>'UPDATE',
								'dispatch'=>'DISPATCH',
								'delivery'=>'DELIVERY',
							);	?>

						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue"><?php echo " >> To: "; echo $action_taken['stage_label']!=""?$action_taken['stage_label']:$dTrk['from_stage']."";  ?></h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>

										<div>
											<div>

														<form class="form-horizontal validation-form" method="post" action="<?php echo $action_page; ?>" enctype="multipart/form-data">
														 <input type="hidden" name="data[actionDate]" value="<?php echo date('Y-m-d H:i:s'); ?>"/>

															<input type="hidden" name="action" value="WORKFLOW ACTION" >
															<input type="hidden" name="table" value="doc_track">
															<input type="hidden" name="data[doc_id]" value="<?php echo $_GET['ac']; ?>"/>

															<input type="hidden" name="data[from_id]" value="<?php echo $_SESSION['user']; ?>"/>
															<input type="hidden" name="data[receiver_status]" value="SENT"/>
															<input type="hidden" name="data[document_type]" value="<?php echo $doc['document_type']; ?>"/>
															<input type="hidden" name="tId" value="<?php echo $_GET['tId']; ?>" >

															<input type="hidden" name="data[from_stage]" value="<?php echo $doc['stage_label']; ?>" >
															<input type="hidden" name="data[to_stage]" value="<?php echo $action_taken['stage_label']; ?>" >
															<input type="hidden" name="data[from_status]" value="<?php echo $doc['status_label']; ?>" >
															<input type="hidden" name="data[to_status]" value="<?php echo $action_taken['status_label']; ?>" >
															<input type="hidden" name="data[action]" value="<?php echo $action_array[$_GET['a']]; ?>" >

															<input type="hidden" name="data2[file_status]" value="<?php echo $action_array[$_GET['a']]; ?>" >
															<input type="hidden" name="data2[stage_label]" value="<?php echo $action_taken['stage_label']; ?>" >
															<input type="hidden" name="data2[status_label]" value="<?php echo $action_taken['status_label']; ?>" >
															<input type="hidden" name="data[actionDate]" value="<?php echo date('Y-m-d H:i:s') ?>" >

													    	<?php if($_GET['a'] == 'update') { $status_label = $action_taken['status_label']; $ups = explode(",",$status_label); ?>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Set Status to:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<select name="data[action]" class="form-control" >
																		<?php foreach($ups as $up){ echo '<option value="'.$up.'">'.$up.'</option>';  } ?>
																		</select>
																	</div>
																</div>
															</div>

															<?php } ?>


															<?php if($is_prog_workflow AND $wf['temp_type']!="none") { ?>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" ></label>
																<div class="col-xs-12 col-sm-9">

															<?php if($wf['temp_type'] == 'form'){

																$strArray = explode("$$",$wf['template']);
																foreach($strArray as $index->$val){
																	$inputs = explode(";",$val);
																	if($inputs[1]!="select" && $inputs[1]!="textarea" && $inputs[1]!="multiselect" ){
																	echo '<div class="form-group">' .
																		'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'.$inputs[0].'">'.ucfirst($inputs[0]).'</label>' .
																		'<div class="col-xs-12 col-sm-9">' .
																			'<div class="clearfix">' .
																				'<input name="_frm['.str_replace(" ","_",$inputs[0]).']" id="'.$inputs[0].'" type="'.$inputs[1].'" ';
																				if($inputs[1]=="text"){ echo 'class="form-control"'; }else{ echo 'class="col-xs-12 col-sm-4"'; }
																				echo $inputs[2]+' />' .
																			'</div>' .
																		'</div>' .
																	'</div>';
																	}
																	if($inputs[1]=="textarea"){
																		echo '<div class="form-group">' .
																		'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'.$inputs[0].'">'.ucfirst($inputs[0]).'</label>' .
																		'<div class="col-xs-12 col-sm-9">' .
																			'<div class="clearfix">' .
																				'<textarea name="_frm['.str_replace(" ","_",$inputs[0]).']" id="'.$inputs[0].'" class="form-control  autosize-transition"  '.$inputs[2].'></textarea>' .
																			'</div>' .
																		'</div>' .
																	'</div>';
																	}
																	if($inputs[1]=="select"){
																		echo '<div class="form-group">' .
																		'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'.$inputs[0].'">'.ucfirst($inputs[0]).'</label>' .
																		'<div class="col-xs-12 col-sm-9">' .
																			'<div class="clearfix">' .
																				'<select name="_frm['.str_replace(" ","_",$inputs[0]).']" id="'.$inputs[0].'" class="form-control"  '.$inputs[2].'>' ;
																				foreach(explode(",",$inputs[3]) as $ths){
																					echo '<option value="'.$ths.'">'.$ths.'</option>';
																				}
																				echo '</select>'.
																			'</div>' .
																		'</div>' .
																	'</div>';
																	}
																	if($inputs[1]=="multiselect"){
																		echo '<div class="form-group">' .
																		'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'.$inputs[0].'">'.ucfirst($inputs[0]).'</label>' .
																		'<div class="col-xs-12 col-sm-9">' .
																			'<div class="clearfix">' .
																				'<select  multiple="multiple" name="_frm['.str_replace(" ","_",$inputs[0]).'][]" id="'.$inputs[0].'" class="ms"  '.$inputs[2].'>' ;
																				foreach(explode(",",$inputs[3]) as $ths){
																					echo '<option value="'.$ths.'">'.$ths.'</option>';
																				}
																				echo '</select>'.
																			'</div>' .
																		'</div>' .
																	'</div>';
																	}

																}

																/*template_str +='<input type="hidden" name="_frm[template]" value="'+template+'" />';

																$("._template").html(template_str);
																$("._template").show();
																$('.ms').multipleSelect({ width: '100%' }); */

															}

															if($wf['temp_type'] == 'spreadsheet'){
																$tbl = '<table class="table-spreadsheet" >';
																$rows = explode('\n',$wf['template']);
																$rs = count($rows);
																$cs = count(explode(',',$rows[0]));

																//excel header row
																$tbl += "<tr>";
																$cell_col = 'A';
																for($j=0;$j<=cs;$j++){
																	if($j==0){ $tbl.='<th class="heading"></th>'; }
																	else { $tbl.='<th>'.$cell_col.'</th>'; $cell_col= chr(ord($cell_col)+1); }
																}

																$k=1;
																foreach($rows as $ro){
																	$tbl .= "<tr>";
																	$cols = explode(',',$ro);

																	$i=0;
																	foreach($cols as $co){
																		if($i==0){ $tbl.='<td class="heading" >'.$k.'</td>'; }
																		$tbl.='<td><input type="text" name="template[spreadsheet]['.$k.'][]" ';
																		if(trim($co)!="") $tbl.= ' readonly style="color:#000"';
																		$tbl.='value="'.$co.'" /></td>';
																		$i++;
																	}
																	$tbl .= "</tr>";
																	$k++;
																}
																$tbl.='</table>';

																$tbl.='<div id="sh-addbtn"><a class="btn btn-minier addRowBtn" nxt="'.$k.'" cols="'.$cs.'"><i class="fa fa-plus"></i> add row</a></div>';

																echo $tbl;
															}

															if($wf['temp_type'] == 'attachments'){

																echo 	'<p>Additional Files</p>
																	<table class="table ">
																		<thead class="thin-border-bottom">
																			<tr>
																				<th><i class="ace-icon fa fa-caret-right blue"></i>Description</th>
																				<th><i class="ace-icon fa fa-caret-right blue"></i>File</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td colspan="2">
																					<div class="center"><a class="btn btn-xs addAttachmnt" >Add attachment</a></div>
																					<input type="hidden" name="att[append_type]" value="doc_track" />
																				</td>
																			</tr>
																		</tbody>
																	</table>';

															}

															?>
															</div></div>

															<?php } ?>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Remarks</label>
																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<textarea class="form-control" name="data[remark]" required id="description"></textarea>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">Sign this document(Optional)</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<label><input type="checkbox" value="<?= $_SESSION['user']; ?>" name="signature" >Sign</label>
																	</div>
																</div>
															</div>
															<div class="hr hr-dotted"></div>

														<?php
														//////////SEND TO///////////
														//GEt active delegations
														$active_delegs = array();
														$delegs = $conn->query("SELECT requested_by,delegate_to,duration FROM delegations WHERE approved='1'");
														while($deleg = $delegs->fetch_assoc()) {
															$deleg_strt = date("Y-m-d", strtotime(substr($deleg['duration'], 0, 10)));
															$deleg_end = date("Y-m-d", strtotime(substr($deleg['duration'], 13, 10)));

															if(date("Y-m-d")<=$deleg_end){
																$active_delegs[$deleg['requested_by']]=$deleg['delegate_to'];
															}
														}

														if($action_taken['send_type'] == 'select' || $action_taken["send_type"]=="auto"){
														if(@$action_taken["send_filters"]=="*"){
															$where = " WHERE id<>'".$_SESSION['user']."' AND status='ACTIVE' ";
														} elseif(strpos($action_taken['send_filters'], 'group') !== false){

																$grp = explode("=",$action_taken['send_filters']);
																$grp_result = $conn->query("SELECT `members` FROM groups WHERE  id='".$grp[1]."' ");
																$grp_m = $grp_result->fetch_assoc();
																$where = " WHERE  id IN(".$grp_m["members"].") ";

														} elseif(@$action_taken["send_filters"]!="") {
															$where = " WHERE  department_id=".$action_taken["send_filters"]." AND id<>'".$_SESSION['user']."'  AND status='ACTIVE' ";
														//	echo $where;
														} else {
															$where = " WHERE  'a'='b' ";
														}

														$filter_result = $conn->query("SELECT id FROM admins ".$where." ");
														if($filter_result->num_rows==0){ $no_recepient=true; ?>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right"></label>
																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<div class="alert alert-danger">
																			<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
																			<strong>
																				<i class="ace-icon fa fa-times"></i>
																				No Assigned Recepient!
																			</strong>
																			Contact your admin to set the recepient for this document.<br>
																			<br>
																		</div>
																	</div>
																</div>
															</div>

														<?php } elseif($filter_result->num_rows>1){
																$recepients=array();
																while($filter_data = $filter_result->fetch_assoc()) { array_push($recepients, $filter_data["id"]); }
																$whr = " a.id IN(".implode(",",$recepients).") "; ?>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right">Send to:</label>
																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<div class="clearfix">
																			<select  name="data[to_id]"  class="chosen-select sendto" id="sendto" data-placeholder="select recepient..." required >
																				<option value="">  </option>
																			<?php $form_query = "SELECT a.*, b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE  $whr AND status='ACTIVE' ORDER BY fname, lname";
																				  $form_result = $conn->query($form_query);
																				  while($ro_data = $form_result->fetch_assoc()) {   ?>
																				<option value="<?php echo $ro_data['id']; ?>" <?php  if(isset($active_delegs[$ro_data['id']])){ echo ' data-delegate="'.$active_delegs[$ro_data['id']].'" '; }  ?> >
																					<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?>
																					<?php echo $ro_data['duty_station']<>0?" - ".$ro_data['duty_station']:""; ?>
																					<?php //echo $ro_data['department_id']<>0?" (". getDept($ro_data['department_id']).")":""; ?>
																					<?php echo $ro_data['job_title_id']!=0?" (". getTitle($ro_data['job_title_id']).")":""; ?>
																					<?php  if(isset($active_delegs[$ro_data['id']])){
																						   echo ' >>Delegated To:'.getStaffName($active_delegs[$ro_data['id']]);
																					}  ?>
																				</option>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														<?php } else { $filter_data = $filter_result->fetch_assoc();	?>
															<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
															<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															<div class="well well-sm">
																Sending to: <?php echo getStaffName($filter_data["id"]); ?>
																<?php if(isset($active_delegs[$filter_data["id"]])){
																	   echo ' >>Delegated To:'.getStaffName($active_delegs[$filter_data["id"]]);
																	   echo '<input type="hidden" name="delegate_to" value="'.$active_delegs[$filter_data["id"]].'" />';
																}?>
															</div>
															<input type="hidden" name="data[to_id]" value="<?php echo $filter_data["id"]; ?>" />
															</div></div></div>
														<?php } ?>

													<?php } else {

														if($action_taken['send_type'] == 'back'){
															if($action_taken['send_filters'] == 'initiator'){
																//get init
																$init = $doc['created_by'];

															?>
															<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
															<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															<div class="well well-sm"> Sending to: <?php echo getStaffName($init); ?> </div>
															<input type="hidden" name="data[to_id]" value="<?php echo $init; ?>" />
															</div></div></div>
															<?php }
															if($action_taken['send_filters'] == 'sender'){
																//get sender
																$sndr = $dTrk['from_id']; ?>
															<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
															<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															<div class="well well-sm">
															Sending to: <?php echo getStaffName($sndr); ?>
															<?php if(isset($active_delegs[$sndr])){
																	echo ' >>Delegated To:'.getStaffName($active_delegs[$sndr]);
																	echo '<input type="hidden" name="delegate_to" value="'.$active_delegs[$sndr].'" />';
																}?>
															</div>
															<input type="hidden" name="data[to_id]" value="<?php echo $sndr; ?>" />
															<input type="hidden" name="data[to_stage]" value="<?php echo $dTrk['from_stage']; ?>" >
															<input type="hidden" name="data2[stage_label]" value="<?php echo $dTrk['from_stage']; ?>" >
															<input type="hidden" name="data2[status_label]" value="Updated by Initiator" >
															</div></div></div>

															<?php }

															if( strpos($action_taken['send_filters'], 'stage') !== false ){

															$rev_stg = explode("=",$action_taken['send_filters']);

															//get holder at that stage
															$sndr_query = "SELECT * FROM doc_track WHERE  doc_id='".$_GET['ac']."' AND to_stage='".$rev_stg[1]."' ORDER BY id LIMIT 1";
															$sndr_result = $conn->query($sndr_query);
															$sndr_data = $sndr_result->fetch_assoc();

															$sndr = $sndr_data['to_id']; ?>
															<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
															<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															<div class="well well-sm">
															Sending to: <?php echo getStaffName($sndr); ?>
															<?php if(isset($active_delegs[$sndr])){
																	echo ' >>Delegated To:'.getStaffName($active_delegs[$sndr]);
																	echo '<input type="hidden" name="delegate_to" value="'.$active_delegs[$sndr].'" />';
																}?>
															</div>
															<input type="hidden" name="data[to_id]" value="<?php echo $sndr_data['to_id']; ?>" />
															<input type="hidden" name="data[to_stage]" value="<?php echo $sndr_data['to_stage']; ?>" >
															<input type="hidden" name="data2[stage_label]" value="<?php echo $sndr_data['to_stage']; ?>" >
															<input type="hidden" name="data2[status_label]" value="Returned from <?php echo $dTrk['to_stage']; ?>" >
															</div></div></div>

															<?php }

														}

														if($action_taken['send_type'] == 'end'){ ?>

														<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
															<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															<div class="well well-sm"><?php echo $doc['document_type']." TO BE CLOSED"; ?> </div>
															<input type="hidden" name="data[to_stage]" value="CLOSED" >
															<input type="hidden" name="data[to_id]" value="0" />
															<input type="hidden" name="data[receiver_status]" value="ARCHIVED" />
															<input type="hidden" name="data2[stage_label]" value="CLOSED" >
															<input type="hidden" name="data2[status_label]" value="<?php echo $action_taken['status_label']; ?>" >
															<input type="hidden" name="data2[closed_by]" value="<?php echo $_SESSION['user']; ?>" >
															</div></div>
														</div>

														<?php }
														if($action_taken['send_type'] == 'stay'){?>
															<input type="hidden" name="data[to_id]" value="<?php echo $_SESSION['user']; ?>"/>
														<?php } ?>
													<?php } ?>


															<?php if($doc['signatures']=='1'){

																  $ss=$conn->query("SELECT * FROM document_type_signatories
																					WHERE doc_type_id='".$doc['doc_type_id']."' AND signatory_id='".$_SESSION['user']."'");
																  if($ss->num_rows>0){
																	$sgt = $ss->fetch_assoc();
																	$sg=$conn->query("SELECT * FROM document_signatures WHERE doc_id='".$_GET['ac']."' ");
																	while($s=$sg->fetch_assoc()){ $sgn[]=$s['signatory_id']; }
																	if($sgt['position']=='1' AND !in_array($_SESSION['user'],$sgn)){ $to_sign = true; }
																	if($sgt['position']>'1' AND $doc['signatures_sequential']=='0' AND !in_array($_SESSION['user'],$sgn)){ $to_sign = true; }
																	if($sgt['position']>'1'  AND $doc['signatures_sequential']=='1' ){
																		$b4me=$conn->query("SELECT * FROM document_type_signatories
																					WHERE doc_type_id='".$doc['doc_type_id']."' AND position='".($sgt['position']-1)."'");
																		$prev = $b4me->fetch_assoc();
																		if(in_array($prev['signatory_id'],$sgn)){ $to_sign = true; }
																	}
																  }
																if(@$to_sign==true){
															?>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">This Document Requires your signature</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<label><input type="checkbox" value="<?= $_SESSION['user']; ?>" name="signed" >Sign Document</label>
																	</div>
																</div>
															</div>
															<?php } } ?>
															<div class="hr hr-dotted"></div>
															<!-- <div class="form-group">
																	<label class="control-label col-xs-12 col-sm-3 no-padding-right">Attach document(Optional)</label>
																	<div class="col-sm-7">
																		<label class="ace-file-input">
																			<input type="file" class="nice-input-file" name="photo[]" multiple />
																			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
																		</label>
																	</div>
																     </div> -->

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" ></label>
                               <div class="col-xs-12 col-sm-9">
																<table class="table ">
																	<thead class="thin-border-bottom">
																		<tr>
																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>Remove
																			</th>
																			<th>
																				<i class="ace-icon fa fa-caret-right blue"></i>File Attachments
																			</th>

																		</tr>
																	</thead>

																	<tbody>
																		<tr>
																			<td colspan="2">
																				<div class="center"><a class="btn btn-xs addAttachmnt1" >Add attachment</a></div>

																			</td>
																		</tr>
																	</tbody>
																</table>
                                </div>
															</div>

															<div class="hr hr-dotted"></div>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">&nbsp;</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="submit" value="<?php echo strtoupper($action_taken['label']); ?>" >
																	</div>
																</div>
															</div>

															<div id="delegateto"></div>




														</form>



													</div>
										</div>
									</div>
								</div><!-- /.row -->
					<?php
				}
			}else{
$action_array = array(
'approve'=>'APPROVED',
'reject'=>'REJECTED',
'forward'=>'FORWARDED',
'update'=>'UPDATED STATUS',
'close'=>'CLOSED',
'dispatch'=>'DISPATCHED',
'delivery'=>'DELIVERED',
'forward_in_current_dept'=>'FORWARDED',
);

$action_btns = array(
'approve'=>'SUBMIT',
'reject'=>'SUBMIT',
'forward'=>'SUBMIT',
'update'=>'UPDATE',
'close'=>'SUBMIT',
'dispatch'=>'DISPATCH',
'delivery'=>'DELIVERY',
'forward_in_current_dept'=>'SUBMIT',
);


$action_page = 'documents.php?new';

?>

<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue"><?php echo ucfirst($_GET['a']) ?></h3>

<div class="clearfix">
	<div class="pull-right tableTools-container"></div>
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->
<div>
	<div >

				<form class="form-horizontal" name="myform" id="validation-form" method="post" action="<?php echo $action_page; ?>" enctype="multipart/form-data">

					<input type="hidden" name="data[doc_id]" value="<?php echo $_GET['ac'] ?>"/>
					<input type="hidden" name="data[from_id]" value="<?php echo $_SESSION['user'] ?>"/>
					<input type="hidden" name="data[receiver_status]" value="SENT"/>
					<?php if($_GET['a']=='approve'){?>
					<!--<input type="hidden" name="data[to_id]" value="9"/>-->
					<?php }?>



					<?php
						if(isset($_GET['at']) && $_GET['at'] == 1){
					?>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">PO/LOC Number</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[atlas_number]" />
								</div>
							</div>
						</div>
					<?php } ?>


					<?php
						if(isset($_GET['a']) && $_GET['a'] == 'dispatch'){
					?>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Delivery By</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[delivered_by]" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Contact</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[delivery_contact]" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Courier Tracking No.</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[tracking_no]" />
								</div>
							</div>
						</div>

					<?php } elseif(isset($_GET['a']) && $_GET['a'] == 'delivery') {?>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Received By</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[received_by]" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Receiver Tel</label>

							<div class="col-xs-12 col-sm-9">
								<div class="clearfix">
									<input type="text" name="data2[tel]" />
								</div>
							</div>
						</div>

					<?php } ?>

					<?php if($_GET['a'] == 'update') { ?>

					<div class="form-group">
						<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="description">Status</label>

						<div class="col-xs-12 col-sm-9">
							<div class="clearfix">
								<select name="data[action]">
									<option>LCRC Review</option>
									<option>LPO Issued</option>
									<option>Delivered/Awaiting Delivery</option>
									<option>Payment Process</option>
									<option>On Hold</option>
									<option>Under Review</option>
								</select>
							</div>
						</div>
					</div>

					<?php } else {?>

							<input type="hidden" name="data[action]" value="<?php echo $action_array[$_GET['a']] ?>"/>
					<?php } ?>
					<?php if($_GET['a'] == 'forward' || $_GET['a'] == 'reject'||$_GET['a']=='approve'){ ?>
					 <div class="form-group">
						<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="subject">Subject</label>

						<div class="col-xs-12 col-sm-6">
							<div class="clearfix">
							<?php
					     $doc_id=$_GET['ac'];
					     $result=$conn->query("SELECT subject FROM documents WHERE id='$doc_id' ");
					     $row = mysqli_fetch_array($result);
					      ?>
								<textarea name="data[subject]" id="subject" value="<?=$row['subject'];?>" class="form-control" readonly><?=$row['subject'];?></textarea>
							</div>
						</div>
					</div>
					<?php } ?>

					<div class="hr hr-dotted"></div>

<?php if($_GET['a'] == 'forward' || $_GET['a'] == 'reject'||$_GET['a']=='approve'){ ?>
	<div class="form-group">
	<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Forward to<span class="red">*</span></label>

						<div class="col-xs-12 col-sm-6">
							<div class="clearfix">

								<select  name="data[to_id]"  class="form-control" id="sendto" data-placeholder="select staff..." required >
									<option value="">  </option>
								<?php $form_query = "SELECT * FROM admins WHERE  id<>'$id' ORDER BY fname, lname";
									  $form_result = $conn->query($form_query);
									  while($ro_data = $form_result->fetch_assoc()) {   ?>
									<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php if($ro_data['delegate_to']!=0){echo $ro_data['delegate_to'];}else{echo $ro_data['id'];} ?>">
										<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']." - ".getDept($ro_data['department_id']); ?>
										<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
									</option>
									<?php } ?>

								</select>
							</div>
						</div>
					</div>
					<?php } elseif($_GET['a'] == 'update' || $_GET['a'] == 'dispatch'){?>
						<input type="hidden" name="data[to_id]" value="<?php echo $_SESSION['user'] ?>"/>
					<?php } ?>
					<div id="delegateto"></div>

					<input type="hidden" name="tId" value="<?php echo $_GET['tId']; ?>" >

					<input type="hidden" name="action" value="NEW ACTION" >
					<input type="hidden" name="table" value="doc_track">
					<input type="hidden" name="table2" value="documents">
					<input type="hidden" name="data2[file_status]" value="<?php echo $action_array[$_GET['a']] ?>" >


				</form>

	</div>
   </div>
</div>
</div><!-- /.row -->
<?php	}?>

					<?php if(isset($_GET['type']) AND $_GET['type']=="staff"){
							if(isset($_GET['view'])){ $mode = "VIEW"; $acid = $_GET['view']; }
							if(isset($_GET['edit'])){ $mode = "EDIT"; $acid = $_GET['edit']; }
								$form_result = $conn->query("SELECT a.*, b.department, c.duty_station, d.job_title
																FROM
																	admins a
																   LEFT JOIN
																	units b ON a.department_id=b.id
																   LEFT JOIN
																	dutystations c ON a.station_id=c.id
																   LEFT JOIN
																	job_titles d ON a.job_title_id=d.id
																WHERE a.id='$acid'");
								$staff_data = $form_result->fetch_assoc();

					?>
						<?php if($mode=="VIEW"){?>
						<style>
						 .clearfix{
							text-align:left;
							margin-bottom: 0;
							padding-top: 7px;
							font-weight: bold;
						 }
						 .form-group{
							margin-bottom: 0px;
						 }
						</style>
						<?php } ?>
						<div class="row">
							<div class="col-xs-12">
								<h3 class="header smaller lighter blue">Staff Profile</h3>

								<div class="clearfix">
									<div class="pull-right tableTools-container"></div>
								</div>

								<!-- div.table-responsive -->

								<!-- div.dataTables_borderWrap -->
								<div>
									<div>

										<form novalidate class="form-horizontal validation-form" method="post" action="staff.php?edit">

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="company">Name:</label>
												<div class="col-xs-12 col-sm-8">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[fname]" class="col-sm-3" type="text" value="':''; ?><?php echo $staff_data['fname']; ?><?= $mode=="EDIT"?'" placeholder="First Name" />':''; ?>
														<?= $mode=="EDIT"?'<input name="data[lname]" class="col-sm-3" type="text" value="':''; ?><?php echo $staff_data['lname']; ?><?= $mode=="EDIT"?'" placeholder="Last Name" />':''; ?>
														<?= $mode=="EDIT"?'<input name="data[oname]" class="col-sm-3" type="text" value="':''; ?><?php echo $staff_data['oname']; ?><?= $mode=="EDIT"?'" placeholder="Other Names" />':''; ?>
													</div>
												</div>
											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="form-field-gender">Gender:</label>
												<div class="col-xs-12 col-sm-8">
													<div class="clearfix">
													<?php if($mode=="EDIT"){?>
													<label >Male
														<input type="radio" <?php if($staff_data['gender']=="MALE") echo "checked"; ?> name="data[gender]" value="MALE" /></label>
													<label >Female
														<input type="radio" <?php if($staff_data['gender']=="FEMALE") echo "checked"; ?> name="data[gender]" value="FEMALE" /></label>
													<?php } else {  echo $staff_data['gender']<>""?$staff_data['gender']:"Not Set"; } ?>
													</div>
												</div>
											</div>


											<div class="hr hr-dotted"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="unit">Department: </label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?php if($mode=="EDIT"){?>
														<select class="chosen-select col-xs-12 col-sm-4 " id="unit"  name="data[department_id]" data-placeholder="Select Department ...">
															<option value="">&nbsp;</option>
															<?php $form_query = "SELECT * FROM units ORDER BY department";
																$form_result = $conn->query($form_query);
																while($ro_data = $form_result->fetch_assoc()) {   ?>
																<option value="<?php echo $ro_data['id']; ?>" <?php if($staff_data['department_id']==$ro_data['id']) echo "selected";?> ><?php echo $ro_data['department']; ?></option>
																<?php } ?>
														</select>
														<?php } else {  echo $staff_data['department']<>""?$staff_data['department']:"Not Set"; } ?>
													</div>
												</div>

											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="branch">Duty Station:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?php if($mode=="EDIT"){?>
														<select class="chosen-select col-xs-12 col-sm-4" id="branch"  name="data[station_id]" data-placeholder="Select Duty Station ...">
															<option value="0">&nbsp;</option>
															<?php $form_query = "SELECT * FROM dutystations ORDER BY duty_station";
																$form_result = $conn->query($form_query);
																while($ro_data = $form_result->fetch_assoc()) {   ?>
																<option value="<?php echo $ro_data['id']; ?>" <?php if($staff_data['station_id']==$ro_data['id']) echo "selected";?> ><?php echo $ro_data['duty_station']; ?></option>
																<?php } ?>
														</select>
														<?php } else {  echo $staff_data['duty_station']<>""?$staff_data['duty_station']:"Not Set"; } ?>
													</div>
												</div>
											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="position">Job Title:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?php if($mode=="EDIT"){?>
														<select class="chosen-select col-xs-12 col-sm-4" id="position"  name="data[job_title_id]" data-placeholder="Select Position ...">
															<option value="0">&nbsp;</option>

															<?php $form_query = "SELECT * FROM job_titles ORDER BY job_title";
																$form_result = $conn->query($form_query);
																while($ro_data = $form_result->fetch_assoc()) {   ?>
																<option value="<?php echo $ro_data['id']; ?>" <?php if($staff_data['job_title_id']==$ro_data['id']) echo "selected";?> ><?php echo $ro_data['job_title']; ?></option>
																<?php } ?>
														</select>
														<?php } else {  echo $staff_data['job_title']<>""?$staff_data['job_title']:"Not Set"; } ?>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="ext">Work ID No:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[idNo]" id="idNo" class="col-xs-12 col-sm-4" type="text" value="':''; ?><?php echo $staff_data['idNo']; ?><?= $mode=="EDIT"?'" >':''; ?>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="ext">ID Expiry:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[idExpiry]" id="idExpiry" class="date-picker col-xs-12 col-sm-4" type="text" value="':''; ?><?php echo  $staff_data['idExpiry']<>"0000-00-00"?$staff_data['idExpiry']:""; ?><?= $mode=="EDIT"?'" placeholder="Expiry Date" data-date-format="yyyy-mm-dd"  >':''; ?>
													</div>
												</div>
											</div>

											<div class="hr hr-dotted"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Email:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[email]" id="email" class="col-xs-12 col-sm-4" type="email" value="':''; ?><?php echo $staff_data['email']; ?><?= $mode=="EDIT"?'" >':''; ?>
													</div>
												</div>
											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Phone:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[phone]" id="phone" class="col-xs-12 col-sm-4" type="text" value="':''; ?><?php echo $staff_data['phone']; ?><?= $mode=="EDIT"?'" >':''; ?>
													</div>
												</div>
											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="ext">Extension:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[ext]" id="phone" class="col-xs-12 col-sm-4" type="text" value="':''; ?><?php echo $staff_data['ext']; ?><?= $mode=="EDIT"?'" >':''; ?>
													</div>
												</div>
											</div>

											<div class="hr hr-dotted"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="uname">Username:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?= $mode=="EDIT"?'<input name="data[uname]" id="uname" class="col-xs-12 col-sm-4" type="text" value="':''; ?><?php echo $staff_data['uname']; ?><?= $mode=="EDIT"?'" >':''; ?>
													</div>
												</div>
											</div>

											<div class="space-2"></div>

											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="phone">Account Status:</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<?php if($mode=="EDIT"){?>
														<select class="col-xs-12 col-sm-4"  name="data[status]" >
															<option value="ACTIVE" <?php if($staff_data['status']=="ACTIVE") echo "selected";?> >ACTIVE</option>
															<option value="SUSPENDED" <?php if($staff_data['status']=="SUSPENDED") echo "selected";?> >SUSPENDED</option>
														</select>
														<?php } else {  echo $staff_data['status']<>""?$staff_data['status']:"Not Set"; } ?>
													</div>
												</div>
											</div>

											<?php if($mode=="EDIT"){?>
											<div class="form-group">
												<label class="control-label col-xs-12 col-sm-3 no-padding-right">&nbsp;</label>

												<div class="col-xs-12 col-sm-9">
													<div class="clearfix">
														<input type="submit" value="SAVE CHANGES" >
													</div>
												</div>
											</div>

											<input type="hidden" name="action" value="EDIT STAFF" >
											<input type="hidden" name="table" value="admins" >
											<input type="hidden" name="f_id" value="id" >
											<input type="hidden" name="acid" value="<?php echo $acid;  ?>" >
											<?php } ?>
										</form>
									</div>
								</div>
							</div>
						</div><!-- /.row -->
					<?php } ?>



					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
	<script src="assets/js/chosen.jquery.js"></script>
	<script src="assets/js/dropzone.js"></script>
	<script src="assets/js/jquery.validate.js"></script>
		<!-- ace scripts -->
<script src="assets/js/ace/elements.scroller.js"></script>
<script src="assets/js/ace/elements.colorpicker.js"></script>
<script src="assets/js/ace/elements.fileinput.js"></script>
<script src="assets/js/ace/elements.typeahead.js"></script>
<script src="assets/js/ace/elements.wysiwyg.js"></script>
<script src="assets/js/ace/elements.spinner.js"></script>
<script src="assets/js/ace/elements.treeview.js"></script>
<script src="assets/js/ace/elements.wizard.js"></script>
<script src="assets/js/ace/elements.aside.js"></script>
<script src="assets/js/ace/ace.js"></script>
<script src="assets/js/ace/ace.ajax-content.js"></script>
<script src="assets/js/ace/ace.touch-drag.js"></script>
<script src="assets/js/ace/ace.sidebar.js"></script>
<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
<script src="assets/js/ace/ace.submenu-hover.js"></script>
<script src="assets/js/ace/ace.widget-box.js"></script>
<script src="assets/js/ace/ace.settings.js"></script>
<script src="assets/js/ace/ace.settings-rtl.js"></script>
<script src="assets/js/ace/ace.settings-skin.js"></script>
<script src="assets/js/ace/ace.widget-on-reload.js"></script>
<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>


		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;


			 if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true});
					//resize the chosen on window resize

					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});


					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						 else $('#form-field-select-4').removeClass('tag-input-style');
					});
				}

			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;

				var sidebar = $sidebar.get(0);
				var $window = $(window);

				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';

					$window.off('scroll.ace.top_menu')
					return;
				}

				var done = false;
				$window.on('scroll.ace.top_menu', function(e) {

					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;


					if (scroll > 16) {
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}

					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');

			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);

			$('[data-rel=tooltip]').tooltip({container:'body'});

			$('[data-rel=popover]').popover({container:'body'});

			$(document).on('change','#doc_type',function(){

				 var type = $('option:selected',this).attr('data-classification')

				 if(type == 'EXTERNAL'){
				 	$('#screening').html(screening)
					//$('#submitted').html(unknown)
				 }
				 else if(type == 'INTERNAL'){
					 $('#screening').html('')
					 //$('#submitted').html(admins)
					 //$('#organisation').val('UNDP')
				 }
				 if($(this).val() == 'Other') $('#input_doc_type').show(); else $('#input_doc_type').hide();

				 $('#hidden_doc_type').val( $('option:selected', this).text() );
			});

			$(document).on('change','#doc_type',function(){

				///////////////////////////////////////
				//////// handle template matters //////
				var $this = $(this);
				var template;
				var template_str = '';
				var temp_type = $('option:selected', this).attr('temp_type');
				var temp_id = $('option:selected', this).val();

				//hide everything
				$("._template").html("*");
				$("#html-template").html('<textarea class="form-control html-editor" name="data[template]" ></textarea>');
				$("._template").hide();
				$(".type-html").hide();

				//load template if exists
				if(temp_type!="none"){

					$('#hidden_temp_type').val(temp_type);

					var frmData = 'q=SELECT template FROM document_types WHERE id='+temp_id;

					$.ajax({
						type: "POST",
						url: "include/ajx_singleselect.php",
						data: frmData,
						success: function(resp) {

							template=resp;

							if(temp_type=="html"){

								$(".type-html").show();
								$("textarea.html-editor").text(template);
								$('.html-editor').summernote();
							}

							else if(temp_type == 'form'){

								var strArray = template.split("$$");
								$.each(strArray,function(index,val){
									var inputs = val.split(";")
									if(inputs[1]!="select" && inputs[1]!="textarea" && inputs[1]!="multiselect" ){
									template_str += '<div class="form-group">' +
										'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'+inputs[0]+'">'+inputs[0].charAt(0).toUpperCase()+inputs[0].slice(1)+'</label>' +
										'<div class="col-xs-12 col-sm-9">' +
											'<div class="clearfix">' +
												'<input name="_frm['+inputs[0].replace(/\s/g,"_")+']" id="'+inputs[0]+'" type="'+inputs[1]+'" ';
												if(inputs[1]=="text"){ template_str+='class="form-control"' }else{ template_str+='class="col-xs-12 col-sm-4"' }
												template_str+=' '+inputs[2]+' />' +
											'</div>' +
										'</div>' +
									'</div>'
									}
									if(inputs[1]=="textarea"){
										template_str += '<div class="form-group">' +
										'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'+inputs[0]+'">'+inputs[0].charAt(0).toUpperCase()+inputs[0].slice(1)+'</label>' +
										'<div class="col-xs-12 col-sm-9">' +
											'<div class="clearfix">' +
												'<textarea name="_frm['+inputs[0].replace(/\s/g,"_")+']" id="'+inputs[0]+'" class="form-control  autosize-transition"  '+inputs[2]+'></textarea>' +
											'</div>' +
										'</div>' +
									'</div>'
									}
									if(inputs[1]=="select"){
										template_str += '<div class="form-group">' +
										'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'+inputs[0]+'">'+inputs[0].charAt(0).toUpperCase()+inputs[0].slice(1)+'</label>' +
										'<div class="col-xs-12 col-sm-9">' +
											'<div class="clearfix">' +
												'<select name="_frm['+inputs[0].replace(/\s/g,"_")+']" id="'+inputs[0]+'" class="form-control"  '+inputs[2]+'>' ;
												$.each(inputs[3].split(","),function(){
													template_str += '<option value="'+this+'">'+this+'</option>'
												});
												template_str += '</select>'+
											'</div>' +
										'</div>' +
									'</div>'
									}
									if(inputs[1]=="multiselect"){
										template_str += '<div class="form-group">' +
										'<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="'+inputs[0]+'">'+inputs[0].charAt(0).toUpperCase()+inputs[0].slice(1)+'</label>' +
										'<div class="col-xs-12 col-sm-9">' +
											'<div class="clearfix">' +
												'<select  multiple="multiple" name="_frm['+inputs[0].replace(/\s/g,"_")+'][]" id="'+inputs[0]+'" class="ms"  '+inputs[2]+'>' ;
												$.each(inputs[3].split(","),function(){
													template_str += '<option value="'+this+'">'+this+'</option>'
												});
												template_str += '</select>'+
											'</div>' +
										'</div>' +
									'</div>'
									}

								});

								template_str +='<input type="hidden" name="_frm[template]" value="'+template+'" />';

								$("._template").html(template_str);
								$("._template").show();
								$('.ms').multipleSelect({ width: '100%' });

							}

							else if(temp_type == 'spreadsheet'){
								var tbl = '<table class="table-spreadsheet" >';
								var rows = template.split('\n');
								var rs = rows.length;
								var cs = rows[0].split(',').length;

								//excel header row
								tbl += "<tr>";
								var cell_col = 'A';
								for(j=0;j<=cs;j++){
									if(j==0){ tbl+='<th class="heading"></th>'; }
									else { tbl+='<th>'+cell_col+'</th>'; cell_col= String.fromCharCode(cell_col.charCodeAt(0) + 1) ; }
								}

								var k=1;
								$.each(rows, function(){
									tbl += "<tr>";
									var cols = this.split(',');

									var i=0;
									$.each(cols, function(){
										if(i==0){ tbl+='<td class="heading" >'+k+'</td>'; }
										tbl+='<td><input type="text" name="template[spreadsheet]['+k+'][]" ';
										if($.trim(this)!="") tbl+= ' readonly style="color:#000"';
										tbl+='value="'+this+'" /></td>';
										i++;
									});
									tbl += "</tr>";
									k++;
								});
								tbl+='</table>';

								tbl+='<div id="sh-addbtn"><a class="btn btn-minier addRowBtn" nxt="'+k+'" cols="'+cs+'"><i class="fa fa-plus"></i> add row</a></div>';

								template_str += '<div class="form-group">' +
										'<label class="control-label col-xs-12 col-sm-3 no-padding-right" ></label>' +
										'<div class="col-xs-12 col-sm-9">' +tbl+'</div></div>';


								$("._template").html(template_str);
								$("._template").show();
							}

						},
						error: function(err){
							//alert(JSON.stringify(err));
						}
					});
				}

			});

			$(document).on('change','#doc_type',function(){

				var doc_id = $('option:selected', this).val();
				var pst = 'doc_id='+doc_id;
				///////////////////////////////////////////////////////
				//////// handle send to matters & submit button ///////

				$.ajax({
					type: "POST",
					url: "include/ajx_doc_actions.php",
					data: pst,
					success: function(resp) {
						$('#doc_sendto').html(resp);
						$('.chosen-select').chosen();
					}
				});
			});

			$('body').on('blur','#file_number',function(){
				var fNumber = $(this).val();

				if(fNumber!=""){

				<?php 	$fNum = $conn->query("SELECT DISTINCT doc_ref_number FROM documents WHERE doc_type_id=1 ORDER BY doc_ref_number");
						$ex_docs = array();
						while($fNums = $fNum->fetch_assoc()){
							$ex_docs[]=$fNums;
						}
						$fNums_jsn = json_encode($ex_docs); ?>

						var fNums = '<?= $fNums_jsn; ?>';

						var obj = JSON.parse(fNums);

						var i = null;
						var isUnique = true;
						for (i = 0; obj.length > i; i += 1) {
							if (obj[i].doc_ref_number === fNumber) {
								isUnique = false;
							}
						}

						if(isUnique){

						} else {
							$.alert('<b>REPEATED ENTRY</b><br> This payment invoice is already captured in the system.<br>Please respond to the request from UNHCR Office');
						}
				}

			});

			$('.nice-input-file').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
			});
			//pre-show a file name, for example a previously selected file
			//$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])

			/////////////////////////////////////////////////////
			//////////// SAVE DRAFT  ////////////////////////////
			$('body').on('click','a.save-draft',function(e){
				$('#file_status').val("DRAFT");
				$.alert('<b>SAVE DRAFT</b><br> Saved as draft. Go to My Documents>Saved Drafts to continue.');

			});
			/////////////////////////////////////////////////////
			///////// PUT A SUBMIT OVERLAY  /////////////////////
			$('body').on('click','button.submit',function(e){
				$('body').append('<div id="submitting-data-overay" class="widget-box-overlay"><div><i class="loading-icon fa fa-spinner fa-spin fa-5x white"></i><br><span class=" white loading-icon bigger-200"><i> Submitting. Please wait ...</i></span></div></div>');

			});

			$('body').on('click','a.remove_att',function(){
				$(this).parents('tr').remove();
			});

			$('body').on('click','a.addAttachmnt1',function(){

				var widgetContainer = $(this).parents('.table');
				var newRow = '<tr>'+
							'	<td>'+
							'		<a href="#" class="remove_att remove" title="Remove file"><i class="fa fa-minus-circle"></i></a>'+
							'	</td>'+
							'	<td>'+
							'		<label class="ace-file-input">'+
							'			<input type="file" class="nice-input-file" name="upload[]" multiple />'+
							'			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>'+
							'		</label>'+
							'	</td>'+
							'</tr>';

				widgetContainer.find('tbody').append(newRow);
				$('.nice-input-file').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});
			});

			$('body').on('click','.addRowBtn',function(){

				var nxt=$(this).attr('nxt')*1;
				var cols=$(this).attr('cols')*1;

				var row = '<tr><td class="heading" >'+nxt+'</td>';
				for(i=0;i<cols;i++){ row+='<td><input type="text" name="template[spreadsheet]['+nxt+'][]" /></td>'; }
				row += '</tr>';
				$('#sh-addbtn').html('<a class="btn btn-minier addRowBtn" nxt="'+(nxt+1)+'" cols="'+cols+'"><i class="fa fa-plus"></i> add row</a>')
				$('table.table-spreadsheet').append(row);
			});

			$(document).on('change','input.agree',function(){
				if ($(this).is(':checked')) {
					$('div.form-actions').show();
				} else {
					$('div.form-actions').hide();
				}
			});

			$(document).on('change','#submitted_by_select',function(){
				$('#phone').val($('option:selected',this).attr('data-phone'));
				$('#email').val($('option:selected',this).attr('data-email'));
				$('#organisation').val($('option:selected',this).attr('data-agency'));
			});

			$(document).on('change','#sendto',function(){
				var delegate = $('option:selected',this).attr('data-delegate');
				var $input =  '';
				if(typeof delegate !== typeof undefined && delegate !== false){
					//alert('we are delegating');
					$input =  '<input type="hidden" name="delegate_to" value="'+delegate+'" >';
					$('#delegateto').html($input);
				} else {
					$('#delegateto').html("");
				}

			});

			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });


			 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			// picture upload
			$('#pic_upclose2').ace_file_input({
					style:'well',
					btn_choose:'Click to upload Scanned Copy',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,

					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});

			try {
			  Dropzone.autoDiscover = false;
			  var myDropzone = new Dropzone("#dropzone" , {
			    paramName: "file", // The name that will be used to transfer the file
			    maxFilesize: 0.5, // MB

				addRemoveLinks : true,
				dictDefaultMessage :
				'<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
				<span class="smaller-80 grey">(or click)</span> <br /> \
				<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>'
			,
				dictResponseError: 'Error while uploading file!',

				//change the previewTemplate to use Bootstrap progress bars
				previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
			  });

			   $(document).one('ajaxloadstart.page', function(e) {
					try {
						myDropzone.destroy();
					} catch(e) {}
			   });



			} catch(e) {
			  //alert('Dropzone.js does not support older browsers!');
			}


				/*jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");
				*/


				$('.validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						email: {
							required: true,
							email:true
						},
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						name: {
							required: true
						},
						phone: {
							required: true,
							phone: 'required'
						},
						url: {
							required: true,
							url: true
						},
						comment: {
							required: true
						},
						state: {
							required: true
						},
						platform: {
							required: true
						},
						organisation: {
							required: true,
							organisation: 'required'
						},
						gender: {
							required: true,
						},
						agree: {
							required: true,
						}
					},

					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						state: "Please choose state",
						organisation: "Please choose at least one option",
						gender: "Please choose gender",
						agree: "Please accept our policy"
					},


					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},

					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},

					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},

					submitHandler: function (form) {
						form.submit();
					},
					invalidHandler: function (form) {
						$('#submitting-data-overay').remove();
						$.alert('Invalid input. Please complete the form well before you submit');
					}
				});


				$('textarea[class*=autosize]').autosize({append: "\n"});



			});


		</script>
		<script>
		function addMore() {
        	   $("<DIV>").load("input.php", function() {
        	      $("#custom-input-container").append($(this).html());
        	   });
        	}
        	function add_attachment() {
        	   $("<DIV>").load("input.php", function() {
        	      $("#custom-input-container2").append($(this).html());
        	   });
        	}
        	function addFiles() {
        	   $("<DIV>").load("input.php", function() {
        	      $("#custom-input-container3").append($(this).html());
        	   });
        	}
        	function attachFiles() {
        	   $("<DIV>").load("input1.php", function() {
        	      $("#custom-input-container5").append($(this).html());
        	   });
        	}
            function add_attach() {
        	   $("<DIV>").load("input.php", function() {
        	      $("#custom-input-container4").append($(this).html());
        	   });
        	}

        	$('body').on('click','a.addAttachmnt',function(){

				var widgetContainer = $(this).parents('.table');
				var newRow = '<tr>'+
							'	<td>'+
							'		<a href="#" class="remove_att remove" title="Remove file"><i class="fa fa-minus-circle"></i></a>'+
							'		<input type="text" name="att[description][]" placeholder="" class="form-control">'+
							'	</td>'+
							'	<td>'+
							'		<label class="ace-file-input">'+
							'			<input type="file" class="nice-input-file" name="upload[]"/>'+
							'			<a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>'+
							'		</label>'+
							'	</td>'+
							'</tr>';

				widgetContainer.find('tbody').append(newRow);
				$('.nice-input-file').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});
			});
		</script>
		<script>
$(document).ready(function(){
 $('#externalsendto').multiselect({
  nonSelectedText: 'Select Staff...',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'700px'
 });
 $('#internalsendto').multiselect({
  nonSelectedText: 'Select Staff...',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'700px'
 });
});
</script>
				<!-- inline scripts related to this page -->

	</body>
</html>
