<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Types</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/css/datepicker.css" />
		<link rel="stylesheet" href="assets/css/bootstrap-timepicker.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="assets/css/excel.css" />
		<link rel="stylesheet" type="text/css" href="assets/plugins/jconfirm/css/jquery-confirm.css"/>
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		<style>
		table.sheet{
			margin-bottom:2px;
		}
		table.sheet input[type=text]{
			margin:0px;
			padding:0px;
			border:0px;
			width:100%;
		}
		table.sheet select{
			margin:0px;
			padding:0px;
			border:0px;
			width:100%;
		}
		.frm-section{
			font-size: 14px;
			font-weight: bold;
		}
		a.remove{
			position: absolute;
			margin-left: -14px;
		}
		</style>
		
		<?php $usr_list1 = $conn->query("SELECT id, CONCAT(fname,' ',lname) nme FROM admins ORDER BY fname ");
			  $usr_json1 = json_encode($usr_list1->fetch_all(MYSQLI_ASSOC));

			  $job_list = $conn->query("SELECT id, job_title FROM job_titles ORDER BY job_title");
			  $job_json = json_encode($job_list->fetch_all(MYSQLI_ASSOC));
			  
			  $brc_list = $conn->query("SELECT id, duty_station FROM dutystations ORDER BY duty_station");
			  $brc_json = json_encode($brc_list->fetch_all(MYSQLI_ASSOC));
			  
			  $dep_list = $conn->query("SELECT id, department FROM units ORDER BY department");
			  $dep_json = json_encode($dep_list->fetch_all(MYSQLI_ASSOC));
			  
			   $grp_list = $conn->query("SELECT id, group_name FROM `groups` ORDER BY group_name");
			  $grp_json = json_encode($grp_list->fetch_all(MYSQLI_ASSOC));

		?>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<?php if(isset($_GET['new'])){ include("include/do_insert.php"); } ?>
									<?php if(isset($_GET['update'])){ include("include/do_edit.php"); } ?>
									

									
									<div class="col-sm-8">
										<!-- #section:elements.tab.position -->
										<div class="tabbable tabs-left widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Document Types</h4>

												
											</div>
											<ul class="nav nav-tabs nav-tabs2" id="myTab3">
											<?php $first_query = $conn->query("SELECT id FROM document_types ORDER BY doc_type LIMIT 1");
												  $frst = $first_query->fetch_assoc();
												  
												  $form_query = "SELECT * FROM document_types ORDER BY doc_type"; 
												  $form_result = $conn->query($form_query);
												  while($ro_data = $form_result->fetch_assoc()) {   ?>	
												<li id="li<?php echo $ro_data['id']; ?>" <?php if(isset($psn_last_id)){ if($ro_data['id']==$psn_last_id) echo 'class="active"'; } else { if($ro_data['id']==$frst['id']) echo 'class="active"'; } ?> >
													<a data-toggle="tab" href="#home<?php echo $ro_data['id']; ?>" >
														<i class="pink ace-icon fa fa-file bigger-110"></i>
														<?php if($ro_data['temp_type']!="none"){
															echo strtoupper($ro_data['doc_type']).'<i class="ace-icon fa fa-bookmark"></i>';
														} else { echo $ro_data['doc_type']; } ?>
													</a>
													
												</li>
											<?php } ?>
												
											</ul>

											<div class="tab-content">
											
											
											<?php $form_query2 = "SELECT * FROM document_types ORDER BY doc_type"; 
												  $form_result2 = $conn->query($form_query2);
												  while($ro_data2 = $form_result2->fetch_assoc()) { $unit_id = $ro_data2['id'];  ?>	
												
												<div id="home<?php echo $unit_id; ?>" class="tab-pane widget-container-col <?php if(isset($psn_last_id)){ if($ro_data2['id']==$psn_last_id) echo "in active"; } else { if($ro_data2['id']==$frst['id']) echo "in active"; } ?>">
													
													<div class="widget-box transparent" style="margin-top:-10px">
														<div class="widget-header" style="margin-bottom:10px">
															<h4 class="widget-title lighter"><?php echo strtoupper($ro_data2['doc_type']); ?></h4>

															<div class="widget-toolbar no-border">
																
																
																<a href="#" title="Fullscreen" data-action="fullscreen">
																	<i class="ace-icon fa fa-arrows-alt bigger-120"></i>
																</a>
																
																<a href="#" data-action="reload">
																	<i class="ace-icon fa fa-refresh"></i>
																</a>
																
																<a href="#" class="save-doc" doc-id="<?php echo $unit_id; ?>" title="Save Changes">
																	<i class="ace-icon fa fa-save green bigger-120"></i>
																</a>
																<?php if($ro_data2['important']==0){?>
																<a href="#" class="delete-doc" data-tbl="document_types" data-id="<?php echo $unit_id; ?>" title="Delete document type">
																	<i class="ace-icon fa fa-trash bigger-120 red"></i>
																</a>
																<?php } ?>
															</div>
														</div>

														<div class="widget-body">
													<form id="form<?php echo $unit_id; ?>" action="?update" method="post">
														<input type="hidden" name="table" value="document_types" />
														<input type="hidden" name="acid" value="<?php echo $unit_id; ?>" />
														<input type="hidden" name="action" value="EDIT DOCTYPE" />
													<p class="frm-section">Document Type: </p>
													<input name="data[doc_type]" type="text" value="<?php echo $ro_data2['doc_type']; ?>" class="form-control"  />
																										
													<p> </p>
													<p class="frm-section">Document Grouping:<a data-rel="tooltip" data-placement="right" data-original-title="Grouping helps organise the selection list"><i class="fa fa-question-circle"></i></a> </p>
													<input name="data[doc_grouping]" type="text" value="<?php echo $ro_data2['doc_grouping']; ?>" class="form-control"  />
													
													<p> </p>
													<p class="frm-section">Description: </p>
													<textarea name="data	[description]" class="form-control" id="form-field-username"  rows="3" ><?php echo $ro_data2['description']; ?></textarea>
													<p> </p>
													<table width="100%">
														<tr>
															<td>Turnaround time (Days) <a data-rel="tooltip" data-original-title="Maximum expected working days for this item to be processed completely"><i class="fa fa-question-circle"></i></a></td>
															<td>Step time (Days)<a data-rel="tooltip" data-original-title="Maximum expected time on an individual's desk"><i class="fa fa-question-circle"></i></a></td>
														</tr>
														<tr>
															<td><input name="data[duration_overall]" type="text" value="<?php echo $ro_data2['duration_overall']; ?>" /></td>
															<td><input name="data[duration_staff]" type="text" value="<?php echo $ro_data2['duration_staff']; ?>" /></td>
														</tr>
													
													</table>
													<p> </p>
													<p class="frm-section">Escalation</p>
													<p>Escalation Time (Hrs:Mins:Secs)<a data-rel="tooltip" data-original-title="If item is unread for this duration, Escalation contact will be notified to take action"><i class="fa fa-question-circle"></i></a></p>
													<p>
														<div class="input-group bootstrap-timepicker">
															<input name="data[duration_escalate]" class="form-control timepicker" type="text" value="<?php echo $ro_data2['duration_escalate']; ?>" />
															<span class="input-group-addon">
																<i class="fa fa-clock-o bigger-110"></i>
															</span>
														</div>
													</p>
													<p>Contact:<br><i>(if not selected, staff's department head will be notified in case of delayed response)</i> </p>
													<p>
														<select name="data[escalation_staff_id]" class="select-unit-head form-control" data-placeholder="Staff from your Department...">
																<option value="0">Department Head</option>
																<option value="1">Line Manager</option>
																<optgroup label="STAFF">
															<?php $form_query = "SELECT * FROM admins WHERE id<>1 ORDER BY fname, lname"; 
																  $form_result = $conn->query($form_query);
																  while($stf = $form_result->fetch_assoc()) {   ?>	
																<option value="<?php echo $stf['id']; ?>" <?php if($stf['id']==$ro_data2['escalation_staff_id']) echo "selected"; ?> ><?php echo $stf['fname']." ".$stf['lname']." ".$stf['oname']; ?></option>
																<?php } ?>
																</optgroup>
														</select>
													</p>
													<p></p>
													
													
													<div class="hr hr-dotted"></div>
													
													<div class="form-group frm-section" style="overflow:hidden;">
														<p>Template Type</p>
														
														<select class="form-control set-temp-type" name="data[temp_type]" >
															<option value="none" <?php echo ($ro_data2['temp_type']=="none")?"selected":""; ?> >None</option>
															<option value="html" <?php echo ($ro_data2['temp_type']=="html")?"selected":""; ?> >Text Editor</option>
															<option value="form" <?php echo ($ro_data2['temp_type']=="form")?"selected":""; ?> >Form elements</option>
															<option value="spreadsheet" <?php echo ($ro_data2['temp_type']=="spreadsheet")?"selected":""; ?> >Spreadsheet</option>
															<option value="custom" <?php echo ($ro_data2['temp_type']=="custom")?"selected":""; ?> >Custom</option>
															<?php /*<option value="attachment" <?php echo ($ro_data2['temp_type']=="none")?"selected":""; ?> >Downloadable Attachment</option> */ ?>
														</select>
													</div>
													
													<div class="form-group html-div" <?php echo ($ro_data2['temp_type']=="html")?"":'style="display:none"'; ?>>		
														<div style="margin-top:10px;">
															<span><em class="temp-desc grey"></em></span>
															<div class="template-container">
																<div <?php echo ($ro_data2['template']=="" OR !isset($ro_data2['template']))?'style="display:none"':""; ?> >
																<div style="border:solid 1px #000; padding:10px">
																	<?php echo ($ro_data2['temp_type']=="html")?$ro_data2['template']:""; ?></div>
																<a class="btn btn-minier" ><i class="fa fa-pencil"></i> edit html</a>
																</div>
																<div <?php echo ($ro_data2['template']=="" OR !isset($ro_data2['template']))?"":'style="display:none"'; ?> >
																<textarea class="form-control"  name="template[html]" rows="10" placeholder="Template box" >
																<?php echo ($ro_data2['temp_type']=="html")?$ro_data2['template']:""; ?></textarea>
																<a class="btn btn-minier" ><i class="fa fa-pencil"></i> close html</a>
																</div>
															</div>
														</div>
													</div>
													
													<div class="form-group form-div" <?php echo ($ro_data2['temp_type']=="form")?"":'style="display:none"'; ?>>
														<label>Form Elements</label>
														<table class="table table-bordered sheet form-elements">
															<thead class="thin-border-bottom">
																<tr>
																	<th>Label</th>
																	<th>Field</th>
																	<th>Default<br>Value(s)</th>
																	<th>Required</th>
																</tr>
															</thead>
															<tbody>
															<?php 
															if($ro_data2['temp_type']=="form"){
															$elements = explode("$$",$ro_data2['template']);
															foreach($elements as $element){	
																$attr = explode(";",$element);
															?>
																<tr>
																	<td><input type="text" placeholder="Field Label" name="template[form][label][]" value="<?php echo $attr[0]; ?>" /></td>
																	<td>
																		<select name="template[form][field][]">
																			<option value="text" <?php echo $attr[1]=="text"?"selected":"" ?> >Short Text</option>
																			<option value="number" <?php echo $attr[1]=="number"?"selected":"" ?> >Number</option>
																			<option value="date" <?php echo $attr[1]=="date"?"selected":"" ?> >Date</option>
																			<option value="textarea" <?php echo $attr[1]=="textarea"?"selected":"" ?> >Large text</option>
																			<option value="select" <?php echo $attr[1]=="select"?"selected":"" ?> >Single Select</option>
																			<option value="multiselect" <?php echo $attr[1]=="multiselect"?"selected":"" ?> >Multiple Select</option>
																		</select>
																	</td>
																	<td><input type="text" placeholder="" name="template[form][default][]" value="<?php echo $attr[3]; ?>" /></td>
																	<td>
																		<select name="template[form][required][]" >
																			<option value="required" <?php echo $attr[2]=="required"?"selected":"" ?> >Yes</option>
																			<option value="" <?php echo $attr[2]==""?"selected":"" ?> >No</option>
																		</select>
																	</td>
																	<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>
																</tr>
															<?php } } ?>			
															</tbody>
														</table>	
														<div class="col-sm-12 releaseBtnClass">
															<span class="btn btn-minier addRowBtn"><i class="fa fa-plus"></i> add form field</span>
														</div>														
													</div>
													
													<div class="form-group spreadsheet-div" <?php echo ($ro_data2['temp_type']=="spreadsheet")?"":'style="display:none"'; ?> >		
														<div style="margin-top:10px; <?php echo ($ro_data2['template']!="")?"display:none;":""; ?>" >															
															<div class="col-sm-4">
																<input type="text" class="form-control cols" placeholder="No. of columns" />
															</div>
															<div class="col-sm-4">
																<input type="text" class="form-control rows" placeholder="No. of rows" />
															</div>
															<div class="col-sm-4">
																<a class="btn btn-sm make-spreadsheet"> generate</a>
															</div>
															
														</div>
														<div class="s-sheet" style="<?php echo ($ro_data2['temp_type']=="spreadsheet" AND $ro_data2['template']!="")?"":'display:none;'; ?> overflow: scroll; min-width: 250px;">
														
														<?php if($ro_data2['temp_type']=="spreadsheet" AND $ro_data2['template']!="") { 
														$temp_rows = explode("\n",$ro_data2['template']);
														$count_cols = count(explode(",",$temp_rows[0]));
														
														?>
														<table class="table-spreadsheet" >
															<tr>
															<?php 
															$cell_col = 'A';
															for($j=0;$j<=$count_cols;$j++){
																if($j==0){ echo '<th class="heading">&nbsp;</th>'; }
																else { echo '<th>'.$cell_col.'</th>'; $cell_col++; } 
															} ?>
															</tr>
															<?php
															$i=1;															
															foreach($temp_rows as $temp_row){
																$temp_cols = explode(',',$temp_row);
																$k=0;
																echo '<tr>';
																foreach($temp_cols as $temp_col){
																	if($k==0){ echo '<td class="heading" >'.$i.'</td>'; }
																	echo '<td><input type="text" name="template[spreadsheet]['.$i.'][]" value="'.$temp_col.'" /></td>'; 
																	$k++;
																}
																echo '</tr>';
																$i++;
															}
														?>
														</table>

														<?php } ?>
														</div>
													</div>	
													
													<div>
													<p class="frm-section">Initiators: </p>
													<p> 
														<label><input type="radio" name="init" value="*" <?php echo ($ro_data2['senders']=="*")?'checked':""; ?> >Everyone</label> &nbsp;&nbsp;&nbsp; 
														<label><input type="radio" name="init" value="filter" <?php echo ($ro_data2['senders']!="*")?'checked':""; ?> >Specified user(s)</label></p>
													<div id="init-filter" <?php echo ($ro_data2['senders']=="*")?'style="display:none"':""; ?> >
													<table class="table table-bordered sheet init-filters">
														<thead class="thin-border-bottom">
															<tr>
																<th>WHERE</th>
																<th>EQUALS</th>
																<th></th>
																<th></th>
															</tr>
														</thead>
														<tbody>
														<?php //echo $ro_data2['senders'];
													
															if($ro_data2['senders']!="*"){
																
																$rep=array("AND","OR"); $wth=array("+","-");
																$strrr = str_replace($rep,$wth,$ro_data2['senders']);
																$delimiters = "+-";
																$filters = preg_split('/([' . $delimiters . '])/m', $strrr, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY); 
																//echo "<pre>".$ro_data2['senders']."<hr>"; print_r($filters); echo "</pre>"; 
																
																$f=0;
																
																while($f<count($filters)){
																	
																	$ex = explode("=",$filters[$f]);
																	
																	echo '<tr><td><select class="sendFilterType" name="senders[field][]">';
																	echo '<option value="" >No Filter</option>';
																	echo '<option value="id" '; echo (trim($ex[0])=="id")?"selected":""; echo ' >User</option>';
																	echo '<option value="job_title_id" '; echo (trim($ex[0])=="job_title_id")?"selected":""; echo ' >Position</option>';
																	echo '<option value="station_id" '; echo (trim($ex[0])=="station_id")?"selected":""; echo ' >Branch</option>';
																	echo '<option value="department_id" '; echo (trim($ex[0])=="department_id")?"selected":""; echo ' >Department</option>';
																	echo '</select>'.
																	'</td>'.
																	'<td>'.
																	'	<select class="sendFilterVals" name="senders[value][]">';
																	echo getDropDown(trim($ex[0]),trim($ex[1]));
																	echo '	</select>'.
																	'</td>';
																	if(isset($filters[$f+1])){ $f++; }
																	echo '<td>'.
																	'	<select name="senders[operator][]" >'.
																	'		<option value="" ></option>'.
																	'		<option value="AND" ';
																	echo $filters[$f]=="+"?"selected":"";
																	echo ' >AND</option>'.
																	'		<option value="OR" ';
																	echo $filters[$f]=="-"?"selected":"";
																	echo ' >OR</option>'.
																	'	</select>'.
																	'</td>'.
																	'<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'.
																	'</tr>';
																	
																	$f++;
																}
															}	
															?>
															
														</tbody>
													</table>
													<!--<span class="btn btn-minier addFilterBtn"><i class="fa fa-plus"></i> add filter</span>-->
													
													
															
													</div>
													
													
													
													
													</div>
													
													
													<div>
													<p>&nbsp;</p>
													<p class="frm-section">Signatures: </p>
													<p> 
														<label><input type="radio" class="signatures" name="data[signatures]" value="0" <?php echo ($ro_data2['signatures']=="0")?'checked':""; ?> >Not Required</label> &nbsp;&nbsp;&nbsp; 
														<label><input type="radio" class="signatures" name="data[signatures]" value="1" <?php echo ($ro_data2['signatures']=="1")?'checked':""; ?> >Required</label></p>
													<div id="signatory-filter" <?php echo ($ro_data2['signatures']=="0")?'style="display:none"':""; ?> >
													<!--<p>
														<input type="hidden" name="data[signatures_sequential]" value="0" >
														<label><input type="checkbox" name="data[signatures_sequential]" value="1" <?php echo ($ro_data2['signatures_sequential']=="1")?'checked':""; ?>  > Enforce Sequential Signing </label>
													</p>-->
													
													<table class="table table-bordered sheet sign-filters">
														<thead class="thin-border-bottom">
															<tr>
																<th>Signatory</th>
																<th></th>
															</tr>
														</thead>
														<tbody>
														<?php //echo $ro_data2['senders'];
													
															//if($ro_data2['signatures']=="1"){
																
																$filters = $conn->query("SELECT * FROM document_type_signatories WHERE doc_type_id='$unit_id'"); 
																while($s=$filters->fetch_assoc()){
																	echo '<tr><td><a href="javascript:void()" class="remove_sig remove" title="Remove file"><i class="fa fa-minus-circle"></i></a>'.
																	'	<select class="form-control" name="signatory[]">';
																	echo getDropDown("id",$s["signatory_id"]);
																	echo '	</select>'.
																	'</td>'.
																	'<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'.
																	'</tr>';
																}
															//}	
															?>
															
														</tbody>
													</table>
													<!--<span class="btn btn-minier addSignatoryBtn" dtype="<?= $unit_id; ?>" ><i class="fa fa-plus"></i> add signatory</span>-->
															
													</div>
												
													</div>
														
													</form>
													
													<p>&nbsp;</p>
													
												</div> <!-- /.widget-body -->
												</div> <!-- /.widget-box -->
												</div> <!-- /.tab-pane -->
												
											<?php } ?> 
												
											</div>
										</div>

										<!-- /section:elements.tab.position -->
									</div><!-- /.col -->
									
									<div class="col-sm-4">
										
										
										<!-- #section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Add New Document Type</h4>												
											</div>

											<div class="widget-body" style="background-color: #f9f9f9; padding-left:10px; padding-right:10px;">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													<form method="post" action="doctypes.php?new" >
														<div class="form-group">
															<label for="form-field-username">Document Type</label>
															<div>
																<input type="text" class="form-control" required id="form-field-username"  name="data[doc_type]" placeholder="Document type"  />
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-grp">Document Grouping</label>
															<div>
																<input type="text" class="form-control" id="form-field-grp"  name="data[doc_grouping]"   />
															</div>
															
														</div>
														<div class="form-group">
															<label for="doc_classification">Origin</label>
															<div>
																<select name="data[doc_classification]" id="doc_classification">
																	<option value="INTERNAL">INTERNAL</option>
																	<option value="EXTERNAL">EXTERNAL</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="description">Description</label>
															<div>
																<textarea class="form-control" id="description"  name="data[description]" rows="3" ></textarea>
															</div>
														</div>
														
														
														<button class="btn btn-primary pull-right" >
															<i class="ace-icon fa fa-floppy-o bigger-120"></i>
															Save Document Type
														</button>
														
														<input type="hidden" name="action" value="NEW DOC TYPE" >	
														<input type="hidden" name="table" value="document_types">
														
													</form>
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
									</div>
									<div class="col-sm-4">
										
										
										<!-- #section:custom/widget-box.options.transparent -->
										<div class="widget-box transparent">
											<div class="widget-header">
												<h4 class="widget-title lighter">Add New Signatory</h4>												
											</div>

											<div class="widget-body" style="background-color: #f9f9f9; padding-left:10px; padding-right:10px;">
												<div class="widget-main padding-6 no-padding-left no-padding-right">
													<form method="post" action="doctypes.php?new" >
													<div class="form-group">
															<label for="doc_classification">Name of the signatory</label>
															<div>
																<select name="data[signatory_id]" id="signatory_id">
																<option value="">--Select name of signatory--</option>
																<?php $form_qry = "SELECT * FROM admins ORDER BY fname asc"; 
												                $form_res = $conn->query($form_qry);
												                while($ro_data = $form_res->fetch_assoc()) {?>
																	<option value="<?=$ro_data['id'];?>"><?=$ro_data['fname']." ".$ro_data['lname'];?></option>
																<?php } ?>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Document Type</label>
															<div>
																<select name="data[doc_type_id]" id="doc_type_id">
																<option value="">--Select document type--</option>
																<?php $form_qry = "SELECT * FROM document_types ORDER BY doc_type"; 
												                $form_res = $conn->query($form_qry);
												                while($ro_data = $form_res->fetch_assoc()) {?>
																	<option value="<?=$ro_data['id'];?>"><?=$ro_data['doc_type'];?></option>
																<?php } ?>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="form-field-username">Job Title</label>
															<div>
																<select name="data[position]" id="position">
																<option value="">--Select job title--</option>
																<?php $form_qry = "SELECT * FROM job_titles ORDER BY job_title asc"; 
												                $form_res = $conn->query($form_qry);
												                while($ro_data = $form_res->fetch_assoc()) {?>
																	<option value="<?=$ro_data['id'];?>"><?=$ro_data['job_title'];?></option>
																<?php } ?>
																</select>
															</div>
														</div>
														<button class="btn btn-primary pull-right" >
															<i class="ace-icon fa fa-floppy-o bigger-120"></i>
															Save Signature
														</button>
														
														<input type="hidden" name="action" value="NEW SIGNATURE" >	
														<input type="hidden" name="table" value="document_type_signatories">
														
													</form>
												</div>
											</div>
										</div>

										<!-- /section:custom/widget-box.options.transparent -->
									</div>
									<!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		
		<div class="modal fade" id="myModal333" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			  </div>
			  <div class="modal-body"></div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div> 

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/bootstrap-timepicker.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.colorpicker.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.treeview.js"></script>
		<script src="assets/js/ace/elements.wizard.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		<script src="assets/plugins/jconfirm/js/jquery-confirm.js" ></script>
	
		

<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
	 var $sidebar = $('.sidebar').eq(0);
	 if( !$sidebar.hasClass('h-sidebar') ) return;
	
	 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
		if( event_name !== 'sidebar_fixed' ) return;
	
		var sidebar = $sidebar.get(0);
		var $window = $(window);
	
		//return if sidebar is not fixed or in mobile view mode
		var sidebar_vars = $sidebar.ace_sidebar('vars');
		if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
			$sidebar.removeClass('lower-highlight');
			//restore original, default marginTop
			sidebar.style.marginTop = '';
	
			$window.off('scroll.ace.top_menu')
			return;
		}
	
	
		 var done = false;
		 $window.on('scroll.ace.top_menu', function(e) {
	
			var scroll = $window.scrollTop();
			scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
			if (scroll > 17) scroll = 17;
	
	
			if (scroll > 16) {			
				if(!done) {
					$sidebar.addClass('lower-highlight');
					done = true;
				}
			}
			else {
				if(done) {
					$sidebar.removeClass('lower-highlight');
					done = false;
				}
			}
	
			sidebar.style['marginTop'] = (17-scroll)+'px';
		 }).triggerHandler('scroll.ace.top_menu');
	
	 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
	
	 $(window).on('resize.ace.top_menu', function() {
		$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
	 });
	 
	$('[data-rel=tooltip]').tooltip();

	/////// remember to comeback and disable inputs if set to hide
	$('body').on('change','input[name=init]',function(){ 
		if($(this).val()=="*"){
			$('div#init-filter').hide();
		} else {
			$('div#init-filter').show();
		}
	});
	
	$('body').on('change','.signatures',function(){ 
		if($(this).val()=="0"){
			$('div#signatory-filter').hide();
		} else {
			$('div#signatory-filter').show();
		}
	});
		
	
	 $('body').on('change','.set-temp-type',function(){ 
		var $ths = $(this);
		var widgetContainer = $ths.parents('.widget-body');
		var temp_type =  $('option:selected', $ths).val();
		widgetContainer.find('div.form-div, div.html-div, div.spreadsheet-div,div.attachment-div').hide();
		if(temp_type=="html"){
			widgetContainer.find('div.html-div').show();
		}
		if(temp_type=="form"){
			widgetContainer.find('div.form-div').show();
		}
		if(temp_type=="spreadsheet"){
			widgetContainer.find('div.spreadsheet-div').show();
		}
		if(temp_type=="attachment"){
			widgetContainer.find('div.attachment-div').show();
		}	
	 });
	 
	 $("body").on("click","a.make-spreadsheet", function(e) {
		e.preventDefault();
		var widgetContainer = $(this).parents('.widget-body');
		var sheetContainer = $(this).parents('.spreadsheet-div');
		var cols = sheetContainer.find('input.cols').val();
		var rows = sheetContainer.find('input.rows').val();
		var table = '<table class="table-spreadsheet" >';
		for(i=0;i<=rows;i++){
			table += "<tr>";
			if(i==0){
				var cell_col = 'A';
				for(j=0;j<=cols;j++){
					if(j==0){ table+='<th class="heading">&nbsp;</th>'; }
					else { table+='<th>'+cell_col+'</th>'; cell_col= String.fromCharCode(cell_col.charCodeAt(0) + 1) ; } 
				}
			} else{
				for(j=0;j<=cols;j++){
					if(j==0){ table+='<td class="heading" >'+i+'</td>'; }
					else { table+='<td><input type="text" name="template[spreadsheet]['+i+'][]" /></td>'; }
				}
			}	
			table += "</tr>";
		}
		table+='</table>';
		widgetContainer.find('div.s-sheet').html(table).show();	
	});
	 
	$('body').on('click','.fa-arrow-up,.fa-arrow-down',function(){

		
		var row = $(this).parents('tr:first');
		if ($(this).is('.fa-arrow-up')) {
            row.insertBefore(row.prev());	
		}
		else {
            row.insertAfter(row.next());
		}
	
	});
	
	
	$('body').on('click','.addRowBtn',function(){
		var widgetContainer = $(this).parents('.widget-body');
		var newRow = '<tr>' +
					'	<td><input type="text" placeholder="Field Label" name="template[form][label][]" /></td>' +
					'	<td>' +
					'		<select name="template[form][field][]">' +
					'			<option value="text" >Short Text</option>' +
					'			<option value="number" >Number</option>' +
					'			<option value="date" >Date</option>' +
					'			<option value="textarea" >Large text</option>' +
					'			<option value="select" >Single Select</option>' +
					'			<option value="multiselect" >Multiple Select</option>' +
					'		</select>' +
					'	</td>' +
					'	<td><input type="text" placeholder="" name="template[form][default][]" /></td>' +
					'	<td>' +
					'		<select name="template[form][required][]" >' +
					'			<option value="required" >Yes</option>' +
					'			<option value="" >No</option>' +
					'		</select>' +
					'	</td>' +
					'	<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'+
					'</tr>';
		widgetContainer.find('.form-elements tbody').append(newRow);

	});
	
	$('body').on('click','.addFilterBtn',function(){
		var widgetContainer = $(this).parents('.widget-body');
		var newRow = '<tr>'+
					'<td>'+
					'	<select class="sendFilterType" name="senders[field][]">'+
					'		<option value="" >No Filter</option>'+
					'		<option value="id" >User</option>'+
					'		<option value="job_title_id" >Position</option>'+
					'		<option value="station_id" >Branch</option>'+
					'		<option value="department_id" >Department</option>'+
					'		<option value="group" >Group</option>'+
					'	</select>'+
					'</td>'+
					'<td>'+
					'	<select class="sendFilterVals" name="senders[value][]">'+
					'		<option value="" ></option>'+
					'	</select>'+
					'</td>'+
					'<td>'+
					'	<select name="senders[operator][]" >'+
					'		<option value="" ></option>'+
					'		<option value="AND" >AND</option>'+
					'		<option value="OR" >OR</option>'+
					'	</select>'+
					'</td>'+
					'<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'+
					'</tr>';
		widgetContainer.find('.init-filters tbody').append(newRow);

	});
	
	$('body').on('click','.addSignatoryBtn',function(){
		var $ths = $(this);
		var widgetContainer = $ths.parents('.widget-body');
		var doc_id = $ths.attr('dtype');
		selct = '<option value="" > - select signatory - </option>';
		var usrData = $.parseJSON(usrList());
		$.each(usrData, function(key,value) {
		  selct += '<option value="'+value.id+'" >'+value.nme+'</option>';
		});
		var newRow = '<tr>'+
					'<td><a href="javascript:void()" class="remove_sig remove" title="Remove file"><i class="fa fa-minus-circle"></i></a>'+
					'	<select class="form-control" name="signatory[]">'+
					selct+
					'	</select>'+
					'</td>'+
					'<td><i class="fa fa-arrow-up"></i> <i class="fa fa-arrow-down"></i></td>'+
					'</tr>';
		widgetContainer.find('.sign-filters tbody').append(newRow);

	});
	
	$('body').on('click','a.remove_sig',function(){
		$(this).parents('tr').remove();
	});
	
	$("body").on("change",".sendFilterType", function(e) {
		var $ths = $(this);
		var widgetContainer = $ths.parents('tr');
		var filter_type =  $('option:selected', $ths).val();
		
		var selct = "";
					
		if(filter_type=="id"){
			selct = '<option value="" > - select user - </option>';
			var usrData = $.parseJSON(usrList());
			$.each(usrData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.nme+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="job_title_id"){
			selct = '<option value="" > - select title - </option>';
			var jobData = $.parseJSON(jobList());
			$.each(jobData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.job_title+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="station_id"){
			selct = '<option value="" > - select branch - </option>';
			var brcData = $.parseJSON(brcList());
			$.each(brcData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.duty_station+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="department_id"){
			selct = '<option value="" > - select dept - </option>';
			var depData = $.parseJSON(depList());
			$.each(depData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.department+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		if(filter_type=="group"){
			selct = '<option value="" > - select group - </option>';
			var grpData = $.parseJSON(grpList());
			$.each(grpData, function(key,value) {
			  selct += '<option value="'+value.id+'" >'+value.group_name+'</option>';
			}); 
			widgetContainer.find('.sendFilterVals').html(selct);
			
		} 
		
		
	});
	
	
	$("body").on("click","a.delete-doc", function(e) {
		e.preventDefault();
		var $this = $(this);
		$.confirm({
			title: 'Delete Document Type',
			content: 'Are you sure you want to delete this document type?', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var $id = $this.attr('data-id');
						var $tbl = $this.attr('data-tbl');
						var tab_div = 'li'+$id;
						var content_div = 'home'+$id;
						var dataString = 'table='+$tbl+'&item_id='+$id;
						
						$.ajax({
							type: "POST",
							url: "include/ajax_delete.php",
							data: dataString,
							success: function(data) {
							  if(data=="DELETED SUCCESSFULLY"){
								$('#'+tab_div).remove();
							    $('#'+content_div).remove();
								$('#myTab3 li').first().addClass("active");
								$('.tab-content div.tab-pane').first().addClass("active");
							  } else {
								$.alert("<b>ERROR!</b>Delete failed!!!");  
							  }
							},
							error: function(err){
								$.alert("Errrrrrrrrrr");
							}
						  }); 
					}
				},
				cancel: function () {
					$.alert('Delete <strong>canceled</strong>');
				}
			}

		});
		
	});
	
	$("body").on("click","a.save-doc", function(e) {
		e.preventDefault();
		var $this = $(this);
		$.confirm({
			title: 'Save Document Type',
			content: 'Confirm SAVE? NOTE: No UNDO function for this action.', 
			buttons: {
				'confirm': {
					text: 'YES',
					btnClass: 'btn-blue',
					action: function () {
						var doc_id = $this.attr("doc-id");
						var formId = "form"+doc_id;
						$('#'+formId).submit();
						//save doc info
						/*
						var dataString = $('#'+formId).serialize();
						
						$.ajax({
							type: "POST",
							url: "include/do_edit.php",
							data: dataString,
							success: function(data) {
							  alert(data);
							},
							error: function(err){
								alert(JSON.stringify(err));  
							}
						  }); //*/
						//save template info
						
						//save workflow settings
												
						
					}
				},
				cancel: function () {
					$.alert('SAVE <strong>canceled</strong>');
				}
			}

		});
		
	});
	
	
	

	$('body').on('click','.remove-row',function(){
		$(this).parents('.table-row').remove();
	});

	 
	$('.timepicker').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	 
	 $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		
	
		
	// picture upload
	$('#pic_upclose').ace_file_input({
			style:'well',
			btn_choose:'Click to upload Scanned Copy',
			btn_change:null,
			no_icon:'ace-icon fa fa-picture-o',
			thumbnail:'large',
			droppable:true,
			
			allowExt: ['jpg', 'jpeg', 'png', 'gif'],
			allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
	});	
			
});
</script>
	<script type="text/javascript">
			function usrList(){
				var str = <?php echo  str_replace("'", "", $usr_json1); ?>;
				return str;
			}
			function jobList(){
				var str = <?php echo str_replace("'", "", $job_json); ?>;
				return str;
			}
			function brcList(){
				var str = <?php echo str_replace("'", "", $brc_json); ?>;
				return str;
			}
			function depList(){
				var str = <?php echo str_replace("'", "", $dep_json); ?>;
				return str;
			}
			function grpList(){
				var str = <?php echo str_replace("'", "", $grp_json); ?>;
				return str;
			}
			
		</script>
		
				<!-- inline scripts related to this page -->


		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />


</html>
