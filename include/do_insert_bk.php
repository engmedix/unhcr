<?php 
 define('ROOT', str_ireplace('\include','',(dirname(__FILE__))));

$tbl = $_POST['table'];
$refNumber = '';
$email_name = 'CINNAMON WORKFLOW';

$sys_log_user = $my['fname']." ".$my['lname'];

if(isset($_POST['ddt'])){
	foreach (array_keys($_POST['ddt']) as $field)
	{
		if($_POST['ddt'][$field]['yyyy']!="YYYY" AND $_POST['ddt'][$field]['mm']!="MM" AND $_POST['ddt'][$field]['dd']!="DD"){
			$_POST['data'][$field] = $_POST['ddt'][$field]['yyyy']."-".$_POST['ddt'][$field]['mm']."-".$_POST['ddt'][$field]['dd'];
		}
	}
}

if($_POST['action']=="NEW DOC"){
	if($_POST['data']['document_type']=="LOAN APPLICATION"){
		
		$loan_complete = true;
		
		if(isset($_POST['data']['applicant_date_of_birth'])){
			$_POST['dataL']['applicant_date_of_birth'] = $_POST['data']['applicant_date_of_birth'];
			unset($_POST['data']['applicant_date_of_birth']);
		}
	}
}

$insert_data = databaseInsert($tbl, $_POST['data']);

if($conn->query($insert_data) === TRUE){ //if insert query successful
	//get last ID
	$psn_last_id = $conn->insert_id;
	
	sys_log($sys_log_user,$_POST['action'],$insert_data,"Success");
	
	//set success message
	$response = 'RECORD SAVED SUCCESSFULLY.'.(($tbl == 'documents')?' <br> Reference Number: <strong>'.$refNumber.'</strong>':'');
	$response_icon="check"; $response_type="success"; $response_title ="SAVED!";
	
	if($_POST['action']=="NEW DOC TYPE"){
		
		$actns = '[{ \"type\":\"submit\", \"label\":\"Submit\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"select\", \"send_filters\":\"*\", \"stage_label\":\"Processing\", \"status_label\":\"Submitted\" }]';
		$wflws = '[{ \"type\":\"update\", \"label\":\"Update Status\", \"color\":\"white\", \"icon\":\"\", \"send_type\":\"stay\", \"stage_label\":\"Processing\", \"status_label\":\"Not Started,In Progress,On Hold, Completed\" },\r\n{ \"type\":\"forward\", \"label\":\"Review & Forward\", \"color\":\"info\", \"icon\":\"\", \"send_type\":\"select\", \"send_filters\":\"*\", \"stage_label\":\"Processing\", \"status_label\":\"Forwarded\" },\r\n{ \"type\":\"reject\", \"label\":\"Reject & Close\", \"color\":\"danger\", \"icon\":\"\", \"send_type\":\"end\", \"stage_label\":\"Closed\", \"status_label\":\"Rejected\" },\r\n{ \"type\":\"approve\", \"label\":\"Approve & Close\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"end\", \"stage_label\":\"Closed\", \"status_label\":\"Approved\" }]';
		
		$update_wf = "UPDATE $tbl SET actions='$actns', workflow='$wflws' WHERE id='$psn_last_id'";
		
		if($conn->query($update_wf) === TRUE){	
			sys_log($sys_log_user,$_POST['action'].":SUB PROCESS",$update_wf,"Success");
		} else {
			sys_log($sys_log_user,$_POST['action'].":SUB PROCESS",$update_wf,"Failed: ".$conn->error);
		}
		
		unset($actns); unset($wflws);  unset($update_wf); 
		
	}
	
	if($_POST['action']=="NEW DOC"){
	
	//create account directory
	$file1 = explode("-",date("Y-m-d")); 

	$doc_type = $_POST['data']['document_type'];
	$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $doc_type;

	$dir1="doc_archive/".$file1[0];
	$dir2=$dir1."/".$file1[1];
	$dir3=$dir2."/".$doc_type;

	if(!is_dir($destination_dir)) {  
		
		if(!is_dir($dir1)) {
			if(mkdir($dir1, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir1,"Success"); }
			if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir2,"Success"); }
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
		if(!is_dir($dir2)) {
			if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir2,"Success"); }
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
		if(!is_dir($dir3)) {
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
	}
	
	//handle template data
	if($_POST['data']['temp_type']=="form"){
		
		$_frm = array();
		
		foreach($_POST['_frm'] as $key=>$val){
			if(is_array($val)){ $_frm[$key]=implode(",",$val); }
			else { $_frm[$key]=$val; }
		}
		
		$frm_data = json_encode($_frm);
		
		$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";
		if ($conn->query($update_frm) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_frm,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_frm,"Failed: ".$conn->error);
		}
	}
	
	//handle data from spreadsheet
	if($_POST['data']['temp_type']=="spreadsheet"){
		
		$temp = $_POST['template']['spreadsheet'];
		$ros = array();
		foreach($temp as $temp_ro){
			array_push($ros, implode(",",commaless($temp_ro)));
		}
		$template=implode("\n",$ros);
		$temp_type_spreadshhet = "UPDATE documents SET template='$template' WHERE id='$psn_last_id'";
		if ($conn->query($temp_type_spreadshhet) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $temp_type_spreadshhet,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $temp_type_spreadshhet,"Failed: ".$conn->error);
		}
	
	}
	
	/////////CUSTOM FORMS////////////////
	if($_POST['data']['document_type']=="BUSINESS LOAN"){
		
		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['file_number'] = $_POST['data']['doc_ref_number'];
		
		$loan_details = databaseInsert('cstm_business_loan', $_POST['dataL']);
		
		if ($conn->query($loan_details) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Success");
			$cstm_loan_id = $conn->insert_id;

			//if has attachments
			if(count($_FILES['upload'])>0) {
				
				$errmsg = "<hr/>";
				
				$uploads=$_FILES['upload'];
				//start uploading the attachement
				$uploaddir = $destination_dir."/".$psn_last_id;
				if(!is_dir($uploaddir)) {
					if(mkdir($uploaddir, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$uploaddir,"Success"); }
				}
				//for attachments uploads
				$accepted_file_types = array('jpg','gif','png','pdf' );
				
				foreach($uploads['name'] as $file_key=>$file_attr){
					
					if($file_attr!=""){
						$attch = $file_key;
						
						$file_attr = str_replace("'","",$file_attr);
						
						$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
						$orig_name = basename($file_attr,".".$file_type);
						$file_name = $attch.".".$file_type;	
						
						if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
							//upload the attachment
							$ii = 2;
							$uploadfile = $uploaddir."/".$file_attr;
							while(file_exists($uploadfile)){           
								$uploadfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
								$ii++;
							}
							
							if(move_uploaded_file($uploads['tmp_name'][$file_key], $uploadfile)) {
								sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", "Uploading ".$uploads['tmp_name'][$file_key]." to ".$uploadfile,"Success");
								$errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.'; 
								
								//add to the insert statement
								$loan_upload_details = "UPDATE cstm_loan_applications SET `$attch`='$uploadfile' WHERE appId='$cstm_loan_id'";
								if ($conn->query($loan_upload_details) === TRUE){
									sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_upload_details,"Success");
								} else {
									sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_upload_details,"Failed: ".$conn->error);
								}
								
							} else {
								sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", "Uploading ".$uploads['tmp_name'][$file_key]." to ".$uploadfile,"Failed");
							}
						} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
					}	
					
				}

				$response .= $errmsg;	  
		
			}//end if has attachement
		
		} else { 
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Failed: ".$conn->error);
			$response .= "DB Update Failed! <hr>".$conn->error."<hr>".$loan_details;  
		}
		
		
		
	}
	
	if($_POST['data']['document_type']=="SE LOAN"){
		
		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['file_number'] = $_POST['data']['doc_ref_number'];
		
		$loan_details = databaseInsert('cstm_se_loan', $_POST['dataL']);
		
		if ($conn->query($loan_details) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Success");
			$cstm_loan_id = $conn->insert_id;

			//if has attachments
			if(count($_FILES['upload'])>0) {
				
				$errmsg = "<hr/>";
				
				$uploads=$_FILES['upload'];
				//start uploading the attachement
				$uploaddir = $destination_dir."/".$psn_last_id;
				if(!is_dir($uploaddir)) {
					if(mkdir($uploaddir, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$uploaddir,"Success"); }
				}
				//for attachments uploads
				$accepted_file_types = array('jpg','gif','png','pdf' );
				
				foreach($uploads['name'] as $file_key=>$file_attr){
					
					if($file_attr!=""){
						$attch = $file_key;
						
						$file_attr = str_replace("'","",$file_attr);
						
						$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
						$orig_name = basename($file_attr,".".$file_type);
						$file_name = $attch.".".$file_type;	
						
						if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
							//upload the attachment
							$ii = 2;
							$uploadfile = $uploaddir."/".$file_attr;
							while(file_exists($uploadfile)){           
								$uploadfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
								$ii++;
							}
							
							if(move_uploaded_file($uploads['tmp_name'][$file_key], $uploadfile)) {
								sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", "Uploading ".$uploads['tmp_name'][$file_key]." to ".$uploadfile,"Success");
								$errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.'; 
								
								//add to the insert statement
								$loan_upload_details = "UPDATE cstm_loan_applications SET `$attch`='$uploadfile' WHERE appId='$cstm_loan_id'";
								if ($conn->query($loan_upload_details) === TRUE){
									sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_upload_details,"Success");
								} else {
									sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_upload_details,"Failed: ".$conn->error);
								}
								
							} else {
								sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", "Uploading ".$uploads['tmp_name'][$file_key]." to ".$uploadfile,"Failed");
							}
						} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
					}	
					
				}

				$response .= $errmsg;	  
		
			}//end if has attachement
		
		} else { 
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Failed: ".$conn->error);
			$response .= "DB Update Failed! <hr>".$conn->error."<hr>".$loan_details;  
		}
		
		
		
	}
	
	
 	//+++++++++++++++++++++++++++++++++++++++++++++++
	//       UPLOAD PROFILE PICTURE
	//start uploading the picture
	
	if(file_exists($_FILES['photo']['tmp_name']) || is_uploaded_file($_FILES['photo']['tmp_name'])) {
    
   $accepted_file_types = array('jpg',
								'gif',
								'png',
								'pdf',
								'doc',
								'docx',
								'xls',
								'xlsx',
								'ppt',
								'pptx',
								'txt');
	
	
	$uploaddir = $destination_dir;
	$old_file_name = $psn_last_id.basename($_FILES['photo']['name']);
	$file_type = strtolower(substr(strrchr($old_file_name,'.'),1));
	$file_name = $doc_type."-".$psn_last_id.".".$file_type;
	
	if(in_array($file_type, $accepted_file_types)){ //must be a picture
		
	//upload the picture
	$uploadfile = $uploaddir."/".$file_name;
	if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile)) {
	  $response .= '<br> Document uploaded.'; 
	  //add picture info to the article
	  //$file_loc = $uploaddir.$file_name;
	  $update = "UPDATE documents SET file_location='$uploadfile' WHERE id='$psn_last_id'";
	  //$update ="INSERT INTO application_uploads SET location = '$uploadfile', file_description='$file_name', form_id='$id'";
      if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. "; 	  }
	}
	} else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
	}//if has attachement 
	//---------------------------------------------------------

	/// add track information 
		$tbl = 'doc_track';
		$_POST['data2']['document_type'] = $_POST['data']['document_type'];
		$_POST['data2']['from_id'] = $_POST['data']['sender_id'];
		$_POST['data2']['to_id'] = $_POST['data']['current_holder'];
		$_POST['data2']['doc_id'] = $psn_last_id;
		$_POST['data2']['from_stage'] = "INITIATION";
		$_POST['data2']['from_status'] = "INITIATION";
		$_POST['data2']['remark'] = $_POST['data']['description'];
		$_POST['data2']['action'] = 'SUBMITTED';
		$_POST['data2']['receiver_status'] = 'SENT';
		
		$insert_data2 = databaseInsert($tbl, $_POST['data2']);

		if($conn->query($insert_data2) === TRUE){
			
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $insert_data2,"Success");
			
			$response .= ' <div><em>Your document is now being tracked.</em></div>';
			
			$myTrackId = $conn->insert_id;
			
			$to = getStaffEmail($_POST['data2']['to_id']);
			$subject = $_POST['data2']['action'].': '.$refNumber.' '.getStaffName($_POST['data2']['from_id']);
			$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;
			
			$message = '<p>Dear '.getStaffName($_POST['data2']['to_id']) .'<br>The document listed below requires your attention. Follow the link to get more detail.</p>';
			$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($psn_last_id).": ".$refNumber.'</span></p>';
			$message .= '<p>
			<br>
			<table width="90%" cellpadding="7" align="center" cellspacing="0" >
				<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Remarks</th></tr>
				<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data2']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($psn_last_id).'</td><td style="border:solid 1px #ccc">'.$_POST['data2']['remark'].'</td></tr>
			</table><br><br></p>';
			
			dbmail($to,$subject,$message,$link);
			
			if(isset($_POST['data']['email']) && $_POST['data']['email'] != ''){
				$tmpQry = $conn->query('SELECT * FROM common_settings WHERE setting = "acknowledgment_message"');
				$tmpRst = $tmpQry->fetch_assoc();
				$template = $tmpRst['value'];
				
				$to = $_POST['data']['email'];
				$subject = getDocType($_POST['data2']['doc_id']).' received';
				
				$message = '';
				
				$replace = array('{SENDER}','{RECIPIENT}');
				$with = array($_POST['data']['submitted_by'],getStaffName($_POST['data2']['to_id']));
				$msg = str_replace($replace, $with, $template);
				$message .= $msg;
				
				$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data2']['doc_id']).": ".$refNumber.'</span></p>';
				
				dbmail($to,$subject,$message,$link);
				
			}
				
			
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $insert_data2,"Failed:".$conn->error);
			$response .= ' <div><em>Could not add document to track list.'.$conn->error.'</em></div>';
		}
		
		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			
			$_POST['data2']['from_id'] = $_POST['data']['current_holder'];
			$_POST['data2']['to_id'] = $_POST['delegate_to'];
			$_POST['data2']['action'] = 'DELEGATED';
			$delegTrackId = $myTrackId;
			
			$insert_data3 = databaseInsert($tbl, $_POST['data2']);

			if($conn->query($insert_data3) === TRUE){
				
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $insert_data3,"Success");
				
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				
				$myTrackId = $conn->insert_id;

				$to = getStaffEmail($_POST['data2']['to_id']);
				$from = 'info@cinnamonerp.com';
				$subject = $_POST['data2']['action'].': '.$refNumber.' '.getStaffName($_POST['data2']['from_id']);
				$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;
				
				$message = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data2']['doc_id']).": ".$refNumber.'</span></p>';
				$message .= '<p><br>
				<table width="90%" cellpadding="7" align="center" cellspacing="0" >
					<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
					<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data2']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data2']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data2']['remark'].'</td></tr>
				</table>
				';
				
				dbmail($to,$subject,$message,$link);
				
				
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $insert_data3,"Failed:".$conn->error);
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->error.'</em></div>';
			}
			
			
			//update the current holder in the documents table
			$del_doc_id = $_POST['data2']['doc_id'];
			$del_current_holder = $_POST['data2']['to_id'];
			
			$updateQry = "UPDATE documents SET current_holder='".$del_current_holder."', file_status='DELEGATED' WHERE id='".$del_doc_id."'";
			if($conn->query($updateQry) === TRUE){
				
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Success");
				
				$response .= ' <div><em>Document update success.</em></div>';
				
				//update previous track record to the archives
				$prev_qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $delegTrackId;
				if($conn->query($prev_qry) === TRUE){
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $prev_qry,"Success");
					$response .= ' ';
				} else {
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $prev_qry,"Failed:".$conn->error);
				}
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Failed:".$conn->error);
				$response .= ' <div><em>Document update failure.'.$conn->error.'</em></div>';
			}
			
		}
		
		
	/////////////// end track information
	
	} //if NEW DOC
	
	
	if($_POST['action']=="NEW DELEG"){
		if(isset($_POST['data']['approved']) AND $_POST['data']['approved']=='1'){
		
			$dlg_to_id = $_POST['data']['delegate_to'];
			$stff_id = $_POST['data']['requested_by'];

			$update_staff = "UPDATE admins SET delegate_to='$dlg_to_id' WHERE id='$stff_id'";
			if($conn->query($update_staff) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
	}
	
	
	if($_POST['action'] == "WORKFLOW ACTION"){
		
		$myTrackId = $psn_last_id;
		
		if($_POST['data']['to_stage']=="CLOSED"){
			$update_close = "UPDATE doc_track SET receive_date= NOW(), archive_date= NOW() WHERE id='$myTrackId'";
			if($conn->query($update_close) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_close,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_close,"Failed: ".$conn->error);
			}
		}
		
		$refNumber = getRefNo($_POST['data']['doc_id']);
		
		$to = getStaffEmail($_POST['data']['to_id']);
		$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
		$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
		
		$message = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
		$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data']['doc_id']).": ".$refNumber.'</span></p>';
		$message .= '<p><br>
		<table width="90%" cellpadding="7" align="center" cellspacing="0" >
			<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
			<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data']['remark'].'</td></tr>
		</table></p>';
		
		dbmail($to,$subject,$message,$link);
			
		$tbl = 'documents';
		$item = 'id';
		$itemId = $_POST['data']['doc_id'];
		$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
		$_POST['data2']['sender_id'] = $_POST['data']['from_id'];
		
		$actionQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );
		
		if($conn->query($actionQry) === TRUE){
			
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $actionQry,"Success");
			$response .= ' <div><em>Document update success.</em></div>';
			$archive_qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $_POST['tId'];
			if($conn->query($archive_qry) === TRUE){
				$response .= ' <div><em>Document has been added to your archives.</em></div>';
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $archive_qry,"Success");
				
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $archive_qry,"Failed:".$conn->error);	
			}
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $actionQry,"Failed:".$conn->error);
			$response .= ' <div><em>Document update failed.'.$conn->error.'</em></div>';
		}
		
		if(isset($_POST['data3']['loan_ID'])){
			$ln = "UPDATE cstm_loan_applications SET loan_ID = '".$_POST['data3']['loan_ID']."' WHERE document_id ='". $_POST['data']['doc_id']."'";
			if($conn->query($ln) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $ln,"Success");
				$response .= ' <div><em>Loan ID updated.</em></div>';
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $ln,"Failed:".$conn->error);
			}
		}
		
		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			//create new record for the delegate into the doc_track table
			$tbl = 'doc_track';
			$_POST['data']['from_id'] = $_POST['data']['to_id'];
			$_POST['data']['to_id'] = $_POST['delegate_to'];
			$_POST['data']['action'] = 'DELEGATED';
			$delegTrackId = $myTrackId;
			
			$delegate_data = databaseInsert($tbl, $_POST['data']);

			if($conn->query($delegate_data) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $delegate_data,"Success");
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				$myTrackId = $conn->insert_id;
				
				$refNumber = getRefNo($_POST['data']['doc_id']);
				
				$to = getStaffEmail($_POST['data']['to_id']);
				$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
				$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
				
				$message = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data']['doc_id']).": ".$refNumber.'</span></p>';
				$message .= '<p><br>				
				<table width="90%" cellpadding="7" align="center" cellspacing="0" >
					<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
					<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data']['remark'].'</td></tr>
				</table></p>';
				
				dbmail($to,$subject,$message,$link);
				
				
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $delegate_data,"Failed:".$conn->error);
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->error.'</em></div>';
			}
			
			//update the current holder in the documents table
			$del_doc_id = $_POST['data']['doc_id'];
			$del_current_holder = $_POST['data']['to_id'];

			$updateQry = "UPDATE documents SET current_holder='".$del_current_holder."', sender_id='".$_POST['data']['to_id']."', file_status='DELEGATED' WHERE id='".$del_doc_id."'";
			
			if($conn->query($updateQry) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Success");
				$response .= ' <div><em>Document update success.</em></div>';
				
				//update previous track record to the archives
				$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $delegTrackId;
				if($conn->query($qry) === TRUE){
					$response .= ' ';
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Success");
				} else {
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Failed:".$conn->error);
				}
				
			}
			else{
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Failed:".$conn->error);
				$response .= ' <div class="alert danger"><em>Document update failure.'.$conn->error.'</em></div>';
			}
			
		}
		
	}

	
	if($_POST['action']=="ADD STAGE"){
		
		if($_POST['data']['temp_type']=="form"){
			$temp = $_POST['template']['form'];
			$e = count($temp['label']);
			$elts=array();
			for($i=0;$i<$e;$i++){
				if($temp['label'][$i]!=""){ $elts[$i]=$temp['label'][$i].";".$temp['field'][$i].";".$temp['required'][$i].";".$temp['default'][$i]; }
			}
			$template=implode("$$",$elts);
			$stage_qry  = "UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'";
			if($conn->query($stage_qry)===TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_qry,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_qry,"Failed:".$conn->error);
			}
		}
		
		if($_POST['data']['temp_type']=="spreadsheet"){
			$temp = $_POST['template']['spreadsheet'];
			$ros = array();
			foreach($temp as $temp_ro){
				array_push($ros, implode(",",$temp_ro));
			}
			$template=implode("\n",$ros);
			
			$stage_tmp_qry  = "UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'";
			if($conn->query($stage_tmp_qry)===TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_tmp_qry,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_tmp_qry,"Failed:".$conn->error);
			}
		}
			
	}
	//0778742794
	if($_POST['action'] == "NEW GROUP"){
		
		$groups_qry  = "UPDATE `$tbl` SET `members`='".implode(",",$_POST['members'])."' WHERE id='$psn_last_id'";
		if($conn->query($groups_qry)===TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Failed:".$conn->error);
		}
	}
	
	if($_POST['action'] == "TROUBLESHOOT: RESTORE CLOSED DOCUMENT"){
		
		$restore_qry  = "UPDATE `documents` SET 
						`current_holder`='".$_POST['data']['to_id']."', 
						`date_closed`='0000-00-00', 
						`closed_by`='0', 
						`stage_label`='".$_POST['data']['to_stage']."', 
						`status_label`='".$_POST['data']['to_status']."' 
						WHERE id='".$_POST['data']['doc_id']."'";
						
		if($conn->query($restore_qry)===TRUE){
			
			$response_title = "DOCUMENT RESTORED! ";
			$response = " Document ".$_POST['doc']." restored to ".$_POST['data']['to_stage'];
			
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $restore_qry,"Success");
			
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $restore_qry,"Failed:".$conn->error);
		}
	}
	
	if(count(@$_FILES['att']['name'])>0) { //additional attachments
	
		$atts=$_FILES['att'];
		
		//start uploading the attachement
		if($_POST['att']['append_type']=='documents' || $_POST['att']['append_type']=='doc_track'){
			if(isset($_POST['data']['doc_id'])){ $att_doc_id = $_POST['data']['doc_id']; } else { $att_doc_id = $psn_last_id; }
			
			$dc_qr = $conn->query("SELECT created FROM documents WHERE id = '".$att_doc_id."'");
			$dc_qrr = $dc_qr->fetch_assoc();
			$file1 = explode("-",date("Y-m-d", strtotime($dc_qrr['created']))); 
			$folder_id = $att_doc_id;
			
		} else {
			
			$file1 = explode("-",date("Y-m-d")); 
			$folder_id = $psn_last_id;
			
		}
		
		$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $_POST['data']['document_type'];
		
		$uploaddir = $destination_dir."/".$folder_id;

		$dir1="doc_archive/".$file1[0];
		$dir2=$dir1."/".$file1[1];
		$dir3=$dir2."/".$_POST['data']['document_type'];
		$dir4=$dir3."/".$folder_id;

		if(!is_dir($uploaddir)) {  

			if(!is_dir($dir1)) {
	
				if(mkdir($dir1, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir1,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir1,"Failed"); }
				if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Failed"); }
				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir2)) {

				if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Failed"); }
				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir3)) {

				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir4)) {

				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			
		}
		
		//for attachments uploads
		$accepted_file_types = array('jpg','jpeg','gif','png','pdf' );
		
		foreach($atts['name']['file'] as $file_key=>$file_attr){
			
			if($file_attr!=""){
				
				$attch = $file_key;
				$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
				$orig_name = basename($file_attr,".".$file_type);
				$file_name = $file_attr;	
				
				if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
					//upload the attachment
					$ii = 2;
					$attfile = $uploaddir."/".str_replace("'","",$file_name);
					while(file_exists($attfile)){           
						$attfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
						$ii++;
					}
					
					if(move_uploaded_file($atts['tmp_name']['file'][$file_key], $attfile)) {
					  sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", "Uploading ".$atts['tmp_name']['file'][$file_key]." to ".$attfile,"Success");
					  $errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.'; 
					  //add to the insert statement
					  
					  $ins = "INSERT INTO uploads SET append_type='".$_POST['att']['append_type']."', append_id='".$psn_last_id."', description='".$conn->real_escape_string(trim($_POST['att']['description'][$file_key]))."', file_location='$attfile', created_by='".$_SESSION['user']."'";
					  if($conn->query($ins) === TRUE){ 
						sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", $ins,"Success");
						$errmsg .= " Info added to db. ";	  
						} else {
							sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", $ins,"Failed");
						}
					} else {
						sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", "Uploading ".$atts['tmp_name']['file'][$file_key]." to ".$attfile,"Failed:".$atts['error']['file'][$file_key]);
					}
					
				} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
			}	
			
		}
		
	}//end if has additional attachement
	
 } else { 
	$response_icon="times"; 
	$response_type="danger"; 
	$response_title ="ERROR!"; 
	$response = "The system was unable to save your form at this time. We apologise for inconvenience caused. <hr>ERR:".$conn->error."<hr>QRY:".$insert_data; 
}

?>

<div class="alert alert-block alert-<?php echo  $response_type; ?>">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	<p>
		<strong>
			<i class="ace-icon fa fa-<?php echo $response_icon; ?>"></i>
			<?php echo $response_title; ?>
		</strong>
		<?php echo $response; ?>
	</p>
</div>

										
						
								