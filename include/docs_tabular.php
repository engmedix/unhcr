<div>
		<div class="message-list-container">
			<!-- #section:pages/inbox.message-list -->
			<div class="message-list" id="message-list">
		<table id="dynamic-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Action date</th>
					<th>Sender</th>
					<th>Action taken</th>
					<th>Current Stage</th>
					<th>Document type / Ref #</th>
					<th>Remark</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$form_query = "SELECT doc_track.*,documents.current_holder,documents.document_type,documents.doc_ref_number,documents.undp_ref_number,documents.capture_method, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.from_id = admins.id WHERE to_id=$id AND documents.current_holder=$id GROUP BY doc_id ORDER BY date desc";

			  $form_result = $conn->query($form_query);
			  $now = new DateTime();
			  $now->setTime( 0, 0, 0 );

			  while($form_data = $form_result->fetch_assoc()){

				if($form_data['receiver_status'] == 'SENT') $readStatus = 'message-unread';
				else $readStatus = '';

				if($form_data['action'] == 'DOCUMENT SENT' || $form_data['action'] == 'REQUEST SENT') $label = ' label-info"';
				elseif($form_data['action'] == 'APPROVED') $label = 'label-success';
				elseif($form_data['action'] == 'REJECTED') $label = 'label-danger';
				elseif($form_data['action'] == 'FORWARDED') $label = 'label-pink';
				else $label = 'label-primary';

				$sentDate = new DateTime($form_data['actionDate']);
				$sentDate->setTime( 0, 0, 0 );

				$diff = $now->diff( $sentDate );

				$periodSpent = $diff->days;
				if($periodSpent == 0) $periodSpent = ' Today';
				elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
				elseif($periodSpent >= 2) $periodSpent .= ' days ago';

			  ?>
				<tr>
					<td><?php echo date("j M Y - h:i a",strtotime($form_data['actionDate'])).' <span class="label">'.$periodSpent.'</span>'; ?></td>
					<td><?php echo $form_data['fname'].' '.$form_data['lname']; ?></td>
					<td><span class="label <?php echo $label; ?>"><?php echo $form_data['action']; ?> TO ME</span></td>
					<td><?php echo getStaffDept($form_data['current_holder']); ?></td>
					<td>
						<a href="document.php?ac=<?php echo $form_data['doc_id']; ?>&md=<?php echo $form_data['capture_method']; ?>&tId=<?php echo $form_data['id']; ?>">
							<span style="font-weight:bold"><?php echo $form_data['document_type']; ?> - <?php echo $form_data['doc_ref_number']<>""?strtoupper($form_data['doc_ref_number']):strtoupper($form_data['internal_ref_number']); ?></span>
						</a>
					</td>
					<td><?php echo $form_data['remark']; ?></td>
				</tr>
			  <?php } ?>



			</tbody>
		</table>
		</div>
		</div>
	</div>
