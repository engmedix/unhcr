<?php
session_start();
// **PREVENTING SESSION HIJACKING**
// Prevents javascript XSS attacks aimed to steal the session ID
ini_set('session.cookie_httponly', 1);

// **PREVENTING SESSION FIXATION**
// Session ID cannot be passed through URLs
ini_set('session.use_only_cookies', 1);

// Uses a secure connection (HTTPS) if possible
ini_set('session.cookie_secure', 1);
require_once("connn.php");

//$dt = date("Y-m-d h:i:sa");

$dt = date("Y-m-d");


//$uname = $_POST['userid'];
$pwd = md5($_POST['password']);
//$pwd = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
$uname=filter_var($_POST['userid'], FILTER_SANITIZE_STRING);
$ip = $_SERVER['REMOTE_ADDR'];
$agent = $_SERVER['HTTP_USER_AGENT'];
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if($uname=="" OR $pwd==""){

	$err = base64_encode("Invalid username or password$$$".$uname);
	echo '<script>window.location.href = "../login.php?loginfail='.$err.'";</script>';


} else {

    $stmt = $conn->prepare("SELECT * FROM admins WHERE uname=? AND pwd=? ");
	$stmt->bind_param('ss',$uname,$pwd);
    $stmt->execute();
	$result = $stmt->get_result();

	if($result->num_rows>0){
		while($row = $result->fetch_assoc()) {
			if($row['uname']==$uname AND $row['pwd']===$pwd){
				$matched = TRUE;
				$urs =  $row;
				break;
			}
		}
		$result->free();
	}

	if(isset($matched) AND $matched===TRUE){

		$urs_id =  $urs["id"];
		$urs_grp =  $urs["user_group"];
		$ac_status = $urs["status"];
		$status='LOGIN FAILED: Account suspended';
		$action='LOGIN';
		$user='".$urs["fname"]." ".$urs["lname"]';

		if($ac_status=="SUSPENDED"){
		//echo "Your account has not been activated. Check your email for an activation link.";
		$err = base64_encode("Your account has been Suspended. Contact the administrator.");
		$stmt=$conn->prepare("INSERT INTO sessions SET user=?, ip=?, agent=?, action=?, status=?, url=?");
		$stmt->bind_param('ssssss',$user,$ip,$agent,$action,$status,$url);
		$stmt->execute();
		$stmt->close();
	//	$conn->query("INSERT INTO sessions SET user='".$urs["fname"]." ".$urs["lname"]."', ip='$ip', agent='$agent', action='LOGIN', status='LOGIN FAILED: Account suspended', url='$url'");
		echo '<script>window.location.href = "../login.php?loginfail&rsp='.$err.'";</script>';

		} else {

			$_SESSION['user'] = $urs_id;
			$_SESSION['grp'] = $urs_grp;
			$_SESSION['client_id'] = 1;
			$login_fails=0;
			$date=date('Y-m-d H:i:s');
			$stmt=$conn->prepare("UPDATE admins SET last_login=?, login_fails=? WHERE id=?");
			$stmt->bind_param('sii',$date,$login_fails,$urs_id);
	        $stmt->execute();

			//$conn->query("UPDATE admins SET last_login='".date('Y-m-d H:i:s')."', login_fails=0 WHERE id='$urs_id'");
			$action="LOGIN";
			$status="LOGIN SUCCESSFUL";
			$user='".$urs["fname"]." ".$urs["lname"]."';
			$_SESSION['user_name']=$user;
			//$log_session = "INSERT INTO sessions SET user='".$urs["fname"]." ".$urs["lname"]."', ip='$ip', agent='$agent', action='LOGIN', status='LOGIN SUCCESSFUL', url='$url'";
			$log_session = "INSERT INTO sessions SET user=?, ip=?, agent=?, action=?, status=?, url=?";
			$stmt=$conn->prepare($log_session);
			$stmt->bind_param('ssssss',$user,$ip,$agent,$action,$status,$url);
	        $stmt->execute();

			echo '<script>window.location.href = "../index.php";</script>';

		}//close is "not" pending activation

	} else {
	$err = base64_encode("Invalid username or password$$$".$uname);

	$stmt=$conn->prepare("UPDATE admins SET login_fails=login_fails+1 WHERE uname=?");
	$stmt->bind_param('s',$uname);
	$stmt->execute();
	$status='LOGIN FAILED:Invalid username or password';
	$action='LOGIN';
	$stmt=$conn->prepare("INSERT INTO sessions SET user=?, ip=?, agent=?, action=?, status=?, url=?");
	$stmt->bind_param('ssssss',$uname,$ip,$agent,$action,$status,$url);
	$stmt->execute();
	$stmt->close();
	//$conn->query("INSERT INTO sessions SET user='UNAME: $uname', ip='$ip', agent='$agent', action='LOGIN', status='LOGIN FAILED:Invalid username or password', url='$url'");
	echo '<script>window.location.href = "../login.php?loginfail='.$err.'";</script>';

	}
}
//*/
?>
