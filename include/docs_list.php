<div>
		<div class="message-list-container">
			<!-- #section:pages/inbox.message-list -->
			<div class="message-list" id="message-list">
		<table id="dynamic-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>My Documents</th>
				</tr>
			</thead>

			<tbody>
			
			<?php $form_query = "SELECT doc_track.*,documents.document_type,documents.doc_ref_number,documents.capture_method, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.from_id = admins.id WHERE to_id = $id AND receiver_status <> 'ARCHIVED'"; 
			  $form_result = $conn->query($form_query);
			  
			  $now = new DateTime();
			  $now->setTime( 0, 0, 0 );
			  
			  
			  while($form_data = $form_result->fetch_assoc()) { 
					if($form_data['receiver_status'] == 'SENT') $readStatus = 'message-unread';
					else $readStatus = '';
					
					if($form_data['action'] == 'DOCUMENT SENT' || $form_data['action'] == 'REQUEST SENT') $label = ' label-info"';
					elseif($form_data['action'] == 'APPROVED') $label = 'label-success';
					elseif($form_data['action'] == 'REJECTED') $label = 'label-danger';
					elseif($form_data['action'] == 'FORWARDED') $label = 'label-pink';
					
					$sentDate = new DateTime($form_data['actionDate']);
					$sentDate->setTime( 0, 0, 0 );

					$diff = $now->diff( $sentDate );
					
					$periodSpent = $diff->days;
					if($periodSpent == 0) $periodSpent = ' Today';
					elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
					elseif($periodSpent >= 2) $periodSpent .= ' days ago';
					
					


			  ?>
				<tr>
					
					
					
					<td>
						<a href="document.php?ac=<?php echo $form_data['doc_id']; ?>&md=<?php echo $form_data['capture_method']; ?>&tId=<?php echo $form_data['id']; ?>">
						<div class="message-item <?php echo $readStatus; ?>">
							
							
							<span class="sender" title="<?php echo $form_data['fname'].' '.$form_data['lname']; ?>">FROM: <?php echo $form_data['fname'].' '.$form_data['lname']; ?></span>
							<span class="time"><?php echo '<span class="label">'.$periodSpent.'</span>'. date("j M Y - h:i a",strtotime($form_data['actionDate'])); ?></span>

							<span class="summary">
								<span class="text">
									<span><?php echo $form_data['document_type']; ?> - <?php echo $form_data['doc_ref_number']; ?></span>
									<span class="label <?php echo $label; ?>"><?php echo $form_data['action']; ?> TO ME</span>
									<div class="clearfix">
										<em><?php echo $form_data['remark']; ?></em>
									</div>
									
								</span>
							</span>
						</div>
						</a>
					</td>
					
					
					
					
				</tr>
			  <?php } ?>
			

				
			</tbody>
		</table>
		</div>
		</div>
	</div>