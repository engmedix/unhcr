<div>
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Archives</th>
			</tr>
		</thead>

		<tbody>
		
		<?php $form_query = "SELECT documents.*, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON documents.from_id = admins.id WHERE to_id = $id OR doc_track.from_id = $id AND documents.id IS NOT NULL GROUP BY doc_id ORDER BY date desc"; 
		  $form_result = $conn->query($form_query);
		  //echo $form_result->mysqli_error();
		  
		  $now = new DateTime();
		  $now->setTime(0,0,0);
		  while($form_data = $form_result->fetch_assoc()) {   
			
				$readStatus = '';
				
				if($form_data['file_status'] == 'DOCUMENT SENT' || $form_data['file_status'] == 'REQUEST SENT') $label = ' label-info"';
				elseif($form_data['file_status'] == 'APPROVED') $label = 'label-success';
				elseif($form_data['file_status'] == 'REJECTED') $label = 'label-danger';
				elseif($form_data['file_status'] == 'FORWARDED') $label = 'label-pink';
				
				$sentDate = new DateTime($form_data['date']);
				$sentDate->setTime( 0, 0, 0 );

				$diff = $now->diff( $sentDate );
				
				$periodSpent = $diff->days;
				if($periodSpent == 0) $periodSpent = ' Today';
				elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
				elseif($periodSpent >= 2) $periodSpent .= ' days ago';
		  
		  ?>
			<tr>
				
				
				
				<td>
					<a href="timeline.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>">
					<div class="message-item <?php echo $readStatus; ?>">
						
						
						<span class="sender" title="<?php echo $form_data['fname'].' '.$form_data['lname']; ?>">INITIATED BY: <?php echo $form_data['fname'].' '.$form_data['lname']; ?></span>
						<span class="time"><?php echo '<span class="label">'.$periodSpent.'</span>'. date("j M Y - h:i a",strtotime($form_data['date'])); ?></span>

						<span class="summary">
							<span class="text">
								<span><?php echo $form_data['document_type']; ?> - <?php echo $form_data['doc_ref_number']; ?></span>
								<span class="label <?php echo $label; ?>">CURRENTLY <?php echo $form_data['file_status']; ?> TO <?php echo getStaffName($form_data['current_holder']); ?></span>
								<div class="clearfix">
									<em><?php echo $form_data['description']; ?></em>
								</div>
								
							</span>
						</span>
					</div>
					</a>
				</td>
				
				
				
				
			</tr>
		  <?php } ?>
		

			
		</tbody>
	</table>
</div>