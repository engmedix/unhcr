<?php
require 'mailjet/vendor/autoload.php';
use \Mailjet\Resources;
function databaseInsert($tbl, $dataArray ){
	global $refNumber;
	if($tbl == 'documents'){
		$refNumber = getDocRefNo();
	}

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	/*$qryx = "";
	$qry = "INSERT INTO ".$tbl." SET ";

	foreach (array_keys($dataArray) as $field)
	{
		if($field == 'internal_ref_number') {$qryx .= "`".$field."`"."='".$refNumber."', ";}
		else{ if(trim($dataArray[$field])!=""){ $qryx .= "`".$field."`"."='".$conn->real_escape_string(trim($dataArray[$field]))."', "; } }
	}
	$qry .= rtrim($qryx, ", ");
	$conn->close();
	return  $qry;*/
	foreach ($dataArray as $field=>$value) {
		$fields[] = '`' . $field . '`';

		if ($field == 'internal_ref_number') {
			$values[] = "'" . mysqli_real_escape_string($conn,$refNumber) . "'";
		} else {
			$values[] = "'" . mysqli_real_escape_string($conn,$value) . "'";
		}
	}
	$field_list = join(',', $fields);
	$value_list = join(', ', $values);

	$qry = "INSERT INTO `".$tbl."` (" . $field_list . ") VALUES (" . $value_list . ")";

	$conn->close();
	return $qry;

}

function getDocRefNo(){
	global $conn;

	$qry = 'SELECT COUNT(*) maxId FROM documents WHERE internal_ref_number LIKE "'.date('Ymd').'%"';
	$rst = $conn->query($qry)->fetch_assoc();
	$max = $rst['maxId']+1;

	if($max < 10){
		return date('Ymd').'000'.$max;
	}
	elseif($max < 100){
		return date('Ymd').'00'.$max;
	}
	elseif($max < 1000){
		return date('Ymd').'0'.$max;
	}
	else{
		return date('Ymd').''.$max;
	}
}
function getSubject($ac_id)
{
    include("include/cnx.php");
    $files_result = $conn->query("SELECT * FROM documents WHERE id='$ac_id'");
    while ($files_data = $files_result->fetch_assoc()) {
        $resultt =  $files_data['subject'];
    }
    return $resultt;
}
function databaseInsertWithDates($tbl, $dataArray, $dateArray ){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	foreach ($dateArray as $dt)
	{
		$dataArray[$dt] = date("Y-m-d", strtotime($dataArray[$dt]));
	}
	$qryx = "";
	$qry = "INSERT INTO ".$tbl." SET ";

	foreach (array_keys($dataArray) as $field)
	{
		if(trim($dataArray[$field])!=""){  $qryx .= "`".$field."`"."='".$conn->real_escape_string(trim($dataArray[$field]))."', "; }
	}

	$qry .= rtrim($qryx, ", ");
	$conn->close();
	return  $qry;

}

function commaless($string){
	//return  runQuery($qry);
	return  str_replace(",","",$string);

}


function stripComas($dataArray){

	foreach (array_keys($dataArray) as $field)
	{
		 $dataArray[$field] = str_replace(",","",$dataArray[$field]);
	}
	//return  runQuery($qry);
	return  $dataArray;

}

function databaseInsertExtras($tbl, $dataArray, $extrasArray ){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	foreach ($extrasArray as $key=>$val	)
	{
		$dataArray[$key] = $val;
	}

	$qryx = "";
	$qry = "INSERT INTO ".$tbl." SET ";

	foreach (array_keys($dataArray) as $field)
	{
		if(trim($dataArray[$field])!=""){  $qryx .= "`".$field."`"."='".$conn->real_escape_string(trim($dataArray[$field]))."', "; }
	}

	$qry .= rtrim($qryx, ", ");
	$conn->close();
	return  $qry;

}

function databaseUpdate($tbl, $dataArray, $item, $itemId ){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$qryx = "";
	$qry = "UPDATE ".$tbl." SET ";

	foreach (array_keys($dataArray) as $field)
	{
		if(trim($dataArray[$field])!=""){  $qryx .= "`".$field."`"."='".$conn->real_escape_string(trim($dataArray[$field]))."', "; }
	}

	$qry .= rtrim($qryx, ", ");

	$qry .= " WHERE "."`".$item."`"."='".$itemId."'";
	$conn->close();
	return $qry;

}

function databaseUpdateWithExtras($tbl, $dataArray, $item, $itemId, $extrasArray ){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	foreach ($extrasArray as $key=>$val	)
	{
		$dataArray[$key] = $val;
	}

	$qryx = "";
	$qry = "UPDATE ".$tbl." SET ";

	foreach (array_keys($dataArray) as $field)
	{
		if(trim($dataArray[$field])!=""){  $qryx .= "`".$field."`"."='".$conn->real_escape_string(trim($dataArray[$field]))."', "; }
	}

	$qry .= rtrim($qryx, ", ");

	$qry .= " WHERE "."`".$item."`"."='".$itemId."'";
	$conn->close();

	return $qry;

}


function runQuery($query){
	include("include/cnx.php");
	if($conn->query($query) === TRUE) { return "completed successfully ."; } else { return "Error: " . $conn->error; }
}


function getBranch($id){
	include("include/cnx.php");
	$files_result = $conn->query("SELECT * FROM dutystations WHERE id='$id'");
	if($files_result->num_rows>0){
    $files_data = $files_result->fetch_assoc();
	return $files_data['duty_station']; } else {
		return "";
	}
}

function getStaffStation($id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }


	$files_result = $conn->query("SELECT b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE a.id='$id'");
	if($files_result->num_rows>0){
    $files_data = $files_result->fetch_assoc();
	return $files_data['duty_station']; } else {
		return "";
	}
}

function getStaffName($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT a.fname, a.lname, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['fname'].' '.$files_data['lname'].', '.$files_data['job_title']; }
	 return $resultt;
}
function getStaffUnit($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT * FROM admins WHERE id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['department_id']; }
	 return getDept($resultt);
}

function getStaffEmail($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT * FROM admins WHERE id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['email']; }
	 return $resultt;
}

function getStaffDept($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT b.department FROM admins a LEFT JOIN units b ON a.department_id=b.id WHERE a.id='$ac_id'");
    $files_data = $files_result->fetch_assoc();
	$resultt =  $files_data['department'];
	return $resultt;
}

function getStaffTitle($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id='$ac_id'");
    $files_data = $files_result->fetch_assoc();
	$resultt =  $files_data['job_title'];
	return $resultt;
}

function getStaffBranch($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE a.id='$ac_id'");
    $files_data = $files_result->fetch_assoc();
	$resultt =  $files_data['duty_station'];
	return $resultt;
}

function getStaff($id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$result1 = $conn->query("SELECT a.*, CONCAT(fname,' ',lname,' ',oname) AS fullname, b.department, c.duty_station FROM admins a LEFT JOIN units b ON a.department_id=b.id LEFT JOIN dutystations c ON a.station_id=c.id LEFT JOIN cinnamon_clients d ON a.client_id=d.id WHERE a.id='$id'");
	return $result1->fetch_assoc();
}

function getDocType($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT * FROM documents WHERE id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['document_type']; }
	 return $resultt;
}

function getRefNo($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT * FROM documents WHERE id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['internal_ref_number']; }
	 return $resultt;
}

function getDept($ac_id){
	if($ac_id==0 OR $ac_id==""){
		return "";
	} else {
		if(file_exists("connn.php")){ include("connn.php"); }
		elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
		elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
		elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }
		$files_result = $conn->query("SELECT * FROM units WHERE id='$ac_id'");
		$files_data = $files_result->fetch_assoc();  $resultt =  $files_data['department'];
		return $resultt;
	}
}

function getTitle($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }
	$files_result = $conn->query("SELECT * FROM job_titles WHERE id='$ac_id'");
     while($files_data = $files_result->fetch_assoc()) { $resultt =  $files_data['job_title']; }
	 return $resultt;
}

function getUnitHead($ac_id){
	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$files_result = $conn->query("SELECT * FROM units WHERE id='$ac_id'");
    $files_data = $files_result->fetch_assoc();
	if(isset($files_data['unit_head'])){
		return  $files_data['unit_head'];
	}
}

function getDropDown($field,$value){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$opions = "";
	$labels = array("id"=>"user","job_title_id"=>"title","station_id"=>"branch","department_id"=>"dept");
	if($field=="id"){
		$item_list = $conn->query("SELECT id, CONCAT(fname,' ',lname) nme FROM admins ORDER BY fname");
	}
	if($field=="job_title_id"){
		$item_list = $conn->query("SELECT id, job_title nme FROM job_titles ORDER BY job_title");
	}
	if($field=="station_id"){
		$item_list = $conn->query("SELECT id, duty_station nme FROM dutystations ORDER BY duty_station");
	}
	if($field=="department_id"){
		$item_list = $conn->query("SELECT id, department nme FROM units ORDER BY department");
	}

	$options .= '<option value="" > - select '.$labels[$field].' - .</option>';

	while($option=$item_list->fetch_assoc()){
		$options .= '<option value="'.$option['id'].'"';
		$option['id']==$value?$options .=' selected ':'';
		$options .=	'>'.$option['nme'].'</option>';
	}

	return $options;

}

function isSender($user,$doc_type){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$doctype = $conn->query("SELECT senders FROM document_types WHERE id='$doc_type'");
	$doc=$doctype->fetch_assoc();

	if($doc['senders']=="*"){
		$admins = $conn->query("SELECT id FROM admins ");
	} else {
		$admins = $conn->query("SELECT id FROM admins WHERE ".$doc['senders']." ");
	}
	$senders = array();
	while($sender = $admins->fetch_assoc()){
		array_push($senders,$sender['id']);
	}
	if(in_array($user,$senders)){
		return true;
	} else {
		return false;
	}

}

function isApprover($user){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$approvers = array();

	/////department heads
	$units = $conn->query("SELECT DISTINCT unit_head FROM`units`");
	while($unt=$units->fetch_assoc()){
		array_push($approvers,$unt['unit_head']);
	}

	/////reports to
	$posn = $conn->query("SELECT id FROM admins WHERE job_title_id IN(SELECT reports_to FROM `job_titles`)");
	while($pos=$posn->fetch_assoc()){
		array_push($approvers,$pos['id']);
	}

	if(in_array($user,$approvers)){
		return true;
	} else {
		return false;
	}
}

function inGroup($user,$group){

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$groups = $conn->query("SELECT `members` FROM `groups` WHERE id='$group'");
	$grp=$groups->fetch_assoc();
	$members = explode(",",$grp['members']);

	if(in_array($user,$members)){
		return true;
	} else {
		return false;
	}
}

function array_filter_by_value($my_array, $index, $value){
	if(is_array($my_array) && count($my_array)>0)
	{
		$new_array = array();
		foreach(array_keys($my_array) as $key){
			$temp[$key] = $my_array[$key][$index];

			if ($temp[$key] == $value){
				array_push($new_array, $my_array[$key]);
			}
		}
	  }
  return $new_array;
}

function duration($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $dur = $dtF->diff($dtT)->format('.%a days .%h hrs .%i mins %s sec');
	$clean = array('.0 days','.0 hrs','.0 mins ','.');
    return str_replace($clean,"",$dur);
}

function endsWith($haystack, $needle){
    $length = strlen($needle);

    return $length === 0 ||
    (substr($haystack, -$length) === $needle);
}

function dbmail($to,$subject,$msg,$link){

	$email_name = 'CINNAMON WORKFLOW';
	$from = 'workflow@cinnamonerp.com';

	//////////////////////////////////
	if($link==""){
		$lnk = '';
	} else {
		$lnk = '<tr>
				<td align="left" valign="top" style="margin:0;padding:0;">
					<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" valign="top" style="margin:0;padding:0;">

								<table border="0" cellpadding="0" cellspacing="0" align="center" data-editable="button">
									<tr>
										<td valign="top" width="140" bgcolor="#6fb3e0" align="center" style="padding-top:9px;padding-bottom:10px;padding-left:0;padding-right:0;margin:0;">
											<a href="'.$link.'" style="font-family:Verdana,Arial,Helvetica;color:#ffffff;font-size:12px;text-decoration:none">
												VIEW DETAILS &raquo;
											</a>
										</td>
									</tr>
								</table>

							</td>
						</tr>
					</table>
				</td>
			</tr>';
	}

	//////////////////////////////////
	ob_start();
	include('email_template.php');
	$ob = ob_get_clean();
	$replace = array('{MESSAGE}','{DETAILS_LINK}');
	$with = array($msg,$lnk);
	$message = str_replace($replace, $with, $ob);

	//////////////////////////////////
	$headers = "From: ".$email_name." <".strip_tags($from).">\r\n";
	$headers .= "Reply-To: ". strip_tags($from) . "\r\n";

	$bccx = "medardnduhukire@gmail.com";
	$bccs = explode(";", $bccx);
	foreach($bccs as $bcc){
		if(trim($bcc)!=""){
			$headers .= "BCC: ".trim($bcc)."\r\n";
		}
	}

	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion()."\r\n";

	/////////////////////////////////////////////
	#mail($to, $subject, $message, $headers);
	# begin alternative smtp
	/*$mj = new \Mailjet\Client($x1,$x2);
$body = [
    'FromEmail' => "info@cinnamonerp.com",
    'FromName' => $email_name,
    'Subject' => $subject,
    'Text-part' => $message,
    'Html-part' => $message,
    'Recipients' => [
        [
            'Email' => $to
        ]
    ]
];
$response = $mj->post(Resources::$Email, ['body' => $body]);
$response->success() && var_dump($response->getData());*/


require 'prm.php';
$mj = new \Mailjet\Client($x1,$x2);
$body = [
    'FromEmail' => "info@cinnamonerp.com",
    'FromName' => $email_name,
    'Subject' => $subject,
    'Text-part' => $message,
    'Html-part' => $message,
    'Recipients' => [
        [
            'Email' => $to
        ]
    ]
];
$response = $mj->post(Resources::$Email, ['body' => $body]);
#$response->success() && var_dump($response->getData());
	# end alternative smtp
	/////////////////////////////////////////////
	$postdata = http_build_query(
		array(
			'subject' => $subject,
			'from' => $from,
			'msg' => htmlentities($message),
			'to' => $to,
			'email_name' => $email_name
		)
	);
	$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata
		)
	);
	$context  = stream_context_create($opts);
	#$result = file_get_contents('http://hostalite.com/scripts/send_mail.php', false, $context);

}

function sendemail($to,$from,$subject,$msg,$email_name,$link,$notification,$doc_ref){

	ob_start();
	include('email_template.php');
	$ob = ob_get_clean();

	$replace = array('{NOTIFICATION}','{DOC_REF}','{MESSAGE}','{DETAILS_LINK}');
	$with = array($notification,$doc_ref,$msg,$link);

	$message = str_replace($replace, $with, $ob);

	//////////////////////////////////
	$bccx = "";
	$bccs = explode(";", $bccx);
	$headers = "From: ".$email_name." <".strip_tags($from).">\r\n";
	$headers .= "Reply-To: ". strip_tags($from) . "\r\n";

	foreach($bccs as $bcc){
		if(trim($bcc)!=""){
			$headers .= "BCC: ".trim($bcc)."\r\n";
		}
	}

	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion()."\r\n";
	# begin alternative smtp
	$mj = new \Mailjet\Client($x1,$x2);
$body = [
    'FromEmail' => "info@cinnamonerp.com",
    'FromName' => "$email_name",
    'Subject' => "$subject",
    'Text-part' => $message,
    'Html-part' => $message,
    'Recipients' => [
        [
            'Email' => $to
        ]
    ]
];
$response = $mj->post(Resources::$Email, ['body' => $body]);
	//error_log("TO:".$to."\r\HEADERS: ".$headers."\r\nSUBJECT: ".$subject."\r\MSG: ".$message, 0);

	//mail($to, $subject, $message, $headers);
	//////////////////////////////////
	//include('phpmailer/PHPMailer.php');

	/*use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//Load composer's autoloader
	require 'vendor/autoload.php';

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
		//Server settings
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'user@example.com';                 // SMTP username
		$mail->Password = 'secret';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('from@example.com', 'Mailer');
		$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
		$mail->addAddress('ellen@example.com');               // Name is optional
		$mail->addReplyTo('info@example.com', 'Information');
		$mail->addCC('cc@example.com');
		$mail->addBCC('bcc@example.com');

		//Attachments
		$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Here is the subject';
		$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		$mail->send();
		echo 'Message has been sent';
	} catch (Exception $e) {
		echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	} */


}

function sys_log($user,$action,$query,$status){

	$ip = $_SERVER['REMOTE_ADDR'];
	$agent = $_SERVER['HTTP_USER_AGENT'];
	$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	if(file_exists("connn.php")){ include("connn.php"); }
	elseif(file_exists("include/connn.php")){ include("include/connn.php"); }
	elseif(file_exists("../include/connn.php")){ include("../include/connn.php"); }
	elseif(file_exists("../../include/connn.php")){ include("../../include/connn.php"); }

	$log_action = "INSERT INTO sys_log SET ip='$ip', agent='$agent', url='".$conn->real_escape_string(trim($url))."', user='$user', action='$action', query='".$conn->real_escape_string(trim($query))."', status='$status'";
	if($conn->query($log_action) === TRUE){  } else { error_log($conn->error, 0); }

}

function getOS($user_agent) {

    $os_platform    =   "Unknown OS Platform";

    $os_array       =   array(
                            '/windows nt 10.0/i'    =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

    foreach ($os_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }

    }

    return $os_platform;

}


function getBrowser($user_agent) {

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }

    }

    return $browser;

}
function timeActual($start_date,$end_date){
$start = new DateTime($start_date);
$end = new DateTime($end_date);

//$unread=strtotime('2019-07-20 05:40:00')-strtotime('2019-07-17 06:30:00');
// otherwise the  end date is excluded (bug?)
$end->modify('+1 day');

$interval = $end->diff($start);

// total days
$days = $interval->days;
$days_inMin = ($interval->d*24*60) + ($interval->h*60) + $interval->i;

// create an iterateable period of date (P1D equates to 1 day)
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
$holidays = array();

foreach($period as $dt) {
    $curr = $dt->format('D');

    // for the updated question
    if (in_array($dt->format('Y-m-d'), $holidays)) {
       $days--;
       $days_inMin -= (24*60);
    }

    // substract if Saturday or Sunday
    if ($curr == 'Sat' || $curr == 'Sun') {
        $days--;
        $days_inMin -= (24*60);
    }
}
return $days_inMin*60;
}
function truncate($string, $length, $dots = "...") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}
function biss_hours($start, $end){

    $startDate = new DateTime($start);
    $endDate = new DateTime($end);
    $periodInterval = new DateInterval( "PT1H" );

    $period = new DatePeriod( $startDate, $periodInterval, $endDate );
    $count = 0;

    foreach($period as $date){

    $startofday = clone $date;
    $startofday->setTime(8,00);

    $endofday = clone $date;
    $endofday->setTime(17,00);

        if($date > $startofday && $date <= $endofday && !in_array($date->format('l'), array('Sunday','Saturday'))){

            $count++;
        }

    }

	//Get seconds of Start time
	$start_d = date("Y-m-d H:00:00", strtotime($start));
	$start_d_seconds = strtotime($start_d);
	$start_t_seconds = strtotime($start);
	$start_seconds = $start_t_seconds - $start_d_seconds;

	//Get seconds of End time
	$end_d = date("Y-m-d H:00:00", strtotime($end));
	$end_d_seconds = strtotime($end_d);
	$end_t_seconds = strtotime($end);
	$end_seconds = $end_t_seconds - $end_d_seconds;

	$diff = $end_seconds-$start_seconds;

	if($diff!=0):
		$count--;
	endif;

	$total_min_sec = date('i:s',$diff);

	$str_time = $count .":".$total_min_sec;
	$str_to_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

     sscanf($str_to_time, "%d:%d:%d", $hours, $minutes, $seconds);

    return $hours * 3600 + $minutes * 60 + $seconds;
	//return $str_time;

}
function surveyValue($id){
	switch($id){
    case 0:
        return "Not applicable";
        break;
    case 1:
        return "No problem";
        break;
    case 2:
        return "Minor problem";
        break;
	case 3:
        return "Moderate problem";
        break;
    case 4:
        return "Big problem";
        break;
    case 5:
        return "Immense problem";
        break;
    default:
        return "Not answered";
	}

}

function getAge($birthday) {
    $age = date_create($birthday)->diff(date_create('today'))->y;

    return $age;
}

	function toDate($timeStamp = 0){
		date_default_timezone_set('Africa/Kampala');
		if($timeStamp == 0)
			$timeStamp = time();
		if(is_numeric($timeStamp))
		{
			return date('j M Y - g:i A',$timeStamp);
		}
		else
			return date('j M Y - g:i A',strtotime($timeStamp));
	}


	//$this->multipleSave('job_nos','job_nos_count','job_nos');
	function multipleSave($input_array, $row_count_array, $table){
		include("include/cnx.php");
		global $tbl;
		global $transactionId;
		global $response;



		$important_fields = array(
			'subprocesses'=>'processName'
		);
		$foreign_keys = array(
			'subprocesses'=>'serviceId'
		);

		$types = $_POST[$row_count_array];
		$numOfItems = sizeof($types);
		$fields_set = 0;
		$fields = '';


		$fields = '(';
		$values = '';
		for($index = 0; $index < $numOfItems;$index++) {
			if(@$_POST[$input_array][$important_fields[$input_array]][$index] == '') continue;
			//if($_POST['college']['college'][$index]!='' && $_POST['college']['location'][$index]!=''){
				$values .= '(';
				foreach($_POST[$input_array] as $field => $value)
				{
					if($fields_set == 0)
					{
						$fields .= '`'.$field.'`,';
					}

					if($field == $foreign_keys[$input_array]){
						$values .= $transactionId.',';
					}
					else
					{
						if(in_array($field,$arrayTimeStampFields)){
							$values .= '"'.date('Y-m-d H:i:s',strtotime($value[$index])).'",';
						}
						elseif(in_array($field,$arrayImageField)){
							$sr = ROOT;
							$old = $sr.'/public/img/temp/'.$value;
							$new = $sr.'/public/img/'.$arrayImageFolders[$field].'/'.$value;
							if(rename($old,$new)) {$er = '1';} else $er = '2';
							$values .= '"'.$value[$index].'"';
							echo $er;
						}
						else
							$values .= '"'.$value[$index].'",';
					}
				}

				if($input_array == 'corresponding' && $_POST[$input_array]['authorEmail'][$index] !=''){
					$email_array[$_POST[$input_array]['authorEmail'][$index]] = $_POST[$input_array]['authorName'][$index];
				}

				$fields_set = 1; // after the first round, tell the fields variable not to receive any more field variables
				$values = trim($values,',');
				$values .= '),';
			}
		//}

		if($fields_set != 0){
			$values = trim($values,',');

			$fields = trim($fields,',');
			$fields .= ')';
			//$fields .= ',`values`';
			$fields = str_replace("'","",$fields);

			$myQuery = 'INSERT INTO `'.$table.'` '.$fields.' VALUES '.$values;
			$rst = $conn->query($myQuery);

			$error = $conn->error;

			if($error == '')
			{
				$response .= '<div class="info">Additional fields saved: '.$input_array.'</div>';
			}
			else
			{
				$response .= '<div class="warn">Additional field failure: '.$input_array.'<br/>'.$error.'<br />'.$myQuery.'</div>';
			}
		}
		else
		{
			$response .= '<div class="warn">emptyRows on: '.$input_array.'</div>';
		}
	}


?>
