
<?php
//ini_set('display_errors',1);
date_default_timezone_set("Africa/Kampala");
define('ROOT', str_ireplace('\include','',(dirname(__FILE__))));
$tbl = $_POST['table'];
$refNumber = '';
$email_name = 'DOCTRACKER WORKFLOW';
$sys_log_user = $my['fname']." ".$my['lname'];
$current_holder = '';
 foreach($_POST['data']['current_holder'] as $row)
 {
  $current_holder .= $row . ', ';
 }
$current_holder = substr($current_holder, 0, -2);

if($_POST['table']=='documents'){
$_POST['data']['current_holder']=$current_holder;
}
if($_POST['table']=='admins'){
$_POST['data']['pwd']=md5($_POST['data']['pwd']);
}
if($_POST['action']=="NEW DELEGATE"){
		    $str=$_POST['data']['duration'];
            $str=explode("-",$str);
            $_POST['data']['from_date']=date('Y-m-d',strtotime($str[0]));
            $_POST['data']['to_date']=date('Y-m-d',strtotime($str[1]));
		}
if(isset($_POST['ddt'])){
	foreach (array_keys($_POST['ddt']) as $field)
	{
		if($_POST['ddt'][$field]['yyyy']!="YYYY" AND $_POST['ddt'][$field]['mm']!="MM" AND $_POST['ddt'][$field]['dd']!="DD"){
			$_POST['data'][$field] = $_POST['ddt'][$field]['yyyy']."-".$_POST['ddt'][$field]['mm']."-".$_POST['ddt'][$field]['dd'];
		}
	}
}

if($_POST['action']=="NEW DOC"){
	$_POST['data']['date']=date('Y-m-d H:i:s');
	$_POST['data']['created']=date('Y-m-d H:i:s');
	$_POST['data']['created_by']=$_POST['data']['sender_id'];
}
$insert_data = databaseInsert($tbl, $_POST['data']);

if($conn->query($insert_data) === TRUE){ //if insert query successful
	//get last ID
	$psn_last_id = $conn->insert_id;

	sys_log($sys_log_user,$_POST['action'],$insert_data,"Success");

	//set success message
	$response = 'RECORD SAVED SUCCESSFULLY.'.(($tbl == 'documents')?' <br> Reference Number: <strong>'.$refNumber.'</strong>':'');
	$response_icon="check"; $response_type="success"; $response_title ="SAVED!";

	if($_POST['action']=="NEW DOC TYPE"){

		$actns = '[{ \"type\":\"submit\", \"label\":\"Submit\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"select\", \"send_filters\":\"*\", \"stage_label\":\"Processing\", \"status_label\":\"Submitted\" }]';
		$wflws = '[{ \"type\":\"update\", \"label\":\"Update Status\", \"color\":\"white\", \"icon\":\"\", \"send_type\":\"stay\", \"stage_label\":\"Processing\", \"status_label\":\"Not Started,In Progress,On Hold, Completed\" },\r\n{ \"type\":\"forward\", \"label\":\"Review & Forward\", \"color\":\"info\", \"icon\":\"\", \"send_type\":\"select\", \"send_filters\":\"*\", \"stage_label\":\"Processing\", \"status_label\":\"Forwarded\" },\r\n{ \"type\":\"reject\", \"label\":\"Reject & Close\", \"color\":\"danger\", \"icon\":\"\", \"send_type\":\"end\", \"stage_label\":\"Closed\", \"status_label\":\"Rejected\" },\r\n{ \"type\":\"approve\", \"label\":\"Approve & Close\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"end\", \"stage_label\":\"Closed\", \"status_label\":\"Approved\" }]';

		$update_wf = "UPDATE $tbl SET actions='$actns', workflow='$wflws' WHERE id='$psn_last_id'";

		if($conn->query($update_wf) === TRUE){
			sys_log($sys_log_user,$_POST['action'].":SUB PROCESS",$update_wf,"Success");
		} else {
			sys_log($sys_log_user,$_POST['action'].":SUB PROCESS",$update_wf,"Failed: ".$conn->error);
		}

		unset($actns); unset($wflws);  unset($update_wf);

	}

	if($_POST['action']=="NEW DOC"){

	//create account directory
	$file1 = explode("-",date("Y-m-d"));

	$doc_type = $_POST['data']['document_type'];
	$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $doc_type;

	$dir1="doc_archive/".$file1[0];
	$dir2=$dir1."/".$file1[1];
	$dir3=$dir2."/".$doc_type;

	if(!is_dir($destination_dir)) {

		if(!is_dir($dir1)) {
			if(mkdir($dir1, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir1,"Success"); }
			if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir2,"Success"); }
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
		if(!is_dir($dir2)) {
			if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir2,"Success"); }
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
		if(!is_dir($dir3)) {
			if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS","mkdir ".$dir3,"Success"); }
		}
	}

	//handle template data
	if($_POST['data']['temp_type']=="form"){

		$_frm = array();

		foreach($_POST['_frm'] as $key=>$val){
			if(is_array($val)){ $_frm[$key]=implode(",",$val); }
			else { $_frm[$key]=$val; }
		}

		$frm_data = json_encode($_frm);

		$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";
		if ($conn->query($update_frm) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_frm,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_frm,"Failed: ".$conn->error);
		}
	}

	//handle data from spreadsheet
	if($_POST['data']['temp_type']=="spreadsheet"){

		$temp = $_POST['template']['spreadsheet'];
		$ros = array();
		foreach($temp as $temp_ro){
			array_push($ros, implode(",",commaless($temp_ro)));
		}
		$template=implode("\n",$ros);
		$temp_type_spreadshhet = "UPDATE documents SET template='$template' WHERE id='$psn_last_id'";
		if ($conn->query($temp_type_spreadshhet) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $temp_type_spreadshhet,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $temp_type_spreadshhet,"Failed: ".$conn->error);
		}

	}

	/////////CUSTOM FORMS////////////////
	if($_POST['data']['document_type']=="Goods Payment Invoice"){

		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['created'] = date('Y-m-d H:i:s');
	    $loan_details = databaseInsert('goods_payment_invoice', $_POST['dataL']);

		if ($conn->query($loan_details) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Success");
			$cstm_loan_id = $conn->insert_id;

			//if has attachments
			if (!empty($_FILES['upload']['tmp_name'])) {
			$filesCount = count($_FILES['upload']['tmp_name']);
							$errors = [];
							//$path = 'uploads/';
							$uploaddir = $destination_dir;
							$extensions = array('jpg','gif','png','pdf','doc','docx','xls','xlsx','JPG','GIF','PNG','PDF','DOCX','DOC','XLS','XLSX','xlsm','XLSM','CSV','csv','PPT','ppt','txt');
							$all_files = count($_FILES['upload']['tmp_name']);
							$my_arr=[];
							$my_arr1=[];
							for ($i = 0; $i < $all_files; $i++) {
									//$fileName = $_FILES['photo']['name'][$i];
									$file_tmp = $_FILES['upload']['tmp_name'][$i];
									$file_type = $_FILES['upload']['type'][$i];
									$file_size = $_FILES['upload']['size'][$i];
									$file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
									$file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

									if (!in_array($file_ext, $extensions)) {
											$errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
									}

									if ($file_size > 10097152) {
											$errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
									}

									if (empty($errors)) {
										$uploadfile = $uploaddir."/".$file_name;
											$upload=move_uploaded_file($file_tmp, $uploadfile);
											$response .= '<br> Document uploaded.';
										 }
									$arr=array_push($my_arr, $uploadfile);
									$arr1=array_push($my_arr1, $file_name);

							}
							$json=json_encode($my_arr);
							$json1=json_encode($my_arr1);
							if($upload===TRUE)
							{
								$update = "UPDATE goods_payment_invoice SET attachment='$json' WHERE appId='$cstm_loan_id'";
								$update1 = "UPDATE goods_payment_invoice SET doc_name='$json1'WHERE appId='$cstm_loan_id'";
								$update2 = $conn->query("UPDATE documents SET file_location='$json' WHERE id='$psn_last_id'");
			          $update3 = $conn->query("UPDATE documents SET doc_name='$json1' WHERE id='$psn_last_id'");
								$do_update=$conn->query($update1);
								$_frm = array();
								foreach($_POST['table'] as $key=>$val){
									if(is_array($val)){ $_frm[$key]=implode(",",$val); }
									else { $_frm[$key]=$val; }
								}

								$frm_data = json_encode($_frm);
								//var_dump($_POST['table']);
								//exit();
								$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";

								if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
								} else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
							}
							if ($errors) print_r($errors);
							//exit();
			////////////////////
			}//end if has attachement

		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Failed: ".$conn->error);
			$response .= "DB Update Failed! <hr>".$conn->error."<hr>".$loan_details;
		}

	}

	if($_POST['data']['document_type']=="Construction Works Payment Invoice"){

		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['created'] = date('Y-m-d H:i:s');
		//var_dump($_POST['dataL']);
		//exit();
			$loan_details = databaseInsert('construction_works_payment_invoice', $_POST['dataL']);

		if ($conn->query($loan_details) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Success");
			$cstm_loan_id = $conn->insert_id;

			//if has attachments

						if (!empty($_FILES['upload']['tmp_name'])) {
						$filesCount = count($_FILES['upload']['tmp_name']);
						        $errors = [];
						        //$path = 'uploads/';
						        $uploaddir = $destination_dir;
						        $extensions = array('jpg','gif','png','pdf','doc','docx','xls','xlsx','JPG','GIF','PNG','PDF','DOCX','DOC','XLS','XLSX','xlsm','XLSM','CSV','csv','PPT','ppt','txt');
						        $all_files = count($_FILES['upload']['tmp_name']);
						        $my_arr=[];
						        $my_arr1=[];
						        for ($i = 0; $i < $all_files; $i++) {
						            //$fileName = $_FILES['photo']['name'][$i];
						            $file_tmp = $_FILES['upload']['tmp_name'][$i];
						            $file_type = $_FILES['upload']['type'][$i];
						            $file_size = $_FILES['upload']['size'][$i];
						            $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
						            $file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

						            if (!in_array($file_ext, $extensions)) {
						                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
						            }

						            if ($file_size > 10097152) {
						                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
						            }

						            if (empty($errors)) {
						            	$uploadfile = $uploaddir."/".$file_name;
						                $upload=move_uploaded_file($file_tmp, $uploadfile);
						                $response .= '<br> Document uploaded.';
						               }
						            $arr=array_push($my_arr, $uploadfile);
						            $arr1=array_push($my_arr1, $file_name);

						        }
						        $json=json_encode($my_arr);
						        $json1=json_encode($my_arr1);
						        if($upload===TRUE)
						        {
						          $update = "UPDATE construction_works_payment_invoice SET attachment='$json' WHERE appId='$cstm_loan_id'";
										  $update1 = "UPDATE construction_works_payment_invoice SET doc_name='$json1'WHERE appId='$cstm_loan_id'";
											$update2 = $conn->query("UPDATE documents SET file_location='$json' WHERE id='$psn_last_id'");
						          $update3 = $conn->query("UPDATE documents SET doc_name='$json1' WHERE id='$psn_last_id'");
											$do_update=$conn->query($update1);
											$_frm = array();
											foreach($_POST['table'] as $key=>$val){
												if(is_array($val)){ $_frm[$key]=implode(",",$val); }
												else { $_frm[$key]=$val; }
											}

											$frm_data = json_encode($_frm);
											//var_dump($_POST['table']);
											//exit();
											$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";

						          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
						          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
						        }
						        if ($errors) print_r($errors);
						        //exit();
						////////////////////
						}//end if has attachement

		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Failed: ".$conn->error);
			$response .= "DB Update Failed! <hr>".$conn->error."<hr>".$loan_details;
		}
	}

	if($_POST['data']['document_type']=="Service Payment Invoice"){

		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['file_number'] = $_POST['data']['doc_ref_number'];
		$_POST['dataL']['created'] = date('Y-m-d H:i:s');
		$loan_details = databaseInsert('service_payment_invoice', $_POST['dataL']);

		if ($conn->query($loan_details) === TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Success");
			$cstm_loan_id = $conn->insert_id;

			//if has attachments


			if (!empty($_FILES['upload']['tmp_name'])) {
			$filesCount = count($_FILES['upload']['tmp_name']);
			        $errors = [];
			        //$path = 'uploads/';
			        $uploaddir = $destination_dir;
			        $extensions = array('jpg','gif','png','pdf','doc','docx','xls','xlsx','JPG','GIF','PNG','PDF','DOCX','DOC','XLS','XLSX','xlsm','XLSM','CSV','csv','PPT','ppt','txt');
			        $all_files = count($_FILES['upload']['tmp_name']);
			        $my_arr=[];
			        $my_arr1=[];
			        for ($i = 0; $i < $all_files; $i++) {
			            //$fileName = $_FILES['photo']['name'][$i];
			            $file_tmp = $_FILES['upload']['tmp_name'][$i];
			            $file_type = $_FILES['upload']['type'][$i];
			            $file_size = $_FILES['upload']['size'][$i];
			            $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
			            $file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

			            if (!in_array($file_ext, $extensions)) {
			                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
			            }

			            if ($file_size > 10097152) {
			                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
			            }

			            if (empty($errors)) {
			            	$uploadfile = $uploaddir."/".$file_name;
			                $upload=move_uploaded_file($file_tmp, $uploadfile);
			                $response .= '<br> Document uploaded.';
			               }
			            $arr=array_push($my_arr, $uploadfile);
			            $arr1=array_push($my_arr1, $file_name);

			        }
			        $json=json_encode($my_arr);
			        $json1=json_encode($my_arr1);
			        if($upload===TRUE)
			        {
			          $update = "UPDATE service_payment_invoice SET attachment='$json' WHERE appId='$cstm_loan_id'";
							  $update1 = "UPDATE service_payment_invoice SET doc_name='$json1'WHERE appId='$cstm_loan_id'";
								$update2 = $conn->query("UPDATE documents SET file_location='$json' WHERE id='$psn_last_id'");
			          $update3 = $conn->query("UPDATE documents SET doc_name='$json1' WHERE id='$psn_last_id'");
			          $do_update=$conn->query($update1);
								$_frm = array();
								foreach($_POST['table'] as $key=>$val){
									if(is_array($val)){ $_frm[$key]=implode(",",$val); }
									else { $_frm[$key]=$val; }
								}

								$frm_data = json_encode($_frm);
								//var_dump($_POST['table']);
								//exit();
								$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";

			          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
			          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
			        }
			        if ($errors) print_r($errors);
			        //exit();
			////////////////////
			}
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $loan_details,"Failed: ".$conn->error);
			$response .= "DB Update Failed! <hr>".$conn->error."<hr>".$loan_details;
		}



	}


 	//+++++++++++++++++++++++++++++++++++++++++++++++
	//
	//start uploading the files

if (!empty($_FILES['upload']['tmp_name'])) {
$filesCount = count($_FILES['upload']['tmp_name']);
        $errors = [];
        //$path = 'uploads/';
        $uploaddir = $destination_dir;
        $extensions = array('jpg','gif','png','pdf','doc','docx','xls','xlsx','JPG','GIF','PNG','PDF','DOCX','DOC','XLS','XLSX','xlsm','XLSM','CSV','csv','PPT','ppt','txt');
        $all_files = count($_FILES['upload']['tmp_name']);
        $my_arr=[];
        $my_arr1=[];
        for ($i = 0; $i < $all_files; $i++) {
            //$fileName = $_FILES['photo']['name'][$i];
            $file_tmp = $_FILES['upload']['tmp_name'][$i];
            $file_type = $_FILES['upload']['type'][$i];
            $file_size = $_FILES['upload']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
            $file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 10097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            if (empty($errors)) {
            	$uploadfile = $uploaddir."/".$file_name;
                $upload=move_uploaded_file($file_tmp, $uploadfile);
                $response .= '<br> Document uploaded.';
               }
            $arr=array_push($my_arr, $uploadfile);
            $arr1=array_push($my_arr1, $file_name);

        }
        $json=json_encode($my_arr);
        $json1=json_encode($my_arr1);
        if($upload===TRUE)
        {
          $update = "UPDATE documents SET file_location='$json' WHERE id='$psn_last_id'";
          $update1 = "UPDATE documents SET doc_name='$json1' WHERE id='$psn_last_id'";
          $do_update=$conn->query($update1);
          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
        }
        if ($errors) print_r($errors);
        //exit();
////////////////////
}
	//if has attachement
	//---------------------------------------------------------

	/// add track information
		$tbl = 'doc_track';
		$_POST['data2']['document_type'] = $_POST['data']['document_type'];
		$_POST['data2']['from_id'] = $_POST['data']['sender_id'];
		$_POST['data2']['to_id'] = $_POST['data']['current_holder'];
		$_POST['data2']['doc_id'] = $psn_last_id;
		$_POST['data2']['from_stage'] = "INITIATION";
		$_POST['data2']['from_status'] = "INITIATION";
		$_POST['data2']['remark'] = $_POST['data']['description'];
		$_POST['data2']['action'] = 'SUBMITTED';
		$_POST['data2']['receiver_status'] = 'SENT';
		$_POST['data2']['actionDate']=$_POST['data3']['date_created']." ".date('H:i:s');
		$_POST['data2']['attachment']=$json;
		$_POST['data2']['doc_name']=$json1;
		$insert_data2 = databaseInsert($tbl, $_POST['data2']);

		if($conn->query($insert_data2) === TRUE){

			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $insert_data2,"Success");

			$response .= ' <div><em>Your document is now being tracked.</em></div>';

			$myTrackId = $conn->insert_id;

			$to = getStaffEmail($_POST['data2']['to_id']);
			$subject = 'New '.getDocType($psn_last_id). ' document notification from '.getStaffName($_POST['data2']['from_id']);
			$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;

			$message = '<p>Dear '.getStaffName($_POST['data2']['to_id']) .'. The document listed below requires your attention. Follow the link to get more detail.</p>';
			$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($psn_last_id).": ".$refNumber.'</span></p>';
			$message .= '<p>
			<br>
			<table width="90%" cellpadding="7" align="center" cellspacing="0" >
				<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Remarks</th></tr>
				<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data2']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($psn_last_id).'</td><td style="border:solid 1px #ccc">'.$_POST['data2']['remark'].'</td></tr>
			</table><br><br></p>';

			dbmail($to,$subject,$message,$link);

		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $insert_data2,"Failed:".$conn->error);
			$response .= ' <div><em>Could not add document to track list.'.$conn->error.'</em></div>';
		}

		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){

			$_POST['data2']['from_id'] = $_POST['data']['current_holder'];
			$_POST['data2']['to_id'] = $_POST['delegate_to'];
			$_POST['data2']['action'] = 'DELEGATED';
			$delegTrackId = $myTrackId;

			$insert_data3 = databaseInsert($tbl, $_POST['data2']);

			if($conn->query($insert_data3) === TRUE){

				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $insert_data3,"Success");

				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';

				$myTrackId = $conn->insert_id;

				$to = getStaffEmail($_POST['data2']['to_id']);
				$subject = $_POST['data2']['action'].': '.$refNumber.' '.getStaffName($_POST['data2']['from_id']);
				$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;

				$message = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data2']['doc_id']).": ".$refNumber.'</span></p>';
				$message .= '<p><br>
				<table width="90%" cellpadding="7" align="center" cellspacing="0" >
					<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
					<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data2']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data2']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data2']['remark'].'</td></tr>
				</table>
				';

				dbmail($to,$subject,$message,$link);


			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $insert_data3,"Failed:".$conn->error);
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->error.'</em></div>';
			}


			//update the current holder in the documents table
			$del_doc_id = $_POST['data2']['doc_id'];
			$del_current_holder = $_POST['data2']['to_id'];

			$updateQry = "UPDATE documents SET current_holder='".$del_current_holder."', file_status='DELEGATED' WHERE id='".$del_doc_id."'";
			if($conn->query($updateQry) === TRUE){

				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Success");

				$response .= ' <div><em>Document update success.</em></div>';

				//update previous track record to the archives
				$arc_date=date('Y-m-d H:i:s');
				$prev_qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date =".$arc_date." WHERE id =". $delegTrackId;
				if($conn->query($prev_qry) === TRUE){
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $prev_qry,"Success");
					$response .= ' ';
				} else {
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $prev_qry,"Failed:".$conn->error);
				}
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Failed:".$conn->error);
				$response .= ' <div><em>Document update failure.'.$conn->error.'</em></div>';
			}

		}


	/////////////// end track information

	} //if NEW DOC
  if($_POST['action'] == "NEW ACTION"){
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		//create account directory
	//create account directory
	 $lastid = mysqli_insert_id($conn);
		   $sql = "SELECT * FROM doc_track where id='$lastid' ORDER BY id";
		   $my_result = $conn->query($sql);
		   $result = $my_result->fetch_assoc();
		   $arch_date=$result['actionDate'];
		   $arc_date=date('Y-m-d H:i:s');
           $dox_id=$_POST['data']['doc_id'];
					 // get current holder from the database
					 $from_id=$_POST['data']['from_id'];
					 $to_id=$_POST['data']['to_id'];
					 $doc_id=$_POST['data']['doc_id'];
					 $my_query ="SELECT current_holder FROM documents WHERE id='$doc_id'";

					  $my_query_result=$conn->query($my_query);
					  $result_arr = $my_query_result->fetch_assoc();
					  $array=str_replace($from_id, $to_id, $result_arr);
					 //print_r($array);
					 //$_POST['data']['current_holder']=$array;

					 $current_holder = '';
					  foreach($array as $row)
					  {
					   $current_holder .= $row . ', ';
					  }
					 $holder = substr($current_holder, 0, -2);
		   //$holder=$_POST['data']['to_id'];
		   $file_status=$_POST['data2']['file_status'];
           $qry_ch ="UPDATE documents SET current_holder='$holder',file_status='$file_status' WHERE id='$dox_id'";

            $qry_result=$conn->query($qry_ch);
		    if($_POST['data']['action']=="CLOSED"){
			$tid=$_POST['tId'];
		    $update_status = $conn->query("UPDATE doc_track SET archive_date='$arc_date',receiver_status='ARCHIVED' WHERE id='$tid'");

		    $update_read_status = "UPDATE doc_track SET archive_date='$receive_date' WHERE id='$psn_last_id'";

			if($conn->query($update_read_status) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
		if($_POST['data']['to_status']=="Forwarded"){
			$to_id=$_POST['data']['from_id'];
			$dox_id=$_POST['data']['doc_id'];
			$holder=$_POST['data']['to_id'];
			$to_stage=$_POST['data']['to_stage'];
			$tid=$_POST['tId'];
		    $update_read_status = "UPDATE doc_track SET archive_date='$arch_date',receiver_status='ARCHIVED' WHERE id='$tid'";

			if($conn->query($update_read_status) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
	$file1 = explode("-",date("Y-m-d"));

	$doc_type = $_POST['data']['document_type'];
	$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $doc_type;

	$dir1="doc_archive/".$file1[0];
	$dir2=$dir1."/".$file1[1];
	$dir3=$dir2."/".$doc_type;

	if(!is_dir($destination_dir)) {

		if(!is_dir($dir1)) {
			mkdir($dir1, 0755);
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir2)) {
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir3)) {
			mkdir($dir3, 0755);
		}

	}
	$doc_id = $_POST['data']['doc_id'];
	if (!empty($_FILES['photo']['tmp_name'])) {
     $filesCount = count($_FILES['photo']['tmp_name']);
        $errors = [];
        //$path = 'uploads/';
        $uploaddir = $destination_dir;
        $extensions = array('jpg','jpeg','gif','png','pdf','doc','docx','DOCX','DOC','PDF','XLS','XLSX','xls','xlsx','GIF','JPG','JPEG','PNG','XLSM','xlsm','txt','TXT','html','HTML','XML','xml');
        $all_files = count($_FILES['photo']['tmp_name']);
       // $my_arr=[];
        $my_query = "SELECT * FROM documents where id='$doc_id'";
		$my_result = $conn->query($my_query);
		$result_arr = $my_result->fetch_assoc();
        $my_arr=json_decode($result_arr['file_location']);
        //$my_arr=json_decode($document);
        $my_arr1=json_decode($result_arr['doc_name']);
        //////////////////////////
		if($result_arr['file_location']!=''&& $result_arr['file_location']!='null'){
        for ($i = 0; $i < $all_files; $i++) {
            //$fileName = $_FILES['photo']['name'][$i];
            $file_tmp = $_FILES['photo']['tmp_name'][$i];
            $file_type = $_FILES['photo']['type'][$i];
            $file_size = $_FILES['photo']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['photo']['name'][$i])));
            $file_name = $psn_last_id.'-'.basename($_FILES['photo']['name'][$i]);

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 20097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

           // if (empty($errors)) {
            	$uploadfile = $uploaddir."/".$file_name;
                $upload=move_uploaded_file($file_tmp, $uploadfile);
                $response .= '<br> Document uploaded.';
             //  }
            $arr=array_push($my_arr, $uploadfile);
            $arr1=array_push($my_arr1, $file_name);

        }
       $arr=array_values(array_unique($my_arr));
	    $arr1=array_values(array_unique($my_arr1));
        $json=json_encode($arr);
        $json1=json_encode($arr1);
        //////////////////////

        if($upload===TRUE)
        {
          $update = "UPDATE doc_track SET attachment='$json' WHERE id='$psn_last_id'";
          $update2 = "UPDATE documents SET doc_name='$json1' WHERE id='$doc_id'";
          $do_update=$conn->query($update2);
          $qry1 = "UPDATE documents SET file_location = '$json' WHERE id = '$doc_id'";
          $qry2="UPDATE doc_track SET doc_name = '$json1' WHERE id = '$psn_last_id'";
          $do_update1=$conn->query($qry2);
          $update2=$conn->query($qry1);
          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
        }
		}
		 else{
         $my_arr=[];
        $my_arr1=[];
        for ($i = 0; $i < $all_files; $i++) {
            //$fileName = $_FILES['photo']['name'][$i];
            $file_tmp = $_FILES['photo']['tmp_name'][$i];
            $file_type = $_FILES['photo']['type'][$i];
            $file_size = $_FILES['photo']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['photo']['name'][$i])));
            $file_name = $psn_last_id.'-'.basename($_FILES['photo']['name'][$i]);

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 2097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

           // if (empty($errors)) {
            	$uploadfile = $uploaddir."/".$file_name;
                $upload=move_uploaded_file($file_tmp, $uploadfile);
                $response .= '<br> Document uploaded.';
               //}
            $arr=array_push($my_arr, $uploadfile);
            $arr1=array_push($my_arr1, $file_name);
        }
       $arr=array_values(array_unique($my_arr));
	    $arr1=array_values(array_unique($my_arr1));
        $json=json_encode($arr);
        $json1=json_encode($arr1);
        /*var_dump($json);
         var_dump($json1);
	      exit();
*/
        if($upload===TRUE)
        {
          $update = "UPDATE documents SET file_location='$json' WHERE id='$doc_id'";
          $update1 = "UPDATE doc_track SET attachment='$json' WHERE id='$psn_last_id'";
          $update3 = "UPDATE documents SET doc_name='$json1' WHERE id='$doc_id'";
          $update4 = "UPDATE doc_track SET doc_name='$json1' WHERE id='$psn_last_id'";
          $do_update=$conn->query($update1);
          $do_update1=$conn->query($update3);
          $do_update2=$conn->query($update4);
          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
        }
        }
////////////////////
}

		$myTrackId = $psn_last_id;

		$refNumber = getRefNo($_POST['data']['doc_id']);

		$to = getStaffEmail($_POST['data']['to_id']);
			$from = 'info@cinnamonerp.com';
			$subject = getSubject($_POST['data']['doc_id']);
			$email_name = 'DocTracker';
			$doc_ref = $refNumber;
			$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
			$notification = 'A document with Reference No. <b>'.$refNumber.'</b>  from <b>'.getStaffName($_POST['data']['from_id']).'</b> has been '.strtolower($_POST['data']['action']).' to you';
			$notification .= '<p><b>DOCUMENT SUBJECT: </b>'.$subject.'</p>.';
			$notification .= 'Follow the link below to get more detail.';
			$message="";
			/*$message ='
			<table width="100%" cellpadding="7" cellspacing="0" border="1">
				<tr><th>Sender</th><th>Document Ref No.</th><th>Summary</th></tr>
				<tr><td>'.getStaffName($_POST['data']['from_id']).'</td><td>'.$refNumber.'</td><td>'.$_POST['data']['remark'].'</td></tr>
			</table>
			';*/

			sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);


		$tbl = 'documents';
		$item = 'id';
		$itemId = $_POST['data']['doc_id'];

		$_POST['data2']['current_holder'] = $_POST['data']['to_id'];

		$updateQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );

		if($conn->query($updateQry) === TRUE){
			$response .= ' <div><em>Document update success.</em></div>';
			$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $_POST['tId'];
		   if($conn->query($qry) === TRUE){
				$response .= ' <div><em>Document has been added to your archives.</em></div>';
			}
		}
		else{
			$response .= ' <div><em>Document update failure.'.$conn->mysqli_error().'</em></div>';
		}

		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			//create new record for the delegate into the doc_track table
			$tbl = 'doc_track';
			$_POST['data']['from_id'] = $_POST['data']['to_id'];
			$_POST['data']['to_id'] = $_POST['delegate_to'];
			$_POST['data']['action'] = 'DELEGATED';

			$insert_data = databaseInsert($tbl, $_POST['data']);

			if($conn->query($insert_data) === TRUE){
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				$myTrackId = mysqli_insert_id($conn);

				$refNumber = getRefNo($_POST['data']['doc_id']);

				$to = getStaffEmail($_POST['data']['to_id']);
				$from = 'info@cinnamonerp.com';
				$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
				$email_name = 'DocTracker';
				$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;

				$notification = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$doc_ref = getDocType($_POST['data']['doc_id']).": ".$refNumber;

				$message = '';
				$message .= '

				<table width="100%" cellpadding="7" cellspacing="0" border="1">
					<tr><th>Sender</th><th>Document type</th><th>Summary</th></tr>
					<tr><td>'.getStaffName($_POST['data']['from_id']).'</td><td>'.getDocType($_POST['data']['doc_id']).'</td><td>'.$_POST['data']['remark'].'</td></tr>
				</table>


				';

				sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
			}
			else{
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->mysqli_error().'</em></div>';
			}

			//update the current holder in the documents table
			$tbl = 'documents';
			$item = 'id';
			$itemId = $_POST['data']['doc_id'];

			$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
			$_POST['data2']['file_status'] = 'DELEGATED';

			$updateQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );

			if($conn->query($updateQry) === TRUE){
				$response .= ' <div><em>Document update success.</em></div>';

				//update previous track record to the archives
				$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $myTrackId;
				if($conn->query($qry) === TRUE){
					$response .= ' ';
				}
			}
			else{
				$response .= ' <div class="alert danger"><em>Document update failure.'.$conn->mysqli_error().'</em></div>';
			}

		}

	}

	if($_POST['action']=="NEW DELEG"){
		if(isset($_POST['data']['approved']) AND $_POST['data']['approved']=='1'){

			$dlg_to_id = $_POST['data']['delegate_to'];
			$stff_id = $_POST['data']['requested_by'];

			$update_staff = "UPDATE admins SET delegate_to='$dlg_to_id' WHERE id='$stff_id'";
			if($conn->query($update_staff) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
	}

	if($_POST['action']=="NEW DELEGATE"){
		$deleger = getStaffName($_POST['data']['requested_by']);
		$to = getStaffEmail($_POST['approver']);
		$subject = "DELEGATION REQUEST FOR ".$deleger." PENDING YOUR APPROVAL";
		$link = $site_url.'/deleg.php?ac='.$psn_last_id;

		$message = 'The following delegation request is pending your approval. Click the link at the end to get more detail/approve.';
		$message .= '<p><span style="color:#cb3348;font-size:14px;">REQUESTED BY:';
		$message .= '<br>NAME: '.$deleger;
		$message .= '<br>POSITION: '.getTitle($_POST['data']['requested_by']);
		$message .= '<br>DEPARTMENT: '.getDept($_POST['data']['requested_by']);
		$message .= '</span></p>';
		$message .= '<p><br>
		<table width="90%" cellpadding="7" align="center" cellspacing="0" >
			<tr><th style="border:solid 1px #ccc">Delegate To</th><th style="border:solid 1px #ccc">Period</th><th style="border:solid 1px #ccc">Reason</th></tr>
			<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['delegate_to']).'</td><td style="border:solid 1px #ccc">From: <b>'.date("j M Y", strtotime(substr($_POST['data']['duration'], 0, 10))).'</b> To: <b>'.date("j M Y", strtotime(substr($form_data['duration'], 13, 10))).'</b></td>
			<td style="border:solid 1px #ccc">'.nl2br($_POST['data']['reason']).'</td></tr>
		</table>';

		dbmail($to,$subject,$message,$link);


		if(isset($_POST['data']['approved']) AND $_POST['data']['approved']=='1'){

			$dlg_to_id = $_POST['data']['delegate_to'];
			$stff_id = $_POST['data']['requested_by'];

			$update_staff = "UPDATE admins SET delegate_to='$dlg_to_id' WHERE id='$stff_id'";
			if($conn->query($update_staff) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
	}


	if($_POST['action'] == "WORKFLOW ACTION"){
	       $lastid = mysqli_insert_id($conn);
		   $sql = "SELECT * FROM doc_track where id='$lastid' ORDER BY id";
		   $my_result = $conn->query($sql);
		   $result = $my_result->fetch_assoc();
		   $arch_date=$result['actionDate'];
		   $arc_date=date('Y-m-d H:i:s');
		   if($_POST['data']['to_stage']=="CLOSED"){
	   	    $to_id=$_POST['data']['from_id'];
			$dox_id=$_POST['data']['doc_id'];
			$to_stage=$_POST['data']['to_stage'];
			$tid=$_POST['tId'];
		    $update_status = $conn->query("UPDATE doc_track SET archive_date='$arc_date',receiver_status='ARCHIVED' WHERE id='$tid'");

		    $update_read_status = "UPDATE doc_track SET archive_date='$receive_date' WHERE id='$psn_last_id'";

			if($conn->query($update_read_status) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
		if($_POST['data']['to_status']=="Forwarded"){
			$to_id=$_POST['data']['from_id'];
			$dox_id=$_POST['data']['doc_id'];
			$holder=$_POST['data']['to_id'];
			$to_stage=$_POST['data']['to_stage'];
			$tid=$_POST['tId'];
		    $update_read_status = "UPDATE doc_track SET archive_date='$arch_date',receiver_status='ARCHIVED' WHERE id='$tid'";

			if($conn->query($update_read_status) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
		if($_POST['data']['to_status']=="Sent Back"){
			$to_id=$_POST['data']['from_id'];
			$dox_id=$_POST['data']['doc_id'];
			$holder=$_POST['data']['to_id'];
			$to_stage=$_POST['data']['to_stage'];
			$tid=$_POST['tId'];
		    $update_read_status = "UPDATE doc_track SET archive_date='$arch_date',receiver_status='ARCHIVED' WHERE id='$tid'";

			if($conn->query($update_read_status) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_staff,"Failed: ".$conn->error);
			}
		}
		if(isset($_POST['signed'])&& $_POST['signed'] != ''){
			$tbl = 'document_signatures';
			$_POST['data_sign']['doc_id'] = $_POST['data']['doc_id'];
			$_POST['data_sign']['signatory_id'] = $_POST['signed'];
			$_POST['data_sign']['date_signed'] = date('Y-m-d H:i:s');
		    $signed = databaseInsert($tbl, $_POST['data_sign']);
			if($conn->query($signed) === TRUE){ //if insert query successful

	        $last_sign_id = $conn->insert_id;
			}
		   }
		//////////////////////////////////
			$dox_id=$_POST['data']['doc_id'];
			$holder=$_POST['data']['to_id'];
			$to_stage=$_POST['data']['to_stage'];
           $qry_ch ="UPDATE documents SET current_holder='$holder',stage_label='$to_stage' WHERE id='$dox_id'";
            $qry_result=$conn->query($qry_ch);
	       $receive_date=date('Y-m-d H-i-s');
         //create account directory
	//$psn_last_id = mysqli_insert_id($conn);
	$file1 = explode("-",date("Y-m-d"));

	$doc_type = $_POST['data']['document_type'];
	$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $doc_type;

	$dir1="doc_archive/".$file1[0];
	$dir2=$dir1."/".$file1[1];
	$dir3=$dir2."/".$doc_type;

	if(!is_dir($destination_dir)) {

		if(!is_dir($dir1)) {
			mkdir($dir1, 0755);
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir2)) {
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir3)) {
			mkdir($dir3, 0755);
		}

	}
	//$psn_last_id = mysqli_insert_id($conn);
	$doc_id = $_POST['data']['doc_id'];
	$my_query = "SELECT * FROM documents where id='$doc_id'";
	$my_result = $conn->query($my_query);
	$result_arr = $my_result->fetch_assoc();
	if (!empty($_FILES['upload']['tmp_name'])) {
     $filesCount = count($_FILES['upload']['tmp_name']);
        $errors = [];
        //$path = 'uploads/';
        $uploaddir = $destination_dir;
        $extensions = array('jpg','gif','png','pdf','doc','docx','xls','xlsx','JPG','GIF','PNG','PDF','DOCX','DOC','XLS','XLSX','xlsm','XLSM');
        $all_files = count($_FILES['upload']['name']);
       // $my_arr=[];
		//Test if there files in file location
		if($result_arr['file_location']!=''){
        $my_arr=json_decode($result_arr['file_location']);
        $my_arr1=json_decode($result_arr['doc_name']);
        //////////////////////////
        for ($i = 0; $i < $all_files; $i++) {
            //$fileName = $_FILES['upload']['name'][$i];
            $file_tmp = $_FILES['upload']['tmp_name'][$i];
            $file_type = $_FILES['upload']['type'][$i];
            $file_size = $_FILES['upload']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
            $file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 20097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            //if (empty($errors)) {
            	$uploadfile = $uploaddir."/".$file_name;
                $upload=move_uploaded_file($file_tmp, $uploadfile);
                $response .= '<br> Document uploaded.';
              // }
            $arr=array_push($my_arr, $uploadfile);
            $arr1=array_push($my_arr1, $file_name);

        }
        $json=json_encode($my_arr);
        $json1=json_encode($my_arr1);
        //////////////////////

        if($upload===TRUE)
        {
          $update = "UPDATE doc_track SET attachment='$json' WHERE id='$psn_last_id'";
          $update2 = "UPDATE documents SET doc_name='$json1' WHERE id='$doc_id'";
          $do_update=$conn->query($update2);
          $qry1 = "UPDATE documents SET file_location = '$json' WHERE id = '$doc_id'";
          $qry2="UPDATE doc_track SET doc_name = '$json1' WHERE id = '$psn_last_id'";
          $do_update1=$conn->query($qry2);
          $update2=$conn->query($qry1);
          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
        // handle attachments

        }
    }
     else{
         $my_arr=[];
        $my_arr1=[];
        for ($i = 0; $i < $all_files; $i++) {
            //$fileName = $_FILES['upload']['name'][$i];
            $file_tmp = $_FILES['upload']['tmp_name'][$i];
            $file_type = $_FILES['upload']['type'][$i];
            $file_size = $_FILES['upload']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'][$i])));
            $file_name = $psn_last_id.'-'.basename($_FILES['upload']['name'][$i]);

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 20097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            if (empty($errors)) {
            	$uploadfile = $uploaddir."/".$file_name;
                $upload=move_uploaded_file($file_tmp, $uploadfile);
                $response .= '<br> Document uploaded.';
               }
            $arr=array_push($my_arr, $uploadfile);
            $arr1=array_push($my_arr1, $file_name);

        }
        $json=json_encode($my_arr);
        $json1=json_encode($my_arr1);
        if($upload===TRUE)
        {
          $update = "UPDATE documents SET file_location='$json' WHERE id='$doc_id'";
          $update1 = "UPDATE doc_track SET attachment='$json' WHERE id='$psn_last_id'";
          $update3 = "UPDATE documents SET doc_name='$json1' WHERE id='$doc_id'";
          $update4 = "UPDATE doc_track SET doc_name='$json' WHERE id='$psn_last_id'";
          $do_update=$conn->query($update1);
          $do_update1=$conn->query($update3);
          $do_update2=$conn->query($update4);
          if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. ";
          } else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
        }
        }
////////////////////
   }
      if(isset($_POST['signature'])&& $_POST['signature']!=''){
		if($result_arr['signed_by']!=''){
        $sign_arr=json_decode($result_arr['signed_by']);
		$signatures=array_push($sign_arr, $_POST['signature']);
		$json_sign=json_encode($sign_arr);
		}else{
			$sign_arr=[];
			$signatures=array_push($sign_arr, $_POST['signature']);
			$json_sign=json_encode($sign_arr);
		}
		$sign_update = $conn->query("UPDATE documents SET signed_by = '$json_sign' WHERE id ='".$_POST['data']['doc_id']."' ");
		}
		$myTrackId = $psn_last_id;
		$document_id=$_POST['data']['doc_id'];
		$refNumber = getRefNo($_POST['data']['doc_id']);
		$subject = $_POST['data']['to_status'].' '.getDocType($_POST['data']['doc_id']).' '.'DOCUMENT NOTIFICATION';
		$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
		if($_POST['data']['to_stage']=="CLOSED"||$_POST['data']['to_stage']=="CLOSED"||$_POST['data']['to_status']=="Approved"){
		$init_q = $conn->query("SELECT created_by FROM documents WHERE id='$document_id'");
		$init = $init_q->fetch_assoc();
		$to = getStaffEmail($init['created_by']);
		$initiator=getStaffName($init['created_by']);
		//$to = getStaffEmail($_POST['data']['to_id']);
		$message = 'Dear '.$initiator.', the document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
		$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data']['doc_id']).": ".$refNumber.'</span></p>';
		$message .= '<p><br>
		<table width="90%" cellpadding="7" align="center" cellspacing="0" >
			<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
			<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data']['remark'].'</td></tr>
		</table></p>';
		dbmail($to,$subject,$message,$link);
		}else{
		$receiver=getStaffName($_POST['data']['to_id']);
		$to = getStaffEmail($_POST['data']['to_id']);
		$message = 'Dear '.$receiver.', the document listed below has been '.strtolower($_POST['data']['to_status']).'. Follow the link to get more detail.';
		$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data']['doc_id']).": ".$refNumber.'</span></p>';
		$message .= '<p><br>
		<table width="90%" cellpadding="7" align="center" cellspacing="0" >
			<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
			<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data']['remark'].'</td></tr>
		</table></p>';
		dbmail($to,$subject,$message,$link);
		}
		if($_POST['data']['to_stage']=="CLOSED"){
			$timestamp=date('Y-m-d H:i:s');
			$update_close = "UPDATE doc_track SET receive_date=".$timestamp.", archive_date= ".$timestamp." WHERE id='".$_POST['data']['doc_id']."'";
			if($conn->query($update_close) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_close,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $update_close,"Failed: ".$conn->error);
			}
		}

		$tbl = 'documents';
		$item = 'id';
		$itemId = $_POST['data']['doc_id'];
		$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
		$_POST['data2']['sender_id'] = $_POST['data']['from_id'];
		$_POST['data2']['stage_label'] = $_POST['data']['to_stage'];
		// check if there is existing signatures

		$actionQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );
		if($conn->query($actionQry) === TRUE){

			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $actionQry,"Success");
			$response .= ' <div><em>Document update success.</em></div>';
			$arch_date=date('Y-m-d H:i:s');
			$archive_qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = ".$arch_date." WHERE id =". $_POST['tId'];
			if($conn->query($archive_qry) === TRUE){
				$response .= ' <div><em>Document has been added to your archives.</em></div>';
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $archive_qry,"Success");

			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $archive_qry,"Failed:".$conn->error);
			}
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $actionQry,"Failed:".$conn->error);
			$response .= ' <div><em>Document update failed.'.$conn->error.'</em></div>';
		}
		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			//create new record for the delegate into the doc_track table
			$tbl = 'doc_track';
			$_POST['data']['from_id'] = $_POST['data']['to_id'];
			$_POST['data']['to_id'] = $_POST['delegate_to'];
			$_POST['data']['action'] = 'DELEGATED';
			$delegTrackId = $myTrackId;

			$delegate_data = databaseInsert($tbl, $_POST['data']);

			if($conn->query($delegate_data) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $delegate_data,"Success");
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				$myTrackId = $conn->insert_id;

				$refNumber = getRefNo($_POST['data']['doc_id']);

				$to = getStaffEmail($_POST['data']['to_id']);
				$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
				$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;

				$message = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$message .= '<p><span style="color:#cb3348;font-size:14px;">'.getDocType($_POST['data']['doc_id']).": ".$refNumber.'</span></p>';
				$message .= '<p><br>
				<table width="90%" cellpadding="7" align="center" cellspacing="0" >
					<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Summary</th></tr>
					<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($_POST['data']['doc_id']).'</td><td style="border:solid 1px #ccc">'.$_POST['data']['remark'].'</td></tr>
				</table></p>';

				sendemail($to,$subject,$message,$link);


			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $delegate_data,"Failed:".$conn->error);
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->error.'</em></div>';
			}

			//update the current holder in the documents table
			$del_doc_id = $_POST['data']['doc_id'];
			$del_current_holder = $_POST['data']['to_id'];

			$updateQry = "UPDATE documents SET current_holder='".$del_current_holder."', sender_id='".$_POST['data']['to_id']."', file_status='DELEGATED' WHERE id='".$del_doc_id."'";

			if($conn->query($updateQry) === TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Success");
				$response .= ' <div><em>Document update success.</em></div>';

				//update previous track record to the archives
				$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $delegTrackId;
				if($conn->query($qry) === TRUE){
					$response .= ' ';
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Success");
				} else {
					sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Failed:".$conn->error);
				}

			}
			else{
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : DELEGATION", $updateQry,"Failed:".$conn->error);
				$response .= ' <div class="alert danger"><em>Document update failure.'.$conn->error.'</em></div>';
			}

		}

	}

	unset($psn_last_id);
	if($_POST['action']=="ADD STAGE"){

		if($_POST['data']['temp_type']=="form"){
			$temp = $_POST['template']['form'];
			$e = count($temp['label']);
			$elts=array();
			for($i=0;$i<$e;$i++){
				if($temp['label'][$i]!=""){ $elts[$i]=$temp['label'][$i].";".$temp['field'][$i].";".$temp['required'][$i].";".$temp['default'][$i]; }
			}
			$template=implode("$$",$elts);
			$stage_qry  = "UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'";
			if($conn->query($stage_qry)===TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_qry,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_qry,"Failed:".$conn->error);
			}
		}

		if($_POST['data']['temp_type']=="spreadsheet"){
			$temp = $_POST['template']['spreadsheet'];
			$ros = array();
			foreach($temp as $temp_ro){
				array_push($ros, implode(",",$temp_ro));
			}
			$template=implode("\n",$ros);

			$stage_tmp_qry  = "UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'";
			if($conn->query($stage_tmp_qry)===TRUE){
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_tmp_qry,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $stage_tmp_qry,"Failed:".$conn->error);
			}
		}

	}
	//0778742794
	if($_POST['action'] == "NEW GROUP"){

		$groups_qry  = "UPDATE `$tbl` SET `members`='".implode(",",$_POST['members'])."' WHERE id='$psn_last_id'";
		if($conn->query($groups_qry)===TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Failed:".$conn->error);
		}
	}

	if($_POST['action'] == "TROUBLESHOOT: RESTORE CLOSED DOCUMENT"){

		$restore_qry  = "UPDATE `documents` SET
						`current_holder`='".$_POST['data']['to_id']."',
						`date_closed`='0000-00-00',
						`closed_by`='0',
						`stage_label`='".$_POST['data']['to_stage']."',
						`status_label`='".$_POST['data']['to_status']."'
						WHERE id='".$_POST['data']['doc_id']."'";

		if($conn->query($restore_qry)===TRUE){

			$response_title = "DOCUMENT RESTORED! ";
			$response = " Document ".$_POST['doc']." restored to ".$_POST['data']['to_stage'];

			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $restore_qry,"Success");

		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $restore_qry,"Failed:".$conn->error);
		}
	}

	if(count(@$_FILES['att']['name'])>0) { //additional attachments

		$atts=$_FILES['att'];

		//start uploading the attachement
		if($_POST['att']['append_type']=='documents' || $_POST['att']['append_type']=='doc_track'){
			if(isset($_POST['data']['doc_id'])){ $att_doc_id = $_POST['data']['doc_id']; } else { $att_doc_id = $psn_last_id; }

			$dc_qr = $conn->query("SELECT created FROM documents WHERE id = '".$att_doc_id."'");
			$dc_qrr = $dc_qr->fetch_assoc();
			$file1 = explode("-",date("Y-m-d", strtotime($dc_qrr['created'])));
			$folder_id = $att_doc_id;

		} else {

			$file1 = explode("-",date("Y-m-d"));
			$folder_id = $psn_last_id;

		}

		$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $_POST['data']['document_type'];

		$uploaddir = $destination_dir."/".$folder_id;

		$dir1="doc_archive/".$file1[0];
		$dir2=$dir1."/".$file1[1];
		$dir3=$dir2."/".$_POST['data']['document_type'];
		$dir4=$dir3."/".$folder_id;

		if(!is_dir($uploaddir)) {

			if(!is_dir($dir1)) {

				if(mkdir($dir1, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir1,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir1,"Failed"); }
				if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Failed"); }
				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir2)) {

				if(mkdir($dir2, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir2,"Failed"); }
				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir3)) {

				if(mkdir($dir3, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir3,"Failed"); }
				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}
			if(!is_dir($dir4)) {

				if(mkdir($dir4, 0755, true)){ sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Success"); } else { sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT","mkdir ".$dir4,"Failed"); }
			}

		}

		//for attachments uploads
		$accepted_file_types = array('jpg','jpeg','gif','png','pdf' );

		foreach($atts['name']['file'] as $file_key=>$file_attr){

			if($file_attr!=""){

				$attch = $file_key;
				$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
				$orig_name = basename($file_attr,".".$file_type);
				$file_name = $file_attr;

				if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
					//upload the attachment
					$ii = 2;
					$attfile = $uploaddir."/".str_replace("'","",$file_name);
					while(file_exists($attfile)){
						$attfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
						$ii++;
					}

					if(move_uploaded_file($atts['tmp_name']['file'][$file_key], $attfile)) {
					  sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", "Uploading ".$atts['tmp_name']['file'][$file_key]." to ".$attfile,"Success");
					  $errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.';
					  //add to the insert statement

					  $ins = "INSERT INTO uploads SET append_type='".$_POST['att']['append_type']."', append_id='".$psn_last_id."', description='".$conn->real_escape_string(trim($_POST['att']['description'][$file_key]))."', file_location='$attfile', created_by='".$_SESSION['user']."'";
					  if($conn->query($ins) === TRUE){
						sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", $ins,"Success");
						$errmsg .= " Info added to db. ";
						} else {
							sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", $ins,"Failed");
						}
					} else {
						sys_log($sys_log_user, $_POST['action'].":SUB PROCESS : ATT", "Uploading ".$atts['tmp_name']['file'][$file_key]." to ".$attfile,"Failed:".$atts['error']['file'][$file_key]);
					}

				} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
			}

		}

	}//end if has additional attachement

 } else {
	$response_icon="times";
	$response_type="danger";
	$response_title ="ERROR!";
	$response = "The system was unable to save your form at this time. We apologise for inconvenience caused. <hr>ERR:".$conn->error."<hr>QRY:".$insert_data;
}

?>

<div class="alert alert-block alert-<?php echo  $response_type; ?>">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	<p>
		<strong>
			<i class="ace-icon fa fa-<?php echo $response_icon; ?>"></i>
			<?php echo $response_title; ?>
		</strong>
		<?php echo $response; ?>
	</p>
</div>
