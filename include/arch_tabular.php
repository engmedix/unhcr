					<div>
						<table id="dynamic-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Date initiated</th>
									<th>Initiated by</th>
									<th>Originating Branch</th>
									<th>Document</th>
									<th>Last Action</th>
									<th>Current Status</th>
									<th>Holder</th>
									<th>Description</th>
								</tr>
							</thead>

							<tbody>

							 <?php

							  $form_query = "SELECT documents.*, admins.fname,admins.lname,admins.oname, job_titles.job_title, dutystations.duty_station FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON documents.sender_id = admins.id LEFT JOIN job_titles ON admins.job_title_id=job_titles.id LEFT JOIN dutystations ON admins.station_id=dutystations.id WHERE doc_track.from_id = $id AND documents.id IS NOT NULL GROUP BY doc_id ORDER BY created desc";
							   $form_result = $conn->query($form_query);
							  $now = new DateTime();
							  $now->setTime(0,0,0);
							  while($form_data = $form_result->fetch_assoc()) {

									$readStatus = '';

									if($form_data['file_status'] == 'DOCUMENT SENT' || $form_data['file_status'] == 'REQUEST SENT') $label = ' label-info"';
									elseif($form_data['file_status'] == 'APPROVED') $label = 'label-success';
									elseif($form_data['file_status'] == 'REJECTED') $label = 'label-danger';
									elseif($form_data['file_status'] == 'FORWARDED') $label = 'label-info';
									elseif($form_data['file_status'] == 'UPDATED') $label = 'label-primary';
									elseif($form_data['file_status'] == 'SUBMITTED') $label = 'label-primary';

									$sentDate = new DateTime($form_data['date']);
									$sentDate->setTime( 0, 0, 0 );

									$diff = $now->diff( $sentDate );

									$periodSpent = $diff->days;
									if($periodSpent == 0) $periodSpent = ' Today';
									elseif($periodSpent == 1 ) $periodSpent = ' Yesterday';
									elseif($periodSpent >= 2) $periodSpent .= ' days ago';

							  ?>
								<tr>
									<td><?php echo date("j M Y - h:i a",strtotime($form_data['created'])).'  <span class="label">'.$periodSpent.'</span>'; ?></td>
									<td><?php echo $form_data['fname'].' '.$form_data['lname'].', '.$form_data['job_title']; ?></td>
									<td><?php echo $form_data['duty_station']; ?></td>
									<td><a href="timeline.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>&dti=<?php echo $form_data['doc_type_id'];?>"><?php echo $form_data['document_type']; ?> - <?php echo $form_data['doc_ref_number']<>""?$form_data['doc_ref_number']:$form_data['internal_ref_number']; ?>
							</a></td>

									<td>
										<a href="document.php?ac=<?php echo $form_data['id']; ?>&md=<?php echo $form_data['capture_method']; ?>&tId=<?php echo $form_data['id']; ?>">
											<span class="label <?php echo $label; ?>"><?php if($form_data['file_status']!='SUBMITTED' && $form_data['file_status']!='CLOSED' && $form_data['file_status']!='UPDATED' && $form_data['file_status']!='FORWARDED'){
												 echo "FORWARDED";
											 }
											else {
												 echo $form_data['file_status'];
											  } ?></span>
										</a>
									</td>
									<td><?php echo $form_data['to_stage']; ?></td>
									<td><?php echo getStaffName($form_data['current_holder']); ?></td>
									<td>
										<?php echo $form_data['description'];  ?>
									</td>


								</tr>
							  <?php } ?>



							</tbody>
						</table>
					</div>
