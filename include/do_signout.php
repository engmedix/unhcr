<?php
session_start();
require_once("connn.php");
require_once("database.php");

$usr_ = getStaff($_SESSION['user']);
$my_ip = $_SERVER['REMOTE_ADDR'];
$my_agent = $_SERVER['HTTP_USER_AGENT'];
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$log_session = "INSERT INTO sessions SET user='".$usr_['fullname']."', ip='$my_ip', agent='$my_agent', action='SIGNOUT', status='SIGNOUT SUCCESSFUL', url='$url'";
$conn->query($log_session);

session_unset();
session_destroy();

echo '<script>window.location.href = "../index.php";</script>'; 

?>