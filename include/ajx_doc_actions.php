<?php
session_start();
require_once("connn.php");
require_once("database.php");


//GEt active delegations
$active_delegs = array();
$delegs = $conn->query("SELECT requested_by,delegate_to,duration FROM delegations WHERE approved='1'");
while($deleg = $delegs->fetch_assoc()) {
	$deleg_strt = date("Y-m-d", strtotime(substr($deleg['duration'], 0, 10)));
	$deleg_end = date("Y-m-d", strtotime(substr($deleg['duration'], 13, 10)));

	if(date("Y-m-d")<=$deleg_end){
		$active_delegs[$deleg['requested_by']]=$deleg['delegate_to'];
	}
}

$docT  = $conn->query("SELECT actions FROM document_types WHERE id='".$_POST['doc_id']."'");
											  $docType = $docT->fetch_assoc();
											  $actions = json_decode($docType['actions'],true);
											  $submit = array_filter_by_value($actions,"type","submit");
											  $submit = $submit[0];  ?>

<input type="hidden" name="data[stage_label]" value="<?php echo $submit['stage_label']; ?>" />
<input type="hidden" name="data[status_label]" value="<?php echo $submit['status_label']; ?>"/>
<input type="hidden" name="data2[to_stage]" value="<?php echo $submit['stage_label']; ?>" />
<input type="hidden" name="data2[to_status]" value="<?php echo $submit['status_label']; ?>"/>

<?php




if($submit["send_type"]=="auto"){

	if( strpos($submit['send_filters'], 'group') !== false ){
		$grp = explode("=",$submit['send_filters']);
		$grp_result = $conn->query("SELECT `members` FROM groups WHERE  id='".$grp[1]."' ");
		$grp_m = $grp_result->fetch_assoc();
		$filter_result = $conn->query("SELECT id FROM admins WHERE  id IN(".$grp_m["members"].") AND status='ACTIVE' ");
	} else {
		$filter_result = $conn->query("SELECT id FROM admins WHERE  department_id=".$submit["send_filters"]." AND status='ACTIVE' ");
	}

	if($filter_result->num_rows==0){ $no_recepient=true; ?>
		<div class="hr hr-dotted"></div>

		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-3 no-padding-right"></label>

			<div class="col-xs-12 col-sm-9">
				<div class="clearfix">

					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
						<strong>
							<i class="ace-icon fa fa-times"></i>
							No Assigned Recepient!
						</strong>
						Contact your admin to set the recepient for the document.
						<br>
					</div>

				</div>
			</div>
		</div>


	<?php } elseif($filter_result->num_rows>1){ $recepients=array(); while($filter_data = $filter_result->fetch_assoc()) { array_push($recepients, $filter_data["id"]); }
		  $whr = " a.id IN(".implode(",",$recepients).") "; ?>

		<div class="hr hr-dotted"></div>

		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-3 no-padding-right"></label>

			<div class="col-xs-12 col-sm-9">
				<div class="clearfix">

					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
						<strong>
							<i class="ace-icon fa fa-times"></i>
							More than one recepient!
						</strong>
						Contact your admin to set the recepient for the invoice. Or select from the list below
						<br>
					</div>

					<div class="clearfix">
						<select  name="data[current_holder]"  class="chosen-select form-control" id="sendto" data-placeholder="select recepient..." required >
							<option value="">  </option>
						<?php $form_query = "SELECT a.*, b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE  $whr AND a.id<>'1' AND a.status='ACTIVE'  ORDER BY fname, lname";
							  $form_result = $conn->query($form_query);
							  while($ro_data = $form_result->fetch_assoc()) {   ?>
							<option value="<?php echo $ro_data['id']; ?>" <?php if(isset($active_delegs[$ro_data['id']])){ echo ' data-delegate="'.$active_delegs[$ro_data['id']].'"'; }?> >
								<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?>
								<?php echo $ro_data['duty_station']<>0?" - ".$ro_data['duty_station']:""; ?>
								<?php echo $ro_data['department_id']<>0?" (". getDept($ro_data['department_id']).")":""; ?>

								<?php if(isset($active_delegs[$ro_data['id']])){
								   echo ' >>Delegated To:'.getStaffName($active_delegs[$ro_data['id']]);
								}?>
							</option>
							<?php } ?>

						</select>
					</div>



				</div>
			</div>
		</div>


	<?php } else { $filter_data = $filter_result->fetch_assoc();	?>
	<div class="form-group">
	<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
	<div class="col-xs-12 col-sm-9">
	<div class="clearfix">
	<div class="well well-sm">
		Sending to: <?php echo getStaffName($filter_data["id"]); ?>
		<?php if(isset($active_delegs[$filter_data["id"]])){
		   echo ' >>Delegated To:'.getStaffName($active_delegs[$filter_data["id"]]);
		   echo '<input type="hidden" name="delegate_to" value="'.$active_delegs[$filter_data["id"]].'" />';
		}?>
	</div>
	<input type="hidden" name="data[current_holder]" value="<?php echo $filter_data["id"]; ?>" />

	</div></div></div>
	<?php } ?>

<?php } else { if(@$submit["send_filters"]<>"*"){ $whr ="a.department_id ='" .$submit["send_filters"]."'"; } else { $whr = " a.id<>'".$_SESSION['user']."'" ; } ?>


	<div class="form-group">
		<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Send To<span class="red">*</span></label>

		<div class="col-xs-12 col-sm-5">
			<div class="clearfix">
				<select name="data[current_holder][]" class="form-control chosen-select" id="sendto" data-placeholder="select recepient..." required >
					<option value="">  </option>
				<?php $form_query = "SELECT a.*, b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE  $whr AND a.id<>'1' AND a.status='ACTIVE'  ORDER BY fname, lname";
					  $form_result = $conn->query($form_query);
					  while($ro_data = $form_result->fetch_assoc()) {   ?>
					<option value="<?php echo $ro_data['id']; ?>" <?php if(isset($active_delegs[$ro_data['id']])){ echo ' data-delegate="'.$active_delegs[$ro_data['id']].'"';	}?> >
						<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?>
						<?php echo $ro_data['duty_station']<>0?" - ".$ro_data['duty_station']:""; ?>
						<?php echo $ro_data['department_id']<>0?" (". getDept($ro_data['department_id']).")":""; ?>
						<?php if(isset($active_delegs[$ro_data['id']])){
							   echo ' >>Delegated To:'.getStaffName($active_delegs[$ro_data['id']]);
						}?>
					</option>
					<?php } ?>

				</select>
			</div>
		</div>
	</div>

<?php } ?>

<div class="form-group form-actions">
	

	<div class="clearfix">
		<div class="col-md-offset-5 col-md-9">
		<?php foreach($actions as $action){
				if($action['type']=="submit" AND isset($no_recepient)){} else { ?>
				<input type="submit" value="<?php echo $action['label']; ?>">
			
		<?php } } ?>

		</div>
	</div>

</div>
