<?php
require_once("connn.php");
require_once("database.php");

$docT  = $conn->query("SELECT * FROM document_types WHERE id='".$POST['doc_id']."'");
$docType = $docT->fetch_assoc();
$actions = json_decode($docType['actions'],true);
$submit = array_filter_by_value($actions,"type","submit");
$submit = $submit[0];

if($submit["send_type"]=="auto"){

	$filter_result = $conn->query("SELECT id FROM admins WHERE  ".$submit["send_filters"]." ");

	if($filter_result->num_rows==0){ $no_recepient=true; ?>
		<div class="hr hr-dotted"></div>

		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-3 no-padding-right"></label>

			<div class="col-xs-12 col-sm-9">
				<div class="clearfix">

					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
						<strong>
							<i class="ace-icon fa fa-times"></i>
							No Assigned Recepient!
						</strong>
						Contact your admin to set the recepient for the document.
						<br>
					</div>

				</div>
			</div>
		</div>


	<?php } elseif($filter_result->num_rows>1){ $recepients=array(); while($filter_data = $filter_result->fetch_assoc()) { array_push($recepients, $filter_data["id"]); }
		  $whr = " a.id IN(".implode(",",$recepients).") "; ?>

		<div class="hr hr-dotted"></div>

		<div class="form-group">
			<label class="control-label col-xs-12 col-sm-3 no-padding-right"></label>

			<div class="col-xs-12 col-sm-9">
				<div class="clearfix">

					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
						<strong>
							<i class="ace-icon fa fa-times"></i>
							More than one recepient!
						</strong>
						Contact your admin to set the recepient for the document. Or select from the list below
						<br>
					</div>

					<div class="clearfix">
						<select  name="data[current_holder]"  class="chosen-select" id="sendto" data-placeholder="select recepient..." required >
							<option value="">  </option>
						<?php $form_query = "SELECT a.*, b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE  $whr ORDER BY fname, lname";
							  $form_result = $conn->query($form_query);
							  while($ro_data = $form_result->fetch_assoc()) {   ?>
							<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php echo $ro_data['id']; ?>">
								<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?>
								<?php echo $ro_data['duty_station']<>0?" - ".$ro_data['duty_station']:""; ?>
								<?php echo $ro_data['department_id']<>0?" (". getDept($ro_data['department_id']).")":""; ?>

								<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
							</option>
							<?php } ?>

						</select>
					</div>



				</div>
			</div>
		</div>


	<?php } else { $filter_data = $filter_result->fetch_assoc();	?>
	<div class="form-group">
	<label class="control-label col-xs-12 col-sm-3 no-padding-right" >&nbsp;</label>
	<div class="col-xs-12 col-sm-9">
	<div class="clearfix">
	<div class="well well-sm"> Sending to: <?php echo getStaffName($filter_data["id"]); ?> </div>
	<input type="hidden" name="data[current_holder]" value="<?php echo $filter_data["id"]; ?>" />
	</div></div></div>
	<?php } ?>

	<?php } else { if(@$submit["send_filters"]<>""){ $whr = $submit["send_filters"]; } else { $whr = " a.id<>'$id' " ; } ?>

	<div class="hr hr-dotted"></div>


	<div class="form-group">
		<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="submitted_by">Send To<span class="red">*</span></label>

		<div class="col-xs-12 col-sm-5">
			<div class="clearfix">
				<select  name="data[current_holder]"  class="chosen-select" id="sendto" data-placeholder="select recepient..." required >
					<option value="">  </option>
				<?php $form_query = "SELECT a.*, b.duty_station FROM admins a LEFT JOIN dutystations b ON a.station_id=b.id WHERE  $whr ORDER BY fname, lname";
					  $form_result = $conn->query($form_query);
					  while($ro_data = $form_result->fetch_assoc()) {   ?>
					<option data-delegate="<?php echo $ro_data['delegate_to']; ?>" value="<?php echo $ro_data['id']; ?>">
						<?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?>
						<?php echo $ro_data['duty_station']<>0?" - ".$ro_data['duty_station']:""; ?>
						<?php echo $ro_data['department_id']<>0?" (". getDept($ro_data['department_id']).")":""; ?>
						<?php if($ro_data['delegate_to']!=0){ echo ' >>Delegated To:'.getStaffName($ro_data['delegate_to']); }  ?>
					</option>
					<?php } ?>

				</select>
			</div>
		</div>
	</div>

	<?php } ?>
