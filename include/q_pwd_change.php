<?php
if(isset($_POST['pwd'])){ //if has existing settings

	include("connn.php");
	include("database.php");

	$my = getStaff($_POST['id']);

	$stmt = $conn->prepare("UPDATE admins SET pwd = ?, pwd_changed =? WHERE id=?");
    $stmt->bind_param('ssi',md5($_POST['pwd']),date('Y-m-d H:i:s'),$_POST['id']);
	if($stmt->execute() === TRUE){

		sys_log($my['fname']." ".$my['lname'],"CHANGE PWD",$update_settings_query,"Success");
		echo "UPDATED";

	} else {

		sys_log($my['fname']." ".$my['lname'],"CHANGE PWD",$update_settings_query,"Failed");
		echo "FAILED";
	}

} //end for loop
?>
