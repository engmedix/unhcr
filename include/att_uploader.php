<?php
$_localfolder_ = '/finca'; ///~htesting/aslm
define('DS','/');
define('ROOT', str_ireplace('\include','',(dirname(__FILE__))));
define('BASE_PATH','http://'.$_SERVER['SERVER_NAME'].$_localfolder_);
define('IMG_PATH',BASE_PATH.DS.'assets/img');

$param = $_GET['form'];
upload($param);

function upload($param = 'accounts'){
		
		//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
		//** Wizecho ajax image/file uploader**//
		
		//******** START SETTINGS ****************//
		
		$base 					= BASE_PATH;
		$up_size 				= 6500; //upload size in KB
		$img_path 				= IMG_PATH.DS."temp/";  //url where the images are stored
		$icons 					= IMG_PATH; //url where icons for uplod are stored - no trailing slash
		
		$allowed_file_types		= array('jpg'=>'fa-file-photo-o',
										'jpeg'=>'fa-file-photo-o',
										'gif'=>'fa-file-photo-o',
										'png'=>'fa-file-photo-o',
										'pdf'=>'fa-file-pdf-o',
										'doc'=>'fa-file-word-o',
										'docx'=>'fa-file-word-o',
										'xls'=>'fa-fa-excel-o',
										'xlsx'=>'fa-fa-excel-o',
										'ppt'=>'fa-file-powerpoint-o',
										'pptx'=>'fa-file-powerpoint-o',
										'txt'=>'fa-file-text-o');
		
		//require_once(JS_PATH.DS.'inc/SimpleImage.class.php');	//used for thumbnails
		
		$use_tmb				= 1; // Use thumb resizing 1 = yes, 0 = no
		
		$tmb_w 					= 600; // Thumb size eg 170px wide 140px height
		$tmb_h 					= 600;
		
		$server 				= 0; // Transfer files to remote server via ftp 1 = yes, 0 = no
		
		$target_ftp_server		= "server ip address"; //eg. 45.987.987.76
		$target_ftp_user		= "user";
		$target_ftp_pass		= "pass";
		$target_ftp_path_thumb	= "/thumbs/"; //directory on remote serer from login via ftp
		
		
		//******* END SETTINGS ********************//
		
		$sr = ROOT;
		$path = $sr.'/assets/img/temp/';
		/*
		$nick = $_POST['nick'];
		
		if(!$nick){
		$nick = 'Guest';
		}else{
		$nick = strip_tags($nick);
		}
		*/
		
		$file_type = explode('/',$_FILES["file"]["type"]);
		
		if (array_key_exists($file_type[1],$allowed_file_types)){
		  
		  if($_FILES["file"]["size"] > ($up_size* 1024)){
			echo "
				<div class='uk-notify uk-notify-top-center'>
					<div class='uk-notify-message uk-notify-message-success'>
						<a class='uk-close'></a>
						<div>
							<i class='uk-icon-warning'></i> Filesize ".($_FILES["file"]["size"] / 1024) ." Kb is too big. Allowable upload size is {$up_size} KB - Please upload a smaller one
						</div>
					</div>
				</div>
				
				";
		  }else{
		  
		  if ($_FILES["file"]["error"] > 0){
		  
		  echo "<i class='fa fa-frown-o red'></i>  Return Code: " . $_FILES["file"]["error"] . "<br />";
		  }else{
		  
		  //echo "<img src ='{$icons}/accept.png'>  Upload: " . $_FILES["file"]["name"] .' <br />'.$path.'<br />'.$sr. "<br />";
		  //echo "<img src ='{$icons}/accept.png'>  Size: " . ($_FILES["file"]["size"] / 1024) ." Kb<br />";
		  //echo "<img src ='{$icons}/accept.png'>  Image Type: " . $_FILES["file"]["type"] . "<br />";
		  
		  $newFileName = time().$_FILES["file"]["name"];
		  $newFileName = strtolower(str_replace(' ', '_', $newFileName));
				   
		   if(move_uploaded_file($_FILES["file"]["tmp_name"], $path . $newFileName)){
				echo "
					<div class='uk-notify uk-notify-top-center'>
						<div class='uk-notify-message uk-notify-message-success'>
							<a class='uk-close'></a>
							<div>
								<i class='fa fa-smile-o blue'></i>  File moved to server.<br />
							</div>
						</div>
					</div>"; 
		   }
		  else{
				echo "
					<div class='uk-notify uk-notify-top-center'>
						<div class='uk-notify-message uk-notify-message-success'>
							<a class='uk-close'></a>
							<div>
								<i class='fa fa-frown-o'></i> Ooops, something happened and we couldn't move file to server
							</div>
						</div>
					</div>"; 
		  }
			
		    
			$newImage = new IMAGE();
		
			$dir = $path;
			$file = $_FILES["file"]["name"];
			
			
			if($file_type[0] == 'image'){
			
				//$image = new SimpleImage();
				$newImage->load($dir.$newFileName);
				
				//skip resizing and renaming gif images
				if ( ($_FILES["file"]["type"]) != 'image/gif' && $use_tmb==1){
				
				$new_file = time().rand().".jpg";
				$newImage->resizeToWidth($tmb_w);
				}else{
				
				$new_file = $newFileName;
				}//end if not gif
						
				$newImage->save($dir.$new_file);
				$newImage->load($dir.$file);
			}
			else{
				$new_file = $newFileName;
			}
				
				if (file_exists($dir.$new_file)) {
		
					if($server == 1){
					
					echo "Sending file..<br>";
					
					// connect to target ftp server
					$target_conn = ftp_connect($target_ftp_server) or die("Could not connect to server try again!");
					ftp_login($target_conn, $target_ftp_user, $target_ftp_pass);
				
					// switch to passive mode if behind a firewall - REMOVE THIS IF YOU RUN INTO PROBLEMS
					ftp_pasv($target_conn, true );
				
					// change to the target path
					ftp_chdir($target_conn, $target_ftp_path_thumb);
				
					// upload the thumbnail
					ftp_put($target_conn, $new_file, $dir.$new_file, FTP_BINARY) or die("There was an error uploading the thumbnail. Please try again");
					
					unlink ($dir.$new_file); //delete the new file from local server
					
					ftp_close($target_conn);
					}//end if server yes
		
				$fieldName = array(
					'services'=>'data[template]',
				);
				echo "<div><i class='fa {$allowed_file_types[$file_type[1]]} blue'></i> <b><em>{$file}</em></b> uploaded successfully. <br></div><br>";	
				echo "<input id=\"imageUrl\" name=\"".$fieldName[$param]."\" type=\"hidden\" value=\"{$new_file}\"/>";
				unlink($dir.$file); //delete the original (full size) file from local server
				
				//**Add some database options here if you want**//
					
									
				} else {
					echo "
						<div class='uk-notify uk-notify-top-center'>
							<div class='uk-notify-message uk-notify-message-success'>
								<a class='uk-close'></a>
								<div>
									<i class='fa fa-frown-o'></i> There was an error creating the thumbnail. Please try again
								</div>
							</div>
						</div>
					";
				}//end if file exists
				
			}//end if file error
			}//end if filesize
			
		}else{
		echo "<div><i class='fa fa-frown-o red'></i> Invalid file</div>";
		}
	}
	
	
	
		
class IMAGE {
	public $image;
    public $image_type;
	//image functions	
		function load($filename) {
		  $image_info = getimagesize($filename);
		  $this->image_type = $image_info[2];
		  if( $this->image_type == IMAGETYPE_JPEG ) {
			 $this->image = imagecreatefromjpeg($filename);
		  } elseif( $this->image_type == IMAGETYPE_GIF ) {
			 $this->image = imagecreatefromgif($filename);
		  } elseif( $this->image_type == IMAGETYPE_PNG ) {
			 $this->image = imagecreatefrompng($filename);
		  }
	   }
	   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
		  if( $image_type == IMAGETYPE_JPEG ) {
			 imagejpeg($this->image,$filename,$compression);
		  } elseif( $image_type == IMAGETYPE_GIF ) {
			 imagegif($this->image,$filename);         
		  } elseif( $image_type == IMAGETYPE_PNG ) {
			 imagepng($this->image,$filename);
		  }   
		  if( $permissions != null) {
			 chmod($filename,$permissions);
		  }
	   }
	   function output($image_type=IMAGETYPE_JPEG) {
		  if( $image_type == IMAGETYPE_JPEG ) {
			 imagejpeg($this->image);
		  } elseif( $image_type == IMAGETYPE_GIF ) {
			 imagegif($this->image);         
		  } elseif( $image_type == IMAGETYPE_PNG ) {
			 imagepng($this->image);
		  }   
	   }
	   function getWidth() {
		  return imagesx($this->image);
	   }
	   function getHeight() {
		  return imagesy($this->image);
	   }
	   function resizeToHeight($height) {
		  $ratio = $height / $this->getHeight();
		  $width = $this->getWidth() * $ratio;
		  $this->resize($width,$height);
	   }
	   function resizeToWidth($width) {
		  $ratio = $width / $this->getWidth();
		  $height = $this->getheight() * $ratio;
		  $this->resize($width,$height);
	   }
	   function scale($scale) {
		  $width = $this->getWidth() * $scale/100;
		  $height = $this->getheight() * $scale/100;
		  $this->resize($width,$height);
	   }
	   function resize($width,$height) {
		  $new_image = imagecreatetruecolor($width, $height);
		  imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		  $this->image = $new_image;   
	   }
}
	

?>
