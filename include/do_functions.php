 <?php
 error_reporting(E_ALL);
 
 if(!function_exists('databaseUpdate')){ include("database.php"); include("connn.php"); }
 
 $dt = date("Y-m-d H:i:s");
 
 if(@$_POST['action']=="TROUBLESHOOT: CLOSE HANGING ITEMS"){
	 
	$fbs = 0;
	 
	foreach($_POST['wf'] as $wf){
		
		$fn_query = "UPDATE doc_track SET receiver_status='ARCHIVED', archive_date=NOW() WHERE id='$wf'";
		
		if($conn->query($fn_query) === TRUE){ 
			
			$fbs++;

		} else {
			echo "<pre>".$fn_query."<hr>".$conn->error."</pre><br>";
		}
	}
	
	if($fbs>0){
		$response_title="WORKFLOWS CLOSED"; $response= $fbs." hanging workflow(s) closed."; $response_icon="check"; $response_type="success";
	} else {
		$response_title="ERROR"; $response= " hanging workflows failed to close."; $response_icon="danger"; $response_type="danger";
	}
	
 }
 
 if(@$_POST['action']=="TROUBLESHOOT: CLOSE DOCUMENT"){
	 
	$fbs = 0;
	 
	foreach($_POST['wf'] as $wf){
		
		$fn_query = "UPDATE doc_track SET receiver_status='ARCHIVED', archive_date=NOW() WHERE id='$wf'";
		
		if($conn->query($fn_query) === TRUE){ 
			
			$fbs++;

		} else {
			echo "<pre>".$fn_query."<hr>".$conn->error."</pre><br>";
		}
	}
	
	if($fbs>0){
		$response_title="WORKFLOWS CLOSED"; $response= $fbs." hanging workflow(s) closed."; $response_icon="check"; $response_type="success";
	} else {
		$response_title="ERROR"; $response= " hanging workflows failed to close."; $response_icon="danger"; $response_type="danger";
	}
	
 }

?>

<div class="alert alert-block alert-<?php echo  $response_type; ?>">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<p>
		<strong>
			<i class="ace-icon fa fa-<?php echo $response_icon; ?>"></i>
			<?php echo $response_title; ?>
		</strong>
		<?php echo $response; ?>
	</p>

	
</div>