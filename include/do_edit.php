 <?php
 date_default_timezone_set("Africa/Lusaka");
 error_reporting(E_ALL);

 if(!function_exists('databaseUpdate')){ include("database.php"); include("connn.php"); }
 
 $dt = date("Y-m-d H:i:s");
 $sys_log_user = $my['fname']." ".$my['lname'];
 
$tbl = $_POST['table'];
$psn_last_id = $_POST['acid'];
if(isset($_POST['f_id'])){ $fld = $_POST['f_id']; } else { $fld = "id"; }

if(isset($_POST['data']['credit'])){ $_POST['data']['credit'] = commaless($_POST['data']['credit']); }
if(isset($_POST['data']['debit'])){ $_POST['data']['debit'] = commaless($_POST['data']['debit']); }

if(@$_POST['action']=="PWD RESET"){
	$_POST['data']['pwd_changed']=$dt;
}
//$_POST['data']['receive_date']=$dt;
$insert_data = databaseUpdate($tbl, $_POST['data'],$fld,$psn_last_id,$dt);

if($conn->query($insert_data) === TRUE){ //if insert query successful
	
	sys_log($my['fname']." ".$my['lname'],$_POST['action'],$insert_data,"Success");
	
	//set success message
	$response = 'RECORD UPDATED SUCCESSFULLY.';
	$response_icon="check"; $response_type="success"; $response_title ="UPDATED!";
	

	
	if(@$_POST['action']=="EDIT DOCTYPE"){
		
		if($_POST['data']['temp_type']=="html"){
			$template = $conn->real_escape_string($_POST['template']['html']);
			$conn->query("UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'");
		}
		
		if($_POST['data']['temp_type']=="form"){
			$temp = $_POST['template']['form'];
			$e = count($temp['label']);
			$elts=array();
			for($i=0;$i<$e;$i++){
				if($temp['label'][$i]!=""){ $elts[$i]=$temp['label'][$i].";".$temp['field'][$i].";".$temp['required'][$i].";".$temp['default'][$i]; }
			}
			$template=implode("$$",$elts);
			$conn->query("UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'");
		}
		
		if($_POST['data']['temp_type']=="spreadsheet"){
			$temp = $_POST['template']['spreadsheet'];
			$ros = array();
			foreach($temp as $temp_ro){
				array_push($ros, implode(",",$temp_ro));
			}
			$template=implode("\n",$ros);
			$conn->query("UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'");
		}
		
		if($_POST['init']!="*"){
			$filters = count($_POST['senders']);
			if($filters==0){
				$conn->query("UPDATE $tbl SET senders='*' WHERE id='$psn_last_id'");
			} else {
				$senders = "";
				$filter = $_POST['senders'];
				for($s=0; $s<$filters;$s++){
					if($filter['field'][$s]!=""){ $senders.=$filter['field'][$s]."=".$filter['value'][$s]." ".$filter['operator'][$s]." "; }
				}
				$conn->query("UPDATE $tbl SET senders='$senders' WHERE id='$psn_last_id'");
			}
		
		}
			
	}
	
	
	if(@$_POST['action']=="DOCUMENT RECEIVED"){
		$conn->query("UPDATE $tbl SET receive_date='$dt' WHERE id='$psn_last_id'");
	}
	
	if(@$_POST['action']=="WORKFLOW ACTION"){
		
		$tbl = 'documents';
		$item = 'id';
		$itemId = $_POST['data']['doc_id'];
		$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
		$_POST['data2']['date_closed'] = $dt;
		
		$updateQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );
		
		if($conn->query($updateQry) === TRUE){
			
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $updateQry,"Success");
			
			$response .= ' <div><em>Document update success.</em></div>';
			$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $_POST['acid'];
			if($conn->query($qry) === TRUE){
				$response .= ' <div><em>Document has been added to your archives.</em></div>';
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Success");
			} else {
				sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $qry,"Failed:".$conn->error);
			}
		} else {
			$response .= ' <div><em>Document update failure.'.$conn->error().'</em></div>';
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $updateQry,"Failed:".$conn->error);
		}
		
	}
	
	if($_POST['action'] == "EDIT GROUP"){
		
		$groups_qry  = "UPDATE `$tbl` SET `members`='".implode(",",$_POST['members'])."' WHERE id='$psn_last_id'";
		if($conn->query($groups_qry)===TRUE){
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Success");
		} else {
			sys_log($sys_log_user, $_POST['action'].":SUB PROCESS", $groups_qry,"Failed:".$conn->error);
		}
	}
	
	if(@$_POST['action']=="EDIT ACCOUNT"){
		
 	//+++++++++++++++++++++++++++++++++++++++++++++++
	//       UPLOAD PROFILE PICTURE
	//start uploading the picture
	
	 //if photo_holder clear
	if(file_exists($_FILES['photo']['tmp_name']) || is_uploaded_file($_FILES['photo']['tmp_name'])) {
    
    $accepted_file_types = array('jpg',
								'gif',
								'png');
	
	$uploaddir = 'accounts/'.$psn_last_id."/photos";
	$old_file_name = $psn_last_id.basename($_FILES['photo']['name']);
	$file_type = strtolower(substr(strrchr($old_file_name,'.'),1));
	$file_name = "photo".$psn_last_id.".".$file_type;
	
	if(in_array($file_type, $accepted_file_types)){ //must be a picture
		
	//upload the picture
	$uploadfile = $uploaddir."/".$file_name;
	if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile)) {
	  $response .= '<br> Passport photo uploaded.'; 
	  //add picture info to the article
	  //$file_loc = $uploaddir.$file_name;
	  $update = "UPDATE accounts SET photo_location='$uploadfile' WHERE id='$psn_last_id'";
	  //$update ="INSERT INTO application_uploads SET location = '$uploadfile', file_description='$file_name', form_id='$id'";
      if ($conn->query($update) === TRUE){ $response .= " Photo Info added to db. "; 	  }
	}
	} else { $response .= '<br> Photo not Uploaded: INVALID FILE TYPE. Must be Image file(JPG/PNG/GIF)'; } //if valid file 
	}//if has attachement  
	else {
		//$update = "UPDATE accounts SET photo_location='' WHERE id='$psn_last_id'";
	  
      //if ($conn->query($update) === TRUE){ $response .= " Removed. "; 	  }
	}
	
	 //if photo_holder clear
	//---------------------------------------------------------

	
	} //if NEW ACCOUNT
	


	
 } else { 
	
	sys_log($my['fname']." ".$my['lname'],$_POST['action'],$insert_data,"Failed");
	
	$response_icon="times"; $response_type="danger"; $response_title ="ERROR!"; $response = "The system was unable to save your form at this time. We apologise for inconvenience caused. <hr>".$conn->error."<hr>".$insert_data; }



?>

<div class="alert alert-block alert-<?php echo  $response_type; ?>">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<p>
		<strong>
			<i class="ace-icon fa fa-<?php echo $response_icon; ?>"></i>
			<?php echo $response_title; ?>
		</strong>
		<?php echo $response; ?>
	</p>

	
</div>