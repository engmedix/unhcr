<?php
//header('Content-Type: application/x-www-form-urlencoded');
require 'mailjet/vendor/autoload.php';
require 'prm.php';
use \Mailjet\Resources;
$email = $_POST['reset_email'];

$stmt = $conn->prepare("SELECT * FROM admins WHERE email=?");
$stmt->bind_param('s',$email);
$stmt->execute();
$email_check = $stmt->get_result();

if(mysqli_num_rows($email_check)>0){ //if email exists

while($r1 = $email_check->fetch_assoc()) { $fn =  $r1["fname"]; }
$dt = date("Y-m-d h:i:sa");
$md5 = substr(md5($dt),0,5); $sha1 = substr(sha1($dt),0,9);

$short_code = strtoupper($md5);

$code = $md5."-".$sha1;

$stmt = $conn->prepare("UPDATE admins SET activation_key=? WHERE email=?");
$stmt->bind_param('ss',$code,$email);
if($stmt->execute()===TRUE){

	$email_name = 'Cinnamon Workflow';
	$from = 'workflow@cinnamonerp.com';

	$to = $email;
	$subject = "Reset Password. Code:".$short_code;

	//////////////////////////////////
	ob_start();
	include('email_resetpwd.php');
	$ob = ob_get_clean();

	$act_link = $site_url.'/login.php?do=reset&code='.$code;

	$replace = array('{FIRST_NAME}', '{RESET_LINK}', '{SHORT_CODE}');
	$with = array($fn, $act_link, $short_code);
	$message = str_replace($replace, $with, $ob);

	//////////////////////////////////
	$headers = "From: ".$email_name." <".strip_tags($from).">\r\n";
	$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion()."\r\n";

	//////////////////////////////////
	#mail($to, $subject, $message, $headers);
	# begin alternative smtp
	$mj = new \Mailjet\Client($x1,$x2);
$body = [
    'FromEmail' => "info@cinnamonerp.com",
    'FromName' => "$email_name",
    'Subject' => "$subject",
    'Text-part' => $message,
    'Html-part' => $message,
    'Recipients' => [
        [
            'Email' => $to
        ]
    ]
];
$response = $mj->post(Resources::$Email, ['body' => $body]);
#$response->success() && var_dump($response->getData());
	# end alternative smtp

	///////////////////////////////////////
	$postdata = http_build_query(
		array(
			'subject' => $subject,
			'from' => $from,
			'msg' => htmlentities($message),
			'to' => $to,
			'email_name' => $email_name
		)
	);
	$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata
		)
	);
	$context  = stream_context_create($opts);
	#$result = file_get_contents('http://hostalite.com/scripts/send_mail.php', false, $context);

	/////////////////////////////////////////////////////


	//echo "<br><hr /><i>Headers</i><br>TO: ".$to."<br>".$headers."<hr /><i>Subject</i><br>".$subject."<hr /><i>Email Format</i><br>".$message;

	echo "<p>Dear ".$fn.";<br><br>A password reset link has been sent to your email. Login to <b>".$to."</b> and click the link to change your password.</p>";

	echo "<p>Alternatively you can enter the 5-digit code sent to your email.</p>";

} else {

    echo "Error: " . $insert_query . "<br>" . $conn->error;
}

} else {

  $invalid_email = $email;

  echo '<div class="alert alert-danger">
			<strong>
				<i class="ace-icon fa fa-times"></i>
				Error!
			</strong>
			You entered an Invalid email!<br>
		</div>';


}// end if email exists
?>
