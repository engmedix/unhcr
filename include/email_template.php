<head>
  <style type="text/css">
 	body {
		width: 100%;
		margin: 0;
		padding: 0;
		-webkit-font-smoothing: antialiased;
	}
	@media only screen and (max-width: 600px) {
		table[class="table-row"] {
			float: none !important;
			width: 98% !important;
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		table[class="table-row-fixed"] {
			float: none !important;
			width: 98% !important;
		}
		table[class="table-col"], table[class="table-col-border"] {
			float: none !important;
			width: 100% !important;
			padding-left: 0 !important;
			padding-right: 0 !important;
			table-layout: fixed;
		}
		td[class="table-col-td"] {
			width: 100% !important;
		}
		table[class="table-col-border"] + table[class="table-col-border"] {
			padding-top: 12px;
			margin-top: 12px;
			border-top: 1px solid #E8E8E8;
		}
		table[class="table-col"] + table[class="table-col"] {
			margin-top: 15px;
		}
		td[class="table-row-td"] {
			padding-left: 0 !important;
			padding-right: 0 !important;
		}
		table[class="navbar-row"] , td[class="navbar-row-td"] {
			width: 100% !important;
		}
		img {
			max-width: 100% !important;
			display: inline !important;
		}
		img[class="pull-right"] {
			float: right;
			margin-left: 11px;
            max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		img[class="pull-left"] {
			float: left;
			margin-right: 11px;
			max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		table[class="table-space"], table[class="header-row"] {
			float: none !important;
			width: 98% !important;
		}
		td[class="header-row-td"] {
			width: 100% !important;
		}
	}
	@media only screen and (max-width: 480px) {
		table[class="table-row"] {
			padding-left: 16px !important;
			padding-right: 16px !important;
		}
	}
	@media only screen and (max-width: 320px) {
		table[class="table-row"] {
			padding-left: 12px !important;
			padding-right: 12px !important;
		}
	}
	@media only screen and (max-width: 458px) {
		td[class="table-td-wrap"] {
			width: 100% !important;
		}
	}
  </style>
 </head>
 <body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
 <br/>
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td align="center" valign="top" style="margin:0;padding:0;">
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="555" bgcolor="#ffffff" style="border:thin solid #cccccc;">
                <tr>
                    <td valign="top" align="left" style="padding-top:27px;padding-right:0;padding-bottom:0;padding-left:0;margin:0;">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" valign="top" style="margin:0;padding:0;">

                                    <table border="0" cellpadding="0" cellspacing="0" align="center" data-editable="image">
                                        <tr>
                                            <td class="header-row-td" width="378" style="font-family: Arial, sans-serif; font-weight: bold; line-height: 19px; color: #b50937; margin: 0px; font-size: 20px; padding-bottom: 10px; padding-top: 15px;" valign="top" align="center">

UNHCR<br>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top" style="margin:0;padding:10px;font-family: Arial, Verdana, Helvetica; font-size:12px;">&nbsp;

					{MESSAGE}

                    </td>
                </tr>

				{DETAILS_LINK}

                <tr>
                    <td align="left" valign="top" style="margin:0;padding:0;">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" data-editable="text">
                            <tr>
                                <td align="left" valign="top" style="padding-top:10px;padding-bottom:40px;padding-left:23px;padding-right:23px;margin:0; text-align:center;">
                                   <span style="font-family:Arial,Verdana,Helvetica;color:#6f6f6f;font-size:14px; font-style:italic;">
                                        Please do not reply to this email.
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="left" style="padding-top:0;padding-left:12px;padding-right:0;padding-bottom:40px;margin:0;">

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
</body>
