<?php 
 define('ROOT', str_ireplace('\include','',(dirname(__FILE__))));

$tbl = $_POST['table'];
$refNumber = '';
$email_name = 'Bayport EDMS';

$insert_data = databaseInsert($tbl, $_POST['data']);

if($conn->query($insert_data) === TRUE){ //if insert query successful
	
	//get last ID
	$psn_last_id = $conn->insert_id;
	
	//set success message
	$response = 'RECORD SAVED SUCCESSFULLY.'.(($tbl == 'documents')?' <br> Reference Number: <strong>'.$refNumber.'</strong>':'');
	$response_icon="check"; $response_type="success"; $response_title ="SAVED!";
	
	if($_POST['action']=="NEW DOC TYPE"){
		
		$actns = '[{ \"type\":\"submit\", \"label\":\"Submit\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"select\", \"send_filters\":\"*\", \"stage_label\":\"Processing\", \"status_label\":\"Submitted\" }]';
		$wflws = '[{ \"type\":\"update\", \"label\":\"Update Status\", \"color\":\"white\", \"icon\":\"\", \"send_type\":\"stay\", \"stage_label\":\"Processing\", \"status_label\":\"Not Started,In Progress,On Hold\" },\r\n{ \"type\":\"forward\", \"label\":\"Review and Forward\", \"color\":\"info\", \"icon\":\"\", \"send_type\":\"stay\", \"stage_label\":\"Processing\", \"status_label\":\"Forwarded\" },\r\n{ \"type\":\"reject\", \"label\":\"Reject & Close\", \"color\":\"danger\", \"icon\":\"\", \"send_type\":\"select\", \"stage_label\":\"Processing\", \"status_label\":\"Rejected\" },\r\n{ \"type\":\"approve\", \"label\":\"Approve & Close\", \"color\":\"success\", \"icon\":\"fa-check\", \"send_type\":\"end\", \"stage_label\":\"Closed\", \"status_label\":\"Complete\" }]';
		
		$update_wf = "UPDATE $tbl SET actions='$actns', workflow='$wflws' WHERE id='$psn_last_id'";
		
		if($conn->query($update_wf) === TRUE){			}
		
		unset($actns); unset($wflws);  unset($update_wf); 
		
	}
	
	if($_POST['action']=="NEW DELEG"){
		if(isset($_POST['data']['approved']) AND $_POST['data']['approved']=='1'){
		
			$dlg_to_id = $_POST['data']['delegate_to'];
			$stff_id = $_POST['data']['requested_by'];

			$update_staff = "UPDATE admins SET delegate_to='$dlg_to_id' WHERE id='$stff_id'";
			if($conn->query($update_staff) === TRUE){			}
		}
	}
	
	if($_POST['action']=="NEW DELEGATE"){
		if(isset($_POST['data']['approved']) AND $_POST['data']['approved']=='1'){
		
			$dlg_to_id = $_POST['data']['delegate_to'];
			$stff_id = $_POST['data']['requested_by'];

			$update_staff = "UPDATE admins SET delegate_to='$dlg_to_id' WHERE id='$stff_id'";
			if($conn->query($update_staff) === TRUE){			}
		}
	}
	
	if($_POST['action']=="NEW DOC"){
	
	//create account directory
	$file1 = explode("-",date("Y-m-d")); 

	$doc_type = $_POST['data']['document_type'];
	$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $doc_type;

	$dir1="doc_archive/".$file1[0];
	$dir2=$dir1."/".$file1[1];
	$dir3=$dir2."/".$doc_type;

	if(!is_dir($destination_dir)) {  
		
		if(!is_dir($dir1)) {
			mkdir($dir1, 0755);
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir2)) {
			mkdir($dir2, 0755);
			mkdir($dir3, 0755);
		}
		if(!is_dir($dir3)) {
			mkdir($dir3, 0755);
		}
	}
	
	//handle template data
	if($_POST['data']['temp_type']=="form"){
		
		$_frm = array();
		
		foreach($_POST['_frm'] as $key=>$val){
			if(is_array($val)){ $_frm[$key]=implode(",",$val); }
			else { $_frm[$key]=$val; }
		}
		
		$frm_data = json_encode($_frm);
		
		$update_frm = "UPDATE documents SET template='$frm_data' WHERE id='$psn_last_id'";
		if ($conn->query($update_frm) === TRUE){  	  } else {  	  }
	}
	
	//handle data from spreadsheet
	if($_POST['data']['temp_type']=="spreadsheet"){
		$temp = $_POST['template']['spreadsheet'];
		$ros = array();
		foreach($temp as $temp_ro){
			array_push($ros, implode(",",commaless($temp_ro)));
		}
		$template=implode("\n",$ros);
		$conn->query("UPDATE documents SET template='$template' WHERE id='$psn_last_id'");
	}
	
	/////////LOAN APPLICATIONS////////////////
	if($_POST['data']['document_type']=="LOAN APPLICATION"){
		
		$_POST['dataL']['document_id'] = $psn_last_id;
		$_POST['dataL']['doc_status'] = $_POST['data']['file_status'];
		$_POST['dataL']['file_number'] = $_POST['data']['doc_ref_number'];
		
		$update = databaseInsert('cstm_loan_applications', $_POST['dataL']);
		
		$errmsg = "<hr/>";
		//if has attachments
		if(count($_FILES['upload'])>0) {
			$uploads=$_FILES['upload'];
			//start uploading the attachement
			$uploaddir = $destination_dir."/".$psn_last_id;
			if(!is_dir($uploaddir)) {
				mkdir($uploaddir, 0755);
			}
			//for attachments uploads
			$accepted_file_types = array('jpg','gif','png','pdf' );
			
			foreach($uploads['name'] as $file_key=>$file_attr){
				
				if($file_attr!=""){
					$attch = $file_key;
					$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
					$orig_name = basename($file_attr,".".$file_type);
					$file_name = $attch.".".$file_type;	
					
					if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
						//upload the attachment
						$ii = 2;
						$uploadfile = $uploaddir."/".$file_attr;
						while(file_exists($uploadfile)){           
							$uploadfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
							$ii++;
						}
						
						if(move_uploaded_file($uploads['tmp_name'][$file_key], $uploadfile)) {
						  $errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.'; 
						  //add to the insert statement
						  $update .= ", $attch='$uploadfile'";
						}
					} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
				}	
				
			}
			
		}//end if has attachement
		
		if ($conn->query($update) === TRUE){ $response .= $errmsg;	  } else { $response .= "DB Update Failed! <hr>".$conn->error."<hr>".$update;  }
		
		
	}
	
	
 	//+++++++++++++++++++++++++++++++++++++++++++++++
	//       UPLOAD PROFILE PICTURE
	//start uploading the picture
	
	if(isset($_FILES['photo']) || file_exists($_FILES['photo']['tmp_name']) || is_uploaded_file($_FILES['photo']['tmp_name'])) {
    
   $accepted_file_types = array('jpg',
								'gif',
								'png',
								'pdf',
								'doc',
								'docx',
								'xls',
								'xlsx',
								'ppt',
								'pptx',
								'txt');
	
	
	$uploaddir = $destination_dir;
	$old_file_name = $psn_last_id.basename($_FILES['photo']['name']);
	$file_type = strtolower(substr(strrchr($old_file_name,'.'),1));
	$file_name = $doc_type."-".$psn_last_id.".".$file_type;
	
	if(in_array($file_type, $accepted_file_types)){ //must be a picture
		
	//upload the picture
	$uploadfile = $uploaddir."/".$file_name;
	if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile)) {
	  $response .= '<br> Document uploaded.'; 
	  //add picture info to the article
	  //$file_loc = $uploaddir.$file_name;
	  $update = "UPDATE documents SET file_location='$uploadfile' WHERE id='$psn_last_id'";
	  //$update ="INSERT INTO application_uploads SET location = '$uploadfile', file_description='$file_name', form_id='$id'";
      if ($conn->query($update) === TRUE){ $response .= " Document Info added to db. "; 	  }
	}
	} else { $response .= '<div class="danger">Attachment not Uploaded: INVALID FILE TYPE. Must be of type (JPG/PNG/GIF/PDF/DOC/DOCX/XLS/XLSX/PPT/PPTX/TXT)</div>'; } //if valid file
	}//if has attachement 
	//---------------------------------------------------------

	/// add track information 
		$tbl = 'doc_track';
		$_POST['data2']['document_type'] = $_POST['data']['document_type'];
		$_POST['data2']['from_id'] = $_POST['data']['from_id'];
		$_POST['data2']['to_id'] = $_POST['data']['current_holder'];
		$_POST['data2']['doc_id'] = $psn_last_id;
		$_POST['data2']['from_stage'] = "INITIATION";
		$_POST['data2']['from_status'] = "INITIATION";
		$_POST['data2']['remark'] = $_POST['data']['description'];
		$_POST['data2']['action'] = 'SUBMITTED';
		$_POST['data2']['receiver_status'] = 'SENT';
		
		$insert_data2 = databaseInsert($tbl, $_POST['data2']);

		if($conn->query($insert_data2) === TRUE){
			$response .= ' <div><em>Your document is now being tracked.</em></div>';
			
			$myTrackId = $conn->insert_id;
			
			$to = getStaffEmail($_POST['data2']['to_id']);
			$from = 'info@cinnamonerp.com';
			$subject = $_POST['data2']['action'].': '.$refNumber.' '.getStaffName($_POST['data2']['from_id']);
			$email_name = 'Bayport EDMS';
			$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;
			
			$notification = 'Dear '.getStaffName($_POST['data2']['to_id']) .'<br>The document listed below requires your attention. Follow the link to get more detail.';
			$doc_ref = getDocType($psn_last_id).": ".$refNumber;
			
			$message = '';
			$message .= '
			<table width="90%" cellpadding="7" align="center" cellspacing="0" >
				<tr><th style="border:solid 1px #ccc">Sender</th><th style="border:solid 1px #ccc">Document type</th><th style="border:solid 1px #ccc">Remarks</th></tr>
				<tr><td style="border:solid 1px #ccc">'.getStaffName($_POST['data2']['from_id']).'</td><td style="border:solid 1px #ccc">'.getDocType($psn_last_id).'</td><td style="border:solid 1px #ccc">'.$_POST['data2']['remark'].'</td></tr>
			</table><br><br>';
			
			sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
			
			
			if(isset($_POST['data']['email']) && $_POST['data']['email'] != ''){
				$tmpQry = $conn->query('SELECT * FROM common_settings WHERE setting = "acknowledgment_message"');
				$tmpRst = $tmpQry->fetch_assoc();
				$template = $tmpRst['value'];
				
				
				$to = $_POST['data']['email'];
				$from = 'info@cinnamonerp.com';
				$subject = 'Bayport EDMS: Document received';
				
				$link = '';
			
				$notification = 'The document has been received.<br>';
				$doc_ref = getDocType($_POST['data2']['doc_id']).": ".$refNumber;
				
				$replace = array('{SENDER}','{DOC_REF_NO}');
				$with = array($_POST['data']['submitted_by'],$refNumber);
				
				$message = str_replace($replace, $with, $template);
			
			
				sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
			}
			
			
			
			
		}
		else{
			$response .= ' <div><em>Could not add document to track list.'.$conn->error.'</em></div>';
		}
		
		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			$_POST['data2']['from_id'] = $_POST['data']['current_holder'];
			$_POST['data2']['to_id'] = $_POST['delegate_to'];
			$_POST['data2']['action'] = 'DELEGATED';
			
			$insert_data2 = databaseInsert($tbl, $_POST['data2']);

			if($conn->query($insert_data2) === TRUE){
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				
				$myTrackId = $conn->insert_id;
				
				
				$to = getStaffEmail($_POST['data2']['to_id']);
				$from = 'info@cinnamonerp.com';
				$subject = $_POST['data2']['action'].': '.$refNumber.' '.getStaffName($_POST['data2']['from_id']);
				$email_name = 'Bayport EDMS';
				$link = $site_url.'/document.php?ac='.$_POST['data2']['doc_id'].'&md=hc&tId='.$myTrackId;
				
				$notification = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$doc_ref = getDocType($_POST['data2']['doc_id']).": ".$refNumber;
				
				$message = '';
				$message .= '
				<table width="100%" cellpadding="7" cellspacing="0" border="1">
					<tr><th>Sender</th><th>Document type</th><th>Summary</th></tr>
					<tr><td>'.getStaffName($_POST['data2']['from_id']).'</td><td>'.getDocType($_POST['data2']['doc_id']).'</td><td>'.$_POST['data2']['remark'].'</td></tr>
				</table>
				
				
				';
				
				sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
				
				
			}
			else{
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->error.'</em></div>';
			}
			
			
			//update the current holder in the documents table
			$tbl = 'documents';
			$item = 'id';
			$itemId = $_POST['data2']['doc_id'];
			unset($_POST['data']['internal_ref_number']);
			
			$_POST['data']['current_holder'] = $_POST['data2']['to_id'];
			$_POST['data']['file_status'] = 'DELEGATED';
			
			$updateQry = databaseUpdate($tbl, $_POST['data'], $item, $itemId );
			
			if($conn->query($updateQry) === TRUE){
				$response .= ' <div><em>Document update success.</em></div>';
				
				//update previous track record to the archives
				$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $myTrackId;
				if($conn->query($qry) === TRUE){
					$response .= ' ';
				}
			}
			else{
				$response .= ' <div><em>Document update failure.'.$conn->error.'</em></div>';
			}
			
		}
		
		
	/////////////// end track information
	
	} //if NEW DOC
	
	
	if($_POST['action']=="ADD STAGE"){
		
		if($_POST['data']['temp_type']=="form"){
			$temp = $_POST['template']['form'];
			$e = count($temp['label']);
			$elts=array();
			for($i=0;$i<$e;$i++){
				if($temp['label'][$i]!=""){ $elts[$i]=$temp['label'][$i].";".$temp['field'][$i].";".$temp['required'][$i].";".$temp['default'][$i]; }
			}
			$template=implode("$$",$elts);
			$conn->query("UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'");
		}
		
		if($_POST['data']['temp_type']=="spreadsheet"){
			$temp = $_POST['template']['spreadsheet'];
			$ros = array();
			foreach($temp as $temp_ro){
				array_push($ros, implode(",",$temp_ro));
			}
			$template=implode("\n",$ros);
			$conn->query("UPDATE $tbl SET template='$template' WHERE id='$psn_last_id'");
		}
			
	}
	
	if($_POST['action'] == "NEW ACTION"){
		
		$myTrackId = $psn_last_id;
		
		$refNumber = getRefNo($_POST['data']['doc_id']);
		
		$to = getStaffEmail($_POST['data']['to_id']);
		$from = 'info@cinnamonerp.com';
		$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
		$email_name = 'Bayport EDMS';
		$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
		
		$notification = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
		$doc_ref = getDocType($_POST['data']['doc_id']).": ".$refNumber;
		
		$message = '';
		$message .= '
		<table width="100%" cellpadding="7" cellspacing="0" border="1">
			<tr><th>Sender</th><th>Document type</th><th>Summary</th></tr>
			<tr><td>'.getStaffName($_POST['data']['from_id']).'</td><td>'.getDocType($_POST['data']['doc_id']).'</td><td>'.$_POST['data']['remark'].'</td></tr>
		</table>
		';
		
		sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
			
		$tbl = 'documents';
		$item = 'id';
		$itemId = $_POST['data']['doc_id'];
		
		$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
		
		$updateQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );
		
		if($conn->query($updateQry) === TRUE){
			$response .= ' <div><em>Document update success.</em></div>';
			$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $_POST['tId'];
			if($conn->query($qry) === TRUE){
				$response .= ' <div><em>Document has been added to your archives.</em></div>';
			}
		}
		else{
			$response .= ' <div><em>Document update failure.'.$conn->error().'</em></div>';
		}
		
		if(isset($_POST['delegate_to']) && $_POST['delegate_to'] != ''){
			//create new record for the delegate into the doc_track table
			$tbl = 'doc_track';
			$_POST['data']['from_id'] = $_POST['data']['to_id'];
			$_POST['data']['to_id'] = $_POST['delegate_to'];
			$_POST['data']['action'] = 'DELEGATED';
			
			$insert_data = databaseInsert($tbl, $_POST['data']);

			if($conn->query($insert_data) === TRUE){
				$response .= ' <div><em>Please note this action was delegated to </em>'.getStaffName($_POST['delegate_to']).'</div>';
				$myTrackId = $conn->insert_id;
				
				$refNumber = getRefNo($_POST['data']['doc_id']);
				
				$to = getStaffEmail($_POST['data']['to_id']);
				$from = 'info@cinnamonerp.com';
				$subject = $_POST['data']['action'].': '.$refNumber.' '.getStaffName($_POST['data']['from_id']);
				$email_name = 'Bayport EDMS';
				$link = $site_url.'/document.php?ac='.$_POST['data']['doc_id'].'&md=hc&tId='.$myTrackId;
				
				$notification = 'The document listed below has been '.strtolower($_POST['data']['action']).'. Follow the link to get more detail.';
				$doc_ref = getDocType($_POST['data']['doc_id']).": ".$refNumber;
				
				$message = '';
				$message .= '
				
				<table width="100%" cellpadding="7" cellspacing="0" border="1">
					<tr><th>Sender</th><th>Document type</th><th>Summary</th></tr>
					<tr><td>'.getStaffName($_POST['data']['from_id']).'</td><td>'.getDocType($_POST['data']['doc_id']).'</td><td>'.$_POST['data']['remark'].'</td></tr>
				</table>
				
			
				';
				
				sendemail($to,$from,$subject,$message,$email_name,$link,$notification,$doc_ref);
			}
			else{
				$response .= ' <div><em>Could not be forwarded to the delegated person.'.$conn->mysqli_error().'</em></div>';
			}
			
			//update the current holder in the documents table
			$tbl = 'documents';
			$item = 'id';
			$itemId = $_POST['data']['doc_id'];
			
			$_POST['data2']['current_holder'] = $_POST['data']['to_id'];
			$_POST['data2']['file_status'] = 'DELEGATED';
			
			$updateQry = databaseUpdate($tbl, $_POST['data2'], $item, $itemId );
			
			if($conn->query($updateQry) === TRUE){
				$response .= ' <div><em>Document update success.</em></div>';
				
				//update previous track record to the archives
				$qry = "UPDATE doc_track SET receiver_status = 'ARCHIVED', archive_date = NOW() WHERE id =". $myTrackId;
				if($conn->query($qry) === TRUE){
					$response .= ' ';
				}
			}
			else{
				$response .= ' <div class="alert danger"><em>Document update failure.'.$conn->mysqli_error().'</em></div>';
			}
			
		}
		
	}
	
	if(count($_FILES['att']['name'])>0) { //additional attachments
	
		$atts=$_FILES['att'];
		
		//start uploading the attachement
		if($_POST['att']['append_type']=='documents'){
			$file1 = explode("-",date("Y-m-d")); 
			$folder_id = $psn_last_id;
		} else {
			$dc_qr = $conn->query("SELECT created FROM documents WHERE id = '".$_POST['data']['doc_id']."'");
			$dc_qrr = $dc_qr->fetch_assoc();
			$file1 = explode("-",date("Y-m-d", strtotime($dc_qrr['created']))); 
			$folder_id = $_POST['data']['doc_id'];
		}
		
		$destination_dir = "doc_archive/".$file1[0] ."/". $file1[1]."/". $_POST['data']['document_type'];
		$uploaddir = $destination_dir."/".$folder_id;
		
		if(!is_dir($uploaddir)) {
			mkdir($uploaddir, 0755);
		}
		//for attachments uploads
		$accepted_file_types = array('jpg','gif','png','pdf' );
		
		foreach($atts['name']['file'] as $file_key=>$file_attr){
			
			if($file_attr!=""){
				
				$attch = $file_key;
				$file_type = strtolower(substr(strrchr(basename($file_attr),'.'),1));
				$orig_name = basename($file_attr,".".$file_type);
				$file_name = $file_attr;	
				
				if(in_array($file_type, $accepted_file_types)){ //must be accepted file type
					//upload the attachment
					$ii = 2;
					$attfile = $uploaddir."/".$file_name;
					while(file_exists($attfile)){           
						$attfile = $uploaddir."/".$orig_name."(".$ii.").".$file_type;
						$ii++;
					}
					
					if(move_uploaded_file($atts['tmp_name']['file'][$file_key], $attfile)) {
					  $errmsg .= '<br><i class="ace-icon fa fa-check"></i> '.str_replace("_"," ",$file_key).' uploaded.'; 
					  //add to the insert statement
					  $ins = "INSERT INTO uploads SET append_type='".$_POST['att']['append_type']."', append_id='".$psn_last_id."', description='".$_POST['att']['description'][$file_key]."', file_location='$attfile', created_by='".$_SESSION['user']."'";
					  if($conn->query($ins) === TRUE){ $errmsg .= " Info added to db. ";	  }
					}
				} else { $errmsg .= '<br><i class="ace-icon fa fa-times"></i> File not Uploaded: INVALID FILE TYPE:'.$file_type.''; } //if valid file
			}	
			
		}
		
	}//end if has additional attachement
	
 } else { 
	$response_icon="times"; 
	$response_type="danger"; 
	$response_title ="ERROR!"; 
	$response = "The system was unable to save your form at this time. We apologise for inconvenience caused. <hr>ERR:".$conn->error."<hr>QRY:".$insert_data; 
}

?>

<div class="alert alert-block alert-<?php echo  $response_type; ?>">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	<p>
		<strong>
			<i class="ace-icon fa fa-<?php echo $response_icon; ?>"></i>
			<?php echo $response_title; ?>
		</strong>
		<?php echo $response; ?>
	</p>
</div>

										
						
								