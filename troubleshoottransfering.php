<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Troubleshooting</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		
		<link rel="stylesheet" href="assets/plugins/jconfirm/css/jquery-confirm.css" type="text/css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		#searchFrm input, #searchFrm select {width:100%;}
		.profile-info-value,.profile-user-info-striped .profile-info-value{padding:0;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		
		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<div class="col-xs-12 col-sm-12 widget-container-col">
										<?php if(isset($_GET['track'])){ include("include/do_insert.php"); } ?>
										<div class="widget-box ">
											<!-- #section:custom/widget-box.options -->
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">
													<i class="ace-icon fa fa-table"></i>
													Troubleshoot: Transfer Documents
												</h5>
											</div>

											<!-- /section:custom/widget-box.options -->
											<div class="widget-body">
												<div class="widget-main no-padding">
													
													<div>
														<form class="form-horizontal validation-form" action="?search" method="post">
															<input type="hidden" name="search" />
														<div class="input-group">
															<span class="input-group-addon">
																Select Inbox
															</span>
															
															<select  name="staff"  class=" form-control chosen-select" data-placeholder="select staff...">
																<option value="">  </option>
																<?php $form_query = "SELECT * FROM admins ORDER BY fname"; 
																  $form_result = $conn->query($form_query);
																  while($ro_data = $form_result->fetch_assoc()) {   ?>	
																<option value="<?php echo $ro_data['id']; ?>" <?= @$_POST['staff']==$ro_data['id']?"selected":""; ?> >
																	<?php echo $ro_data['fname']." ".$ro_data['lname']; ?>
																</option>
																<?php } ?>
															</select>
															
															<span class="input-group-btn">
																<button type="submit" class="btn btn-purple btn-sm search-btn" >
																	<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																	Open
																</button>
															</span>
													
														</div>
														
														</form>
													</div>
													
												</div>
											</div>
											
											<?php if(isset($_GET['new'])){ include('include/do_insert.php');  } ?>
											<?php if(isset($_GET['update'])){ include('include/do_edit.php');  } ?>
											<?php if(isset($_GET['fn'])){ include('include/do_functions.php');  } ?>
											
											<?php if(isset($_POST['search'])){ ?>
											
											<div>
																						
											<div class="space-8"></div>
											
											<div id="faq-list-2" class="panel-group">
											
												<div class="panel panel-default">
													
													<div class="panel-heading">
														<h5><?php echo "Transfer  documents from ". getStaffName($_POST['staff']); ?></h5> 															
													</div>
												
													<div class="panel-body">
														
														<table class="table table-striped table-bordered">
															
															<tbody>	
																<tr>
																	<td colspan="2">
																	<table align="center" width="90%">
																		<?php 
																		$staff_q=$conn->query("SELECT job_title_id FROM admins WHERE id='".$_POST['staff']."'");
																		$staff = $staff_q->fetch_assoc();  
																		
																		$wf_query = "SELECT doc_track.*,documents.document_type,documents.doc_type_id,documents.doc_ref_number,documents.stage_label,documents.internal_ref_number,documents.capture_method, admins.fname,admins.lname,admins.oname FROM doc_track LEFT JOIN documents ON doc_track.doc_id = documents.id LEFT JOIN admins ON doc_track.from_id = admins.id WHERE to_id = '".$_POST["staff"]."' AND receiver_status <> 'ARCHIVED' AND documents.current_holder='".$_POST["staff"]."' AND documents.stage_label<>'CLOSED'";
																		$wf_result = $conn->query($wf_query);
																		$wfs = $wf_result->num_rows;  
																		if($wfs>0){
																		?>
																		
																		
																		<form action="?fn" method="post">
																			<input type="hidden" name="action" value="TROUBLESHOOT: TRANSFER INBOX" />
																			<input type="hidden" name="staff" value="<?= @$_REQUEST['staff']; ?>" />																			
																			<input type="hidden" name="doc_type" value="<?= @$_REQUEST['doc_type']; ?>" />																			
																			<input type="hidden" name="search" value="search" />																			
																		<tr>
																			<td align="right" style="padding-right:5px"></td>
																			<td colspan="2"><?php if($wfs>3){?><label><input type="checkbox" class="all_wf" >select all (<?= $wfs; ?>)</label>   <?php } ?> </td>
																		</tr>
																		<tr>
																			<td colspan="2" style="padding:10px"> 
																			
																			<div class="wfs" >
																			
																				<?php
																					$i=1;																		
																					while($wf_data = $wf_result->fetch_assoc()) { 
																						echo '<label style="font-size:13px"><input type="checkbox" value="'.$wf_data['id'].'" name="wf[]" class="close-item" /> <b>['.$i.']</b> '.$wf_data['actionDate'].' | '.strtoupper($wf_data['document_type']).' | '.strtoupper($wf_data['doc_ref_number']).' | Holder:'.getStaffName($wf_data['to_id']).' | Stage: '.$wf_data['to_stage'].' | From: '.getStaffName($wf_data['from_id']).'</label>';
																						echo "<br>";
																						$i++;
																					} 
																				?>
																			</div>
																			
																			</td>
																		</tr>
																		<tr>
																			<td align="right" style="padding-right:5px"></td>
																			<td colspan="2"> &nbsp; </td>
																		</tr>
																		
																		<tr>
																			<td align="right" style="padding-right:5px"></td>
																			<td colspan="2"> 
																				<div class="form-group">
																					<label class="control-label">Transfer To:</label>
																					<select  name="transfer"  class=" form-control chosen-select" data-placeholder="select staff..." required>
																						<option value="">  </option>
																						<?php $form_query = "SELECT * FROM admins WHERE id<>'".$_SESSION['user']."' AND id<>'".$_POST['staff']."' ORDER BY fname"; 
																						  $form_result = $conn->query($form_query);
																						  while($ro_data = $form_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $ro_data['id']; ?>" <?= @$_POST['staff']==$ro_data['id']?"selected":""; ?> >
																							<?php echo $ro_data['fname']." ".$ro_data['lname']; ?>
																						</option>
																						<?php } ?>
																					</select>
																				
																				</div>
																			</td>
																		</tr>
																		
																		<tr>
																			<td align="right" style="padding-right:5px"></td>
																			<td colspan="2"> 
																				<div class="form-group">
																					<label class="control-label">Reason:</label>
																					<textarea class="form-control" name="reason" required></textarea>
																				
																				</div>
																			</td>
																		</tr>
																		
																		<tr>
																			<td align="right" style="padding-right:5px"></td>
																			<td colspan="2"><label> <button disabled class="btn btn-success btn-minier sbmt">Transfer Selected Items</button>  </td>
																		</tr>
																		
																		
																		</form>
																		
																		<?php } ?>
																		
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>																							
											</div>
											
											
										</div>
										<?php } ?>
										</div>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
                        
                        <div id="search-results"></div>
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/jquery.validate.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		
		<!-- ace scripts -->
		<script src="assets/plugins/jconfirm/js/jquery-confirm.js" ></script>
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			
			});
			
			//$('.chosen-select').chosen({allow_single_deselect:true}); 
			
			$('a.accordion-toggle').on('click', function () { 
				
				var link = $(this);
				var paneldiv = link.closest('div.panel');
				var bodydiv = paneldiv.find('div.panel-collapse');
				
				//console.log(bodydiv.attr("id"));
				
				if(link.hasClass("collapsed")){
					//bodydiv.find('select.chosen-select').chosen({allow_single_deselect:true}); 	
				} else {
					//bodydiv.find('select.chosen-select').chosen({allow_single_deselect:true}); 	
				}
			});

			
			$('body').on('click','.search-btn', function(e){
				e.preventDefault();
				var frm = $(this).closest("form");
				if($('.search-query').val()==""){
					$.alert('<b>REQUIRED FIELD</b><br> Please enter the reference number of the document you want to troubleshoot');
				} else {
					frm.submit();
				}
				
			});
			
			$('body').on('change','.close-item', function(e){
				var chks = $('.close-item:checked').length;
				if(chks>0){
					$('button.sbmt').prop('disabled', false);
				} else {
					$('button.sbmt').prop('disabled', true);
				}
			});
			
			$(".all_wf").click(function () {
				$('input:checkbox').not(this).prop('checked', this.checked);
			 });
			
			
		</script>

	</body>
</html>
