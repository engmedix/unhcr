<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php"); 
include("include/session_tracker.php");
if(isset($_GET['staff'])){ $staff=$_GET['staff']; } else { $staff=$myid; }
$profile = getStaff($staff);
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Staff Profile - <?php echo $profile['fname']." ".$profile['lname']." ".$profile['oname'];?></title>

		<meta name="description" content="3 styles with inline editable feature" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="assets/css/jquery-ui.custom.css" />
		<link rel="stylesheet" href="assets/css/jquery.gritter.css" /> 	
		<link rel="stylesheet" href="assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="assets/css/bootstrap-editable.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
					
						<!-- #section:settings.box -->
						<?php if(isset($_GET['delegate'])){ include("include/do_insert.php"); } ?>
						<?php if(isset($_GET['edit'])){ include("include/do_edit.php"); } ?>
						<!-- /section:settings.box -->
						<div class="page-header">
							<h1>
								Staff Profile
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									<?php echo $profile['fname']." ".$profile['lname']." ".$profile['oname'];?>
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								
								<div >
									<div id="user-profile-2" class="user-profile">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-18">
												<li class="hide">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-line-chart bigger-120"></i>
														Performance
													</a>
												</li>

												<li <?= (isset($_GET['delegate']))?'':'class="active"'; ?> >
													<a data-toggle="tab" href="#feed">
														<i class="orange ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-cogs bigger-120"></i>
														Preferences
													</a>
												</li>

												<li <?= (isset($_GET['delegate']))?'class="active"':''; ?> >
													<a data-toggle="tab" href="#pictures">
														<i class="pink ace-icon fa fa-exchange bigger-120"></i>
														Delegation
													</a>
												</li>
											</ul>

											<div class="tab-content no-border padding-24">
												<div id="home" class="tab-pane">
													
													<div class="row">
														<div class="col-xs-12 col-sm-7">
															<div class="widget-box transparent">
																<div class="widget-header widget-header-small">
																	<h4 class="widget-title smaller">
																		<i class="ace-icon fa fa-check-square-o bigger-110"></i>
																		Overall
																	</h4>
																</div>

																<div class="widget-body">
																	<div class="widget-main infobox-container">
																		
																		
																		
																		
																			<!-- #section:pages/dashboard.infobox -->
																			<div class="infobox infobox-green">
																				<div class="infobox-icon">
																					<i class="ace-icon fa fa-comments"></i>
																				</div>

																				<div class="infobox-data">
																					<span class="infobox-data-number">32</span>
																					<div class="infobox-content">reviews</div>
																				</div>

																				<!-- #section:pages/dashboard.infobox.stat -->
																				<div class="stat stat-success">8%</div>

																				<!-- /section:pages/dashboard.infobox.stat -->
																			</div>

																			<div class="infobox infobox-blue">
																				<div class="infobox-icon">
																					<i class="ace-icon fa fa-twitter"></i>
																				</div>

																				<div class="infobox-data">
																					<span class="infobox-data-number">11</span>
																					<div class="infobox-content">new followers</div>
																				</div>

																				<div class="badge badge-success">
																					+32%
																					<i class="ace-icon fa fa-arrow-up"></i>
																				</div>
																			</div>

																			<div class="infobox infobox-pink">
																				<div class="infobox-icon">
																					<i class="ace-icon fa fa-shopping-cart"></i>
																				</div>

																				<div class="infobox-data">
																					<span class="infobox-data-number">8</span>
																					<div class="infobox-content">new orders</div>
																				</div>
																				<div class="stat stat-important">4%</div>
																			</div>

																			<div class="infobox infobox-red">
																				<div class="infobox-icon">
																					<i class="ace-icon fa fa-flask"></i>
																				</div>

																				<div class="infobox-data">
																					<span class="infobox-data-number">7</span>
																					<div class="infobox-content">experiments</div>
																				</div>
																			</div>

																			<div class="infobox infobox-orange2">
																				<!-- #section:pages/dashboard.infobox.sparkline -->
																				<div class="infobox-chart">
																					<span class="sparkline" data-values="196,128,202,177,154,94,100,170,224"><canvas width="44" height="33" style="display: inline-block; width: 44px; height: 33px; vertical-align: top;"></canvas></span>
																				</div>

																				<!-- /section:pages/dashboard.infobox.sparkline -->
																				<div class="infobox-data">
																					<span class="infobox-data-number">6,251</span>
																					<div class="infobox-content">pageviews</div>
																				</div>

																				<div class="badge badge-success">
																					7.2%
																					<i class="ace-icon fa fa-arrow-up"></i>
																				</div>
																			</div>

																			<div class="infobox infobox-blue2">
																				<div class="infobox-progress">
																					<!-- #section:pages/dashboard.infobox.easypiechart -->
																					<div class="easy-pie-chart percentage" data-percent="42" data-size="46" style="height: 46px; width: 46px; line-height: 45px;">
																						<span class="percent">42</span>%
																					<canvas height="46" width="46"></canvas></div>

																					<!-- /section:pages/dashboard.infobox.easypiechart -->
																				</div>

																				<div class="infobox-data">
																					<span class="infobox-text">traffic used</span>

																					<div class="infobox-content">
																						<span class="bigger-110">~</span>
																						58GB remaining
																					</div>
																				</div>
																			</div>

																			<!-- /section:pages/dashboard.infobox -->
																			<div class="space-6"></div>

																			<!-- #section:pages/dashboard.infobox.dark -->
																			<div class="infobox infobox-green infobox-small infobox-dark">
																				<div class="infobox-progress">
																					<!-- #section:pages/dashboard.infobox.easypiechart -->
																					<div class="easy-pie-chart percentage" data-percent="61" data-size="39" style="height: 39px; width: 39px; line-height: 38px;">
																						<span class="percent">61</span>%
																					<canvas height="39" width="39"></canvas></div>

																					<!-- /section:pages/dashboard.infobox.easypiechart -->
																				</div>

																				<div class="infobox-data">
																					<div class="infobox-content">Task</div>
																					<div class="infobox-content">Completion</div>
																				</div>
																			</div>

																			<div class="infobox infobox-blue infobox-small infobox-dark">
																				<!-- #section:pages/dashboard.infobox.sparkline -->
																				<div class="infobox-chart">
																					<span class="sparkline" data-values="3,4,2,3,4,4,2,2"><canvas width="39" height="19" style="display: inline-block; width: 39px; height: 19px; vertical-align: top;"></canvas></span>
																				</div>

																				<!-- /section:pages/dashboard.infobox.sparkline -->
																				<div class="infobox-data">
																					<div class="infobox-content">Earnings</div>
																					<div class="infobox-content">$32,000</div>
																				</div>
																			</div>

																			<div class="infobox infobox-grey infobox-small infobox-dark">
																				<div class="infobox-icon">
																					<i class="ace-icon fa fa-download"></i>
																				</div>

																				<div class="infobox-data">
																					<div class="infobox-content">Downloads</div>
																					<div class="infobox-content">1,205</div>
																				</div>
																			</div>

																			<!-- /section:pages/dashboard.infobox.dark -->
																		
																		
																		
																		
																		
																		
																		
																		
																		
																		
																	</div>
																</div>
															</div>
														</div>

														<div class="col-xs-12 col-sm-5">
															<div class="widget-box transparent">
																<div class="widget-header widget-header-small header-color-blue2">
																	<h4 class="widget-title smaller">
																		<i class="ace-icon fa fa-lightbulb-o bigger-120"></i>
																		This Month
																	</h4>
																</div>

																<div class="widget-body">
																	<div class="widget-main padding-16">
																		<div class="clearfix">
																			<div class="grid3 center">
																				<!-- #section:plugins/charts.easypiechart -->
																				<div class="easy-pie-chart percentage" data-percent="45" data-color="#CA5952">
																					<span class="percent">45</span>
																				</div>

																				<!-- /section:plugins/charts.easypiechart -->
																				<div class="space-2"></div>
																				Initiated
																			</div>

																			<div class="grid3 center">
																				<div class="center easy-pie-chart percentage" data-percent="90" data-color="#59A84B">
																					<span class="percent">90</span>
																				</div>

																				<div class="space-2"></div>
																				Received
																			</div>

																			<div class="grid3 center">
																				<div class="center easy-pie-chart percentage" data-percent="80" data-color="#9585BF">
																					<span class="percent">80</span>
																				</div>

																				<div class="space-2"></div>
																				Incomplete
																			</div>
																		</div>

																		<div class="hr hr-16"></div>

																		<!-- #section:pages/profile.skill-progress -->
																		<div class="profile-skills">
																			<div class="progress">
																				<div class="progress-bar" style="width:80%">
																					<span class="pull-left">HTML5 & CSS3</span>
																					<span class="pull-right">80%</span>
																				</div>
																			</div>

																			<div class="progress">
																				<div class="progress-bar progress-bar-success" style="width:72%">
																					<span class="pull-left">Javascript & jQuery</span>

																					<span class="pull-right">72%</span>
																				</div>
																			</div>

																			<div class="progress">
																				<div class="progress-bar progress-bar-purple" style="width:70%">
																					<span class="pull-left">PHP & MySQL</span>

																					<span class="pull-right">70%</span>
																				</div>
																			</div>

																			<div class="progress">
																				<div class="progress-bar progress-bar-warning" style="width:50%">
																					<span class="pull-left">Wordpress</span>

																					<span class="pull-right">50%</span>
																				</div>
																			</div>

																			<div class="progress">
																				<div class="progress-bar progress-bar-danger" style="width:38%">
																					<span class="pull-left">Photoshop</span>

																					<span class="pull-right">38%</span>
																				</div>
																			</div>
																		</div>

																		<!-- /section:pages/profile.skill-progress -->
																	</div>
																</div>
															</div>
														</div>
													</div>
													

													<div class="space-20"></div>

													
												</div><!-- /#home -->

												<div id="feed" class="tab-pane in active">
													
													<div class="row">
														
														<div class="col-xs-12 col-sm-9">
															<h4 class="blue">
																<span class="middle"><?php echo $profile['fname']." ".$profile['lname']." ".$profile['oname'];?></span>
															</h4>

															<div class="profile-user-info">
																<div class="profile-info-row">
																	<div class="profile-info-name"> Duty Station </div>

																	<div class="profile-info-value">
																		<i class="fa fa-map-marker light-orange bigger-110"></i>
																		<span><?php echo getBranch($profile['station_id']); ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Department </div>

																	<div class="profile-info-value">
																		<span><?php echo getDept($profile['department_id']); ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Job Title </div>

																	<div class="profile-info-value">
																		<span><?php echo getTitle($profile['job_title_id']); ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Email </div>

																	<div class="profile-info-value">
																		<span><?php echo $profile['email']; ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Phone </div>

																	<div class="profile-info-value">
																		<span><?php echo $profile['phone']; ?></span>
																	</div>
																</div>
																
																<div class="profile-info-row">
																	<div class="profile-info-name"> Last Login </div>

																	<div class="profile-info-value">
																		<span><?php echo $profile['last_login']; ?></span>
																	</div>
																</div>
																
															</div>

															

															
														</div><!-- /.col -->
													</div><!-- /.row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed -->

												<div id="friends" class="tab-pane">
													<!-- #section:pages/profile.friends -->
													<div class="profile-feed row">
														<div class="col-sm-6">
															<div class="profile-activity clearfix">
																<div>
																	<p>Enable instant email alerts to receive an email whenever a new document is assigned to you.</p>
													
																	<label>
																		<input name="email_alerts" class="ace ace-switch ace-switch-1" type="checkbox">
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Instant Email Alerts</span>
																	</label>
																</div>

										
															</div>

															<div class="profile-activity clearfix">
																<div>	
																	<p>Enable daily summary email to receive compilation of the days activies.</p>
													
																	<label>
																		<input name="email_alerts" class="ace ace-switch ace-switch-1" type="checkbox">
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Daily Summary Email</span>
																	</label>
																</div>
															</div>

															<div class="profile-activity clearfix">
																<div>	
																	<p>Enable system notifications.</p>
													
																	<label>
																		<input name="email_alerts" class="ace ace-switch ace-switch-1" type="checkbox">
																		<span class="lbl"> &nbsp;&nbsp;&nbsp;Notifications</span>
																	</label>
																</div>
															</div>

															<div class="profile-activity clearfix">
																<div>
																	
																	<p>Default view</p>
																	<label>
																		<select>
																			<option value="0">Tabular</option>
																			<option value="1">List</option>
																		</select>
																	</label>

																</div>
															</div>

															<div class="profile-activity clearfix">
																<div>
																	<p>Archives page</p>
																	<label>
																		<select>
																			<option value="0">Show All Documents</option>
																			<option value="1">Show Last 6 Months Only</option>
																			<option value="2">Show Last 3 Months Only</option>
																			<option value="3">Show Last 1 Month Only</option>
																		</select>
																	</label>
																</div>

															</div>
														</div><!-- /.col -->

														<div class="col-sm-6">
															<div class="profile-activity clearfix">
																<div>
																	<p>Set new password</p>
																	<div id="pwd_div"></div>
																	<div style="margin-top: -5px;">
																		<input name="password1" placeholder="New Password" id="pwd1" class="col-xs-12 col-sm-5" type="password">
																		<input name="password2" placeholder="Confirm Password" id="pwd2" class="col-xs-12 col-sm-5" type="password">
																		<input id="uid" value="<?php echo $profile['id']; ?>" type="hidden">
																		<button class="btn btn-primary btn-sm" id="pwd_button"><i class="ace-icon fa fa-floppy-o"></i>Reset</button>
																	</div>
																
																</div>

															</div>

														</div><!-- /.col -->
													</div>

													<!-- /section:pages/profile.friends -->
													

													
												</div><!-- /#friends -->

												<div id="pictures" class="tab-pane">
													
													<div class="row">
									
														<div class="col-sm-6">
										
															<!-- #section:custom/widget-box.options.transparent -->
															<div class="widget-box transparent">
																<div class="widget-header">
																	<h4 class="widget-title lighter">Delegation Request</h4>

																	
																</div>
																
																<?php 	
																		$unithead = getUnitHead($profile['department_id']); 
																		
																		$pending=0; $approved=0;
																		$form_query = "SELECT * FROM delegations WHERE requested_by='".$myid."'"; 
																					$form_result = $conn->query($form_query); 
																		while($form_data = $form_result->fetch_assoc()) {   
																		$strt = date("Y-m-d", strtotime(substr($form_data['duration'], 0, 10)));
																		$end = date("Y-m-d", strtotime(substr($form_data['duration'], 13, 10)));
																		
																			if($strt>date("Y-m-d") OR $end>date("Y-m-d")){ 
																				if($form_data['approved']==0){ 
																					$pending++; 
																				} else {
																					$approved++;
																				}
																			}
																		}			
																	if($pending>0 OR $approved>0){ ?>
																<div class="widget-body">
																	<div class="widget-main padding-6 no-padding-left no-padding-right">
																		<div class="col-md-12">
																			<div class="clearfix">
					
																				<div class="alert alert-warning">
																					<strong>
																						<i class="ace-icon fa fa-times"></i>
																						You already have a delegation request!
																					</strong> <br><br>
																					If you wish to cancel it, please contact your department head (<?= getStaffName($unithead); ?>).
																					<br>
																				</div>
																				
																			</div>
																			
																			<div class="clearfix">
																				&nbsp;
																			</div>
																		</div>
																	</div>	
																</div>
																<?php } else {?>
																<div class="widget-body">
																	<div class="widget-main padding-6 no-padding-left no-padding-right">
																		
																		
																		<div class="col-md-12">
																		<form method="post" action="?delegate">
																			
																			<input type="hidden" name="action" value="NEW DELEGATE" >	
																			<input type="hidden" name="table" value="delegations">
																			<input type="hidden" name="approver" value="<?= $unithead; ?>">
																			<input type="hidden" name="data[requested_by]" value="<?php echo $myid; ?>" >	
																			
																				<div class="form-group">
																					<label for="form-field-select-3" class="col-xs-12	">Delegate my incoming documents To:</label>

																			
																					<div class="col-md-12 col-xs-12 col-sm-12">
																						<select name="data[delegate_to]" class="chosen-select form-control col-md-12" data-placeholder="Staff from your department..."  required >
																							<option value="">  </option>
																						<?php $form_query = "SELECT * FROM admins WHERE department_id='$myUnit' AND id<>'$id' ORDER BY fname, lname"; 
																							  $form_result = $conn->query($form_query);
																							  while($ro_data = $form_result->fetch_assoc()) {   ?>	
																							<option value="<?php echo $ro_data['id']; ?>"><?php echo $ro_data['fname']." ".$ro_data['lname']." ".$ro_data['oname']; ?></option>
																							<?php } ?>
																							
																						</select>
																					</div>
																				</div>
																			
																				<div class="form-group">
																					<label for="id-date-range-picker-1">For the period:</label>
																					<div class="col-md-12 col-xs-12 col-sm-12">
																						<div class="input-group">
																								<span class="input-group-addon">
																									<i class="fa fa-calendar bigger-110"></i>
																								</span>
																								<input class="form-control date-range-picker" type="text" name="data[duration]" required />
																						</div>
																					</div>
																				</div>
																			
																				<div class="form-group">
																					<label for="form-field-username">Reason</label>
																					<div class="col-md-12 col-xs-12 col-sm-12">
																						<textarea class="form-control" id="form-field-username"  name="data[reason]" rows="3" required ></textarea>
																					</div>
																				</div>
																			
																				<div class="input-group">
																					<p><em> NOTE: Your documents will be forwarded to the specified person for the specified period if the Department Manager approves your Delegation Request</em></p>
																				</div>
																			
																				<label></label>
																			
																				<div class="input-group">
																					<button class="btn btn-primary">
																						<i class="ace-icon fa fa-floppy-o bigger-120"></i>
																						Submit
																					</button>
																				</div>
																				
																			
																		</form>
																		
																		<br /><br />
																		
																		</div>
																		
																		
																		
																	</div>
																</div>
																<?php } ?>
															</div>

															<!-- /section:custom/widget-box.options.transparent -->
														</div>
									
														<div class="col-sm-7">
															
														</div><!-- /.col -->
									
									
									
									<!-- /.span -->
								</div>
													
												</div><!-- /#pictures 
												
												
												
											</div>
										</div>
									</div>
								</div>

							

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="assets/js/date-time/moment.js"></script>
		<script src="assets/js/date-time/daterangepicker.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		<script src="assets/js/bootbox.js"></script>
		
		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-ui.custom.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.js"></script>
		<script src="assets/js/jquery.gritter.js"></script>
		<script src="assets/js/jquery.easypiechart.js"></script>
		<script src="assets/js/jquery.hotkeys.js"></script>
		
		
		<!-- ace scripts -->
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.fileinput.js"></script>
		<script src="assets/js/ace/elements.typeahead.js"></script>
		<script src="assets/js/ace/elements.wysiwyg.js"></script>
		<script src="assets/js/ace/elements.spinner.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.settings.js"></script>
		<script src="assets/js/ace/ace.settings-rtl.js"></script>
		<script src="assets/js/ace/ace.settings-skin.js"></script>
		<script src="assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				
				if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			
			
				
				}
				
				//change password
				$("#pwd_button").click(function() {
				
					var pwd1 = $("#pwd1").val();
					var pwd2 = $("#pwd2").val();
					var uid = $("#uid").val();
					
					var msg = "";
					
					if(pwd1==""){ msg = 'ERROR: New password cannot be blank!'; }
					else if(pwd1!=pwd2){ msg = 'ERROR: Passwords must match!'; }
					else { 
					
						var dataString = 'pwd='+ pwd1 + '&id=' + uid;
						
						$.ajax({
							type: "POST",
							url: "include/q_pwd_change.php",
							data: dataString,
							success: function() {
							 msg='Your new Password has been saved';
														
							 $("#pwd1").val("");
							 $("#pwd2").val("");
							 
							 var feed = '<div class="alert alert-success"><button class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>'+
										'<i class="ace-icon fa fa-lock bigger-120 blue"></i>'+ msg +
										'</div>';
							 
							 $("#pwd_div").html(feed);
								
							}
						});

					}

					bootbox.dialog({
						message: msg, 
						buttons: {
							"success" : {
							  "label" : "OK",
							  "className" : "btn-sm btn-primary",
							  callback :function(e){

								}
							}
						}
					});
				});
				
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
				$('.date-range-picker').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});	
				
				
			});
		</script>

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="../docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="assets/js/ace/elements.onpage-help.js"></script>
		<script src="assets/js/ace/ace.onpage-help.js"></script>
		<script src="../docs/assets/js/rainbow.js"></script>
		<script src="../docs/assets/js/language/generic.js"></script>
		<script src="../docs/assets/js/language/html.js"></script>
		<script src="../docs/assets/js/language/css.js"></script>
		<script src="../docs/assets/js/language/javascript.js"></script>
	</body>
</html>
