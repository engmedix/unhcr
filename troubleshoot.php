<?php 
session_start();
require_once("include/cnx.php");
require_once("include/database.php");
include("include/session_tracker.php"); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Document Troubleshooting</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.css" />
		
		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		
		<link rel="stylesheet" href="assets/plugins/jconfirm/css/jquery-confirm.css" type="text/css" />
		<link rel="stylesheet" href="assets/css/chosen.css" />

		
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<style>
		#searchFrm input, #searchFrm select {width:100%;}
		.profile-info-value,.profile-user-info-striped .profile-info-value{padding:0;}
		</style>

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		
		</script>
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<?php include("pages/webparts/titlebar.php"); ?>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar.horizontal -->
			<?php include("pages/webparts/navbar.php"); ?>

			<!-- /section:basics/sidebar.horizontal -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									
									<div class="col-xs-12 col-sm-12 widget-container-col">
										<?php if(isset($_GET['track'])){ include("include/do_insert.php"); } ?>
										<div class="widget-box ">
											<!-- #section:custom/widget-box.options -->
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">
													<i class="ace-icon fa fa-table"></i>
													Troubleshoot Document Issues
													<a href="closing.php" class="pull-right">>> Close Inbox </a>
												</h5>
											</div>

											<!-- /section:custom/widget-box.options -->
											<div class="widget-body">
												<div class="widget-main no-padding">
													
													<div>
														<form class="form-horizontal validation-form" action="?search" method="post">
														<div class="input-group">
															<span class="input-group-addon">
																Document Reference
															</span>

															<input value="<?= @$_REQUEST['doc']; ?>" type="text" name="doc" class="form-control search-query" placeholder="Find problem document by Document Reference No. or Internal Reference No." required />
															<span class="input-group-btn">
																<button type="submit" class="btn btn-purple btn-sm search-btn" >
																	<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																	Search
																</button>
															</span>
														</div>
														</form>
													</div>
													
												</div>
											</div>
											
											<?php if(isset($_GET['new'])){ include('include/do_insert.php');  } ?>
											<?php if(isset($_GET['update'])){ include('include/do_edit.php');  } ?>
											<?php if(isset($_GET['fn'])){ include('include/do_functions.php');  } ?>
											
											<?php if(isset($_REQUEST['doc'])){ ?>
											
											<div>
											
											<h4 class="blue">
												<i class="green ace-icon fa fa-search bigger-100"></i>
												Search Results for <?= @$_REQUEST['doc']; ?>
											</h4>
											<div class="space-8"></div>
											<div id="faq-list-2" class="panel-group accordion-style1 accordion-style2">
											<?php $form_query = "SELECT * FROM documents WHERE doc_ref_number LIKE '%".trim($_REQUEST['doc'])."%' OR internal_ref_number LIKE '".trim($_REQUEST['doc'])."' OR external_ref_number LIKE '%".trim($_REQUEST['doc'])."%'"; 
												  $form_result = $conn->query($form_query);
												  $results = $form_result->num_rows;
												  while($form_data = $form_result->fetch_assoc()) {   ?>
												
												<div class="panel panel-default">
													
													<div class="panel-heading">
														<a href="#faq-2-<?php echo $form_data['id']; ?>" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle <?= $results==1?"":"collapsed"; ?>" >
															<i class="ace-icon fa fa-chevron-right smaller-80" data-icon-hide="ace-icon fa fa-chevron-down align-top" data-icon-show="ace-icon fa fa-chevron-right"></i>&nbsp;
															<?php echo $form_data['document_type']; ?> 
															<?php if($form_data['doc_ref_number']){ echo " - REF:". $form_data['doc_ref_number']; } ?> 
															<?php echo " - <i>Submited by: ". getStaffName($form_data['created_by']); ?> 
															<?php echo " (".getStaffStation($form_data['created_by']).")</i>"; ?> 
															
															<?php echo $form_data['stage_label']=='CLOSED'?'<span class="badge badge-danger">CLOSED</span>':''; ?>
																													</a>
													</div>
													
													<div class="panel-collapse collapse <?= $results==1?"in":""; ?>" id="faq-2-<?php echo $form_data['id']; ?>">
														<div class="panel-body">
															<b>Document Info</b><br />
															
															<table class="table table-striped table-bordered">
																
																<thead>	
																	<tr>
																		<th class="">Document</th>
																		<th class="">Time line</th>
																	</tr>
																</thead>
																
																<tbody>	
																	
																	<tr>
																		
																		<td class="">
																			Ref No: <?php echo $form_data['doc_ref_number']; ?> <br>
																			System Ref: <?php echo $form_data['internal_ref_number']; ?> 
																			<hr>
																			Date Created: <?php echo $form_data['created']; ?>	<br>
																			Created By: <?php echo $form_data['submitted_by']; ?>	<br>
																			Branch: <?php echo $form_data['organisation']; ?> <br>
																			Email: <?php echo $form_data['email']; ?> 
																			<hr>
																			
																			Current Holder: <?php echo getStaffName($form_data['current_holder']); ?> <br>
																			Current Stage: <?php echo $form_data['stage_label']; ?>  <br>
																			Current Status: <?php echo $form_data['status_label']; ?> 
																		</td>
																		
																		<td class="">
																		
																		<?php 
											
																		$timeline_query = "SELECT 
																						dt.*,
																						doc.created, doc.document_type, doc.status_label, doc.capture_method, 
																						admins.fname, admins.lname, admins.oname, 
																						original.fname or_fname, original.lname or_lname, original.department_id or_dept, 
																						_to.fname to_fname, _to.lname to_lname, _to.department_id to_dept, 
																						TIME_TO_SEC(wf.duration) dur 
																					   FROM 
																						doc_track dt
																						LEFT JOIN documents doc ON dt.doc_id = doc.id 
																						LEFT JOIN admins ON dt.from_id = admins.id 
																						LEFT JOIN admins original ON doc.sender_id = original.id 
																						LEFT JOIN admins _to ON dt.to_id = _to.id 
																						LEFT JOIN document_type_stages wf ON wf.stage=dt.to_stage AND wf.doc_type_id='".$form_data['doc_type_id']."' 
																					   WHERE 
																						doc_id = ".$form_data['id']." 
																					   ORDER BY 
																						dt.actionDate ASC"; 
																			
																		 
																		  //echo $timeline_query;
																		 
																		  $timeline_result = $conn->query($timeline_query);
																		  
																		  $now = new DateTime();
																		  $now->setTime( 0, 0, 0 );
																		  
																		  $index = 0;
																		  
																		  while($timeline = $timeline_result->fetch_assoc()) {

																			$last_dtrack = $timeline['id'];
																		  
																			if($timeline['receive_date']=='0000-00-00 00:00:00'){
																				$time = 0;
																				$unread = strtotime(date("Y-m-d H:i:s"))-strtotime($timeline['actionDate']);
																			} elseif($timeline['archive_date']=='0000-00-00 00:00:00'){
																				$time = strtotime(date("Y-m-d H:i:s"))-strtotime($timeline['receive_date']);
																				$unread = strtotime($timeline['receive_date'])-strtotime($timeline['actionDate']);
																			} else {
																				$time = strtotime($timeline['archive_date'])-strtotime($timeline['receive_date']);
																				$unread = strtotime($timeline['receive_date'])-strtotime($timeline['actionDate']);
																			}
																			if(($time+$unread)>$timeline['dur']){ $color = "red"; } else { $color = "green"; }
																			?>
																		
																		<?php  if($index == 0){  ?>
																	
																			Date: <?php echo date("j M Y g:i A",strtotime($timeline['created'])); ?>
																			| Holder: <?php echo $timeline['or_fname'].' '. $timeline['or_lname']; ?>
																			| Stage: Initiation
																			| Status: Initiation
																			| Action: Submitted
																		<?php	}   if($timeline['receive_date']!='0000-00-00 00:00:00'){ ?>
																			<?php if($timeline['to_stage']!='CLOSED'){ ?>
																			<br>Date: <?php echo date("j M Y g:i A",strtotime($timeline['actionDate'])); ?>
																			| Holder: <?php echo ($index == 0)? $timeline['to_fname'].' '. $timeline['to_lname']:$timeline['fname'].' '. $timeline['lname']; ?>
																			| Stage: <?php echo $timeline['to_stage']; ?>
																			| Status: <?php echo $timeline['to_status']; ?>
																			| Action: uread
																			<?php } else { ?>
																			<br>Date: <?php echo date("j M Y g:i A",strtotime($timeline['actionDate'])); ?>
																			| <?php echo $timeline['to_stage']; ?>
																			| <?php echo $timeline['to_status']; ?>
																			<?php } ?>
																			
																			<?php if($timeline['to_stage']!='CLOSED'){ ?>
																			<br>Date: <?php echo date("j M Y g:i A",strtotime($timeline['receive_date'])); ?>
																			| Holder: <?php echo $timeline['to_fname'].' '. $timeline['to_lname']; ?>
																			| Stage: <?php echo $timeline['to_stage']; ?>
																			| Status: <?php echo $timeline['to_status']; ?>
																			| Action: <?php if($timeline['archive_date']=='0000-00-00 00:00:00'){
																							echo 'Received';
																					} else {
																						
																						$nxtDocT  = $conn->query("SELECT a.to_stage, b.fname, b.lname FROM doc_track a LEFT JOIN admins b ON a.to_id=b.id WHERE a.doc_id='".$form_data['id']."' AND a.id>'".$timeline['id']."' ORDER BY a.id  LIMIT 1");
																						
																						$nxtDoc = $nxtDocT->fetch_assoc();
																						
																						echo 'To <b>'.$nxtDoc['fname'].' '.$nxtDoc['lname'].'</b> ['.$nxtDoc['to_stage'].']'; 
																						
																					}	
																			} ?>
																		<?php  } else { ?>
																		
																			<br>Date: <?php echo date("j M Y g:i A",strtotime($timeline['actionDate'])); ?>
																			| Holder: <?php echo $timeline['fname'].' '. $timeline['lname']; ?>
																			| Stage: <?php echo $timeline['to_stage']; ?>
																			| Status: <?php echo $timeline['to_status']; ?>
																			| Action: uread
																		<?php	}  ?>
																		<?php $index ++; } ?>
																				
																		</td>
																		
																	</tr>
																	
																	
																	<tr>
																		<td colspan="2">
																		<b>Troubleshooting Actions</b><br />
																		<table align="center" width="90%">
																			
																			
																			<?php 
																			$wf_max = $conn->query("SELECT id FROM doc_track WHERE doc_id='".$form_data['id']."' ORDER BY id DESC LIMIT 1 "); 
																			
																			$wf_max_ro = $wf_max->fetch_assoc();  
																																						
																			$wf_query = "SELECT * FROM doc_track WHERE doc_id='".$form_data['id']."' AND receiver_status<>'ARCHIVED' AND id<'".$wf_max_ro['id']."'"; 
																			$wf_result = $conn->query($wf_query);
																			
																			$wfs = $wf_result->num_rows;  
																			if($wfs>0){
																			?>
																			
																			
																			<form action="?fn" method="post">
																				<input type="hidden" name="action" value="TROUBLESHOOT: CLOSE HANGING ITEMS" />
																				<input type="hidden" name="doc" value="<?= @$_REQUEST['doc']; ?>" />																			
																			<tr>
																				<td align="right" valign="top" style="padding-right:5px; width:30%"><b>Close Hanging Workflow Items (<?= $wfs; ?>)</b></td>
																				<td colspan="2"> 
																				
																				<div class="wfs" >
																				
																					<?php if($wfs>3){?><label><input type="checkbox" class="all_wf" >select all</label><br><br> <?php } ?>
																					
																					<?php
																						$i=1;																		
																						while($wf_data = $wf_result->fetch_assoc()) { 
																							echo '<label style="font-size:13px"><input type="checkbox" value="'.$wf_data['id'].'" name="wf[]" /> <b>['.$i.']</b> '.$wf_data['actionDate'].' | '.$wf_data['document_type'].' | Holder:'.getStaffName($wf_data['to_id']).' | Stage: '.$wf_data['to_stage'].' | From: '.getStaffName($wf_data['from_id']).'</label>';
																							echo "<br>";
																							$i++;
																						} 
																					?>
																				</div>
																				
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"><label> <button class="btn btn-success btn-minier">Close Selected Items</button>  </td>
																			</tr>
																			
																			</form>
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"><hr></td>
																				<td style="width:10px"> OR </td>
																				<td><hr></td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			<?php } ?>
																			
																			<?php if($form_data['stage_label']!="CLOSED"){?>
																			<tr>
																				<td align="right" style="padding-right:5px; width:30%;"><b>Close Document</b></td>
																				<td colspan="2">
																				
																				<form action="?new" method="post">
																				
																				<input type="hidden" name="action" value="TROUBLESHOOT: CLOSE DOCUMENT" />
																				<input type="hidden" name="doc" value="<?= @$_REQUEST['doc']; ?>" />
																				
																				<input type="hidden" name="table" value="doc_track">
																				<input type="hidden" name="data[doc_id]" value="<?= $form_data['id']; ?>"/>
																				<input type="hidden" name="data[document_type]" value="<?= $form_data['document_type']; ?>"/>
																				<input type="hidden" name="tId" value="<?= $last_dtrack; ?>" >
																				
																				<input type="hidden" name="data[from_id]" value="<?= $form_data['current_holder']; ?>"/>
																				<input type="hidden" name="data[from_stage]" value="<?= $form_data['stage_label']; ?>" >
																				<input type="hidden" name="data[from_status]" value="<?= $form_data['status_label']; ?>" >
																				<input type="hidden" name="data[to_id]" value="<?= $form_data['current_holder']; ?>" />
																				<input type="hidden" name="data[to_stage]" value="CLOSED" >
																				<input type="hidden" name="data[to_status]" value="Closed by Troubleshoot" >
																				<input type="hidden" name="data[receiver_status]" value="ARCHIVED" />
																				<input type="hidden" name="data[action]" value="CLOSED TROUBLESHOOT" >
																				
																				<input type="hidden" name="data2[file_status]" value="CLOSED BY TROUBLESHOOT" >
																				<input type="hidden" name="data2[stage_label]" value="CLOSED" >
																				<input type="hidden" name="data2[status_label]" value="Closed by Troubleshoot" >
																				<input type="hidden" name="data2[closed_by]" value="<?= $_SESSION['user']; ?>" >							
																																						
																				<button type="submit" class="btn btn-danger btn-minier">Close <?= $form_data['document_type']." ".$form_data['internal_ref_number']; ?></button> 
																				
																				</form>
																				
																				</td>
																			</tr>
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"><hr></td>
																				<td style="width:10px"> OR </td>	
																				<td><hr></td>
																			</tr>
																			<?php } ?>
																			
																			<?php if($form_data['stage_label']!="CLOSED"){?>
																			<tr>
																				<td align="right" style="padding-right:5px; width:30%"><b>Document Properties</b></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			
																			<form action="?update" method="post">
																				<input type="hidden" name="table" value="documents" />
																				<input type="hidden" name="action" value="TROUBLESHOOT" />
																				<input type="hidden" name="acid" value="<?= $form_data['id']; ?>" />
																				<input type="hidden" name="doc" value="<?= @$_REQUEST['doc']; ?>" />
																				
																			<tr>
																				<td align="right" style="padding-right:5px">Change Current Holder</td>
																				<td colspan="2">
																					<?php $holder_query = "SELECT a.fname, a.lname, a.oname, a.id, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id<>'1' AND a.status='ACTIVE' ORDER BY a.fname, a.lname"; ?>
																					<select name="data[current_holder]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $holder_result = $conn->query($holder_query);
																						  while($holdr_data = $holder_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $holdr_data['id']; ?>" <?= $holdr_data['id']==$form_data['current_holder']?"selected":""; ?>  ><?php echo $holdr_data['fname']." ".$holdr_data['lname']." ".$holdr_data['oname']." - ".$holdr_data['job_title']; ?></option>
																						  <?php } ?>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px;">Change Sender</td>
																				<td colspan="2">
																					<?php $holder_query = "SELECT a.fname, a.lname, a.oname, a.id, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id<>'1' AND a.status='ACTIVE' ORDER BY a.fname, a.lname"; ?>
																					<select name="data[sender_id]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $holder_result = $conn->query($holder_query);
																						  while($holdr_data = $holder_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $holdr_data['id']; ?>" <?= $holdr_data['id']==$form_data['sender_id']?"selected":""; ?>  ><?php echo $holdr_data['fname']." ".$holdr_data['lname']." ".$holdr_data['oname']." - ".$holdr_data['job_title']; ?></option>
																						  <?php } ?>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px">Change Current Stage</td>
																				<td colspan="2">
																					<?php $stage_query = "SELECT * FROM document_type_stages WHERE doc_type_id='".$form_data['doc_type_id']."'  "; ?>
																					<select name="data[stage_label]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $stage_result = $conn->query($stage_query);
																						  while($stg_data = $stage_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $stg_data['stage']; ?>" <?= $stg_data['stage']==$form_data['stage_label']?"selected":""; ?> ><?php echo $stg_data['stage']; ?></option>
																						  <?php } ?>
																					</select>
																				
																				</td>
																			</tr>
																			
																			<tr>
																				<td align="right" style="padding-right:5px">Change Current Status</td>
																				<td colspan="2"> <input value="<?php echo $form_data['status_label']; ?>" name="data[status_label]" class="form-control"> </td>
																			</tr>
																			
																			<tr>
																				<td align="right" style="padding-right:5px">Add as new timeline</td>
																				<td colspan="2"><label> <input name="switch-field-1" type="checkbox"  /></label>  </td>
																			</tr>
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"><label> <button class="btn btn-success btn-minier">Save Changes</button>  </td>
																			</tr>
																			
																			</form>
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px"><hr></td>
																				<td style="width:10px"> OR </td>	
																				<td><hr></td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px; width:30%"><b>Workflow Properties</b></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			
																			<?php $doc_trk = $conn->query("SELECT * FROM doc_track WHERE doc_id='".$form_data['id']."' AND receiver_status<>'ARCHIVED' ORDER BY id DESC LIMIT 1"); 
																			$trk_item = $doc_trk->fetch_assoc();
																			?>
																			
																			<form action="?update" method="post">
																				<input type="hidden" name="table" value="doc_track" />
																				<input type="hidden" name="action" value="TROUBLESHOOT" />
																				<input type="hidden" name="acid" value="<?= $trk_item['id']; ?>" />
																				<input type="hidden" name="doc" value="<?= @$_REQUEST['doc']; ?>" />
																			
																			<tr>
																				<td align="right" style="padding-right:5px">From</td>
																				<td colspan="2">
																					<?php $holder_query = "SELECT a.fname, a.lname, a.oname, a.id, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id<>'1' AND a.status='ACTIVE' ORDER BY a.fname, a.lname"; ?>
																					<select name="data[from_id]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $holder_result = $conn->query($holder_query);
																						  while($holdr_data = $holder_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $holdr_data['id']; ?>" <?= $holdr_data['id']==$trk_item['from_id']?"selected":""; ?>  ><?php echo $holdr_data['fname']." ".$holdr_data['lname']." ".$holdr_data['oname']." - ".$holdr_data['job_title']; ?></option>
																						  <?php } ?>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px">To</td>
																				<td colspan="2">
																					<?php $holder_query = "SELECT a.fname, a.lname, a.oname, a.id, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id<>'1' AND a.status='ACTIVE' ORDER BY a.fname, a.lname"; ?>
																					<select name="data[to_id]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $holder_result = $conn->query($holder_query);
																						  while($holdr_data = $holder_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $holdr_data['id']; ?>" <?= $holdr_data['id']==$trk_item['to_id']?"selected":""; ?>  ><?php echo $holdr_data['fname']." ".$holdr_data['lname']." ".$holdr_data['oname']." - ".$holdr_data['job_title']; ?></option>
																						  <?php } ?>
																					</select>
																				</td>
																			</tr>
																																						
																			<tr>
																				<td align="right" style="padding-right:5px">From Stage</td>
																				<td colspan="2">
																					<?php $stage_query = "SELECT * FROM document_type_stages WHERE doc_type_id='".$form_data['doc_type_id']."'  "; ?>
																					<select name="data[from_stage]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $stage_result = $conn->query($stage_query);
																						  while($stg_data = $stage_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $stg_data['stage']; ?>" <?= $stg_data['stage']==$trk_item['from_stage']?"selected":""; ?> ><?php echo $stg_data['stage']; ?></option>
																						  <?php } ?>
																					</select>
																				
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px">To Stage</td>
																				<td colspan="2">
																					<?php $stage_query = "SELECT * FROM document_type_stages WHERE doc_type_id='".$form_data['doc_type_id']."'  "; ?>
																					<select name="data[to_stage]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $stage_result = $conn->query($stage_query);
																						  while($stg_data = $stage_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $stg_data['stage']; ?>" <?= $stg_data['stage']==$trk_item['to_stage']?"selected":""; ?> ><?php echo $stg_data['stage']; ?></option>
																						  <?php } ?>
																					</select>
																				
																				</td>
																			</tr>
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"><label> <button class="btn btn-success btn-minier">Save Changes</button>  </td>
																			</tr>
																			
																			</form>
																			<?php } else { ?>
																			<tr>
																				<td align="right" style="padding-right:5px; width:30%"><b>OPEN CLOSED DOCUMENT</b></td>
																				<td colspan="2"> &nbsp; </td>
																			</tr>
																			
																			<?php $close_query = "SELECT * FROM doc_track WHERE doc_id='".$form_data['id']."' AND to_stage='CLOSED' ORDER BY id DESC LIMIT 1"; 
																				$close_result = $conn->query($close_query);
																				$close_data = $close_result->fetch_assoc();?>
																			
																			<form action="?new" method="post">
																				<input type="hidden" name="table" value="doc_track" />
																				<input type="hidden" name="action" value="TROUBLESHOOT: RESTORE CLOSED DOCUMENT" />
																				<input type="hidden" name="doc" value="<?= @$_REQUEST['doc']; ?>" />
																				<input type="hidden" name="data[document_type]" value="<?php echo $close_data['document_type']; ?>"/>
																				<input type="hidden" name="data[doc_id]" value="<?php echo $form_data['id']; ?>"/>
																				<input type="hidden" name="data[from_id]" value="<?php echo $_SESSION['user']; ?>"/>
																				<input type="hidden" name="data[to_id]" value="<?php echo $close_data['to_id']; ?>"/>
																				<input type="hidden" name="data[from_stage]" value="CLOSED" >
																				<input type="hidden" name="data[to_stage]" value="<?php echo $close_data['from_stage']; ?>" >
																				<input type="hidden" name="data[from_status]" value="<?php echo $close_data['to_status']; ?>" >
																				<input type="hidden" name="data[to_status]" value="CLOSED DOCUMENT RESTORED" >
																				<input type="hidden" name="data[action]" value="RESTORED" >
																				<input type="hidden" name="data[remark]" value="Closed Document Restored by <?php echo getStaffName($_SESSION['user']); ?>" >
																				<input type="hidden" name="data[receiver_status]" value="SENT"/>
																				<input type="hidden" name="tId" value="<?php echo $close_data['id']; ?>" >
																				
																			<tr>
																				<td align="right" style="padding-right:5px">Restore to</td>
																				<td colspan="2">
																					<?php $holder_query = "SELECT a.fname, a.lname, a.oname, a.id, b.job_title FROM admins a LEFT JOIN job_titles b ON a.job_title_id=b.id WHERE a.id<>'1' AND a.status='ACTIVE' ORDER BY a.fname, a.lname"; ?>
																					<select name="data[to_id]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $holder_result = $conn->query($holder_query);
																						  while($holdr_data = $holder_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $holdr_data['id']; ?>" <?= $holdr_data['id']==$form_data['closed_by']?"selected":""; ?>  ><?php echo $holdr_data['fname']." ".$holdr_data['lname']." ".$holdr_data['oname']." - ".$holdr_data['job_title']; ?> <?= $holdr_data['id']==$form_data['closed_by']?" (closed by)":""; ?></option>
																						  <?php } ?>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td align="right" style="padding-right:5px">Restore Stage</td>
																				<td colspan="2">
																					<?php $stage_query = "SELECT * FROM document_type_stages WHERE doc_type_id='".$form_data['doc_type_id']."'  "; ?>
																					<select name="data[to_stage]"  class="form-control chosen-select " >
																						<option value="">  </option>
																						  <?php $stage_result = $conn->query($stage_query);
																						  while($stg_data = $stage_result->fetch_assoc()) {   ?>	
																						<option value="<?php echo $stg_data['stage']; ?>" <?= $stg_data['stage']==$close_data['from_stage']?"selected":""; ?> ><?php echo $stg_data['stage']; ?> <?= $stg_data['stage']==$close_data['from_stage']?" (closed stage)":""; ?> </option>
																						  <?php } ?>
																					</select>
																				
																				</td>
																			</tr>
																			
																			
																			<tr>
																				<td align="right" style="padding-right:5px"></td>
																				<td colspan="2"><label> <button class="btn btn-success btn-minier">OPEN DOCUMENT</button>  </td>
																			</tr>
																			
																			</form>
																			
																			<?php } ?>
																		</table>
																		
																		</td>
																	</tr>

																	
																</tbody>
															</table>
														</div>
													</div>
												</div>
												
												<?php } ?>

											
											</div>
											
											
										</div>
											<?php } ?>
										</div>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="space-24"></div>



								
							</div><!-- /.col -->
						</div><!-- /.row --><!-- /.row -->
                        
                        <div id="search-results"></div>
						
						
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("pages/webparts/footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/jquery.validate.js"></script>
		<script src="assets/js/chosen.jquery.js"></script>
		
		<!-- ace scripts -->
		<script src="assets/plugins/jconfirm/js/jquery-confirm.js" ></script>
		<script src="assets/js/ace/elements.scroller.js"></script>
		<script src="assets/js/ace/elements.aside.js"></script>
		<script src="assets/js/ace/ace.js"></script>
		<script src="assets/js/ace/ace.ajax-content.js"></script>
		<script src="assets/js/ace/ace.touch-drag.js"></script>
		<script src="assets/js/ace/ace.sidebar.js"></script>
		<script src="assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="assets/js/ace/ace.submenu-hover.js"></script>
		<script src="assets/js/ace/ace.widget-box.js"></script>
		<script src="assets/js/ace/ace.searchbox-autocomplete.js"></script>
		
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			 
			
			});
			
			//$('.chosen-select').chosen({allow_single_deselect:true}); 
			
			$('a.accordion-toggle').on('click', function () { 
				
				var link = $(this);
				var paneldiv = link.closest('div.panel');
				var bodydiv = paneldiv.find('div.panel-collapse');
				
				//console.log(bodydiv.attr("id"));
				
				if(link.hasClass("collapsed")){
					//bodydiv.find('select.chosen-select').chosen({allow_single_deselect:true}); 	
				} else {
					//bodydiv.find('select.chosen-select').chosen({allow_single_deselect:true}); 	
				}
			});

			
			$('body').on('click','.search-btn', function(e){
				e.preventDefault();
				var frm = $(this).closest("form");
				if($('.search-query').val()==""){
					$.alert('<b>REQUIRED FIELD</b><br> Please enter the reference number of the document you want to troubleshoot');
				} else {
					frm.submit();
				}
				
			});
			
			$(".all_wf").click(function () {
				$('input:checkbox').not(this).prop('checked', this.checked);
			 });
			
			
		</script>

	</body>
</html>
